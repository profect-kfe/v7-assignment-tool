docker-compose down
docker volume rm exa-data
docker-compose up -d
Write-Output "Waiting 3 minutes for exasol to properly start"
Start-Sleep -s 180
Write-Output "Trying to increase exasol volume size"
docker exec dev-exa truncate --size=15GB /exa/data/storage/dev.1
docker exec dev-exa cshdd --enlarge --node-id 11 -h /exa/data/storage/dev.1
Write-Output "Loading data into exasol"
& "C:\Program Files (x86)\Exasol\EXASolution-6.2\EXAplus\exaplusx64.exe" -c localhost:8563 -u sys -p exasol -L -f ./exasol/load_testdb_into_exa_local.sql