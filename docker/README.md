# How to setup the local dev environment (beta) 


## Prequisites:
* A current installation of Docker with 2+ CPUs, 3GB+ RAM, 1GB+ Swap file and 48GB+ harddrive
* For automatic execution: A current installation of [Exaplus Client 6.2](https://www.exasol.com/portal/display/DOWNLOAD/6.2) installed in the folder `C:\Program Files (x86)\Exasol\EXASolution-6.2\EXAplus\`
* For manual execution: A current installation of DBeaver Community Edition with configured Exasol jdbc driver
* **Please note that all the mentioned files and scripts here are taken from core development and probably require adjustment for your project needs.**


## Steps:
1. Provide the necessary csv raw data of your desired test database at store it at `C:\v7_testdb_exasol`
2. Adjust and execute the script [install-environment.ps1](./v7-dev-env/install-environment.ps1) on windows. This should do all the below manual steps automatically.

### Manual Steps
2. Open a console (e.g. Powershell) in the folder where the file [docker-compose.yml](./v7-dev-env/docker-compose.yml) is located (normally nearby this readme file) and execute the command `docker-compose up`. This should start a preconfigured exasol, postgres, redis, camunda, teedy and clamav container in your local docker. If you do not need all those services you should edit the `docker-compose.yml` for your project needs.
After everything has started up, the console can be closed.
3. After the exasol container has started, open another console and enter the following commands to adjust the exasol volume size:
   1. Enter `docker exec dev-exa truncate --size=15GB /exa/data/storage/dev.1` to increase the storage size of exasol
   2. Enter `docker exec dev-exa cshdd --enlarge --node-id 11 -h /exa/data/storage/dev.1` to activate the setting. This console can now be closed
4. Open DBeaver and create a new connection to Exasol with hostlist: localhost, port: 8563 (default), username: sys and password: exasol
   * Adjust the content of the file `exasol/load_testdb_into_exa_local.sql` to create and load your tables.
   * Connect to Exasol and execute the script `exasol/load_testdb_into_exa_local.sql`. This loads the configured tables into your local exasol. You only have to do this once. Afterwards Exasol should contain the configured database and you can start developing with the Maven exasol-profile

TODO: add guide how to configure your local camunda (admin user, adding / removing bpmn files, etc.)

## Troubleshooting

### I cannot load the local csv files into exasol
If importing the files locally doesn't work, you can import from a Nostromo file server. This requires a valid connection to the intranet (e.g. a VPN connection)
#### Please note that this should not be done from homeoffice, because it will require a lot of bandwidth from the VPN connection, which will slow down the internet speed for everybody!
To import the data, in step 4 execute the script `exasol/load_testdb_into_exa_nostromo.sql` instead, which will create a connection to nostromo and load the data automatically from there. This will take at least 20 minutes, depending on your internet connection.

### I ran all scripts, but Docker doesn't properly start the containers
- If you have previously used Docker and added other containers / volumes, they might break something. To delete them you can use `docker system prune` to clear all unused (tangeling = not running) containers and `docker volume prune` to delete all unused volumes. Warning! This will delete all data and settings from currently not running containers!
- You can also try to restart docker. There is a button for this in the Docker for Desktop gui.

### After a windows / docker restart the containers don't run properly
- Check if the containers are running properly. To do so you can click on the Docker tray icon and select "Dashboard". Here you can check if all containers are running and take a look at the log files. If the containers fail to start, you will probably see error messages here.
- You can reinitialize the containers by executing `docker-compose restart`. This will remove the container and add them again.

# How to setup virtual schema

## Prequisites:
Additionaly, to the local dev env prequisites, you need to have curl installed (it is part of Profect Dev Bundle so you should have it already).

## Steps:
After docker-compose up, follow these steps
1. Execute the script [virtual-schema.ps1](./virtual-schema.ps1) from within v7-dev-env folder. Use cd command to navigate to this folder.
2. After script is executed, the virtual schema should be created and ready to use. You can check virtual schema by inspecting EXASOL DB in dbeaver. 
   There should be two new schemas under Virtual Schemas filled with tables of V7 and camunda.

### Manual steps:
After docker-compose up, follow these steps
1. Execute following command in powershell
  `docker exec dev-exa cat exa/etc/EXAConf`
2. In the output, find the WritePasswd line in BucketFS paragraph and copy the value assigned to the WritePasswd. This is base64 encoded password for uploading files to the bucketfs.
3. Decode copied password i.e. by using https://www.base64decode.org/ and save the password for later usage.
4. Execute following commands in powershell. Replace PASSWORD with your decoded password.
  `curl -X PUT -T ./exasol/jars/postgresql-42.2.11.jar http://w:PASSWORD@localhost:6583/default/postgresql-42.2.11.jar`
  `curl -X PUT -T ./exasol/jars/virtualschema-jdbc-adapter-dist-3.1.0.jar http://w:PASSWORD@localhost:6583/default/virtualschema-jdbc-adapter-dist-3.1.0.jar`
5. If both curl requests are successfull, execute sql script using EXAPLUS
  `& "C:\Program Files (x86)\Exasol\EXASolution-6.2\EXAplus\exaplusx64.exe" -c localhost:8563 -u sys -p exasol -L -f ./exasol/create_virtual_schema.sql`
6. The virtual schema should be now ready to use. You can check if it is working by inspecting EXASOL DB in dbeaver. There should be two new schemas under Virtual Schemas filled with tables of V7 and camunda 

## Troubleshooting:
If the curl requests don't execute properly, try running following command
  `Remove-item alias:curl`
