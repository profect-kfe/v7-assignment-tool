apiVersion: v1
kind: Service
metadata:
  name: v7-backend-service
spec:
  ports:
  - name: "8080"
    port: 8080
    targetPort: 8080
  selector:
    v7.service: v7-backend
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    v7.service: v7-backend
  name: v7-backend
spec:
  selector:
    matchLabels:
      v7.service: v7-backend
  replicas: 1
  template:
    metadata:
      labels:
        v7.service: v7-backend
    spec:
      containers:
      - image: profectde/assignmenttool:backend-${BUILD_VERSION}
        imagePullPolicy: IfNotPresent
        name: v7-backend
        resources:
          limits:
            memory: 1Gi
            cpu: 500m
          requests:
            memory: 1Gi
            cpu: 100m
        ports:
        - containerPort: 8080
      restartPolicy: Always
      imagePullSecrets:
        - name: docker-profectde-secret
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: v7-backend-ingress
  annotations:
    nginx.ingress.kubernetes.io/affinity: cookie
    nginx.ingress.kubernetes.io/affinity-mode: persistent
    nginx.ingress.kubernetes.io/session-cookie-name: V7BACKEND
    nginx.ingress.kubernetes.io/proxy-body-size: "0"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "600"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "600"
spec:
  tls:
  - hosts:
    - assignment.profect.de
    secretName: assignment.profect.de
  rules:
  - host: assignment.profect.de
    http:
      paths:
      - path: /v7api/
        backend:
          serviceName: v7-backend-service
          servicePort: 8080
