# PROFILES
spring.profiles.active=k8s

# For more options see https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html
# ----------------------------------------
# CORE PROPERTIES
# ----------------------------------------
debug=false
trace=false

# LOGGING
logging.config=classpath:log4j2.xml

# IDENTITY (ContextIdApplicationContextInitializer)
spring.application.name=AssignmentTool

# SPRING CACHE (CacheProperties)
spring.cache.type=none

# APPLICATION SETTINGS (SpringApplication)
# This is needed for overriding the default PermissionEvaluator in
# our custom XACMLPermissionEvaluator.
spring.main.allow-bean-definition-overriding=true

# ----------------------------------------
# WEB PROPERTIES
# ----------------------------------------

# EMBEDDED SERVER CONFIGURATION (ServerProperties)
# no embedded server in k8s

# SPRING SESSION (SessionProperties)
#this saves the session to redis and connects to a local redis database
spring.session.store-type=redis
#spring.redis.host=
#spring.redis.password=
spring.redis.port=6379
spring.redis.sentinel.master=v7master
spring.redis.sentinel.nodes=v7-redis:26379
# SPRING SESSION REDIS (RedisSessionProperties)
spring.session.redis.flush-mode=on-save
spring.session.redis.namespace=v7:spring:session


# MULTIPART (MultipartProperties)
spring.servlet.multipart.enabled=true
spring.servlet.multipart.max-file-size=200MB
spring.servlet.multipart.max-request-size=200MB
spring.servlet.multipart.resolve-lazily=true

# JACKSON (JacksonProperties)
# Disable all autodetection features of jackson. Only explicit @JsonProperty annotated fields will be included in JSON
spring.jackson.mapper.auto-detect-creators=false
spring.jackson.mapper.auto-detect-fields=false
spring.jackson.mapper.auto-detect-getters=false
spring.jackson.mapper.auto-detect-is-getters=false
spring.jackson.mapper.auto-detect-setters=false
# Jackson won't include null value properties into the serialized JSON
spring.jackson.default-property-inclusion=NON_NULL
# Jackson will serialize all dates as iso-timestamp (i.e. 2007-12-24T18:21Z)
spring.jackson.serialization.WRITE_DATES_AS_TIMESTAMPS=false
spring.jackson.serialization.WRITE_DATE_KEYS_AS_TIMESTAMPS=false

# ----------------------------------------
# DATA PROPERTIES
# ----------------------------------------

# LIQUIBASE (LiquibaseProperties) 
spring.liquibase.enabled=true
spring.liquibase.change-log=classpath:/db/changelog/db.changelog-project.xml
spring.liquibase.drop-first=false
spring.liquibase.default-schema=public

# DATASOURCE (DataSourceAutoConfiguration & DataSourceProperties)
spring.datasource.jdbc-url=jdbc:postgresql://at-postgresql-ha-pgpool:5432/v7
spring.datasource.username=postgres
spring.datasource.password=nOyVjjsvN1
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.hikari.maximum-pool-size=5
spring.datasource.hikari.connection-timeout=3600000


# JPA (JpaBaseConfiguration, HibernateJpaAutoConfiguration)
# Possible values: create / create-drop / update / validate / none
spring.jpa.hibernate.ddl-auto=validate
# This is needed for Lazy JPA entities to be loaded by jackson. Default is true,
# but Spring will issue a warning if not set explicitly
spring.jpa.open-in-view=true
# Defines the maximum join depth for EAGER relationships in Hibernate
# This parameter can be used to quickly get rid of "MariaDB can only handly 61 tables in a join" exceptions.
# Another approach is to check every connection relationships and set them to LAZY where it makes sense. 
spring.jpa.properties.hibernate.max_fetch_depth=0

# ----------------------------------------
# ACTUATOR PROPERTIES
# ----------------------------------------

# ENDPOINTS CORS CONFIGURATION (CorsEndpointProperties)
management.endpoints.web.cors.allow-credentials=true
management.endpoints.web.cors.allowed-headers=X-XSRF-TOKEN, origin, content-type
management.endpoints.web.cors.allowed-methods=GET, POST, PUT, DELETE
management.endpoints.web.cors.allowed-origins= https://assignment.profect.de
management.endpoints.web.cors.exposed-headers=X-XSRF-TOKEN

# ----------------------------------------
# V7 PROPERTIES
# ----------------------------------------

v7.permission.evaluator=xacml
# Amount of attempts before the user is blocked
de.profect.v7.login-attempt.max-attempts=10
# Amount of seconds to wait after too many failed login attempts
de.profect.v7.login-attempt.ban-timeout=600
# document management settings
# see application-default.properties or application-k8s.properties
# Number of threads used for parallel data requests
# Change this property at you own risk. It is an experimental feature which will execute database queries in paralell on
# the number of threads set by this parameter. Check de.profect.data.datasource.DataSourceAccessService.java for more info.
de.profect.v7.data-source-access.number-of-threads=1

# cookie
de.profect.v7.cookie.name=V7SESSION
de.profect.v7.cookie.httponly=true
de.profect.v7.cookie.secure=true
de.profect.v7.cookie.samesite=NONE

ms.graph.id=c5e6d20b-98b4-451a-a28d-be7f817cef6f
ms.graph.scopes=User.Read,Calendars.Read
ms.graph.client_secret=42S_4B2Ljz0ojy~VCmpQePl~uP.R._7hHE
ms.graph.client_credential_authority=https://login.microsoftonline.com/46390799-6bed-4d68-98bf-fbf4c15a073b/
ms.graph.client_credential_scope = https://graph.microsoft.com/.default

# Here-api apiKey
v7.nokiahere.apikey=wqCaAtuuuRO5Tx_y8Idqd_51pWTJ8yE7qE3yO1m8Cu0
