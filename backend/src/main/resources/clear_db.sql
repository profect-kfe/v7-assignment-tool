# Change this to actually do a sql-based cleanup

/*
SET FOREIGN_KEY_CHECKS = 0;
DROP DATABASE IF EXISTS `v7`;
DROP DATABASE IF EXISTS `v7_camunda`;
DROP DATABASE IF EXISTS `v7_unittests`;
CREATE SCHEMA `v7`;
CREATE SCHEMA `v7_camunda`;
CREATE SCHEMA `v7_unittests`;
SET FOREIGN_KEY_CHECKS = 1;
*/