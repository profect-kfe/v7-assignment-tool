package de.profect.config;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import de.profect.AbstractGUIState;
import de.profect.util.ReflectionService;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This class contains all configurations that are needed for json serialization
 * to work properly.
 *
 * @author acw
 */
@Configuration
@Log4j2
public class JacksonConfig {

    @Autowired
    private ReflectionService reflectionService;

    /**
     * This bean adds the {@link Hibernate5Module} to the Spring Boot default
     * {@link com.fasterxml.jackson.databind.ObjectMapper}, in order to provide
     * support for lazy loading during Jackson serialization.
     *
     * How the overwrite works, see here:
     * https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-customize-the-jackson-objectmapper
     *
     * @return The configured {@link Hibernate5Module} that will be added to the
     * default {@link com.fasterxml.jackson.databind.ObjectMapper} automatically
     * by Spring magic.
     */
    @Bean
    public Hibernate5Module getHibernate5Module() {
        //Register Hibernate5Module to support lazy loaded objects in Jackson serialization
        Hibernate5Module hibernate5Module = new Hibernate5Module();
        hibernate5Module.enable(Hibernate5Module.Feature.FORCE_LAZY_LOADING);
        hibernate5Module.disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION);
        hibernate5Module.enable(Hibernate5Module.Feature.REPLACE_PERSISTENT_COLLECTIONS);
        return hibernate5Module;
    }

    /**
     * This bean adds a custom Module to the Spring Boot default
     * {@link com.fasterxml.jackson.databind.ObjectMapper}, in order to register
     * all annotated implementations of {@link AbstractGUIState} as
     * JsonSubtypes.
     *
     * How the overwrite works, see here:
     * https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-customize-the-jackson-objectmapper
     *
     * @return A configured {@link SimpleModule} that has all implementations of
     * {@link AbstractGUIState} as registrated subtypes, that will be added to
     * the default {@link com.fasterxml.jackson.databind.ObjectMapper}
     * automatically by Spring magic.
     */
    @Bean
    public SimpleModule getSubtypeModule() {
        SimpleModule module = new SimpleModule();

        try {
            //Registers all classes that have a @JsonTypeName-Annotation as JsonSubtypes to Jackson (for json deserialization)
            List<Class<?>> allClasses = reflectionService.getClassesForPackage("de.profect");
            for (Class<? extends Object> c : allClasses) {
                if (c.getAnnotation(JsonTypeName.class) != null) {
                    String value = c.getAnnotation(JsonTypeName.class).value();
                    module.registerSubtypes(new NamedType(c, value));
                }
            }
        } catch (ClassNotFoundException ex) {
            log.error("Could not retrieve classes from package \"de.profect\" for subtype registration in jackson", ex);
        }
        return module;
    }
    
    
//    @Bean
//    public AfterburnerModule afterburnerModule() {
//        return new AfterburnerModule();
//    }
}
