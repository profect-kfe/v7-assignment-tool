package de.profect.config;

import com.rits.cloning.Cloner;
import com.zaxxer.hikari.pool.HikariPool;
import org.hibernate.internal.SessionImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This class contains configurations that don't fit into any other
 * configuration class and are just not important enough to dedicate a own
 * configuration class to it.
 *
 * @author acw
 */
@Configuration
public class MiscConfig {

    /**
     * This Bean is a cloner object that can be used to deep clone arbitrary
     * java objects. Since <code>cloner.deepClone(x)</code> creates a deep copy
     * of object x, it could happen that unwanted nested objects (for example
     * database connections etc.) of x are also cloned. To prevent such
     * accidental copies, the cloner can be configured to ignore classes for the
     * deep copy by configuring it with <code>cloner.dontClone(A.class)</code>
     * or <code>cloner.dontCloneInstanceOf(B.class)</code>.
     *
     * @return The configured cloner.
     */
    @Bean
    public Cloner getCloner() {
        Cloner cloner = new Cloner();
        cloner.dontCloneInstanceOf(HikariPool.class);
        cloner.dontCloneInstanceOf(SessionImpl.class);
        return cloner;
    }
}
