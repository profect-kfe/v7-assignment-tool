package de.profect.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.MapSessionRepository;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author acw
 */
@ConditionalOnProperty(
        value="spring.session.store-type",
        havingValue = "none")
@Configuration
@EnableAutoConfiguration(exclude = {RedisAutoConfiguration.class, RedisRepositoriesAutoConfiguration.class})
public class NoSessionStorageConfig {

    /**
     * This initializes a MapSessionRepository which is used to "store" HTTP
     * sessions in a storage. When you don't use a SessionRepository the custom
     * Cookie Serializer (see {@link WebSecurityConfig#cookieSerializer()}) doesn't work. Previously
     * this session repository was created automatically by Spring Boot. This
     * behaviour seems to be introduced with Spring Boot 2.2.X
     *
     * @return A MapSessionRepository storing the sessions in a ConcurrentHashMap
     */
    @Bean
    public MapSessionRepository sessionRepository() {
        return new MapSessionRepository(new ConcurrentHashMap<>());
    }
}
