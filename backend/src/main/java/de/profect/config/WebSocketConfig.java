package de.profect.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * @author hku
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    
    /**
     * Configuring the message broker, which handles the messages back the 
     * client with destination prefixed '/topic'.
     *
     * @param config The MessageBrokerRegistry object that is configured.
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/v7");
    }

    /**
     * Registring the websocket endpoint and enabling SockJS fallback options 
     * to use the best transport available(websocket, xhr-streaming, xhr-polling, etc).
     *
     * @param registry The Registry where the Stomp Endpoints are registered.
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws").setAllowedOrigins("*").withSockJS();
    }
}
