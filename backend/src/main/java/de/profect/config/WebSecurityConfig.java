package de.profect.config;

import de.profect.authentication.MySavedRequestAwareAuthenticationFailureHandler;
import de.profect.authentication.MySavedRequestAwareAuthenticationSuccessHandler;
import de.profect.authentication.RestAuthenticationEntryPoint;
import de.profect.authentication.V7CookieConfiguration;
import de.profect.authentication.filter.CsrfHeaderFilter;
import de.profect.authentication.privilege.PrivilegeService;
import de.profect.authentication.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.CorsEndpointProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.session.config.annotation.web.http.EnableSpringHttpSession;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * @author hku
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableSpringHttpSession
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    @Qualifier("mySaved")
    private MySavedRequestAwareAuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    @Qualifier("mySavedFailure")
    private MySavedRequestAwareAuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    private CorsEndpointProperties corsConfig;

    @Autowired
    private V7CookieConfiguration cookieConfig;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PrivilegeService privilegeService;

    /**
     * Returns a BCryptEncoder with password strength. It is neccesary for encoding
     * the password of the user in the application
     *
     * @return The password encoder with password strength = 4.
     */
    @Bean(name = "passwordEncoder")
    public PasswordEncoder passwordEncoder() {
        // int parameter -> password strength
        return new BCryptPasswordEncoder(4);
    }

    /**
     * Configuring the authentification of the application. When an user log in,
     * this method will be called to prove the authentification in our selfmade
     * UserDetailService with an given password encoder.
     *
     * @param auth The AuthenticationManagerBuilder that is automatically injected
     *             by Spring
     * @throws Exception If an error occurs while adding the userDetailsService to
     *                   the AuthenticationManagerBuilder.
     */
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    /**
     * Creates a AccessDeniedHandler that logs out exceptions that cause an access
     * denied error. Normally these exceptions are catched and don't appear in the
     * server log and the client only receives a 403 error code.
     */
    // @Bean
    // public AccessDeniedHandler accessDeniedHandler() {
    // return new CustomAccessDeniedHandler();
    // }

    /**
     * Configuring the HttpSecurity of the application. Here you can decide how to
     * treat the security policy like authentification, login or setting headers to
     * defend against attacks. It is a very important function in that case.
     *
     * @param http The HttpSecurity object that is automatically injected by Spring.
     * @throws Exception If an error occurs during configuration of the http object.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // adding a CSRF-Filter, which is
        // executed after CSRF-Filter to set the
        // actual token into the response
        http.csrf().csrfTokenRepository(getCsrfTokenRepository());

        http.cors().configurationSource(corsConfigurationSource()) // enabling cors and setting configuration
                .and().requiresChannel().anyRequest().requiresSecure() // forcing the application to work with https
                // (disable this if you are behind a safe proxy
                // which communicates via http)
                .and()
                .addFilterAfter(getAppropriateFilter(), getAppropriateClass())
                .exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint) // setting the
                // authentication endpoint
                // to handle what happens
                // when an error occure
                // .accessDeniedHandler(accessDeniedHandler()) // enable this for logging of 403
                // errors on client side without logging on server side.
                .and().authorizeRequests() // authorizing some endpoints to make request without authentication
                .antMatchers("/initstate/**").authenticated().antMatchers("/logincontroller").permitAll().and()
                .formLogin().loginProcessingUrl("/login") // defining the login url, where the request has to be sent;
                // by default of spring security it is '/login'
                .usernameParameter("username").passwordParameter("password") // defining the names of the parameter for
                // the login
                .successHandler(authenticationSuccessHandler) // handler for success
                .failureHandler(authenticationFailureHandler) // handler for failure
                .permitAll().and().logout() // configuring logout action
                .deleteCookies(cookieConfig.getName()) // deleting the session id from the cookie
                .invalidateHttpSession(true) // invalidating the session
                .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK)) // returning an http
                // code when finish
                // logout with success
                .and().headers() // defining the headers
                .contentSecurityPolicy("script-src 'self' 'unsafe-eval'" + " 'unsafe-inline'" + "; " // allow self, eval
                        // and camunda
                        // exceptions for
                        // javascript
                        + "style-src 'self' 'unsafe-inline'; " // allow self and inline stylesheets
                        + "connect-src 'self' ws: https:; " // allow self and all https-connections for websockets
                        + "report-uri https://version7.report-uri.com/r/d/csp/enforce;") // report to report-uri.com
                .and().httpStrictTransportSecurity().includeSubDomains(true).maxAgeInSeconds(63072000).and()
                .frameOptions() // enabling frame options to be deny
                .deny();
    }

    /**
     * Configuration of the cors. It is neccessary to define what is enabled except
     * the same origin and which methods can be used.
     *
     * TODO: Maybe this can be done more elegant, by completely relying on the
     * Spring Boot CorsConfiguration. I didn't get it working, but it should be
     * possible. -ACW20190318
     *
     * @return The CorsConfigurationSource object.
     */
    @Bean(name = "corsConfig")
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(corsConfig.getAllowedOrigins());
        configuration.setAllowedMethods(corsConfig.getAllowedMethods());
        configuration.setAllowedHeaders(corsConfig.getAllowedHeaders());
        configuration.setExposedHeaders(corsConfig.getExposedHeaders());
        configuration.setAllowCredentials(corsConfig.getAllowCredentials());

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    /**
     * Configuring a CookieCsrfTokenRepository with a base cookie path to fix the
     * problem with the lite server and port 3000.
     *
     * @return The CookieCsrfTokenRepository object.
     */
    @Bean
    public CsrfTokenRepository getCsrfTokenRepository() {
        HttpSessionCsrfTokenRepository tokenRepository = new HttpSessionCsrfTokenRepository();
        tokenRepository.setHeaderName("X-XSRF-TOKEN");
        return tokenRepository;
    }

    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer serializer = new DefaultCookieSerializer();
        serializer.setCookieName(cookieConfig.getName());
        serializer.setSameSite(cookieConfig.getSamesite());
        serializer.setUseSecureCookie(cookieConfig.isSecure());
        serializer.setUseHttpOnlyCookie(cookieConfig.isHttponly());
        return serializer;
    }

    /**
     * @return Corresponding instance of filter
     */
    private OncePerRequestFilter getAppropriateFilter() {
        return new CsrfHeaderFilter();
    }

    /**
     * @param <T>
     * @return Class reference of one the existing filters
     */
    private <T> T getAppropriateClass() {
        return (T) CsrfFilter.class;
    }
}
