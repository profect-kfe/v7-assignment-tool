package de.profect.config;

import javax.sql.DataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * This class contains configurations that configure necessary data sources.
 * Since the introduction of the camunda engine, we need a second datasource for
 * camunda and can no longer rely on the automatic spring boot datasource.
 * Therefore we must explicitly define the primary datasource and all further
 * datasources here.
 *
 * @author acw
 */
@Configuration
public class DataSourceConfig {

    /**
     * This Bean references the primary {@link DataSource} for almost all JPA
     * related transactions. Since we introduced a secondary datasource for
     * camunda, we can't rely on the spring default datasource anymore.
     *
     * @return The primary {@link DataSource}
     */
    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource primaryDataSource() {
        return DataSourceBuilder.create().build();
    }
}
