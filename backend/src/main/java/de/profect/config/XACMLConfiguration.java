package de.profect.config;

import java.io.IOException;
import org.ow2.authzforce.core.pdp.impl.BasePdpEngine;
import org.ow2.authzforce.core.pdp.impl.PdpEngineConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

/**
 * This class contains all configurations needed, to properly run and access the
 * XACML Engine (currently we use the AuthZForce Engine).
 *
 * Since Spring Boot 2.1.1 we no longer need to extend
 * {@link org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration}
 * because Spring Boot now seems to automatically scan for all implementations
 * of {@link org.springframework.security.access.PermissionEvaluator}. This way
 * {@link de.profect.authentication.accesscontrol.XACMLPermissionEvaluator} is
 * loaded automatically without any further configuration. This behaviour seems
 * only to work when we set the flag
 * {@code spring.main.allow-bean-definition-overriding=true} in the
 * application.properties file.
 *
 * @author acw
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class XACMLConfiguration {

    /**
     * Generates a PDP (Policy Decision Point) Engine using the
     * AuthZForce-Library. This is necessary to evaluate the XACML Policies that
     * are defined in src/main/resources/policy.xml. The engine is mainly used
     * in
     * {@link de.profect.authentication.accesscontrol.XACMLPermissionEvaluator}.
     *
     * This implementation of XACML necessarily needs the JVM parameter
     * "-Djavax.xml.accessExternalSchema=http" set. Otherwise it won't start.
     *
     * @return Returns the PdpEngine aka the XACML Policy Decision Point aka the
     * Engine that evaluates the access rules.
     * @throws IOException Thrown if no config file is found
     */
    @Bean
    public BasePdpEngine getBasePdpEngine() throws IOException {
        return new BasePdpEngine(PdpEngineConfiguration.getInstance("classpath:pdp.xml"));
    }
}
