package de.profect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;

/**
 * This class is the basic Spring start-up class and can be used for standalone
 * deployment. It also contains some basic bean definitions, that wouldn't fit
 * into one of the other Config classes.
 *
 * @author acw
 */
@EnableCaching
@SpringBootApplication(scanBasePackages = {"de.profect"})
public class Application extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
