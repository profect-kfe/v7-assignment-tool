package de.profect.patches;
import de.profect.component.ComponentState;
import de.profect.component.formbuilder.controls.primitives.DynamicFormControlNumberState;
import de.profect.component.formbuilder.controls.primitives.DynamicFormControlTextState;
import de.profect.patches.helpers.V7Patch;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0012_AT25 extends V7Patch {

    @Autowired
    PatchHelperService patchHelper;

    @Override
    public String getPatchVersion() {
        return "0012";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-25 set default value for number inputs to 0d'");
        List<ComponentState> numberFields = patchHelper.getComponentStatesByClass(DynamicFormControlNumberState.class);
        for (ComponentState state: numberFields) {
            DynamicFormControlNumberState textFieldState = (DynamicFormControlNumberState) state;
            textFieldState.setInitValue(0d);
        }
    }
}