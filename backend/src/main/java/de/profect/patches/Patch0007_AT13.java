package de.profect.patches;

import de.profect.component.dataview.compass.report.CompassReport;
import de.profect.component.dataview.compass.report.CompassReportRepository;
import de.profect.patches.helpers.V7Patch;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0007_AT13 extends V7Patch {

    @Autowired
    private CompassReportRepository compassReportRepository;

    @Override
    public String getPatchVersion() {
        return "0007";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-13 wrong date in the reports'");
        List<CompassReport> reports = compassReportRepository.findAll();
        for (CompassReport report : reports) {
            report.setReferenceDate(null);
        }
        log.debug("yop");
    }
}
