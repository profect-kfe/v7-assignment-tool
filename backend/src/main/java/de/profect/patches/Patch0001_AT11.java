package de.profect.patches;

import de.profect.component.ComponentStateRepository;
import de.profect.component.formbuilder.controls.primitives.DynamicFormControlDropdownState;
import de.profect.data.hierarchy.simplehierarchy.HierarchySimpleDefinition;
import de.profect.data.hierarchy.simplehierarchy.SimpleHierarchyItem;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Log4j2
public class Patch0001_AT11 extends V7Patch {

    @Autowired
    ComponentStateRepository componentStateRepository;

    @Autowired
    InitHelperService initService;

    @Override
    public String getPatchVersion() {
        return "0001";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-11 Extension of the contract types'");
        DynamicFormControlDropdownState dropdownState = (DynamicFormControlDropdownState) componentStateRepository.findById(227).get();
        HierarchySimpleDefinition contractTypeHierarchy = initService.hierarchy().initHierarchySimpleDefinition("Project Contract Type Hierarchy", new SimpleHierarchyItem("root", "root",
                new SimpleHierarchyItem("Hosting", "Hosting"),
                new SimpleHierarchyItem("License", "License"),
                new SimpleHierarchyItem("Maintenance", "Maintenance"),
                new SimpleHierarchyItem("Outline Contract", "Outline Contract"),
                new SimpleHierarchyItem("Service", "Service")));
        dropdownState.setHierarchyDefinition(contractTypeHierarchy);
    }
}
