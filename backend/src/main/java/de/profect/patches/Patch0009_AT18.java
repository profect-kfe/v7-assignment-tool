package de.profect.patches;

import de.profect.Category;
import de.profect.component.ComponentInstance;
import de.profect.component.ComponentInstanceRepository;
import de.profect.component.ComponentState;
import de.profect.component.ComponentStateRepository;
import de.profect.component.input.workflow.WorkflowSelectState;
import de.profect.component.panel.leftright.LeftRightPanelState;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0009_AT18 extends V7Patch {

    @Autowired
    InitHelperService initService;

    @Autowired
    ComponentStateRepository componentStateRepository;

    @Autowired
    ComponentInstanceRepository componentInstanceRepository;

    @Autowired
    PatchHelperService patchHelper;

    @Override
    public String getPatchVersion() {
        return "0009";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-18 Workflows in zwei Gruppen aufteilen'");
        List<ComponentState> componentStates = componentStateRepository.findAll();
        for (ComponentState componentState : componentStates) {
            if (patchHelper.isOfClass(componentState, LeftRightPanelState.class) && componentState.getCaption().equals("Startseite Body")) {
                LeftRightPanelState startPage = (LeftRightPanelState) patchHelper.initializeAndUnproxy(componentState);
                ComponentInstance databaseWorkflowSelect = initService.initWorkflowSelect("Create Dataset", WorkflowSelectState.DIRECTION.VERTICAL);
                ((WorkflowSelectState) databaseWorkflowSelect.getDefaultState()).setWorkflowIds(Arrays.asList(39, 179, 212));
                ComponentInstance reportWorkflowSelect = initService.initWorkflowSelect("Create Report", WorkflowSelectState.DIRECTION.VERTICAL);
                ((WorkflowSelectState) reportWorkflowSelect.getDefaultState()).setWorkflowIds(Arrays.asList(256, 264, 272, 280, 288, 298));
                ComponentInstance flowPanel = initService.initFlowPanel("Workflow Select Wrapper", "assignment-workflow-select-wrapper", false, true, true, (Category) null, databaseWorkflowSelect, reportWorkflowSelect);
                ComponentInstance oldRight = startPage.getPositions().get("MAIN");
                startPage.getPositions().put("MAIN", flowPanel);
                componentStateRepository.delete(oldRight.getDefaultState());
                componentInstanceRepository.delete(oldRight);
            }
        }
    }
}