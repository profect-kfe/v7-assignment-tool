package de.profect.patches;

import de.profect.Category;
import de.profect.InitState;
import de.profect.assignmenttool.customer.hierarchy.CustomerHierarchyDefinition;
import de.profect.assignmenttool.employee.hierarchy.EmployeeHierarchyDefinition;
import de.profect.assignmenttool.project.hierarchy.ProjectHierarchyDefinition;
import de.profect.authentication.privilege.PrivilegeSet;
import de.profect.authentication.secured.OperationType;
import de.profect.component.ComponentInstance;
import de.profect.component.ComponentInstanceService;
import de.profect.component.ComponentStateService;
import de.profect.component.dataview.compass.kpi.CompassComplexKpi;
import de.profect.component.dataview.compass.kpi.CompassComplexKpiFormula;
import de.profect.component.dataview.compass.kpi.CompassKpi;
import de.profect.component.dataview.compass.kpi.CompassSimpleKpi;
import de.profect.component.input.workflow.WorkflowGraphState;
import de.profect.component.input.workflow.WorkflowSelectState;
import de.profect.data.datasource.DataSourceService;
import de.profect.data.datasource.sql.HikariSQLDataSource;
import de.profect.data.datasource.sql.PostgreSQLDataSource;
import de.profect.data.datasource.sql.SQLDataModel;
import de.profect.data.datasource.sql.SQLTable;
import de.profect.data.datasource.sql.field.SQLDataSourceField;
import de.profect.data.hierarchy.databasehierarchy.HierarchyDatabaseDefinition;
import de.profect.data.hierarchy.databasehierarchy.HierarchyLevelDefinition;
import de.profect.data.metadata.Metadata;
import de.profect.data.metadata.MetadataContainer;
import de.profect.data.metadata.MetadataDefinition;
import de.profect.data.query.field.Field;
import de.profect.data.query.field.constant.IntegerConstantField;
import de.profect.data.query.field.dataformat.*;
import de.profect.data.query.field.kpi.CoalesceField;
import de.profect.data.query.field.kpi.SumField;
import de.profect.inithelper.InitAssignmentHelperService;
import de.profect.inithelper.InitAssignmentReportsService;
import de.profect.inithelper.InitAssignmentWorkflowsService;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import de.profect.util.InitPageService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

/**
 * This initializes the database for testing purposes. It is loaded after spring
 * context and database connection is established. This is achieved with an
 * ApplicationListener that listens to the ContextRefreshedEvent. When this
 * event is thrown, the application should be loaded completely and the database
 * can be filled safely.
 *
 * @author acw
 */
@Service
@Transactional
@Log4j2
public class MinimalDatabaseInitializer extends V7Patch {

    @Autowired
    private DataSourceService dbService;

    @Autowired
    private InitHelperService initService;

    @Autowired
    private ComponentInstanceService compInstService;

    @Autowired
    private ComponentStateService compService;

    @Autowired
    private InitPageService initPageService;

    @Autowired
    private InitAssignmentWorkflowsService initAssignmentWorkflowsService;

    @Autowired
    private InitAssignmentReportsService initAssignmentReportsService;

    @Autowired
    private InitAssignmentHelperService initAssignmentHelperService;

    @Override
    public String getPatchVersion() {
        return "0";
    }

    @Override
    public void executePatch() throws Exception, AccessDeniedException {
        initDatabase();
    }

    @Value("${spring.datasource.jdbc-url}")
    private String dbURL = "";

    @Value("${spring.datasource.username}")
    private String dbUser = "";

    @Value("${spring.datasource.password}")
    private String dbPassword = "";

    @Value("${spring.profiles.active}")
    private String activeProfile;

    public void initDatabase() {
        initService.initSystemUser();

        HikariSQLDataSource psqlDataSource = initPostgreSQLDataSource(dbURL, dbUser, dbPassword);

        if (initService.isFilled()) {
            return;
        }

        long ts = System.currentTimeMillis();
        log.debug("Starting initDatabase");

        // ------------------------------- METADATA DEFINITIONS -------------------------------------------------
        initService.getAutorMetadataDefinition();
        initService.getReadPublicMetadataDefinition();
        initService.getReadSelectiveMetadataDefinition();
        initService.getReadRoleMetadataDefinition();
        initService.getReadlockUserMetadataDefinition();
        initService.getReadlockTimestampMetadataDefinition();
        initService.getWritelockUserMetadataDefinition();
        initService.getWritelockTimestampMetadataDefinition();

        // ------------------------------- MINIMAL INITSTATE
        // -------------------------------------------------


        // FORMATS
        StringFormat stringFormat = initService.getDefaultStringFormat();
        IntegerFormat integerFormat = initService.getDefaultIntegerFormat();
        FloatFormat floatFormat = initService.getDefaultFloatFormat();
        BooleanFormat booleanFormat = initService.getDefaultBooleanFormat();
        CustomFloatFormat customFloatFormat = initService.getCustomFloatFormat("Custom Float Format", true, 1, 2, 2, "de", "DE");
        CustomFloatFormat hoursFloatFormat = initService.getCustomFloatFormat("Hours", true, 1, 1, 1, "de", "DE");
        PercentFormat defaultPercentFormat = initService.getDefaultPercentFormat();
        CurrencyFormat currencyFormat = initService.getDefaultCurrencyFormat();
        DateFormat defaultDateFormat = initService.getDefaultDateFormat();
        DateFormat defaultTimestampFormat = initService.getDefaultTimestampFormat();

        // TABLES
        SQLTable absencesTable = initService.initDBTable("Absences", "public", "ABSENCES", psqlDataSource);
        SQLTable customersTable = initService.initDBTable("Customers", "public", "CUSTOMERS", psqlDataSource);
        SQLTable invoiceRecipientTable = initService.initDBTable("Invoice Recipients", "public", "INVOICE_RECIPIENTS", psqlDataSource);
        SQLTable benefitRecipientTable = initService.initDBTable("Benefit Recipients", "public", "BENEFIT_RECIPIENTS", psqlDataSource);
        SQLTable assignmentsTable = initService.initDBTable("Assignments", "public", "ASSIGNMENTS", psqlDataSource);
        SQLTable employeesTable = initService.initDBTable("Employees", "public", "EMPLOYEES", psqlDataSource);
        SQLTable assignedEmployeesTable = initService.initDBTable("Assigned Employees", "public", "ASSIGNED_EMPLOYEES", psqlDataSource);
        SQLTable projectsTable = initService.initDBTable("Projects", "public", "PROJECTS", psqlDataSource);
//        SQLTable teamLeaderTable = initService.initDBTable("Team Leader", "public","TEAMLEADER", psqlDataSource);

        SQLTable monthlyAvailabilityTable = initService.initDBTable("Monthly Availability", "public", "MONTHLY_AVAILABILITY", psqlDataSource);
        SQLTable monthlyManDaysTable = initService.initDBTable("Monthly Man Days", "public", "MONTHLY_MAN_DAYS", psqlDataSource);
        SQLTable monthlyProjectSalesTable = initService.initDBTable("Monthly Project Sales", "public", "MONTHLY_PROJECT_SALES", psqlDataSource);
        SQLTable weeklyQuotasTable = initService.initDBTable("Weekly quota per employee", "public", "WEEKLY_QUOTA_PER_EMPLOYEE", psqlDataSource);
        SQLTable weeklyAssignmentsTable = initService.initDBTable("Weekly assignments", "public", "WEEKLY_ASSIGNMENTS", psqlDataSource);

        // FIELDS
        SQLDataSourceField absenceIdField = initService.field().initDBField("ID", "ID", integerFormat, absencesTable);
        SQLDataSourceField absenceEmployeeIdField = initService.field().initDBField("ID", "EMPLOYEE_ID", integerFormat, absencesTable);
        SQLDataSourceField absenceStartDateField = initService.field().initDBField("Start Date", "START_DATE", defaultDateFormat, absencesTable);
        SQLDataSourceField absenceEndDateField = initService.field().initDBField("End Date", "END_DATE", defaultDateFormat, absencesTable);
        SQLDataSourceField absenceReasonField = initService.field().initDBField("Reason", "REASON", stringFormat, absencesTable);

        SQLDataSourceField customerIdField = initService.field().initDBField("ID", "ID", integerFormat, customersTable);
        SQLDataSourceField customerNameField = initService.field().initDBField("Customer Name", "CUSTOMER_NAME", stringFormat, customersTable);
        SQLDataSourceField customerDivisionField = initService.field().initDBField("Division", "DIVISION", stringFormat, customersTable);
        SQLDataSourceField customerCaptionField = initService.field().initDBField("Customer Name", "CAPTION", stringFormat, customersTable);

        SQLDataSourceField invoiceRecipientIdField = initService.field().initDBField("ID", "ID", integerFormat, invoiceRecipientTable);
        SQLDataSourceField invoiceRecipientNameField = initService.field().initDBField("Invoice Recipient Name", "CUSTOMER_NAME", stringFormat, invoiceRecipientTable);
        SQLDataSourceField invoiceRecipientDivisionField = initService.field().initDBField("Invoice Recipient Division", "DIVISION", stringFormat, invoiceRecipientTable);

        SQLDataSourceField benefitRecipientIdField = initService.field().initDBField("ID", "ID", integerFormat, benefitRecipientTable);
        SQLDataSourceField benefitRecipientDivisionField = initService.field().initDBField("Benefit Recipient Division", "DIVISION", stringFormat, benefitRecipientTable);

        SQLDataSourceField assignmentIdField = initService.field().initDBField("ID", "ID", integerFormat, assignmentsTable);
        SQLDataSourceField assignmentCaptionField = initService.field().initDBField("Assignment Caption", "CAPTION", integerFormat, assignmentsTable);
        SQLDataSourceField assignmentEmployeeIdField = initService.field().initDBField("Employee ID", "EMPLOYEE_ID", integerFormat, assignmentsTable);
        SQLDataSourceField assignmentProjectIdField = initService.field().initDBField("Project ID", "PROJECT_ID", integerFormat, assignmentsTable);
        SQLDataSourceField assignmentStartDateField = initService.field().initDBField("Start Date", "START_DATE", defaultDateFormat, assignmentsTable);
        SQLDataSourceField assignmentEndDateField = initService.field().initDBField("End Date", "END_DATE", defaultDateFormat, assignmentsTable);
        SQLDataSourceField assignmentWeeklyHoursField = initService.field().initDBField("Weekly Hours", "WEEKLY_HOURS", hoursFloatFormat, assignmentsTable);
        SQLDataSourceField assignmentMondayHoursField = initService.field().initDBField("Monday Hours", "MONDAY_HOURS", hoursFloatFormat, assignmentsTable);
        SQLDataSourceField assignmentTuesdayHoursField = initService.field().initDBField("Tuesday Hours", "TUESDAY_HOURS", hoursFloatFormat, assignmentsTable);
        SQLDataSourceField assignmentWednesdayHoursField = initService.field().initDBField("Wednesday Hours", "WEDNESDAY_HOURS", hoursFloatFormat, assignmentsTable);
        SQLDataSourceField assignmentThursdayHoursField = initService.field().initDBField("Thursday Hours", "THURSDAY_HOURS", hoursFloatFormat, assignmentsTable);
        SQLDataSourceField assignmentFridayHoursField = initService.field().initDBField("Friday Hours", "FRIDAY_HOURS", hoursFloatFormat, assignmentsTable);

        SQLDataSourceField employeeIdField = initService.field().initDBField("ID", "ID", integerFormat, employeesTable);
        SQLDataSourceField employeeCaptionField = initService.field().initDBField("Employee", "CAPTION", stringFormat, employeesTable);
        SQLDataSourceField employeeCaptionAsProjectLeaderField = initService.field().initDBField("Project Leader", "CAPTION", stringFormat, employeesTable);
        SQLDataSourceField employeeStaffNumberField = initService.field().initDBField("Staff Number", "STAFF_NUMBER", stringFormat, employeesTable);
        SQLDataSourceField employeeFirstNameField = initService.field().initDBField("First Name", "FIRST_NAME", stringFormat, employeesTable);
        SQLDataSourceField employeeLastNameField = initService.field().initDBField("Last Name", "LAST_NAME", stringFormat, employeesTable);
        SQLDataSourceField employeeLocationField = initService.field().initDBField("Location", "LOCATION", stringFormat, employeesTable);
        SQLDataSourceField employeeContractTypeField = initService.field().initDBField("Contract Type", "CONTRACT_TYPE", stringFormat, employeesTable);
        SQLDataSourceField employeeWeeklyHoursField = initService.field().initDBField("Weekly Hours", "WEEKLY_HOURS", hoursFloatFormat, employeesTable);
        SQLDataSourceField employeeRetirementDateField = initService.field().initDBField("Retirement Date", "RETIREMENT_DATE", defaultDateFormat, employeesTable);
        SQLDataSourceField employeeRetirementReasonField = initService.field().initDBField("Comment", "COMMENT", stringFormat, employeesTable);
        SQLDataSourceField employeeEmailField = initService.field().initDBField("E-Mail", "EMAIL", stringFormat, employeesTable);
        SQLDataSourceField employeeShorthandField = initService.field().initDBField("Shorthand", "SHORTHAND", stringFormat, employeesTable);
        SQLDataSourceField employeeIsTeamLeaderField = initService.field().initDBField("Is Team Leader", "IS_TEAMLEADER", /*booleanFormat -- does not work somehow -sme*/ stringFormat, employeesTable);
        SQLDataSourceField employeeTeamLeaderIdField = initService.field().initDBField("Team Leader ID", "TEAM_LEADER_EMPLOYEE_ID", stringFormat, employeesTable);
        SQLDataSourceField employeeTeamLeaderShorthandField = initService.field().initDBField("Team Leader", "TEAM_LEADER_SHORTHAND", stringFormat, employeesTable);

        SQLDataSourceField assignedEmployeeIdField = initService.field().initDBField("ID", "ID", integerFormat, assignedEmployeesTable);
        SQLDataSourceField assignedEmployeeCaptionField = initService.field().initDBField("Assigned Employee", "CAPTION", stringFormat, assignedEmployeesTable);
        SQLDataSourceField assignedEmployeeTeamLeaderIdField = initService.field().initDBField("Team Leader ID", "TEAM_LEADER_EMPLOYEE_ID", stringFormat, assignedEmployeesTable);
        SQLDataSourceField assignedEmployeeLocationField = initService.field().initDBField("Location", "LOCATION", stringFormat, assignedEmployeesTable);
        SQLDataSourceField assignedEmployeeContractTypeField = initService.field().initDBField("Contract Type", "CONTRACT_TYPE", stringFormat, assignedEmployeesTable);
        SQLDataSourceField assignedEmployeeWeeklyHoursField = initService.field().initDBField("Weekly Hours", "WEEKLY_HOURS", hoursFloatFormat, assignedEmployeesTable);
        SQLDataSourceField assignedEmployeeTeamLeaderShorthandField = initService.field().initDBField("Team Leader", "TEAM_LEADER_SHORTHAND", stringFormat, assignedEmployeesTable);


        SQLDataSourceField projectIdField = initService.field().initDBField("ID", "ID", integerFormat, projectsTable);
        SQLDataSourceField projectCaptionField = initService.field().initDBField("Project Name", "caption", stringFormat, projectsTable);
        SQLDataSourceField projectKeyField = initService.field().initDBField("Project Key", "PROJECT_KEY", stringFormat, projectsTable);
        SQLDataSourceField projectNameField = initService.field().initDBField("Project Name", "PROJECT_NAME", stringFormat, projectsTable);
        SQLDataSourceField projectProjectTypeField = initService.field().initDBField("Project Type", "PROJECT_TYPE", stringFormat, projectsTable);
        SQLDataSourceField projectStartDateField = initService.field().initDBField("Start Date", "START_DATE", defaultDateFormat, projectsTable);
        SQLDataSourceField projectEndDateField = initService.field().initDBField("End Date", "END_DATE", defaultDateFormat, projectsTable);
        SQLDataSourceField projectContractTypeField = initService.field().initDBField("Contract Type", "CONTRACT_TYPE", stringFormat, projectsTable);
        SQLDataSourceField projectSalesField = initService.field().initDBField("Sales", "SALES", currencyFormat, projectsTable);
        SQLDataSourceField projectNotesField = initService.field().initDBField("Project Notes", "NOTES", stringFormat, projectsTable);
        SQLDataSourceField projectBenefitRecipientField = initService.field().initDBField("Benefit Recipient", "BENEFIT_RECIPIENT_CUSTOMER_ID", integerFormat, projectsTable);
        SQLDataSourceField projectInvoiceRecipientField = initService.field().initDBField("Invoice Recipient", "INVOICE_RECIPIENT_CUSTOMER_ID", integerFormat, projectsTable);
        SQLDataSourceField projectLeaderField = initService.field().initDBField("Project Leader", "PROJECT_LEADER_EMPLOYEE_ID", integerFormat, projectsTable);

        SQLDataSourceField weeklyQuotaPerEmployeeIdField = initService.field().initDBField("ID", "ID", integerFormat, weeklyQuotasTable);
        SQLDataSourceField weeklyQuotaPerEmployeeEmployeeIdField = initService.field().initDBField("Employee ID", "EMPLOYEE_ID", integerFormat, weeklyQuotasTable);
        SQLDataSourceField weeklyQuotaPerEmployeeWeeklyHoursField = initService.field().initDBField("Weekly Quota", "WEEKLY_QUOTA", hoursFloatFormat, weeklyQuotasTable);
        SQLDataSourceField weeklyQuotaPerEmployeeDateField = initService.field().initDBField("Week Date", "WEEK_DATE", defaultDateFormat, weeklyQuotasTable);

        SQLDataSourceField weeklyAssignmentsIdField = initService.field().initDBField("ID", "ID", integerFormat, weeklyAssignmentsTable);
        SQLDataSourceField weeklyAssignmentsEmployeeIdField = initService.field().initDBField("Employee", "EMPLOYEE_ID", integerFormat, weeklyAssignmentsTable);
        SQLDataSourceField weeklyAssignmentsProjectIdField = initService.field().initDBField("Project", "PROJECT_ID", integerFormat, weeklyAssignmentsTable);
        SQLDataSourceField weeklyAssignmentsCustomerIdField = initService.field().initDBField("Customer", "CUSTOMER_ID", integerFormat, weeklyAssignmentsTable);
        SQLDataSourceField weeklyAssignmentsHoursField = initService.field().initDBField("Hours", "HOURS", hoursFloatFormat, weeklyAssignmentsTable);
        SQLDataSourceField weeklyAssignmentManDaysField = initService.field().initDBField("Man Days", "MAN_DAYS", hoursFloatFormat, weeklyAssignmentsTable);
        SQLDataSourceField weeklyAssignmentsDailyRatesField = initService.field().initDBField("Daily Rates", "DAILY_RATES", currencyFormat, weeklyAssignmentsTable);
        SQLDataSourceField weeklyAssignmentsDateField = initService.field().initDBField("Week Date", "WEEK_DATE", defaultDateFormat, weeklyAssignmentsTable);

        SQLDataSourceField monthlyManDaysIdField = initService.field().initDBField("ID", "ID", integerFormat, monthlyManDaysTable);
        SQLDataSourceField monthlyManDaysEmployeeIdField = initService.field().initDBField("Employee", "EMPLOYEE_ID", integerFormat, monthlyManDaysTable);
        SQLDataSourceField monthlyManDaysProjectIdField = initService.field().initDBField("Project", "PROJECT_ID", integerFormat, monthlyManDaysTable);
        SQLDataSourceField monthlyManDaysCustomerIdField = initService.field().initDBField("Customer", "CUSTOMER_ID", integerFormat, monthlyManDaysTable);
        SQLDataSourceField monthlyManDaysManDaysField = initService.field().initDBField("Man Days", "MAN_DAYS", hoursFloatFormat, monthlyManDaysTable);
        SQLDataSourceField monthlyManDaysDailyRatesField = initService.field().initDBField("Daily Rates", "DAILY_RATES", currencyFormat, monthlyManDaysTable);
        SQLDataSourceField monthlyManDaysProjectTypeField = initService.field().initDBField("Project Type", "PROJECT_TYPE", stringFormat, monthlyManDaysTable);
        SQLDataSourceField monthlyManDaysDateField = initService.field().initDBField("Date", "DATE", defaultDateFormat, monthlyManDaysTable);

        SQLDataSourceField monthlyAvailabilityIdField = initService.field().initDBField("ID", "ID", integerFormat, monthlyAvailabilityTable);
        SQLDataSourceField monthlyAvailabilityEmployeeIdField = initService.field().initDBField("Employee", "EMPLOYEE_ID", integerFormat, monthlyAvailabilityTable);
        SQLDataSourceField monthlyAvailabilityAvailabilityField = initService.field().initDBField("Availability", "AVAILABILITY", hoursFloatFormat, monthlyAvailabilityTable);
        SQLDataSourceField monthlyAvailabilityDateField = initService.field().initDBField("Date", "AVAILABILITY_DATE", defaultDateFormat, monthlyAvailabilityTable);

        SQLDataSourceField monthlyProjectSalesIdField = initService.field().initDBField("ID", "ID", integerFormat, monthlyProjectSalesTable);
        SQLDataSourceField monthlyProjectSalesProjectIdField = initService.field().initDBField("Project", "PROJECT_ID", integerFormat, monthlyProjectSalesTable);
        SQLDataSourceField monthlyProjectSalesCustomerIdField = initService.field().initDBField("Customer", "CUSTOMER_ID", integerFormat, monthlyProjectSalesTable);
        SQLDataSourceField monthlyProjectSalesSalesField = initService.field().initDBField("Sales", "SALES", hoursFloatFormat, monthlyProjectSalesTable);
        SQLDataSourceField monthlyProjectSalesDateField = initService.field().initDBField("Date", "SALES_DATE", defaultDateFormat, monthlyProjectSalesTable);

        Field countEmployees = initService.field().initCountDistinct("Count Employees", "Employees", integerFormat, employeeIdField);
        Field countAbsences = initService.field().initCountDistinct("Count Absences", "Absences", integerFormat, absenceIdField);
        Field countAssignments = initService.field().initCountDistinct("Count Assignments", "Assignments", integerFormat, assignmentIdField);
        Field countCustomers = initService.field().initCountDistinct("Count Customers", "Customers", integerFormat, customerIdField);
        Field countProjects = initService.field().initCountDistinct("Count Projects", "Projects", integerFormat, projectIdField);

        final IntegerConstantField zeroConstant = initService.field().initIntegerConstant("ZERO CONSTANT", 0, integerFormat, "");
        CoalesceField coalesceWeeklyHoursZeroField = initService.field().initCoalesce("COALESCE(weekly_hours, 0", "", hoursFloatFormat, weeklyQuotaPerEmployeeWeeklyHoursField, zeroConstant);
        SumField employeeWeeklyQuotaField = initService.field().initSum("SUM(COALESCE(weekly_hours, 0))", "", coalesceWeeklyHoursZeroField, hoursFloatFormat);

        CoalesceField coalesceHoursZeroField = initService.field().initCoalesce("COALESCE(hours, 0", "", hoursFloatFormat, weeklyAssignmentsHoursField, zeroConstant);
        SumField sumCoalesceHoursZeroField = initService.field().initSum("SUM(COALESCE(hours, 0))", "", coalesceHoursZeroField, hoursFloatFormat);

        CoalesceField coalesceDailyRatesZeroField = initService.field().initCoalesce("COALESCE(daily_rates, 0", "", currencyFormat, weeklyAssignmentsDailyRatesField, zeroConstant);
        SumField sumCoalesceDailyRatesField = initService.field().initSum("SUM(COALESCE(daily_rates, 0))", "", coalesceDailyRatesZeroField, currencyFormat);

        CoalesceField coalesceMonthlyDailyRatesZeroField = initService.field().initCoalesce("COALESCE(daily_rates, 0", "", currencyFormat, monthlyManDaysDailyRatesField, zeroConstant);
        SumField sumCoalesceMonthlyDailyRatesField = initService.field().initSum("SUM(COALESCE(daily_rates, 0))", "", coalesceMonthlyDailyRatesZeroField, currencyFormat);

        CoalesceField coalesceWeeklyManDaysZeroField = initService.field().initCoalesce("COALESCE(man_days, 0", "", hoursFloatFormat, weeklyAssignmentManDaysField, zeroConstant);
        SumField sumCoalesceWeeklyManDaysZeroField = initService.field().initSum("SUM(COALESCE(man_days, 0))", "", coalesceWeeklyManDaysZeroField, hoursFloatFormat);

        CoalesceField coalesceMonthlyManDaysZeroField = initService.field().initCoalesce("COALESCE(man_days, 0", "", hoursFloatFormat, monthlyManDaysManDaysField, zeroConstant);
        SumField sumCoalesceMonthlyManDaysZeroField = initService.field().initSum("SUM(COALESCE(man_days, 0))", "", coalesceMonthlyManDaysZeroField, hoursFloatFormat);

        CoalesceField coalesceMonthlyAvailabilityZeroField = initService.field().initCoalesce("COALESCE(availability, 0", "", hoursFloatFormat, monthlyAvailabilityAvailabilityField, zeroConstant);
        SumField sumCoalesceMonthlyAvailabilityZeroField = initService.field().initSum("SUM(COALESCE(availability, 0))", "", coalesceMonthlyAvailabilityZeroField, hoursFloatFormat);

        CoalesceField coalesceMonthlyProjectSalesZeroField = initService.field().initCoalesce("COALESCE(sales, 0", "", currencyFormat, monthlyProjectSalesSalesField, zeroConstant);
        SumField sumCoalesceMonthlyProjectSalesZeroField = initService.field().initSum("SUM(COALESCE(sales, 0))", "", coalesceMonthlyProjectSalesZeroField, currencyFormat);

        // monthlyAvailabilityAvailabilityField

        // DATABASE HIERARCHIES
        HierarchyLevelDefinition employeeHierarchyLevelDefinition = initService.hierarchy().initHierarchyLevelDefinition(employeeIdField, employeeCaptionField);
        HierarchyDatabaseDefinition employeeHierarchy = initService.hierarchy().initHierarchyDatabaseDefinition("Employee Hierarchy", employeeHierarchyLevelDefinition);

        HierarchyLevelDefinition teamLeaderHierarchyLevelDefinition = initService.hierarchy().initHierarchyLevelDefinition(employeeTeamLeaderShorthandField, employeeTeamLeaderShorthandField);
        HierarchyDatabaseDefinition teamLeaderHierarchy = initService.hierarchy().initHierarchyDatabaseDefinition("Team Leader Hierarchy", teamLeaderHierarchyLevelDefinition);

        HierarchyLevelDefinition assignedEmployeeHierarchyLevelDefinition = initService.hierarchy().initHierarchyLevelDefinition(assignedEmployeeIdField, assignedEmployeeCaptionField);
        HierarchyDatabaseDefinition assignedEmployeeHierarchy = initService.hierarchy().initHierarchyDatabaseDefinition("Assigned Employee Hierarchy", assignedEmployeeHierarchyLevelDefinition);

        HierarchyLevelDefinition locationHierarchyLevelDefinition = initService.hierarchy().initHierarchyLevelDefinition(employeeLocationField, employeeLocationField);
        HierarchyDatabaseDefinition locationHierarchy = initService.hierarchy().initHierarchyDatabaseDefinition("Team Hierarchy", locationHierarchyLevelDefinition);

        HierarchyLevelDefinition projectHierarchyLevelDefinition = initService.hierarchy().initHierarchyLevelDefinition(projectIdField, projectKeyField);
        HierarchyDatabaseDefinition projectHierarchy = initService.hierarchy().initHierarchyDatabaseDefinition("Project Hierarchy", projectHierarchyLevelDefinition);

        HierarchyLevelDefinition assignmentHierarchyLevelDefinition = initService.hierarchy().initHierarchyLevelDefinition(assignmentIdField, assignmentCaptionField);
        HierarchyDatabaseDefinition assignmentHierarchy = initService.hierarchy().initHierarchyDatabaseDefinition("Assignment Hierarchy", assignmentHierarchyLevelDefinition);

        HierarchyLevelDefinition invoiceRecipientHierarchyLevelDefinition = initService.hierarchy().initHierarchyLevelDefinition(invoiceRecipientNameField, invoiceRecipientNameField);
        HierarchyLevelDefinition invoiceRecipientDivisionHierarchyLevelDefinition = initService.hierarchy().initHierarchyLevelDefinition(invoiceRecipientIdField, invoiceRecipientDivisionField);
        HierarchyDatabaseDefinition invoiceRecipientHierarchy = initService.hierarchy().initHierarchyDatabaseDefinition("Invoice Recipient Hierarchy", invoiceRecipientHierarchyLevelDefinition, invoiceRecipientDivisionHierarchyLevelDefinition);

        // ------------------------------- DATA MODEL -------------------------------------------------
        SQLDataModel model = initService.initSQLDataModel("Datenmodell", new ArrayList<>());
        //employees
        initService.addConnections(model, employeesTable, employeeIdField, absencesTable, absenceEmployeeIdField);
        initService.addConnections(model, employeesTable, employeeIdField, assignmentsTable, assignmentEmployeeIdField);
        initService.addConnections(model, employeesTable, employeeIdField, projectsTable, projectLeaderField);
        initService.addConnections(model, employeesTable, employeeIdField, weeklyQuotasTable, weeklyQuotaPerEmployeeEmployeeIdField);
        initService.addConnections(model, employeesTable, employeeIdField, weeklyAssignmentsTable, weeklyAssignmentsEmployeeIdField);
        initService.addConnections(model, employeesTable, employeeIdField, monthlyManDaysTable, monthlyManDaysEmployeeIdField);
        initService.addConnections(model, employeesTable, employeeIdField, monthlyAvailabilityTable, monthlyAvailabilityEmployeeIdField);
        //assigned_employees
        initService.addConnections(model, assignedEmployeesTable, assignedEmployeeIdField, assignmentsTable, assignmentEmployeeIdField);
        initService.addConnections(model, assignedEmployeesTable, assignedEmployeeIdField, monthlyManDaysTable, monthlyManDaysEmployeeIdField);
        initService.addConnections(model, assignedEmployeesTable, projectsTable, assignmentsTable);
        initService.addConnections(model, assignedEmployeesTable, invoiceRecipientTable, projectsTable);
        initService.addConnections(model, assignedEmployeesTable, assignedEmployeeIdField, monthlyAvailabilityTable, monthlyAvailabilityEmployeeIdField);
        //absences
        initService.addConnections(model, absencesTable, absenceEmployeeIdField, employeesTable, employeeIdField);
        //assignments
        initService.addConnections(model, assignmentsTable, assignmentEmployeeIdField, employeesTable, employeeIdField);
        initService.addConnections(model, assignmentsTable, assignmentProjectIdField, projectsTable, projectIdField);
        initService.addConnections(model, assignmentsTable, assignmentProjectIdField, weeklyAssignmentsTable, weeklyAssignmentsProjectIdField);
        initService.addConnections(model, assignmentsTable, assignmentProjectIdField, monthlyManDaysTable, monthlyManDaysProjectIdField);
        initService.addConnections(model, assignmentsTable, assignmentEmployeeIdField, assignedEmployeesTable, assignedEmployeeIdField);
        //projects
        initService.addConnections(model, projectsTable, projectLeaderField, employeesTable, employeeIdField);
        initService.addConnections(model, projectsTable, projectBenefitRecipientField, benefitRecipientTable, benefitRecipientIdField);
        initService.addConnections(model, projectsTable, projectInvoiceRecipientField, invoiceRecipientTable, invoiceRecipientIdField);
        initService.addConnections(model, projectsTable, projectIdField, assignmentsTable, assignmentProjectIdField);
        initService.addConnections(model, projectsTable, projectIdField, weeklyAssignmentsTable, weeklyAssignmentsProjectIdField);
        initService.addConnections(model, projectsTable, projectIdField, monthlyManDaysTable, monthlyManDaysProjectIdField);
        initService.addConnections(model, projectsTable, assignedEmployeesTable, assignmentsTable);
        initService.addConnections(model, projectsTable, projectIdField, monthlyProjectSalesTable, monthlyProjectSalesProjectIdField);
        //benefit recipients
        initService.addConnections(model, benefitRecipientTable, benefitRecipientIdField, projectsTable, projectBenefitRecipientField);
        //invoice recipients
        initService.addConnections(model, invoiceRecipientTable, invoiceRecipientIdField, projectsTable, projectInvoiceRecipientField);
        initService.addConnections(model, invoiceRecipientTable, invoiceRecipientIdField, weeklyAssignmentsTable, weeklyAssignmentsCustomerIdField);
        initService.addConnections(model, invoiceRecipientTable, assignedEmployeesTable, assignmentsTable);
        initService.addConnections(model, invoiceRecipientTable, assignmentsTable, projectsTable);
        initService.addConnections(model, invoiceRecipientTable, invoiceRecipientIdField, monthlyProjectSalesTable, monthlyProjectSalesCustomerIdField);
        //monthly_man_days
        initService.addConnections(model, monthlyManDaysTable, monthlyManDaysEmployeeIdField, employeesTable, employeeIdField);
        initService.addConnections(model, monthlyManDaysTable, monthlyManDaysProjectIdField, projectsTable, projectIdField);
        initService.addConnections(model, monthlyManDaysTable, monthlyManDaysProjectIdField, assignmentsTable, assignmentProjectIdField);
        initService.addConnections(model, monthlyManDaysTable, monthlyManDaysCustomerIdField, invoiceRecipientTable, invoiceRecipientIdField);
        initService.addConnections(model, monthlyManDaysTable, monthlyManDaysEmployeeIdField, assignedEmployeesTable, assignedEmployeeIdField);
        //monthly_project_sales
        initService.addConnections(model, monthlyProjectSalesTable, monthlyProjectSalesProjectIdField, projectsTable, projectIdField);
        initService.addConnections(model, monthlyProjectSalesTable, monthlyProjectSalesCustomerIdField, invoiceRecipientTable, invoiceRecipientIdField);

        //monthly_availability
        initService.addConnections(model, monthlyAvailabilityTable, monthlyAvailabilityEmployeeIdField, employeesTable, employeeIdField);
        initService.addConnections(model, monthlyAvailabilityTable, monthlyAvailabilityEmployeeIdField, assignedEmployeesTable, assignedEmployeeIdField);
        //weekly_quotas
        initService.addConnections(model, weeklyQuotasTable, weeklyQuotaPerEmployeeEmployeeIdField, employeesTable, employeeIdField);
        //weekly_assignments
        initService.addConnections(model, weeklyAssignmentsTable, weeklyAssignmentsEmployeeIdField, employeesTable, employeeIdField);
        initService.addConnections(model, weeklyAssignmentsTable, weeklyAssignmentsProjectIdField, projectsTable, projectIdField);
        initService.addConnections(model, weeklyAssignmentsTable, weeklyAssignmentsProjectIdField, assignmentsTable, assignmentProjectIdField);
        initService.addConnections(model, weeklyAssignmentsTable, weeklyAssignmentsCustomerIdField, invoiceRecipientTable, invoiceRecipientIdField);

        try {
            initService.addSQLTableDateField(model, weeklyQuotasTable, Arrays.asList(weeklyQuotaPerEmployeeDateField));
            initService.addSQLTableDateField(model, weeklyAssignmentsTable, Arrays.asList(weeklyAssignmentsDateField));
            initService.addSQLTableDateField(model, monthlyManDaysTable, Arrays.asList(monthlyManDaysDateField));
            initService.addSQLTableDateField(model, monthlyAvailabilityTable, Arrays.asList(monthlyAvailabilityDateField));
            initService.addSQLTableDateField(model, projectsTable, Arrays.asList(projectEndDateField));
            initService.addSQLTableDateField(model, monthlyProjectSalesTable, Arrays.asList(monthlyProjectSalesDateField));
        } catch (IllegalArgumentException ex) {
            log.warn(ex.getMessage(), ex);
        }

        psqlDataSource.setDatamodel(model);
        dbService.save(psqlDataSource);


        // COMPASS KPIS
        CompassSimpleKpi quotaKpi = new CompassSimpleKpi();
        quotaKpi.setCaption("QUOTA");
        quotaKpi.setField(employeeWeeklyQuotaField);
        quotaKpi = (CompassSimpleKpi) initService.initCompassKpi(quotaKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi actualHoursKpi = new CompassSimpleKpi();
        actualHoursKpi.setCaption("ACTUAL");
        actualHoursKpi.setField(sumCoalesceHoursZeroField);
        actualHoursKpi = (CompassSimpleKpi) initService.initCompassKpi(actualHoursKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassComplexKpiFormula employeeVacancyFormula = new CompassComplexKpiFormula();
        employeeVacancyFormula.setExpression(Arrays.asList("kpi_" + quotaKpi.getId(), " - ", "kpi_" + actualHoursKpi.getId()));
        Set<CompassKpi> variables = new HashSet<>();
        variables.add(quotaKpi);
        variables.add(actualHoursKpi);
        employeeVacancyFormula.setVariables(variables);

        CompassComplexKpi employeeVacancyKpi = new CompassComplexKpi();
        employeeVacancyKpi.setCaption("VACANCY");
        employeeVacancyKpi.setFormula(employeeVacancyFormula);
        employeeVacancyKpi = (CompassComplexKpi) initService.initCompassKpi(employeeVacancyKpi, "star", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-COMPLEX-KPI"));

        CompassSimpleKpi weeklyManDaysKpi = new CompassSimpleKpi();
        weeklyManDaysKpi.setCaption("M/D");
        weeklyManDaysKpi.setField(sumCoalesceWeeklyManDaysZeroField);
        weeklyManDaysKpi = (CompassSimpleKpi) initService.initCompassKpi(weeklyManDaysKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi monthlyManDaysKpi = new CompassSimpleKpi();
        monthlyManDaysKpi.setCaption("M/D");
        monthlyManDaysKpi.setField(sumCoalesceMonthlyManDaysZeroField);
        monthlyManDaysKpi = (CompassSimpleKpi) initService.initCompassKpi(monthlyManDaysKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi monthlyManDaysInternKpi = new CompassSimpleKpi();
        monthlyManDaysInternKpi.setCaption("INTERN");
        monthlyManDaysInternKpi.setField(sumCoalesceMonthlyManDaysZeroField);
        monthlyManDaysInternKpi.setCondition(initService.condition().initCategorialCondition("Intern", "", "", "", null, null, monthlyManDaysProjectTypeField, null, "Intern"));
        monthlyManDaysInternKpi = (CompassSimpleKpi) initService.initCompassKpi(monthlyManDaysInternKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi monthlyManDaysExternKpi = new CompassSimpleKpi();
        monthlyManDaysExternKpi.setCaption("EXTERN");
        monthlyManDaysExternKpi.setField(sumCoalesceMonthlyManDaysZeroField);
        monthlyManDaysExternKpi.setCondition(initService.condition().initCategorialCondition("Intern", "", "", "", null, null, monthlyManDaysProjectTypeField, null, "Extern"));
        monthlyManDaysExternKpi = (CompassSimpleKpi) initService.initCompassKpi(monthlyManDaysExternKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi monthlyManDaysOfferingKpi = new CompassSimpleKpi();
        monthlyManDaysOfferingKpi.setCaption("OFFERING");
        monthlyManDaysOfferingKpi.setField(sumCoalesceMonthlyManDaysZeroField);
        monthlyManDaysOfferingKpi.setCondition(initService.condition().initCategorialCondition("Intern", "", "", "", null, null, monthlyManDaysProjectTypeField, null, "Offering"));
        monthlyManDaysOfferingKpi = (CompassSimpleKpi) initService.initCompassKpi(monthlyManDaysOfferingKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi monthlyAvailabilityKpi = new CompassSimpleKpi();
        monthlyAvailabilityKpi.setCaption("AVAILABLE");
        monthlyAvailabilityKpi.setField(sumCoalesceMonthlyAvailabilityZeroField);
        monthlyAvailabilityKpi = (CompassSimpleKpi) initService.initCompassKpi(monthlyAvailabilityKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassComplexKpiFormula monthlyVacancyFormula = new CompassComplexKpiFormula();
        monthlyVacancyFormula.setExpression(Arrays.asList("kpi_" + monthlyAvailabilityKpi.getId(), " - ", "kpi_" + monthlyManDaysKpi.getId()));
        Set<CompassKpi> monthlyVacancyVariables = new HashSet<>();
        monthlyVacancyVariables.add(monthlyManDaysKpi);
        monthlyVacancyVariables.add(monthlyAvailabilityKpi);
        monthlyVacancyFormula.setVariables(monthlyVacancyVariables);

        CompassComplexKpi monthlyVacancyKpi = new CompassComplexKpi();
        monthlyVacancyKpi.setCaption("VACANCY");
        monthlyVacancyKpi.setFormula(monthlyVacancyFormula);
        monthlyVacancyKpi.setFormat(hoursFloatFormat);
        monthlyVacancyKpi = (CompassComplexKpi) initService.initCompassKpi(monthlyVacancyKpi, "star", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-COMPLEX-KPI"));

        CompassSimpleKpi weeklyDailyRatesKpi = new CompassSimpleKpi();
        weeklyDailyRatesKpi.setCaption("DAILY RATE");
        weeklyDailyRatesKpi.setField(sumCoalesceDailyRatesField);
        weeklyDailyRatesKpi = (CompassSimpleKpi) initService.initCompassKpi(weeklyDailyRatesKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi monthlyDailyRatesKpi = new CompassSimpleKpi();
        monthlyDailyRatesKpi.setCaption("DAILY RATES");
        monthlyDailyRatesKpi.setField(sumCoalesceMonthlyDailyRatesField);
        monthlyDailyRatesKpi = (CompassSimpleKpi) initService.initCompassKpi(monthlyDailyRatesKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi projectSalesKpi = new CompassSimpleKpi();
        projectSalesKpi.setCaption("PROJECT SALES");
        projectSalesKpi.setField(sumCoalesceMonthlyProjectSalesZeroField);
        projectSalesKpi = (CompassSimpleKpi) initService.initCompassKpi(projectSalesKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassComplexKpiFormula totalSalesFormula = new CompassComplexKpiFormula();
        totalSalesFormula.setExpression(Arrays.asList("kpi_" + monthlyDailyRatesKpi.getId(), " + ", "kpi_" + projectSalesKpi.getId()));
        Set<CompassKpi> totalSalesVariables = new HashSet<>();
        totalSalesVariables.add(monthlyDailyRatesKpi);
        totalSalesVariables.add(projectSalesKpi);
        totalSalesFormula.setVariables(totalSalesVariables);

        CompassComplexKpi totalSalesKpi = new CompassComplexKpi();
        totalSalesKpi.setCaption("TOTAL SALES");
        totalSalesKpi.setFormula(totalSalesFormula);
        totalSalesKpi = (CompassComplexKpi) initService.initCompassKpi(totalSalesKpi, "star", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-COMPLEX-KPI"));

        CompassSimpleKpi employeeLocationKpi = new CompassSimpleKpi();
        employeeLocationKpi.setCaption("LOCATION");
        employeeLocationKpi.setField(assignedEmployeeLocationField);
        employeeLocationKpi.setFormat(stringFormat);
        employeeLocationKpi = (CompassSimpleKpi) initService.initCompassKpi(employeeLocationKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi teamLeaderShorthandKpi = new CompassSimpleKpi();
        teamLeaderShorthandKpi.setCaption("TEAM");
        teamLeaderShorthandKpi.setField(employeeTeamLeaderShorthandField);
        teamLeaderShorthandKpi.setFormat(stringFormat);
        teamLeaderShorthandKpi = (CompassSimpleKpi) initService.initCompassKpi(teamLeaderShorthandKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi assignedEmployeeTeamLeaderShorthandKpi = new CompassSimpleKpi();
        assignedEmployeeTeamLeaderShorthandKpi.setCaption("TEAM");
        assignedEmployeeTeamLeaderShorthandKpi.setField(assignedEmployeeTeamLeaderShorthandField);
        assignedEmployeeTeamLeaderShorthandKpi.setFormat(stringFormat);
        assignedEmployeeTeamLeaderShorthandKpi = (CompassSimpleKpi) initService.initCompassKpi(assignedEmployeeTeamLeaderShorthandKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi employeeContractTypeKpi = new CompassSimpleKpi();
        employeeContractTypeKpi.setCaption("CONTRACT TYPE");
        employeeContractTypeKpi.setField(assignedEmployeeContractTypeField);
        employeeContractTypeKpi.setFormat(stringFormat);
        employeeContractTypeKpi = (CompassSimpleKpi) initService.initCompassKpi(employeeContractTypeKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi employeeWeeklyHoursKpi = new CompassSimpleKpi();
        employeeWeeklyHoursKpi.setCaption("WEEKLY HOURS");
        employeeWeeklyHoursKpi.setField(assignedEmployeeWeeklyHoursField);
        employeeWeeklyHoursKpi = (CompassSimpleKpi) initService.initCompassKpi(employeeWeeklyHoursKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi invoiceRecipientNameKpi = new CompassSimpleKpi();
        invoiceRecipientNameKpi.setCaption("CUSTOMER");
        invoiceRecipientNameKpi.setField(invoiceRecipientNameField);
        invoiceRecipientNameKpi.setFormat(stringFormat);
        invoiceRecipientNameKpi = (CompassSimpleKpi) initService.initCompassKpi(invoiceRecipientNameKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        CompassSimpleKpi invoiceRecipientDivisionKpi = new CompassSimpleKpi();
        invoiceRecipientDivisionKpi.setCaption("CUSTOMER DIVISION");
        invoiceRecipientDivisionKpi.setField(invoiceRecipientDivisionField);
        invoiceRecipientDivisionKpi.setFormat(stringFormat);
        invoiceRecipientDivisionKpi = (CompassSimpleKpi) initService.initCompassKpi(invoiceRecipientDivisionKpi, "speed", null, new Metadata(MetadataDefinition.OBJECT_TYPE, "COMPASS-SIMPLE-KPI"));

        // NEWS
        Category cat_dataUpdateNews = initService.initCategory("Data updates", null, "storage",
                new Metadata(MetadataDefinition.READ_PUBLIC, true),
                new Metadata(MetadataDefinition.OBJECT_TYPE, "NEWS"));
        Category cat_maintenanceNews = initService.initCategory("Maintenance", null, "build",
                new Metadata(MetadataDefinition.READ_PUBLIC, true),
                new Metadata(MetadataDefinition.OBJECT_TYPE, "NEWS"));
        Category catFilterHierarchies = initService.initCategory("Filter-Hierarchie", null, "account_tree",
                new Metadata(MetadataDefinition.READ_PUBLIC, true));

        //notificationComponent
        ComponentInstance notification = initService.initNotificationComponent("notification", 5000, null);

        //HEADER IN WORKFLOWS
        ComponentInstance applicationLogo = initService.initImageState("Application Logo", "Application Logo", "v7-application-logo");
        ComponentInstance flexFillLabel = initService.initLabel("", null, "v7-flexfill-label v7-font-p-strong order-3", "");
        ComponentInstance dataholdLabel = initService.initLabel("Datahold:", null, "v7-datahold-label v7-font-p-strong order-4", "");
        ComponentInstance dataholdValue = initService.initLabel(defaultDateFormat.toString(LocalDate.now()), null, "v7-datahold-value v7-font-p order-5", "");
        ComponentInstance userinformation = initService.initUserInformation("User-Info", "order-5");
        ComponentInstance header = initService.panel().initFlowPanel("Header", "v7-header", false, true, false, applicationLogo, flexFillLabel, dataholdLabel, dataholdValue, userinformation, notification);
        List<ComponentInstance> headerComponents = Arrays.asList(applicationLogo, flexFillLabel, dataholdLabel, dataholdValue, userinformation, notification);

        //STARTPAGE CONTENT
        ComponentInstance newsContainer = initService.initNewsContainer("News", "Some description");
        ComponentInstance workflowSelect = initService.initWorkflowSelect("Workflows", WorkflowSelectState.DIRECTION.HORIZONTAL);
        ComponentInstance startPageBody = initService.panel().initLeftRightPanel("Startseite Body", "v7-startpage flex-nowrap", newsContainer, workflowSelect);
        ComponentInstance startPageContainer = initService.panel().initFlowPanel("Startseite Body Container", "hidden-container", false, true, false, startPageBody);

        //STARTPAGE HEADER
        ComponentInstance startPage = initService.panel().initTopBottomPanel("Startseite", "h-100", header, startPageContainer);
        ComponentInstance mainPage = initService.panel().initCenterPanel("Hauptseite", "h-100", startPage);
        ComponentInstance overlayPanel = initService.panel().initOverlayPanel("Layerpanel", "h-100", mainPage, null);

        //FILTER COMPONENTS
        ComponentInstance projectKeyCheckboxTree = initService.initDBCheckBoxTree("Filter project key", catFilterHierarchies, projectHierarchy, true, false, true, null);
        ComponentInstance customerCheckboxTree = initService.initDBCheckBoxTree("Filter customer", catFilterHierarchies, invoiceRecipientHierarchy, true, false, true, null);

        //NEWS
        initPageService.attachNewsFormToNewsContainer(overlayPanel, newsContainer, cat_maintenanceNews.getId());
        initPageService.attachNewsEntryPanelToNewsContainer(overlayPanel, newsContainer);

        CustomerHierarchyDefinition customerHierarchyDefinition = initAssignmentHelperService.initCustomerHierarchy("Customer Hierarchy", null);
        EmployeeHierarchyDefinition employeeHierarchyDefinition = initAssignmentHelperService.initEmployeeHierarchy("Employee Hierarchy", null);
        MetadataContainer teamLeaderFilter = initService.initMetadataContainer(new Metadata("IS_TEAM_LEADER", true));
        EmployeeHierarchyDefinition teamLeaderHierarchyDefinition = initAssignmentHelperService.initEmployeeHierarchy("Team Leader Hierarchy", teamLeaderFilter);
        ProjectHierarchyDefinition projectHierarchyDefinition = initAssignmentHelperService.initProjectHierarchy("Project Hierarchy", null);

        WorkflowGraphState employeeWorkflow = initAssignmentWorkflowsService.createEmployeeWorkflow(
                startPage.getId(),
                mainPage.getId(),
                headerComponents,
                overlayPanel,
                countEmployees,
                employeeIdField,
                employeeLastNameField,
                Arrays.asList(
                        employeeStaffNumberField,
                        employeeFirstNameField,
                        employeeLastNameField,
                        employeeShorthandField,
                        employeeLocationField,
                        employeeEmailField,
                        employeeTeamLeaderShorthandField,
                        employeeContractTypeField,
                        employeeWeeklyHoursField,
                        employeeRetirementDateField,
                        employeeRetirementReasonField
                ),
                Arrays.asList(employeeIdField),

                countAbsences,
                absenceIdField,
                absenceStartDateField,
                Arrays.asList(
                        absenceStartDateField,
                        absenceEndDateField,
                        employeeCaptionField,
                        absenceReasonField),
                Arrays.asList(absenceIdField),

                countAssignments,
                assignmentIdField,
                assignmentStartDateField,
                Arrays.asList(
                        assignmentStartDateField,
                        assignmentEndDateField,
                        employeeCaptionField,
                        projectKeyField,
                        assignmentWeeklyHoursField,
                        assignmentMondayHoursField,
                        assignmentTuesdayHoursField,
                        assignmentWednesdayHoursField,
                        assignmentThursdayHoursField,
                        assignmentFridayHoursField),
                Arrays.asList(assignmentIdField),
                employeeHierarchyDefinition,
                projectHierarchyDefinition,
                teamLeaderHierarchyDefinition,
                assignmentStartDateField,
                absenceStartDateField);

        WorkflowGraphState customerWorkflow = initAssignmentWorkflowsService.createCustomerWorkflow(startPage.getId(),
                mainPage.getId(),
                headerComponents,
                overlayPanel,
                countCustomers,
                customerIdField,
                Arrays.asList(
                        customerNameField,
                        customerDivisionField
                ),
                Arrays.asList(customerIdField));

        WorkflowGraphState projectWorkflow = initAssignmentWorkflowsService.createProjectWorkflow(startPage.getId(),
                mainPage.getId(),
                headerComponents,
                overlayPanel,
                countProjects,
                projectIdField,
                Arrays.asList(
                        invoiceRecipientDivisionField,
                        projectKeyField,
                        benefitRecipientDivisionField,
                        projectNameField,
                        projectProjectTypeField,
                        projectStartDateField,
                        projectEndDateField,
                        employeeCaptionAsProjectLeaderField,
                        projectContractTypeField,
                        projectSalesField,
                        projectNotesField
                ),
                Arrays.asList(projectIdField),
                employeeHierarchyDefinition,
                customerHierarchyDefinition
        );

        WorkflowGraphState staffWorkflowReportWorkflowGraph = initAssignmentWorkflowsService.createCompassReportViewWorkflow(startPage.getId(),
                mainPage.getId(),
                headerComponents,
                initAssignmentReportsService.createStaffWorkloadReport(quotaKpi, actualHoursKpi, employeeVacancyKpi,
                        employeeHierarchy, employeeHierarchyLevelDefinition,
                        assignmentHierarchy, assignmentHierarchyLevelDefinition, hoursFloatFormat));

        WorkflowGraphState teamWorkflowReportWorkflowGraph = initAssignmentWorkflowsService.createCompassReportViewWorkflow(startPage.getId(),
                mainPage.getId(),
                headerComponents,
                initAssignmentReportsService.createTeamWorkloadReport(quotaKpi, actualHoursKpi, employeeVacancyKpi,
                        employeeHierarchy, employeeHierarchyLevelDefinition,
                        teamLeaderHierarchy, teamLeaderHierarchyLevelDefinition,
                        assignmentHierarchy, assignmentHierarchyLevelDefinition, hoursFloatFormat));

        WorkflowGraphState locationWorkflowReportWorkflowGraph = initAssignmentWorkflowsService.createCompassReportViewWorkflow(startPage.getId(),
                mainPage.getId(),
                headerComponents,
                initAssignmentReportsService.createLocationWorkloadReport(quotaKpi, actualHoursKpi, employeeVacancyKpi,
                        employeeHierarchy, employeeHierarchyLevelDefinition,
                        teamLeaderHierarchy, teamLeaderHierarchyLevelDefinition,
                        locationHierarchy, locationHierarchyLevelDefinition,
                        assignmentHierarchy, assignmentHierarchyLevelDefinition, hoursFloatFormat));

        WorkflowGraphState customerWorkflowReportWorkflowGraph = initAssignmentWorkflowsService.createCompassReportViewWorkflow(startPage.getId(),
                mainPage.getId(),
                headerComponents,
                initAssignmentReportsService.createCustomerOverviewReport(
                        monthlyManDaysKpi, monthlyDailyRatesKpi, projectSalesKpi, totalSalesKpi,
                        invoiceRecipientHierarchy, invoiceRecipientHierarchyLevelDefinition, invoiceRecipientDivisionHierarchyLevelDefinition,
                        projectHierarchy, projectHierarchyLevelDefinition,
                        assignedEmployeeHierarchy, assignedEmployeeHierarchyLevelDefinition, hoursFloatFormat));

        WorkflowGraphState manDaysPerProjectWorkflowReportWorkflowGraph = initAssignmentWorkflowsService.createCompassReportViewWithFiltersWorkflow(startPage.getId(),
                mainPage.getId(),
                headerComponents,
                Arrays.asList(projectKeyCheckboxTree, customerCheckboxTree),
                initAssignmentReportsService.createManDaysPerEmployeeReport(
                        monthlyManDaysKpi, employeeLocationKpi, teamLeaderShorthandKpi, employeeContractTypeKpi, employeeWeeklyHoursKpi,
                        invoiceRecipientNameKpi, invoiceRecipientDivisionKpi,
                        projectHierarchy, projectHierarchyLevelDefinition,
                        assignedEmployeeHierarchy, assignedEmployeeHierarchyLevelDefinition, hoursFloatFormat));

        WorkflowGraphState assignmentsPerEmployeeWorkflowReportWorkflowGraph = initAssignmentWorkflowsService.createCompassReportViewWithFiltersWorkflow(startPage.getId(),
                mainPage.getId(),
                headerComponents,
                Arrays.asList(projectKeyCheckboxTree, customerCheckboxTree),
                initAssignmentReportsService.createAssignmentsPerEmployeeReport(
                        monthlyManDaysInternKpi, monthlyManDaysExternKpi, monthlyManDaysOfferingKpi, monthlyVacancyKpi,
                        employeeLocationKpi, assignedEmployeeTeamLeaderShorthandKpi, employeeContractTypeKpi, employeeWeeklyHoursKpi,
                        invoiceRecipientNameKpi, invoiceRecipientDivisionKpi,
                        projectHierarchy, projectHierarchyLevelDefinition,
                        assignedEmployeeHierarchy, assignedEmployeeHierarchyLevelDefinition, hoursFloatFormat));

        PrivilegeSet userRole = initService.initRoleWithoutPrivileges("ROLE_USER",
                compService.getPrivilege(OperationType.Delete),
                compInstService.getPrivilege(OperationType.Create),
                compInstService.getPrivilege(OperationType.Update),
                compInstService.getPrivilege(OperationType.Delete));

        InitState userInitState = initService.initInitState(
                overlayPanel,
                Arrays.asList(
                        employeeWorkflow.getId(),
                        customerWorkflow.getId(),
                        projectWorkflow.getId(),
                        staffWorkflowReportWorkflowGraph.getId(),
                        teamWorkflowReportWorkflowGraph.getId(),
                        locationWorkflowReportWorkflowGraph.getId(),
                        customerWorkflowReportWorkflowGraph.getId(),
                        manDaysPerProjectWorkflowReportWorkflowGraph.getId(),
                        assignmentsPerEmployeeWorkflowReportWorkflowGraph.getId()
                ),
                "User",
                "USER",
                userRole);

        if (this.activeProfile != null) {
            if (!this.activeProfile.contains("k8s")) {
                //initAssignmentHelperService.createObjectsForDebugging();
            }
        }

        initService.initUser("zakowski", "ob6o7r6w", "Laura", "Zakowski", null, true, userInitState);
        initService.initUser("wulfert", "3hebjybk", "Irina", "Wulfert", null, true, userInitState);
        log.info("DatabaseInitializer done after " + (System.currentTimeMillis() - ts) / 1000 + " seconds");
    }

    public PostgreSQLDataSource initPostgreSQLDataSource(String url, String user, String password) {
        PostgreSQLDataSource db = new PostgreSQLDataSource(url, user, password);
        db.setCaption("PostgreSQL: " + user + "@" + url);
        db.setMetadataContainer(initService.addDefaultMetadata(db.getMetadataContainer()));
        return (PostgreSQLDataSource) dbService.save(db);
    }
}
