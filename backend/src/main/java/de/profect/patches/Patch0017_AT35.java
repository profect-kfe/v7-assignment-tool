package de.profect.patches;

import de.profect.component.ComponentInstance;
import de.profect.component.ComponentInstanceRepository;
import de.profect.component.ComponentStateRepository;
import de.profect.component.folderpanel.selectionelements.checkboxtree.CheckBoxTreeState;
import de.profect.component.panel.flow.FlowPanelState;
import de.profect.connector.ConnectorState;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Log4j2
public class Patch0017_AT35 extends V7Patch {

    @Autowired
    InitHelperService initService;

    @Autowired
    ComponentStateRepository componentStateRepository;

    @Autowired
    ComponentInstanceRepository componentInstanceRepository;

    @Autowired
    PatchHelperService patchHelper;

    @Override
    public String getPatchVersion() {
        return "0017";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-35 Aufklappen der project keys'");

        CheckBoxTreeState customerFilter = (CheckBoxTreeState) patchHelper.initializeAndUnproxy(componentStateRepository.findById(16).get());
        CheckBoxTreeState projectKeyFilter = (CheckBoxTreeState) patchHelper.initializeAndUnproxy(componentStateRepository.findById(15).get());

        ComponentInstance compassComponentInstance = patchHelper.initializeAndUnproxy(componentInstanceRepository.findById(290).get());
        ComponentInstance resetButton = patchHelper.initializeAndUnproxy(componentInstanceRepository.findById(293).get());
        FlowPanelState flowPanel = (FlowPanelState) patchHelper.initializeAndUnproxy(componentStateRepository.findById(291).get());

        ComponentInstance projectKeyCheckboxList = initService.initCheckBoxList("Filter project key", projectKeyFilter.getCategory(), projectKeyFilter.getHierarchyDatabaseDefinition().getHierarchyLevels().get(0).getIdentifierField(), projectKeyFilter.getHierarchyDatabaseDefinition().getHierarchyLevels().get(0).getLabelField(), true, null);
        initService.connector().initAddConditionConnector("Apply filter on compass", projectKeyCheckboxList, compassComponentInstance, ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initResetConnector("Reset " + projectKeyCheckboxList.getCaption(), resetButton, projectKeyCheckboxList, false, ConnectorState.TRIGGEREVENT_ACTION);
        ComponentInstance customerCheckboxTree = initService.initDBCheckBoxTree("Filter customer", customerFilter.getCategory(), customerFilter.getHierarchyDatabaseDefinition(), true, false, true, null);
        initService.connector().initAddConditionConnector("Apply filter on compass", customerCheckboxTree, compassComponentInstance, ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initResetConnector("Reset " + customerCheckboxTree.getCaption(), resetButton, customerCheckboxTree, false, ConnectorState.TRIGGEREVENT_ACTION);
        flowPanel.getPositions().put("0", customerCheckboxTree);
        flowPanel.getPositions().put("1", projectKeyCheckboxList);

        ComponentInstance compassComponentInstance2 = patchHelper.initializeAndUnproxy(componentInstanceRepository.findById(290).get());
        ComponentInstance resetButton2 = patchHelper.initializeAndUnproxy(componentInstanceRepository.findById(293).get());
        FlowPanelState flowPanel2 = (FlowPanelState) patchHelper.initializeAndUnproxy(componentStateRepository.findById(291).get());

        ComponentInstance projectKeyCheckboxList2 = initService.initCheckBoxList("Filter project key", projectKeyFilter.getCategory(), projectKeyFilter.getHierarchyDatabaseDefinition().getHierarchyLevels().get(0).getIdentifierField(), projectKeyFilter.getHierarchyDatabaseDefinition().getHierarchyLevels().get(0).getLabelField(), true, null);
        initService.connector().initAddConditionConnector("Apply filter on compass", projectKeyCheckboxList2, compassComponentInstance2, ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initResetConnector("Reset " + projectKeyCheckboxList2.getCaption(), resetButton2, projectKeyCheckboxList2, false, ConnectorState.TRIGGEREVENT_ACTION);
        ComponentInstance customerCheckboxTree2 = initService.initDBCheckBoxTree("Filter customer", customerFilter.getCategory(), customerFilter.getHierarchyDatabaseDefinition(), true, false, true, null);
        initService.connector().initAddConditionConnector("Apply filter on compass", customerCheckboxTree2, compassComponentInstance2, ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initResetConnector("Reset " + customerCheckboxTree2.getCaption(), resetButton2, customerCheckboxTree2, false, ConnectorState.TRIGGEREVENT_ACTION);
        flowPanel2.getPositions().put("0", customerCheckboxTree2);
        flowPanel2.getPositions().put("1", projectKeyCheckboxList2);
    }
}