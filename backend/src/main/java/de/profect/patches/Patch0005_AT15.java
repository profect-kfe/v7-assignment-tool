package de.profect.patches;

import de.profect.component.ComponentInstance;
import de.profect.component.ComponentInstanceRepository;
import de.profect.component.ComponentStateRepository;
import de.profect.component.folderpanel.selectionelements.checkboxtree.CheckBoxTreeState;
import de.profect.component.panel.flow.FlowPanelState;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Log4j2
public class Patch0005_AT15 extends V7Patch {

    @Autowired
    PatchHelperService patchHelper;

    @Autowired
    ComponentStateRepository componentStateRepository;

    @Autowired
    ComponentInstanceRepository componentInstanceRepository;

    @Autowired
    InitHelperService initService;

    @Override
    public String getPatchVersion() {
        return "0005";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-15 Filter \"customer\" doesnt set back after changing the workflow'");
        FlowPanelState flowPanel = (FlowPanelState) patchHelper.initializeAndUnproxy(componentStateRepository.findById(301).get());
        CheckBoxTreeState customerFilter = (CheckBoxTreeState) patchHelper.initializeAndUnproxy(componentStateRepository.findById(16).get());
        CheckBoxTreeState projetKeyFilter = (CheckBoxTreeState) patchHelper.initializeAndUnproxy(componentStateRepository.findById(15).get());
        ComponentInstance projectKeyCheckboxTree = initService.initDBCheckBoxTree("Filter project key", projetKeyFilter.getCategory(), projetKeyFilter.getHierarchyDatabaseDefinition(), true, false, true, null);
        ComponentInstance customerCheckboxTree = initService.initDBCheckBoxTree("Filter customer", customerFilter.getCategory(), customerFilter.getHierarchyDatabaseDefinition(), true, false, true, null);
        flowPanel.getPositions().put("0", customerCheckboxTree);
        flowPanel.getPositions().put("1", projectKeyCheckboxTree);
    }

}