package de.profect.patches;


import de.profect.Category;
import de.profect.CategoryRepository;
import de.profect.InitState;
import de.profect.InitStateRepository;
import de.profect.authentication.privilege.PrivilegeService;
import de.profect.authentication.privilege.PrivilegeSet;
import de.profect.authentication.secured.OperationType;
import de.profect.component.ComponentInstance;
import de.profect.component.ComponentInstanceRepository;
import de.profect.component.ComponentState;
import de.profect.component.ComponentStateRepository;
import de.profect.component.dataview.compass.kpi.CompassKpi;
import de.profect.component.dataview.compass.kpi.CompassKpiRepository;
import de.profect.component.dataview.compass.report2.CompassReport2Service;
import de.profect.component.input.workflow.WorkflowGraphState;
import de.profect.component.input.workflow.WorkflowSelectState;
import de.profect.component.panel.leftright.LeftRightPanelState;
import de.profect.data.datasource.sql.SQLDataModel;
import de.profect.data.datasource.sql.SQLDataModelRepository;
import de.profect.data.datasource.sql.SQLTable;
import de.profect.data.datasource.sql.SQLTableRepository;
import de.profect.data.hierarchy.HierarchyDefinitionRepository;
import de.profect.data.hierarchy.databasehierarchy.HierarchyDatabaseDefinition;
import de.profect.data.hierarchy.databasehierarchy.HierarchyLevelDefinition;
import de.profect.data.hierarchy.databasehierarchy.HierarchyLevelDefinitionRepository;
import de.profect.data.query.field.Field;
import de.profect.data.query.field.FieldRepository;
import de.profect.data.query.field.OrderState;
import de.profect.data.query.field.dataformat.DataFormat;
import de.profect.data.query.field.dataformat.DataFormatRepository;
import de.profect.data.query.field.kpi.ConcatField;
import de.profect.data.query.field.kpi.UpperField;
import de.profect.inithelper.InitAssignmentReports2Service;
import de.profect.inithelper.InitAssignmentWorkflowsService;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
@Log4j2
public class Patch0020_UpgradeCoreVersion extends V7Patch {

    @Autowired
    ComponentStateRepository componentStateRepository;

    @Autowired
    private InitAssignmentReports2Service initAssignmentReportsService;

    @Autowired
    private InitAssignmentWorkflowsService initAssignmentWorkflowsService;

    @Autowired
    private HierarchyDefinitionRepository hierarchyDefinitionRepo;

    @Autowired
    private HierarchyLevelDefinitionRepository hierarchyLevelDefinitionRepo;

    @Autowired
    private DataFormatRepository dataFormatRepo;

    @Autowired
    private CompassKpiRepository compassKpiRepository;

    @Autowired
    private ComponentInstanceRepository componentInstanceRepository;

    @Autowired
    private InitStateRepository initStateRepository;

    @Autowired
    private InitHelperService initService;

    @Autowired
    private PatchHelperService patchHelper;

    @Autowired
    private CompassReport2Service report2Service;

    @Autowired
    private PrivilegeService privilegeService;

    @Autowired
    FieldRepository fieldRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    private SQLDataModelRepository modelRepo;

    @Autowired
    private SQLTableRepository tableRepo;

    @Override
    public String getPatchVersion() {
        return "0020";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch to upgrade compass reports to new core version");

        CompassKpi employeeQuotaKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(1).get());
        CompassKpi employeeActualKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(2).get());
        CompassKpi employeeVacancyKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(3).get());
        CompassKpi monthlyManDaysKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(5).get());
        CompassKpi monthlyDailyRatesKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(11).get());
        CompassKpi projectSalesKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(13).get());
        CompassKpi totalSalesKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(14).get());

        CompassKpi employeeLocationKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(15).get());
        CompassKpi teamLeaderShorthandKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(16).get());
        CompassKpi employeeContractTypeKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(18).get());
        CompassKpi employeeWeeklyHoursKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(19).get());
        CompassKpi invoiceRecipientNameKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(20).get());
        CompassKpi invoiceRecipientDivisionKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(21).get());

        CompassKpi monthlyManDaysInternKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(6).get());
        CompassKpi monthlyManDaysExternKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(7).get());
        CompassKpi monthlyManDaysOfferingKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(8).get());
        CompassKpi monthlyVacancyKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(10).get());
        CompassKpi assignedEmployeeTeamLeaderShorthandKpi = PatchHelperService.initializeAndUnproxy(compassKpiRepository.findById(17).get());

        HierarchyDatabaseDefinition employeeHierarchy = (HierarchyDatabaseDefinition) PatchHelperService.initializeAndUnproxy(hierarchyDefinitionRepo.findById(2).get());
        HierarchyLevelDefinition employeeLevelDefinition = PatchHelperService.initializeAndUnproxy(hierarchyLevelDefinitionRepo.findById(1).get());

        HierarchyDatabaseDefinition teamLeaderHierarchy = (HierarchyDatabaseDefinition) PatchHelperService.initializeAndUnproxy(hierarchyDefinitionRepo.findById(3).get());
        HierarchyLevelDefinition teamLeaderHierarchyLevelDefinition = PatchHelperService.initializeAndUnproxy(hierarchyLevelDefinitionRepo.findById(2).get());

        HierarchyDatabaseDefinition assignedEmployeeHierarchy = (HierarchyDatabaseDefinition) PatchHelperService.initializeAndUnproxy(hierarchyDefinitionRepo.findById(4).get());
        HierarchyLevelDefinition assignedEmployeeHierarchyLevelDefinition = PatchHelperService.initializeAndUnproxy(hierarchyLevelDefinitionRepo.findById(3).get());

        HierarchyDatabaseDefinition locationHierarchy = (HierarchyDatabaseDefinition) PatchHelperService.initializeAndUnproxy(hierarchyDefinitionRepo.findById(5).get());
        HierarchyLevelDefinition locationHierarchyLevelDefinition = PatchHelperService.initializeAndUnproxy(hierarchyLevelDefinitionRepo.findById(4).get());

        HierarchyDatabaseDefinition projectHierarchy = (HierarchyDatabaseDefinition) PatchHelperService.initializeAndUnproxy(hierarchyDefinitionRepo.findById(6).get());
        HierarchyLevelDefinition projectHierarchyLevelDefinition = PatchHelperService.initializeAndUnproxy(hierarchyLevelDefinitionRepo.findById(5).get());

        HierarchyDatabaseDefinition assignmentHierarchy = (HierarchyDatabaseDefinition) PatchHelperService.initializeAndUnproxy(hierarchyDefinitionRepo.findById(7).get());
        HierarchyLevelDefinition assignmentHierarchyLevelDefinition = PatchHelperService.initializeAndUnproxy(hierarchyLevelDefinitionRepo.findById(6).get());

        HierarchyDatabaseDefinition invoiceRecipientHierarchy = (HierarchyDatabaseDefinition) PatchHelperService.initializeAndUnproxy(hierarchyDefinitionRepo.findById(8).get());
        HierarchyLevelDefinition invoiceRecipientHierarchyLevelDefinition = PatchHelperService.initializeAndUnproxy(hierarchyLevelDefinitionRepo.findById(7).get());
        HierarchyLevelDefinition invoiceRecipientDivisionHierarchyLevelDefinition = PatchHelperService.initializeAndUnproxy(hierarchyLevelDefinitionRepo.findById(8).get());

        DataFormat<Number> hoursFloatFormat = PatchHelperService.initializeAndUnproxy(dataFormatRepo.findById(8).get());
        DataFormat stringFormat = PatchHelperService.initializeAndUnproxy(dataFormatRepo.findById(1).get());

        ComponentInstance staffWorkloadReport = initAssignmentReportsService.createStaffWorkloadReport(employeeQuotaKpi, employeeActualKpi, employeeVacancyKpi,
                employeeHierarchy, employeeLevelDefinition,
                assignmentHierarchy, assignmentHierarchyLevelDefinition, hoursFloatFormat);

        ComponentInstance teamWorkloadReport = initAssignmentReportsService.createTeamWorkloadReport(employeeQuotaKpi, employeeActualKpi, employeeVacancyKpi,
                employeeHierarchy, employeeLevelDefinition,
                teamLeaderHierarchy, teamLeaderHierarchyLevelDefinition,
                assignmentHierarchy, assignmentHierarchyLevelDefinition, hoursFloatFormat);

        ComponentInstance locationWorkloadReport = initAssignmentReportsService.createLocationWorkloadReport(employeeQuotaKpi, employeeActualKpi, employeeVacancyKpi,
                employeeHierarchy, employeeLevelDefinition,
                teamLeaderHierarchy, teamLeaderHierarchyLevelDefinition,
                locationHierarchy, locationHierarchyLevelDefinition,
                assignmentHierarchy, assignmentHierarchyLevelDefinition, hoursFloatFormat);

        ComponentInstance customerOverviewReport = initAssignmentReportsService.createCustomerOverviewReport(
                monthlyManDaysKpi, monthlyDailyRatesKpi, projectSalesKpi, totalSalesKpi,
                invoiceRecipientHierarchy, invoiceRecipientHierarchyLevelDefinition, invoiceRecipientDivisionHierarchyLevelDefinition,
                projectHierarchy, projectHierarchyLevelDefinition,
                assignedEmployeeHierarchy, assignedEmployeeHierarchyLevelDefinition, hoursFloatFormat);

        ComponentInstance manDaysPerEmployeeReport = initAssignmentReportsService.createManDaysPerEmployeeReport(
                monthlyManDaysKpi, employeeLocationKpi, teamLeaderShorthandKpi, employeeContractTypeKpi, employeeWeeklyHoursKpi,
                invoiceRecipientNameKpi, invoiceRecipientDivisionKpi,
                projectHierarchy, projectHierarchyLevelDefinition,
                assignedEmployeeHierarchy, assignedEmployeeHierarchyLevelDefinition, hoursFloatFormat);

        ComponentInstance assignmentsPerEmployeeReport = initAssignmentReportsService.createAssignmentsPerEmployeeReport(
                monthlyManDaysInternKpi, monthlyManDaysExternKpi, monthlyManDaysOfferingKpi, monthlyVacancyKpi,
                employeeLocationKpi, assignedEmployeeTeamLeaderShorthandKpi, employeeContractTypeKpi, employeeWeeklyHoursKpi,
                invoiceRecipientNameKpi, invoiceRecipientDivisionKpi,
                projectHierarchy, projectHierarchyLevelDefinition,
                assignedEmployeeHierarchy, assignedEmployeeHierarchyLevelDefinition, hoursFloatFormat);

        ComponentInstance applicationLogo = componentInstanceRepository.findById(2).get();
        ComponentInstance flexFillLabel = componentInstanceRepository.findById(3).get();
        ComponentInstance dataholdLabel = componentInstanceRepository.findById(4).get();
        ComponentInstance dataholdValue = componentInstanceRepository.findById(5).get();
        ComponentInstance userinformation = componentInstanceRepository.findById(6).get();
        ComponentInstance notification = componentInstanceRepository.findById(1).get();

        Field projectIdField = fieldRepository.findById(50).get();
        Field projectKeyField = fieldRepository.findById(52).get();
        Field projectNameField = fieldRepository.findById(53).get();

        ConcatField concatProjectNameAndKeyField = initService.field().initConcat("Project Name (Project Key) Field", "", stringFormat, projectNameField, initService.field().initStringConstant(" (", " (", stringFormat, ""), projectKeyField, initService.field().initStringConstant(")", ")", stringFormat, ""));
        UpperField orderByUpperCaseField = initService.field().initUpper("Upper case", "", stringFormat, concatProjectNameAndKeyField);
        HierarchyLevelDefinition projectHierarchy2LevelDefinition = initService.hierarchy().initHierarchyLevelDefinition(projectIdField, concatProjectNameAndKeyField);
        projectHierarchy2LevelDefinition.setOrderStates(Arrays.asList(new OrderState(orderByUpperCaseField, true)));
        HierarchyDatabaseDefinition projectHierarchy2 = initService.hierarchy().initHierarchyDatabaseDefinition("Project Hierarchy 2", projectHierarchy2LevelDefinition);

        Category catFilterHierarchies = categoryRepository.findById(4).get();

        ComponentInstance projectKeyCheckboxTree = initService.initDBCheckBoxTree("Filter project", catFilterHierarchies, projectHierarchy2, true, false, true, null);
        ComponentInstance customerCheckboxTree = componentInstanceRepository.findById(351).get();

        List<ComponentInstance> headerComponents = Arrays.asList(applicationLogo, flexFillLabel, dataholdLabel, dataholdValue, userinformation, notification);

        WorkflowGraphState staffWorkloadWorkflow = initAssignmentWorkflowsService.createCompassReport2ViewWorkflow(12, 13, headerComponents, staffWorkloadReport);
        WorkflowGraphState teamWorkloadWorkflow = initAssignmentWorkflowsService.createCompassReport2ViewWorkflow(12, 13, headerComponents, teamWorkloadReport);
        WorkflowGraphState locationWorkloadWorkflow = initAssignmentWorkflowsService.createCompassReport2ViewWorkflow(12, 13, headerComponents, locationWorkloadReport);
        WorkflowGraphState customerOverviewWorkflow = initAssignmentWorkflowsService.createCompassReport2ViewWorkflow(12, 13, headerComponents, customerOverviewReport);
        WorkflowGraphState manDaysPerEmployeeWorkflow = initAssignmentWorkflowsService.createCompassReport2ViewWithFiltersWorkflow(12, 13, headerComponents, Arrays.asList(projectKeyCheckboxTree, customerCheckboxTree),manDaysPerEmployeeReport);
        WorkflowGraphState assignmentsPerEmployeeWorkflow = initAssignmentWorkflowsService.createCompassReport2ViewWithFiltersWorkflow(12, 13, headerComponents, Arrays.asList(projectKeyCheckboxTree, customerCheckboxTree),assignmentsPerEmployeeReport);

        Optional<InitState> optionalInitState = initStateRepository.findById(1);
        if(optionalInitState.isPresent()) {
            InitState initState = PatchHelperService.initializeAndUnproxy(optionalInitState.get());
            initState.getWorkflowIds().clear();
            initState.getWorkflowIds().add(39);
            initState.getWorkflowIds().add(179);
            initState.getWorkflowIds().add(212);
            initState.getWorkflowIds().add(staffWorkloadWorkflow.getId());
            initState.getWorkflowIds().add(teamWorkloadWorkflow.getId());
            initState.getWorkflowIds().add(locationWorkloadWorkflow.getId());
            initState.getWorkflowIds().add(customerOverviewWorkflow.getId());
            initState.getWorkflowIds().add(manDaysPerEmployeeWorkflow.getId());
            initState.getWorkflowIds().add(assignmentsPerEmployeeWorkflow.getId());
        }

        List<ComponentState> componentStates = componentStateRepository.findAll();
        for (ComponentState componentState : componentStates) {
            if (patchHelper.isOfClass(componentState, LeftRightPanelState.class) && componentState.getCaption().equals("Startseite Body")) {
                LeftRightPanelState startPage = (LeftRightPanelState) patchHelper.initializeAndUnproxy(componentState);
                ComponentInstance databaseWorkflowSelect = initService.initWorkflowSelect("Create Dataset", WorkflowSelectState.DIRECTION.VERTICAL);
                ((WorkflowSelectState) databaseWorkflowSelect.getDefaultState()).setWorkflowIds(Arrays.asList(39, 179, 212));
                ComponentInstance reportWorkflowSelect = initService.initWorkflowSelect("Create Report", WorkflowSelectState.DIRECTION.VERTICAL);
                ((WorkflowSelectState) reportWorkflowSelect.getDefaultState()).setWorkflowIds(Arrays.asList(staffWorkloadWorkflow.getId(), teamWorkloadWorkflow.getId(), locationWorkloadWorkflow.getId(), customerOverviewWorkflow.getId(), manDaysPerEmployeeWorkflow.getId(), assignmentsPerEmployeeWorkflow.getId()));
                ComponentInstance flowPanel = initService.initFlowPanel("Workflow Select Wrapper", "assignment-workflow-select-wrapper", false, true, true, (Category) null, databaseWorkflowSelect, reportWorkflowSelect);
                ComponentInstance oldRight = startPage.getPositions().get("MAIN");
                startPage.getPositions().put("MAIN", flowPanel);
                componentStateRepository.delete(oldRight.getDefaultState());
                componentInstanceRepository.delete(oldRight);
            }
        }

        List<InitState> initStates = initStateRepository.findAll();
        for (InitState initState : initStates) {
            Set<PrivilegeSet> roles = initState.getRoles();
            for (PrivilegeSet role : roles) {
                role.getPrivileges().add(privilegeService.createPrivilegeIfNotFound(report2Service.getPrivilege(OperationType.Create)));
                role.getPrivileges().add(privilegeService.createPrivilegeIfNotFound(report2Service.getPrivilege(OperationType.Delete)));
                role.getPrivileges().add(privilegeService.createPrivilegeIfNotFound(report2Service.getPrivilege(OperationType.Update)));
                role.getPrivileges().add(privilegeService.createPrivilegeIfNotFound(report2Service.getPrivilege(OperationType.Read)));
            }
        }

        SQLDataModel sqlDataModel = modelRepo.findById(1).get();

        SQLTable employeesTable = tableRepo.findById(6).get();
        SQLTable benefitRecipientTable = tableRepo.findById(4).get();
        SQLTable invoiceRecipientTable = tableRepo.findById(3).get();
        SQLTable projectsTable = tableRepo.findById(8).get();

        // add connections
        initService.addConnections(sqlDataModel, benefitRecipientTable, employeesTable, projectsTable);
        initService.addConnections(sqlDataModel, employeesTable, benefitRecipientTable, projectsTable);

        initService.addConnections(sqlDataModel, invoiceRecipientTable, employeesTable, projectsTable);
        initService.addConnections(sqlDataModel, employeesTable, invoiceRecipientTable, projectsTable);

        initService.addConnections(sqlDataModel, benefitRecipientTable, invoiceRecipientTable, projectsTable);
        initService.addConnections(sqlDataModel, invoiceRecipientTable, benefitRecipientTable, projectsTable);
    }
}