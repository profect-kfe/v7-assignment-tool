package de.profect.patches;

import de.profect.InitState;
import de.profect.InitStateRepository;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0013_AT27 extends V7Patch {

    @Autowired
    InitStateRepository initStateRepository;

    @Autowired
    InitHelperService initService;

    @Override
    public String getPatchVersion() {
        return "0013";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch to create new users'");
        List<InitState> initStates = initStateRepository.findAll();
        InitState defaultInitState = null;
        for (InitState initState : initStates) {
            if (initState.getCaption().equals("User")) {
                defaultInitState = initState;

            }
        }
        initService.initUser("fries", "emj96rgm", "Lukas", "Fries", null, true, defaultInitState);
        initService.initUser("meissner", "gdcpswy9", "Tobias", "Meißner", null, true, defaultInitState);
        initService.initUser("arndt", "evky7r6o", "Dirk", "Arndt", null, true, defaultInitState);
    }
}
