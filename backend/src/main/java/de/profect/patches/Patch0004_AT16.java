package de.profect.patches;

import de.profect.component.ComponentInstance;
import de.profect.component.ComponentInstanceRepository;
import de.profect.component.ComponentState;
import de.profect.component.ComponentStateRepository;
import de.profect.component.panel.flow.FlowPanelState;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0004_AT16 extends V7Patch {

    @Autowired
    ComponentStateRepository componentStateRepository;

    @Autowired
    ComponentInstanceRepository componentInstanceRepository;

    @Autowired
    InitHelperService initService;

    @Autowired
    PatchHelperService patchHelper;

    @Override
    public String getPatchVersion() {
        return "0004";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-16 Change the order of the filters \"Project key\" and \"customer\"'");
        List<ComponentState> flowPanelStates = patchHelper.getComponentStatesByClass(FlowPanelState.class);
        for (ComponentState _flowPanelState : flowPanelStates) {
            FlowPanelState flowPanelState = (FlowPanelState) _flowPanelState;
            if (flowPanelState.getCaption().equals("CompassFlowPanel")) {
                ComponentInstance projectKeyFilterComponent = flowPanelState.getComponent(0);
                ComponentInstance customerFilterComponent = flowPanelState.getComponent(1);
                if (!projectKeyFilterComponent.getDefaultState().getCaption().equals("Filter project key")
                    || !customerFilterComponent.getDefaultState().getCaption().equals("Filter customer")) {
                    log.debug("something went wrong here");
                    continue;
                }
                log.debug("changing customer/projectkey filter");
                flowPanelState.getPositions().put("0", customerFilterComponent);
                flowPanelState.getPositions().put("1", projectKeyFilterComponent);
            }
        }
    }
}