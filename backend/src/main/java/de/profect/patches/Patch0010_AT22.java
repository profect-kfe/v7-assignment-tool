package de.profect.patches;

import de.profect.assignmenttool.tableviewwithcustomcolumn.TableViewWithCustomColumnState;
import de.profect.component.ComponentState;
import de.profect.component.ComponentStateRepository;
import de.profect.component.dataview.tableview.TableViewState;
import de.profect.data.datasource.sql.field.SQLDataSourceField;
import de.profect.data.query.field.Field;
import de.profect.data.query.field.FieldRepository;
import de.profect.patches.helpers.V7Patch;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0010_AT22 extends V7Patch {

    @Autowired
    FieldRepository fieldRepository;

    @Autowired
    ComponentStateRepository componentStateRepository;

    @Autowired
    PatchHelperService patchHelper;

    @Override
    public String getPatchVersion() {
        return "0010";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-22 Display of the project name in the Employee/Assignment Workflow'");
        List<Field> fields = fieldRepository.findAll();
        Field projectNameField = null;
        for (Field field : fields) {
            if (((SQLDataSourceField) patchHelper.initializeAndUnproxy(field)).getColumn().equals("PROJECT_NAME")) {
                projectNameField = field;
                break;
            }
        }
        List<ComponentState> tableViewStates = patchHelper.getComponentStatesByClass(TableViewWithCustomColumnState.class);
        for (ComponentState _tableViewState : tableViewStates) {
            if (_tableViewState.getCaption().equals("Assignments")) {
                TableViewState tableViewState = (TableViewState) patchHelper.initializeAndUnproxy(_tableViewState);
                List<Field> selectedFields = tableViewState.getSelectedFields();
                selectedFields.add(3, projectNameField);
                break;
            }
        }
    }
}
