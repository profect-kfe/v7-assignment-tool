package de.profect.patches;

import de.profect.InitState;
import de.profect.InitStateRepository;
import de.profect.authentication.privilege.PrivilegeSet;
import de.profect.authentication.secured.OperationType;
import de.profect.authentication.user.UserInitState;
import de.profect.component.ComponentInstance;
import de.profect.component.ComponentInstanceRepository;
import de.profect.component.ComponentInstanceService;
import de.profect.component.ComponentStateService;
import de.profect.data.query.condition.CategorialCondition;
import de.profect.data.query.field.Field;
import de.profect.data.query.field.FieldRepository;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0021_AT42 extends V7Patch {

    @Autowired
    InitHelperService initService;

    @Autowired
    private ComponentInstanceService compInstService;

    @Autowired
    private ComponentStateService compService;

    @Autowired
    private ComponentInstanceRepository componentInstanceRepository;

    @Autowired
    FieldRepository fieldRepository;

    @Override
    public String getPatchVersion() {
        return "0021";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-42 Create new users'");

        ComponentInstance overlayPanel = componentInstanceRepository.findById(14).get();

        PrivilegeSet userRole = initService.initRoleWithoutPrivileges("ROLE_USER",
                compService.getPrivilege(OperationType.Delete),
                compInstService.getPrivilege(OperationType.Create),
                compInstService.getPrivilege(OperationType.Update),
                compInstService.getPrivilege(OperationType.Delete));

        InitState teamLeaderInitState = initService.initInitState(
                overlayPanel,
                Arrays.asList(
                        39,
                        366
                ),
                "Teamleader",
                "TEAMLEADER",
                userRole);

        Field teamleaderShorthandField = fieldRepository.findById(42).get();

        // BSO
        CategorialCondition bsoTeamleaderCondition = initService.condition().initCategorialCondition("bso employees", "only employees for bso", "", "", null, null, teamleaderShorthandField, null, "BSO");
        initService.initUser("soell", "NfvYk5og", "Benjamin", "Söll", null, true, new UserInitState(teamLeaderInitState, bsoTeamleaderCondition));

        // HKU
        CategorialCondition hkuTeamleaderCondition = initService.condition().initCategorialCondition("hku employees", "only employees for hku", "", "", null, null, teamleaderShorthandField, null, "HKU");
        initService.initUser("kültür", "Rrxumh6L", "Hamdi", "Kültür", null, true, new UserInitState(teamLeaderInitState, hkuTeamleaderCondition));

        // MZS
        CategorialCondition mzsTeamleaderCondition = initService.condition().initCategorialCondition("mzs employees", "only employees for mzs", "", "", null, null, teamleaderShorthandField, null, "MZS");
        initService.initUser("zsemlye", "zkTE8zBn", "Marian", "Zsemlye", null, true, new UserInitState(teamLeaderInitState, mzsTeamleaderCondition));

        // MJU
        CategorialCondition mjuTeamleaderCondition = initService.condition().initCategorialCondition("mju employees", "only employees for mju", "", "", null, null, teamleaderShorthandField, null, "MJU");
        initService.initUser("juhar", "jiC2xTsN", "Marek", "Juhar", null, true, new UserInitState(teamLeaderInitState, mjuTeamleaderCondition));

        // MBO
        CategorialCondition mboTeamleaderCondition = initService.condition().initCategorialCondition("mbo employees", "only employees for mbo", "", "", null, null, teamleaderShorthandField, null, "MBO");
        initService.initUser("boljesik", "t4RcByMz", "Michal", "Boljesik", null, true, new UserInitState(teamLeaderInitState, mboTeamleaderCondition));
    }
}
