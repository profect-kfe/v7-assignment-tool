package de.profect.patches;

import de.profect.assignmenttool.tableviewwithcustomcolumn.TableViewWithCustomColumnState;
import de.profect.component.ComponentState;
import de.profect.patches.helpers.V7Patch;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0015_AT10 extends V7Patch {

    @Autowired
    PatchHelperService patchHelper;

    @Override
    public String getPatchVersion() {
        return "0015";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-10 Increase TableView rows'");
        List<ComponentState> tableViewStates = patchHelper.getComponentStatesByClass(TableViewWithCustomColumnState.class);
        for (ComponentState state: tableViewStates) {
            TableViewWithCustomColumnState tableViewState = (TableViewWithCustomColumnState) state;
            tableViewState.setLimit(100);
        }
    }
}