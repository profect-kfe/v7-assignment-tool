package de.profect.patches;

import de.profect.InitState;
import de.profect.InitStateRepository;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0008_AT21 extends V7Patch {

    @Autowired
    InitStateRepository initStateRepository;

    @Autowired
    InitHelperService initService;

    @Override
    public String getPatchVersion() {
        return "0008";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-21 Create new user'");
        List<InitState> initStates = initStateRepository.findAll();
        InitState defaultInitState = null;
        for (InitState initState : initStates) {
            if (initState.getCaption().equals("User")) {
                defaultInitState = initState;
            }
        }
        initService.initUser("erb", "cyazj1z9", "Christoph", "Erb", null, true, defaultInitState);
    }
}
