package de.profect.patches;

import de.profect.component.ComponentState;
import de.profect.component.formbuilder.controls.primitives.DynamicFormControlTextState;
import de.profect.patches.helpers.V7Patch;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0003_AT9 extends V7Patch {

    @Autowired
    PatchHelperService patchHelper;

    @Override
    public String getPatchVersion() {
        return "0003";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-9 Increase max length of all TextFields'");
        List<ComponentState> textFieldStates = patchHelper.getComponentStatesByClass(DynamicFormControlTextState.class);
        for (ComponentState state: textFieldStates) {
            DynamicFormControlTextState textFieldState = (DynamicFormControlTextState) state;
            textFieldState.setMaxlength(8192);
        }
    }
}