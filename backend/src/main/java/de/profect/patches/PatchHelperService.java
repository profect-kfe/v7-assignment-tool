package de.profect.patches;

import de.profect.component.ComponentState;
import de.profect.component.ComponentStateRepository;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@Log4j2
public class PatchHelperService {

    @Autowired
    ComponentStateRepository componentStateRepository;

    public List<ComponentState> getComponentStatesByClass(Class<? extends ComponentState> clss) {
        List<ComponentState> resultStates = new ArrayList();
        List<ComponentState> componentStates = componentStateRepository.findAll();
        for (ComponentState componentState : componentStates) {
            if (componentState.getClass() == clss) {
                resultStates.add(clss.cast(componentState));
            } else if (componentState instanceof HibernateProxy && Hibernate.getClass(componentState).equals(clss)) {
                resultStates.add(clss.cast(((HibernateProxy) componentState).getHibernateLazyInitializer().getImplementation()));
            }
        }
        return resultStates;
    }

    public boolean isOfClass(Object obj, Class<?> clss) {
        return obj.getClass() == clss || Hibernate.getClass(obj).equals(clss);
    }

    public static <T> T initializeAndUnproxy(T entity) {
        if (entity == null) {
            throw new NullPointerException("Entity passed for initialization is null");
        }
        Hibernate.initialize(entity);
        if (entity instanceof HibernateProxy) {
            entity = (T) ((HibernateProxy) entity).getHibernateLazyInitializer().getImplementation();
        }
        return entity;
    }
}
