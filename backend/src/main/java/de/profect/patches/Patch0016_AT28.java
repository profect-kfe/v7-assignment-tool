package de.profect.patches;

import de.profect.assignmenttool.tableviewwithcustomcolumn.TableViewWithCustomColumnState;
import de.profect.component.ComponentState;
import de.profect.data.query.field.Field;
import de.profect.patches.helpers.V7Patch;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0016_AT28 extends V7Patch {

    @Autowired
    PatchHelperService patchHelper;

    @Override
    public String getPatchVersion() {
        return "0016";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-28 Nach Anwendung des Filters in der Tabelle wird falsche Anzahl angezeigt'");
        List<ComponentState> tableViewStates = patchHelper.getComponentStatesByClass(TableViewWithCustomColumnState.class);
        for (ComponentState state: tableViewStates) {
            TableViewWithCustomColumnState tableViewState = (TableViewWithCustomColumnState) state;
            Field edit = null;
            Field delete = null;
            for (Field field : tableViewState.getSelectedFields()) {
                if (field.getCaption().equals("edit")) edit = field;
                if (field.getCaption().equals("delete")) delete = field;
            }
            if (edit == null || delete == null) throw new Exception("what");
            tableViewState.getSelectedFields().remove(edit);
            tableViewState.getSelectedFields().remove(delete);
        }
    }
}