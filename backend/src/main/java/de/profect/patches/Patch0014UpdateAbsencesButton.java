package de.profect.patches;

import de.profect.InitState;
import de.profect.InitStateRepository;
import de.profect.assignmenttool.deltatoken.DeltaTokenService;
import de.profect.assignmenttool.msgraph.GraphService;
import de.profect.assignmenttool.msgraph.graphupdateconnector.GraphUpdateConnectorState;
import de.profect.authentication.privilege.PrivilegeService;
import de.profect.authentication.privilege.PrivilegeSet;
import de.profect.authentication.secured.OperationType;
import de.profect.component.ComponentInstance;
import de.profect.component.ComponentInstanceRepository;
import de.profect.component.ComponentState;
import de.profect.component.ComponentStateRepository;
import de.profect.component.panel.flow.FlowPanelState;
import de.profect.connector.ConnectorInitializerService;
import de.profect.connector.ConnectorState;
import de.profect.inithelper.InitAssignmentHelperService;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Service
@Transactional
@Log4j2
public class Patch0014UpdateAbsencesButton extends V7Patch {

    @Autowired
    InitAssignmentHelperService helperServiceAss;

    @Autowired
    GraphService graphService;

    @Autowired
    ComponentInstanceRepository componentInstanceRepository;

    @Autowired
    InitHelperService helperService;

    @Autowired
    ComponentStateRepository componentStateRepository;

    @Autowired
    InitStateRepository initStateRepository;

    @Autowired
    private PrivilegeService privilegeService;

    @Autowired
    ConnectorInitializerService connectorInitializerService;

    @Autowired
    DeltaTokenService deltaTokenService;

    @Override
    public String getPatchVersion() {
        return "0014";
    }

    @Override
    public void executePatch() throws Exception {
        ComponentInstance absencesTable = componentInstanceRepository.findById(108).get();

        ComponentInstance updateButton = helperService.initButton("Update Absences", "Updates Absences", "btn btn-md btn-primary", "", null);

        helperServiceAss.initGraphUpdateConnector(
                "Update Absences", updateButton, absencesTable, ConnectorState.TRIGGEREVENT_ACTION);

        ComponentState cs = componentStateRepository.findById(52).get();
        FlowPanelState flowPanelState = (FlowPanelState) cs;

        Map<String, ComponentInstance> pos = new HashMap<>();
        pos.put("0", updateButton);
        pos.put("1", flowPanelState.getComponent(0));
        flowPanelState.setPositions(pos);

        List<InitState> initStates = initStateRepository.findAll();
        for (InitState initState : initStates) {
            Set<PrivilegeSet> roles = initState.getRoles();
            for (PrivilegeSet role : roles) {
                role.getPrivileges().add(privilegeService.createPrivilegeIfNotFound(deltaTokenService.getPrivilege(OperationType.Create)));
                role.getPrivileges().add(privilegeService.createPrivilegeIfNotFound(deltaTokenService.getPrivilege(OperationType.Delete)));
                role.getPrivileges().add(privilegeService.createPrivilegeIfNotFound(deltaTokenService.getPrivilege(OperationType.Update)));
                role.getPrivileges().add(privilegeService.createPrivilegeIfNotFound(deltaTokenService.getPrivilege(OperationType.Read)));
            }
        }
    }
}
