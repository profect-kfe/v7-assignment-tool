package de.profect.patches;
import de.profect.assignmenttool.tableviewwithcustomcolumn.TableViewWithCustomColumnState;
import de.profect.component.ComponentInstance;
import de.profect.component.ComponentInstanceRepository;
import de.profect.component.ComponentState;
import de.profect.component.ComponentStateRepository;
import de.profect.connector.ConnectorState;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0002_AT8 extends V7Patch {

    @Autowired
    ComponentStateRepository componentStateRepository;

    @Autowired
    ComponentInstanceRepository componentInstanceRepository;

    @Autowired
    InitHelperService initService;

    @Autowired
    PatchHelperService patchHelper;

    @Override
    public String getPatchVersion() {
        return "0002";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-8 The pagination is not working'");
        List<ComponentInstance> componentInstances = componentInstanceRepository.findAll();
        List<ComponentState> componentStatesByClass = patchHelper.getComponentStatesByClass(TableViewWithCustomColumnState.class);
        for (ComponentInstance instance : componentInstances) {
            ComponentState state = instance.getDefaultState();
            if (patchHelper.isOfClass(state, TableViewWithCustomColumnState.class)) {
                TableViewWithCustomColumnState tableViewState = (TableViewWithCustomColumnState) patchHelper.initializeAndUnproxy(state);
                List<String> tableCaptions = Arrays.asList(
                        "Customers",
                        "Employees",
                        "Scheduled Absences",
                        "Assignments",
                        "Projects");
                if (tableCaptions.contains(state.getCaption())) {
                    initService.connector().initRefreshConnector("Refresh Table after state change", instance, instance, ConnectorState.TRIGGEREVENT_STATE_CHANGE);
                }
            }
        }
    }
}
