package de.profect.patches;

import de.profect.assignmenttool.tableviewwithcustomcolumn.TableViewWithCustomColumnState;
import de.profect.component.ComponentState;
import de.profect.component.ComponentStateRepository;
import de.profect.component.dataview.tableview.TableViewState;
import de.profect.data.datasource.sql.field.SQLDataSourceField;
import de.profect.data.query.field.Field;
import de.profect.data.query.field.FieldRepository;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0011_AT23 extends V7Patch {

    @Autowired
    InitHelperService initHelperService;

    @Override
    public String getPatchVersion() {
        return "0011";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-23 Editieren nicht möglich'");

        initHelperService.getWritePublicMetadataDefinition();
        initHelperService.getWriteRoleMetadataDefinition();
        initHelperService.getWriteSelectiveMetadataDefinition();
        initHelperService.getIsCacheableMetadataDefinition();
    }
}
