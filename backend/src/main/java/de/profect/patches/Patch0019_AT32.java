package de.profect.patches;

import de.profect.assignmenttool.assignment.AssignmentValidatorState;
import de.profect.assignmenttool.project.ProjectFormSaveConnectorState;
import de.profect.assignmenttool.project.ProjectValidatorState;
import de.profect.assignmenttool.tableviewwithcustomcolumn.TableViewWithCustomColumnState;
import de.profect.component.ComponentInstance;
import de.profect.component.ComponentState;
import de.profect.component.ComponentStateRepository;
import de.profect.component.formbuilder.SynchronizationIdentifier;
import de.profect.component.formbuilder.container.group.DynamicFormGroupState;
import de.profect.component.formbuilder.validators.AsyncValidatorRepository;
import de.profect.connector.ConnectorState;
import de.profect.connector.ConnectorStateRepository;
import de.profect.connector.target.content.show.ShowConnectorState;
import de.profect.connector.target.form.reset_form.FormResetConnectorState;
import de.profect.connector.target.form.save_form.FormSaveConnectorState;
import de.profect.connector.target.refresh.RefreshConnectorState;
import de.profect.connector.target.reset.ResetConnectorState;
import de.profect.data.query.field.Field;
import de.profect.inithelper.InitAssignmentHelperService;
import de.profect.inithelper.InitAssignmentPagesService;
import de.profect.patches.helpers.V7Patch;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
@Log4j2
public class Patch0019_AT32 extends V7Patch {

    @Autowired
    PatchHelperService patchHelper;

    @Autowired
    ConnectorStateRepository connectorStateRepository;

    @Autowired
    InitAssignmentHelperService initAssignmentHelperService;

    @Autowired
    AsyncValidatorRepository validatorRepo;

    @Autowired
    InitHelperService initService;

    @Autowired
    ComponentStateRepository componentStateRepository;

    @Override
    public String getPatchVersion() {
        return "0019";
    }

    @Override
    public void executePatch() throws Exception {
        log.debug(">> executing patch for 'AT-32 Prüfung des Assignments bei Änderung einer Projektlaufzeit. (Also adds Project Validator)'");
        ComponentInstance saveButton = null;
        ComponentInstance projectFormGroup = null;

        for (ConnectorState connectorState : connectorStateRepository.findAll()) {
            if (connectorState.getSource().getId() == 236) {
                if (patchHelper.isOfClass(connectorState, FormSaveConnectorState.class)) {
                    FormSaveConnectorState formSaveConnectorState = (FormSaveConnectorState) PatchHelperService.initializeAndUnproxy(connectorState);
                    saveButton = formSaveConnectorState.getSource();
                    projectFormGroup = formSaveConnectorState.getTarget();
                    connectorStateRepository.delete(formSaveConnectorState);
                    break;
                }
            }
        }

        if (saveButton == null || projectFormGroup == null) {
            throw new Exception("nuh");
        }

        ConnectorState saveFormConnectorState = initAssignmentHelperService.initProjectFormSaveConnectorState("save", saveButton, projectFormGroup, ConnectorState.TRIGGEREVENT_ACTION);
        saveFormConnectorState.setExecutionOrder(50);

        //add project validator
        ProjectValidatorState validator = new ProjectValidatorState();
        validatorRepo.save(validator);
        DynamicFormGroupState projectForm = (DynamicFormGroupState) patchHelper.initializeAndUnproxy(componentStateRepository.findById(235).get());
        projectForm.setAsyncValidators(Arrays.asList(validator));
    }
}