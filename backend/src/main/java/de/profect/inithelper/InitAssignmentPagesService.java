package de.profect.inithelper;

import de.profect.Category;
import de.profect.assignmenttool.assignment.AssignmentValidatorState;
import de.profect.assignmenttool.customer.hierarchy.CustomerHierarchyDefinition;
import de.profect.assignmenttool.employee.EmployeeValidatorState;
import de.profect.assignmenttool.employee.hierarchy.EmployeeHierarchyDefinition;
import de.profect.assignmenttool.project.hierarchy.ProjectHierarchyDefinition;
import de.profect.component.ComponentInstance;
import de.profect.component.dataview.compass.CompassState;
import de.profect.component.dataview.compass.report.CompassReport;
import de.profect.component.dataview.compass.report.CompassReportSheet;
import de.profect.component.formbuilder.SynchronizationIdentifier;
import de.profect.component.formbuilder.controls.primitives.DynamicFormControlNumberState;
import de.profect.component.formbuilder.validators.AsyncValidatorRepository;
import de.profect.component.panel.PanelInitializerService;
import de.profect.component.panel.flow.FlowPanelState;
import de.profect.connector.ConnectorInitializerService;
import de.profect.connector.ConnectorState;
import de.profect.connector.ConnectorStateService;
import de.profect.connector.target.condition.AddConditionConnectorState;
import de.profect.connector.target.condition.ReplaceConditionConnectorState;
import de.profect.connector.target.refresh.RefreshConnectorState;
import de.profect.data.hierarchy.HierarchyDefinition;
import de.profect.data.hierarchy.simplehierarchy.HierarchySimpleDefinition;
import de.profect.data.hierarchy.simplehierarchy.SimpleHierarchyItem;
import de.profect.data.metadata.Metadata;
import de.profect.data.metadata.MetadataDefinition;
import de.profect.data.query.field.Field;
import de.profect.data.query.field.dataformat.DataFormat;
import de.profect.util.InitHelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class InitAssignmentPagesService {

    @Autowired
    private InitHelperService initService;


    @Autowired
    private InitAssignmentHelperService initAssignmentHelperService;

    @Autowired
    private AsyncValidatorRepository validatorRepo;

    @Autowired
    private PanelInitializerService panelInitializerService;

    @Autowired
    private ConnectorInitializerService connectorInitializerService;

    public ComponentInstance createCustomerPage(ComponentInstance overlayPanel, Field tableMainKPI, Field tableIdField, List<Field> tableFields, List<Field> tableFixedFields) {

        ComponentInstance addCustomerButton = initService.initButton("Add Customer", "Add customer", "btn btn-md btn-primary", "add", "", true);
        ComponentInstance addCustomerButtonFlowPanel = initService.panel().initFlowPanel("Add Customer Flow Panel", "flex-nowrap table-view-button-bar", false, true, false, addCustomerButton);
        ComponentInstance buttonsBar = initService.panel().initCenterPanel("Buttons Bar", "panel-buttonbar-center", addCustomerButtonFlowPanel);

        ComponentInstance customerTable = initAssignmentHelperService.initTableWithCustomColumn("Customers", null, "default-content-size cutomer-table custom-col-table",
                null, null, 12, tableMainKPI, tableIdField, tableFields,
                initService.addDefaultMetadata(null),
                true, tableFixedFields);

        ComponentInstance customerIdCtrl = initService.form2().initFormNumber("Id", "id", null, false, false, true, null, null, null, null);
        ComponentInstance customerNameCtrl = initService.form2().initFormText("Customer", "customerName", null, true, false, false, "", "", "", 64, "customer-name-control");
        ComponentInstance customerDivisionCtrl = initService.form2().initFormText("Division", "division", null, true, false, false, "", "", "", 64, "customer-division-control");

        ComponentInstance customerFormRow1 = initService.panel().initFlowPanel("", "flow-row", false, false, false, customerNameCtrl);
        ComponentInstance customerFormRow2 = initService.panel().initFlowPanel("", "flow-row", false, false, false, customerDivisionCtrl);
        ComponentInstance customerFormControlFlowPanel = initService.panel().initFlowPanel("customerFormControlFlowPanel", "v7-form-shadow", true, false, false,
                customerIdCtrl,
                customerFormRow1,
                customerFormRow2
        );

        SynchronizationIdentifier customerFormGroupSyncId = initService.form2().initSyncId();
        ComponentInstance customerFormGroup = initService.form2().initFormGroup("Customer", "customer", "assignment-form customer-form", null, null, false, customerFormGroupSyncId, "v7-assignment-customer", true, customerFormControlFlowPanel);

        ComponentInstance saveButton = initService.initButton("Save", "Saves customer", "btn btn-md btn-primary", null, "ctrl+s", false);
        initService.connector().initEnableConnector("Enable Save Button", customerFormGroup, saveButton, Boolean.TRUE, ConnectorState.TRIGGEREVENT_VALIDATED);
        initService.connector().initEnableConnector("Disable Save Button", customerFormGroup, saveButton, Boolean.FALSE, ConnectorState.TRIGGEREVENT_INVALIDATED);

        ConnectorState saveCloseDialogConnectorState = initService.connector().initShowConnector("close dialog after save", saveButton, overlayPanel, null, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        saveCloseDialogConnectorState.setExecutionOrder(0);
        ConnectorState saveFormConnectorState = initService.connector().initFormSaveConnectorState("save", saveButton, customerFormGroup, ConnectorState.TRIGGEREVENT_ACTION);
        saveFormConnectorState.setExecutionOrder(100);
        ConnectorState saveRefreshTableConnectorState = initService.connector().initRefreshConnector("refresh customer table", saveButton, customerTable, ConnectorState.TRIGGEREVENT_ACTION);
        saveRefreshTableConnectorState.setExecutionOrder(200);
        ConnectorState saveResetFormConnectorState = initService.connector().initFormResetConnectorState("Reset form group", saveButton, customerFormGroupSyncId, ConnectorState.TRIGGEREVENT_ACTION);
        saveResetFormConnectorState.setExecutionOrder(300);

        ComponentInstance cancelButton = initService.initButton("", "Cancel", "btn btn-md", "close", "esc");
        initService.connector().initShowConnector("OK Save Dialog", cancelButton, overlayPanel, null, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initFormResetConnectorState("Reset form group", cancelButton, customerFormGroupSyncId, ConnectorState.TRIGGEREVENT_ACTION);

        ComponentInstance customerDialogLabel = initService.initLabel("Customer", "", "dialog-label v7-font-p-strong", "");
        ComponentInstance topBar = initService.panel().initFlowPanel("Top-ButtonBar", "panel-dialog-header", false, true, false, customerDialogLabel, cancelButton);
        ComponentInstance bottomBar = initService.panel().initFlowPanel("Bottom-ButtonBar", "panel-dialog-footer", false, true, false, saveButton);
        ComponentInstance rightContent = initService.panel().initFlowPanel("", "v7-background-center-color", true, false, false, topBar, customerFormGroup, bottomBar);
        ComponentInstance dialogPanel = initService.panel().initCenterPanel("Panel Dialog", "panel-dialog", rightContent);

        initService.connector().initShowConnector("Open Save Dialog", addCustomerButton, overlayPanel, dialogPanel, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);

        ComponentInstance page = initService.panel().initCenterPanel("Customer Table Center Panel", "v7-assignment-tool-full-page", customerTable);

        initService.connector().initFetchConnector("Fetch customer form control dialog", page,
                Arrays.asList(dialogPanel,
                        customerIdCtrl,
                        customerNameCtrl,
                        customerDivisionCtrl,
                        customerFormGroup,
                        customerFormControlFlowPanel,
                        cancelButton,
                        customerFormRow1,
                        customerFormRow2,
                        saveButton), ConnectorState.TRIGGEREVENT_COMPONENT_INITIALIZED);


        initService.connector().initFormSetValueByIdentifierAndServiceConnectorState("Customer Form Values Connector", customerTable,
                customerFormGroupSyncId, "v7-assignment-customer", "EDIT");
        initService.connector().initShowConnector("Open Save Dialog", customerTable, overlayPanel, dialogPanel, "OVERLAY", "EDIT");

        setupDeletion(customerTable, overlayPanel);

        return initService.panel().initTopBottomPanel("Customer Top Bottom Panel", "", page, buttonsBar);
    }


    public ComponentInstance createEmployeePage(ComponentInstance overlayPanel,
                                                Field employeeTableMainKPI, Field employeeTableIdField, Field employeeOrderByField, List<Field> employeeTableFields, List<Field> employeeTableFixedFields,
                                                Field absenceTableMainKPI, Field absenceTableIdField, Field absenceOrderByField, List<Field> absenceTableFields, List<Field> absenceTableFixedFields,
                                                Field assignmentTableMainKPI, Field assignmentTableIdField, Field assignmentOrderByField, List<Field> assignmentTableFields, List<Field> assignmentTableFixedFields,
                                                EmployeeHierarchyDefinition employeeHierarchyDefinition, ProjectHierarchyDefinition projectHierarchyDefinition, EmployeeHierarchyDefinition teamLeaderHierarchyDefinition,
                                                Field assignmentStartDateField, Field absenceStartDateField
    ) {

        String activeCssClass = "btn btn-md btn-primary btn-block mb-4";
        String inactiveCssClass = "btn btn-md btn-secondary btn-block mb-4";

        ComponentInstance employeeButton = initService.initButton("Employee", "Add or edit employees", activeCssClass, "face", null);
        ComponentInstance absenceButton = initService.initButton("Scheduled Absence", "Add or edit scheduled absences", inactiveCssClass, "calendar_today", null);
        ComponentInstance assignmentButton = initService.initButton("Assignment", "Add or edit assignment", inactiveCssClass, "watch_later", null);

        Category catAbsence = initService.initCategory("Absence", null, null, new Metadata(MetadataDefinition.READ_PUBLIC, true), new Metadata(MetadataDefinition.OBJECT_TYPE, "SELECTIONCRITERIA"));
        ComponentInstance dateChooserAbsence = initService.initDateChooser("Filter start date", catAbsence, absenceStartDateField, true, null);
        AddConditionConnectorState absenceAddConnector = initService.connector().initAddConditionConnector("Absence Add Conn", dateChooserAbsence, null, ConnectorState.TRIGGEREVENT_ACTION);
        ReplaceConditionConnectorState absenceReplaceConnector = initService.connector().initReplaceConditionConnector("Absence Replace Conn", false, null, dateChooserAbsence, ConnectorState.TRIGGEREVENT_STATE_CHANGE);
        RefreshConnectorState absenceRefreshConnector = initService.connector().initRefreshConnector("Absence DC Refresh Conn", dateChooserAbsence, null, ConnectorState.TRIGGEREVENT_STATE_CHANGE);

        Category catAssignment = initService.initCategory("Assignment", null, null, new Metadata(MetadataDefinition.READ_PUBLIC, true), new Metadata(MetadataDefinition.OBJECT_TYPE, "SELECTIONCRITERIA"));
        ComponentInstance dateChooserAssignment = initService.initDateChooser("Filter start date", catAssignment, assignmentStartDateField, true, null);
        AddConditionConnectorState assignmentAddConnector = initService.connector().initAddConditionConnector("Assignment Add Conn", dateChooserAssignment, null, ConnectorState.TRIGGEREVENT_ACTION);
        ReplaceConditionConnectorState assignmentReplaceConnector = initService.connector().initReplaceConditionConnector("Assignment Replace Conn", false, null, dateChooserAssignment, ConnectorState.TRIGGEREVENT_STATE_CHANGE);
        RefreshConnectorState assignmentRefreshConnector = initService.connector().initRefreshConnector("Assignment DC Refresh Conn", dateChooserAssignment, null, ConnectorState.TRIGGEREVENT_STATE_CHANGE);

        ComponentInstance emptyLabel = initService.initLabel("", "", null, null);
        ComponentInstance dateChooserPanel = initService.panel().initFlowPanel("AbsenceDateChooserPanel", "", true, false, false, emptyLabel);
        initService.connector().initShowConnector("Hide Date Choosers", employeeButton, dateChooserPanel, null, "0", ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initShowConnector("Show Assignment Date Chooser", assignmentButton, dateChooserPanel, dateChooserAssignment, "0", ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initShowConnector("Show Absence Date Chooser", absenceButton, dateChooserPanel, dateChooserAbsence, "0", ConnectorState.TRIGGEREVENT_ACTION);

        ComponentInstance control = initService.panel().initFlowPanel("Employee Switch View Buttons", "panel-edge default-sidebar-height", true, false, true,
                employeeButton, absenceButton, assignmentButton, dateChooserPanel);

        ComponentInstance addEmployeeButton = initService.initButton("Add Employee", "Add employee", "btn btn-md btn-primary", "add", "", true);
        ComponentInstance addEmployeeButtonFlowPanel = initService.panel().initFlowPanel("Add Employee Flow Panel", "flex-nowrap table-view-button-bar", false, true, false, addEmployeeButton);

        ComponentInstance addAbsenceButton = initService.initButton("Add Scheduled Absence", "Add Scheduled Absence", "btn btn-md btn-primary", "add", "", true);
        ComponentInstance addAbsenceButtonFlowPanel = initService.panel().initFlowPanel("Add Scheduled Absence Flow Panel", "flex-nowrap table-view-button-bar", false, true, false, addAbsenceButton);

        ComponentInstance addAssignmentButton = initService.initButton("Add Assignment", "Add assignment", "btn btn-md btn-primary", "add", "", true);
        ComponentInstance addAssignmentButtonFlowPanel = initService.panel().initFlowPanel("Add Assignment Flow Panel", "flex-nowrap table-view-button-bar", false, true, false, addAssignmentButton);

        ComponentInstance buttonsBar = initService.panel().initCenterPanel("Buttons Bar", "panel-buttonbar-center", addEmployeeButtonFlowPanel);

        ComponentInstance employeeListPage = createEmployeeListPage(overlayPanel, employeeTableMainKPI, employeeTableIdField, employeeOrderByField, employeeTableFields, employeeTableFixedFields, addEmployeeButton, teamLeaderHierarchyDefinition);
        ComponentInstance absenceListPage = createAbsenceListPage(overlayPanel, absenceTableMainKPI, absenceTableIdField, absenceOrderByField, absenceTableFields, absenceTableFixedFields, employeeHierarchyDefinition, addAbsenceButton, absenceAddConnector, absenceReplaceConnector, absenceRefreshConnector);
        ComponentInstance assignmentListPage = createAssignmentListPage(overlayPanel, assignmentTableMainKPI, assignmentTableIdField, assignmentOrderByField, assignmentTableFields, assignmentTableFixedFields, employeeHierarchyDefinition, projectHierarchyDefinition, addAssignmentButton, assignmentAddConnector, assignmentReplaceConnector, assignmentRefreshConnector);

        ComponentInstance page = initService.panel().initCollapsibleLeftRightPanel("Employee Left Right Panel",
                "v7-assignment-employee-page", true, control, employeeListPage);

        initService.connector().initShowConnector("Show employee page", employeeButton, page, employeeListPage, "MAIN",
                ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initShowConnector("Show add employee button", employeeButton, buttonsBar, addEmployeeButtonFlowPanel, "MAIN",
                ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initCssConnector("employeeButtonActive", employeeButton, employeeButton, activeCssClass,
                ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initCssConnector("absenceButtonInactive", employeeButton, absenceButton,
                inactiveCssClass, ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initCssConnector("assignmentButtonInactive", employeeButton, assignmentButton,
                inactiveCssClass, ConnectorState.TRIGGEREVENT_ACTION);

        initService.connector().initShowConnector("Show absence page", absenceButton, page, absenceListPage, "MAIN",
                ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initShowConnector("Show add absence button", absenceButton, buttonsBar, addAbsenceButtonFlowPanel, "MAIN",
                ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initCssConnector("absenceButtonActive", absenceButton, absenceButton,
                activeCssClass, ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initCssConnector("employeeButtonInactive", absenceButton, employeeButton,
                inactiveCssClass, ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initCssConnector("assignmentButtonInactive", absenceButton, assignmentButton,
                inactiveCssClass, ConnectorState.TRIGGEREVENT_ACTION);

        initService.connector().initShowConnector("Show assignment page", assignmentButton, page, assignmentListPage, "MAIN",
                ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initShowConnector("Show add assignment button", assignmentButton, buttonsBar, addAssignmentButtonFlowPanel, "MAIN",
                ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initCssConnector("assignmentButtonActive", assignmentButton, assignmentButton, activeCssClass,
                ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initCssConnector("employeeButtonInactive", assignmentButton, employeeButton,
                inactiveCssClass, ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initCssConnector("absenceButtonInactive", assignmentButton, absenceButton,
                inactiveCssClass, ConnectorState.TRIGGEREVENT_ACTION);

        return initService.panel().initTopBottomPanel("Employee Top Bottom Panel", "", page, buttonsBar);
    }

    public ComponentInstance createEmployeeListPage(ComponentInstance overlayPanel, Field tableMainKPI, Field tableIdField, Field tableOrderField, List<Field> tableFields, List<Field> tableFixedFields, ComponentInstance addEmployeeButton, EmployeeHierarchyDefinition teamLeaderHierarchyDefinition) {
        ComponentInstance employeeTable = initAssignmentHelperService.initTableWithCustomColumn(
                "Employees", null, "default-content-size employee-table custom-col-table", null,
                null, 12, null, tableMainKPI, tableIdField, tableFields,
                initService.addDefaultMetadata(null),
                true, false, false, tableFixedFields, null, tableOrderField, true);

        ComponentInstance employeeIdCtrl = initService.form2().initFormNumber("Id", "id", null, false, false, true, -1d, null, null, null);
        ComponentInstance employeeStaffNumberCtrl = initService.form2().initFormText("Staff Number*", "staffNumber", null, true, false, false, "", "0000", "\\d\\d\\d\\d", 4, "employee-staff-number-control");
        ComponentInstance employeeFirstCtrl = initService.form2().initFormText("First Name*", "firstName", null, true, false, false, "", "", "", 64, "employee-first-name-control");
        ComponentInstance employeeLastCtrl = initService.form2().initFormText("Last Name*", "lastName", null, true, false, false, "", "", "", 64, "employee-last-name-control");

        ComponentInstance employeeEmailCtrl = initService.form2().initFormText("E-Mail*", "email", null, true, false, false, "", "firstname.lastname@profect.tld", "", 64, "employee-email-control");
        ComponentInstance employeeShorthandCtrl = initService.form2().initFormText("Shorthand*", "shorthand", null, true, false, false, "", "ABC", "", 3, "employee-shorthand-control");

        HierarchySimpleDefinition locationHierarchy = initService.hierarchy().initHierarchySimpleDefinition("Location Hierarchy", new SimpleHierarchyItem("root", "root",
                new SimpleHierarchyItem("Profect DE", "Profect DE"),
                new SimpleHierarchyItem("Profect SK", "Profect SK"),
                new SimpleHierarchyItem("Profect CH", "Profect CH")));
        ComponentInstance employeeLocationCtrl = initService.form2().initFormDropdown("Location*", "location", null, true, false, false, null, locationHierarchy, true, "employee-location-control");

        ComponentInstance employeeTeamCtrl = initService.form2().initFormDropdown("Team*", "teamLeader", null, true, false, false, null, teamLeaderHierarchyDefinition, true, "employee-team-control");
        ComponentInstance employeeIsTeamLeaderCtrl = initService.form2().initFormCheckbox("Is team leader*", "isTeamLeader", false, false, false, Boolean.FALSE);
        ConnectorState employeeTeamConnectorOnAction = initAssignmentHelperService.initEmployeeTeamleaderConnector("Employee Team Leader Connector", employeeIsTeamLeaderCtrl, employeeTeamCtrl, ConnectorState.TRIGGEREVENT_ACTION);
        ConnectorState employeeTeamConnectorOnInit = initAssignmentHelperService.initEmployeeTeamleaderConnector("Employee Team Leader Connector", employeeIsTeamLeaderCtrl, employeeTeamCtrl, ConnectorState.TRIGGEREVENT_COMPONENT_INITIALIZED);

        HierarchySimpleDefinition contractTypeHierarchy = initService.hierarchy().initHierarchySimpleDefinition("Contract Type Hierarchy", new SimpleHierarchyItem("root", "root",
                new SimpleHierarchyItem("salaried employee", "salaried employee"),
                new SimpleHierarchyItem("student", "student"),
                new SimpleHierarchyItem("intern", "intern")
        ));
        ComponentInstance employeeContractTypeCtrl = initService.form2().initFormDropdown("Contract Type*", "contractType", null, true, false, false, null, contractTypeHierarchy, true, "employee-contract-type-control");

        ComponentInstance employeeWeeklyHoursCtrl = initService.form2().initFormNumber("Weekly Hours*", "weeklyHours", null, true, false, false, null, 0d, 40d, "");

        ComponentInstance employeeMondayHoursCtrl = initFormNumber("Monday Hours*", "mondayHours", null, true, false, false, null, 0d, 24d, "", "daily-hours monday-hours");
        ComponentInstance employeeTuesdayHoursCtrl = initFormNumber("Tuesday Hours*", "tuesdayHours", null, true, false, false, null, 0d, 24d, "", "daily-hours tuesday-hours");
        ComponentInstance employeeWednesdayHoursCtrl = initFormNumber("Wednesday Hours*", "wednesdayHours", null, true, false, false, null, 0d, 24d, "", "daily-hours wednesday-hours");
        ComponentInstance employeeThursdayHoursCtrl = initFormNumber("Thursday Hours*", "thursdayHours", null, true, false, false, null, 0d, 24d, "", "daily-hours thursday-hours");
        ComponentInstance employeeFridayHoursCtrl = initFormNumber("Friday Hours*", "fridayHours", null, true, false, false, null, 0d, 24d, "", "daily-hours friday-hours");

        ComponentInstance employeeEntryDateCtrl = initService.form2().initFormDateChooser("Entry Date*", "employee-entry-date-control", "entryDate", null, true, false, false, "", "", false);
        ComponentInstance employeeRetirementDateCtrl = initService.form2().initFormDateChooser("Retirement Date*", "employee-retirement-date-control", "retirementDate", null, true, false, false, "", "", false);
        ComponentInstance employeeCommentCtrl = initService.form2().initFormTextarea("Comment", "comment", null, false, false, false, null, null, 256, 4, "employee-retirement-reason-control");

        ComponentInstance employeeFormRow1 = initService.panel().initFlowPanel("", "flow-row", false, false, false, employeeStaffNumberCtrl, employeeLocationCtrl);
        ComponentInstance employeeFormRow2 = initService.panel().initFlowPanel("", "flow-row", false, false, false, employeeFirstCtrl, employeeLastCtrl);
        ComponentInstance employeeFormRow3 = initService.panel().initFlowPanel("", "flow-row", false, false, false, employeeEmailCtrl, employeeShorthandCtrl);
        ComponentInstance employeeFormRow4 = initService.panel().initFlowPanel("", "flow-row", false, false, false, employeeIsTeamLeaderCtrl, employeeTeamCtrl);
        ComponentInstance employeeFormRow5 = initService.panel().initFlowPanel("", "flow-row", false, false, false, employeeEntryDateCtrl, employeeRetirementDateCtrl);
        ComponentInstance employeeFormRow6 = initService.panel().initFlowPanel("", "flow-row", false, false, false, employeeContractTypeCtrl, employeeWeeklyHoursCtrl);
        ComponentInstance employeeFormRow7 = initService.panel().initFlowPanel("", "flow-row", false, false, false, employeeMondayHoursCtrl, employeeTuesdayHoursCtrl, employeeWednesdayHoursCtrl, employeeThursdayHoursCtrl, employeeFridayHoursCtrl);
        ComponentInstance employeeFormRow8 = initService.panel().initFlowPanel("", "flow-row", false, false, false, employeeCommentCtrl);

        ComponentInstance employeeFormControlFlowPanel = initService.panel().initFlowPanel("employeeFormControlFlowPanel", "v7-form-shadow", true, false, false,
                employeeIdCtrl,
                employeeFormRow1,
                employeeFormRow2,
                employeeFormRow3,
                employeeFormRow4,
                employeeFormRow5,
                employeeFormRow6,
                employeeFormRow7,
                employeeFormRow8
        );

        SynchronizationIdentifier employeeFormGroupSyncId = initService.form2().initSyncId();
        EmployeeValidatorState validator = new EmployeeValidatorState();
        validatorRepo.save(validator);
        ComponentInstance employeeFormGroup = initService.form2().initFormGroup("Employee", "employee", "assignment-form employee-form", Arrays.asList(validator), null, false, employeeFormGroupSyncId, "v7-assignment-employee", true, employeeIdCtrl, employeeFormControlFlowPanel);

        ComponentInstance saveButton = initService.initButton("Save", "Saves employee", "btn btn-md btn-primary", null, "ctrl+s", false);
        initService.connector().initEnableConnector("Enable Save Button", employeeFormGroup, saveButton, Boolean.TRUE, ConnectorState.TRIGGEREVENT_VALIDATED);
        initService.connector().initEnableConnector("Disable Save Button", employeeFormGroup, saveButton, Boolean.FALSE, ConnectorState.TRIGGEREVENT_INVALIDATED);

        ConnectorState saveCloseDialogConnectorState = initService.connector().initShowConnector("close dialog after save", saveButton, overlayPanel, null, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        saveCloseDialogConnectorState.setExecutionOrder(0);
        ConnectorState saveFormConnectorState = initAssignmentHelperService.initEmployeeFormSaveConnectorState("save", saveButton, employeeFormGroup, ConnectorState.TRIGGEREVENT_ACTION);
        saveFormConnectorState.setExecutionOrder(100);
        ConnectorState saveRefreshTableConnectorState = initService.connector().initRefreshConnector("refresh employee table", saveButton, employeeTable, ConnectorState.TRIGGEREVENT_ACTION);
        saveRefreshTableConnectorState.setExecutionOrder(200);
        ConnectorState saveResetFormConnectorState = initService.connector().initFormResetConnectorState("Reset employee form group", saveButton, employeeFormGroupSyncId, ConnectorState.TRIGGEREVENT_ACTION);
        saveResetFormConnectorState.setExecutionOrder(300);

        ComponentInstance cancelButton = initService.initButton("", "Cancel", "btn btn-md", "close", "esc");
        ConnectorState cancelCloseDialogState = initService.connector().initShowConnector("OK Save Dialog", cancelButton, overlayPanel, null, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        ConnectorState cancelResetFormConnectorState = initService.connector().initFormResetConnectorState("Reset employee form group", cancelButton, employeeFormGroupSyncId, ConnectorState.TRIGGEREVENT_ACTION);

        ComponentInstance employeeDialogLabel = initService.initLabel("Employee", "", "dialog-label v7-font-p-strong", "");
        ComponentInstance topBar = initService.panel().initFlowPanel("Top-ButtonBar", "panel-dialog-header", false, true, false, employeeDialogLabel, cancelButton);
        ComponentInstance bottomBar = initService.panel().initFlowPanel("Bottom-ButtonBar", "panel-dialog-footer", false, true, false, saveButton);
        ComponentInstance rightContent = initService.panel().initFlowPanel("", "v7-background-center-color", true, false, false, topBar, employeeFormGroup, bottomBar);
        ComponentInstance dialogPanel = initService.panel().initCenterPanel("Panel Dialog", "panel-dialog", rightContent);

        initService.connector().initShowConnector("Open Save Dialog", addEmployeeButton, overlayPanel, dialogPanel, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);

        initService.connector().initFetchConnector("Fetch dialog panel components", employeeTable,
                Arrays.asList(dialogPanel, employeeIdCtrl, employeeStaffNumberCtrl,
                        employeeFirstCtrl,
                        employeeLastCtrl,
                        employeeLocationCtrl,
                        employeeTeamCtrl,
                        employeeContractTypeCtrl,
                        employeeWeeklyHoursCtrl,
                        employeeMondayHoursCtrl,
                        employeeTuesdayHoursCtrl,
                        employeeWednesdayHoursCtrl,
                        employeeThursdayHoursCtrl,
                        employeeFridayHoursCtrl,
                        employeeEntryDateCtrl,
                        employeeRetirementDateCtrl,
                        employeeCommentCtrl,
                        employeeFormGroup,
                        employeeFormControlFlowPanel,
                        employeeFormRow1,
                        employeeFormRow2,
                        employeeFormRow3,
                        employeeFormRow4,
                        employeeFormRow5,
                        employeeFormRow6,
                        employeeFormRow7,
                        employeeFormRow8,
                        cancelButton,
                        saveButton), ConnectorState.TRIGGEREVENT_COMPONENT_INITIALIZED);


        initService.connector().initFormSetValueByIdentifierAndServiceConnectorState("Employee Form Values Connector", employeeTable,
                employeeFormGroupSyncId, "v7-assignment-employee", "EDIT");
        initService.connector().initShowConnector("Open Save Dialog", employeeTable, overlayPanel, dialogPanel, "OVERLAY", "EDIT");

        setupDeletion(employeeTable, overlayPanel);

        ComponentInstance confirmButton = initService.initButton("OK", "",
                "btn btn-md btn-secondary", "", null);

        ComponentInstance buttonPanel = panelInitializerService.initFlowPanel("",
                "button-right", false, true, false, confirmButton);

        ComponentInstance label = initService.initLabel("",
                null, "v7-font-h2 mbd-column-selection-hint-label", null);

        initAssignmentHelperService.initDynamicLabelMessageConnector("", employeeTable, label, "PASS_PROJECT_IDS_TO_DIALOG");

        ComponentInstance refIdsPanel = panelInitializerService.initTopBottomPanel("Referenced Ids",
                "col-auto panel-dialog mbd-scrollable-overlay-panel", label, buttonPanel);

        connectorInitializerService.initShowConnector("Show Referenced Ids Panel", employeeTable, overlayPanel, refIdsPanel,
                "OVERLAY", "SHOW_PROJECT_IDS_DIALOG");

        connectorInitializerService.initShowConnector("Close Referenced Ids Panel", confirmButton, overlayPanel, null,
                "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);

        return initService.panel().initCenterPanel("Employee Table Center Panel", "employee-table-center-panel", employeeTable);
    }

    public ComponentInstance createAbsenceListPage(ComponentInstance overlayPanel, Field tableMainKPI, Field tableIdField, Field tableOrderField, List<Field> tableFields, List<Field> tableFixedFields,
                                                   EmployeeHierarchyDefinition employeeHierarchyDefinition, ComponentInstance addAbsenceButton,
                                                   AddConditionConnectorState absenceAddConnector, ReplaceConditionConnectorState absenceReplaceConnector, RefreshConnectorState absenceRefreshConnector) {
        ComponentInstance absenceTable = initAssignmentHelperService.initTableWithCustomColumn(
                "Scheduled Absences", null, "default-content-size absence-table custom-col-table", null,
                null, 12, null, tableMainKPI, tableIdField, tableFields,
                initService.addDefaultMetadata(null),
                true, false, false, tableFixedFields, null, tableOrderField, true);

        absenceAddConnector.setTarget(absenceTable);
        absenceReplaceConnector.setSource(absenceTable);
        absenceRefreshConnector.setTarget(absenceTable);

        ComponentInstance absenceIdCtrl = initService.form2().initFormNumber("Scheduled Absence Number", "id", null, false, false, true, null, null, null, null);

        ComponentInstance absenceEmployeeCtrl = initService.form2().initFormDropdown("Employee*", "employee", null, true, false, false, null, employeeHierarchyDefinition, true, "absence-employee-control");

        ComponentInstance absenceStartDateCtrl = initService.form2().initFormDateChooser("Start of absence*", "absence-start-date-control", "startDate", null, true, false, false, "", "", false);
        ComponentInstance absenceEndDateCtrl = initService.form2().initFormDateChooser("End of absence*", "absence-end-date-control", "endDate", null, true, false, false, "", "", false);

        HierarchySimpleDefinition absenceReasonHierarchy = initService.hierarchy().initHierarchySimpleDefinition("Location Hierarchy", new SimpleHierarchyItem("root", "root",
                new SimpleHierarchyItem("Vacation", "Vacation"),
                new SimpleHierarchyItem("Official Holiday", "Official Holiday"),
                new SimpleHierarchyItem("Sick Leave", "Sick Leave"),
                new SimpleHierarchyItem("Rehab", "Rehab"),
                new SimpleHierarchyItem("Parental Leave","Parental Leave")));
        ComponentInstance absenceReasonCtrl = initService.form2().initFormDropdown("Reason*", "reason", null, true, false, false, null, absenceReasonHierarchy, true, "absence-reason-control");

        ComponentInstance absenceFormRow1 = initService.panel().initFlowPanel("", "flow-row", false, false, false, absenceEmployeeCtrl);
        ComponentInstance absenceFormRow2 = initService.panel().initFlowPanel("", "flow-row", false, false, false, absenceStartDateCtrl, absenceEndDateCtrl);
        ComponentInstance absenceFormRow3 = initService.panel().initFlowPanel("", "flow-row", false, false, false, absenceReasonCtrl);

        ComponentInstance absenceFormControlFlowPanel = initService.panel().initFlowPanel("employeeFormControlFlowPanel", "v7-form-shadow", true, false, false,
                absenceIdCtrl,
                absenceFormRow1,
                absenceFormRow2,
                absenceFormRow3);

        SynchronizationIdentifier absenceFormGroupSyncId = initService.form2().initSyncId();
        ComponentInstance absenceFormGroup = initService.form2().initFormGroup("Scheduled Absence", "absence", "assignment-form absence-form", null, null, false, absenceFormGroupSyncId, "v7-assignment-absence", true, absenceFormControlFlowPanel);

        ComponentInstance saveButton = initService.initButton("Save", "Saves absence", "btn btn-md btn-primary", null, "ctrl+s", false);
        initService.connector().initEnableConnector("Enable Save Button", absenceFormGroup, saveButton, Boolean.TRUE, ConnectorState.TRIGGEREVENT_VALIDATED);
        initService.connector().initEnableConnector("Disable Save Button", absenceFormGroup, saveButton, Boolean.FALSE, ConnectorState.TRIGGEREVENT_INVALIDATED);

        ConnectorState saveCloseDialogConnectorState = initService.connector().initShowConnector("close dialog after save", saveButton, overlayPanel, null, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        saveCloseDialogConnectorState.setExecutionOrder(0);
        ConnectorState saveFormConnectorState = initService.connector().initFormSaveConnectorState("save", saveButton, absenceFormGroup, ConnectorState.TRIGGEREVENT_ACTION);
        saveFormConnectorState.setExecutionOrder(100);
        ConnectorState saveRefreshTableConnectorState = initService.connector().initRefreshConnector("refresh absence table", saveButton, absenceTable, ConnectorState.TRIGGEREVENT_ACTION);
        saveRefreshTableConnectorState.setExecutionOrder(200);
        ConnectorState saveResetFormConnectorState = initService.connector().initFormResetConnectorState("Reset absence form group", saveButton, absenceFormGroupSyncId, ConnectorState.TRIGGEREVENT_ACTION);
        saveResetFormConnectorState.setExecutionOrder(300);

        ComponentInstance cancelButton = initService.initButton("", "Cancel", "btn btn-md", "close", "esc");
        ConnectorState cancelCloseDialogState = initService.connector().initShowConnector("OK Save Dialog", cancelButton, overlayPanel, null, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        ConnectorState cancelResetFormConnectorState = initService.connector().initFormResetConnectorState("Reset absence form group", cancelButton, absenceFormGroupSyncId, ConnectorState.TRIGGEREVENT_ACTION);

        ComponentInstance absenceDialogLabel = initService.initLabel("Absence", "", "dialog-label v7-font-p-strong", "");
        ComponentInstance topBar = initService.panel().initFlowPanel("Top-ButtonBar", "panel-dialog-header", false, true, false, absenceDialogLabel, cancelButton);
        ComponentInstance bottomBar = initService.panel().initFlowPanel("Bottom-ButtonBar", "panel-dialog-footer", false, true, false, saveButton);
        ComponentInstance rightContent = initService.panel().initFlowPanel("", "v7-background-center-color", true, false, false, topBar, absenceFormGroup, bottomBar);
        ComponentInstance dialogPanel = initService.panel().initCenterPanel("Panel Dialog", "panel-dialog", rightContent);

        initService.connector().initShowConnector("Open Save Dialog", addAbsenceButton, overlayPanel, dialogPanel, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);

        initService.connector().initFetchConnector("Fetch scheduled absence dialog", absenceTable,
                Arrays.asList(dialogPanel, absenceIdCtrl,
                        absenceEmployeeCtrl,
                        absenceStartDateCtrl,
                        absenceEndDateCtrl,
                        absenceFormRow1,
                        absenceFormRow2,
                        absenceFormRow3,
                        absenceReasonCtrl,
                        absenceFormControlFlowPanel,
                        absenceFormGroup,
                        absenceFormControlFlowPanel,
                        cancelButton,
                        saveButton), ConnectorState.TRIGGEREVENT_COMPONENT_INITIALIZED);

        initService.connector().initFormSetValueByIdentifierAndServiceConnectorState("Absence Form Values Connector", absenceTable,
                absenceFormGroupSyncId, "v7-assignment-absence", "EDIT");
        initService.connector().initShowConnector("Open Save Dialog", absenceTable, overlayPanel, dialogPanel, "OVERLAY", "EDIT");

        setupDeletion(absenceTable, overlayPanel);

        return initService.panel().initCenterPanel("Absence Table Center Panel", "absence-table-center-panel", absenceTable);
    }

    public ComponentInstance createAssignmentListPage(ComponentInstance overlayPanel, Field tableMainKPI, Field tableIdField, Field tableOrderField, List<Field> tableFields, List<Field> tableFixedFields,
                                                         HierarchyDefinition employeeHierarchyDefinition, HierarchyDefinition projectHierarchyDefinition, ComponentInstance addAssignmentButton,
                                                        AddConditionConnectorState assignmentAddConnector, ReplaceConditionConnectorState assignmentReplaceConnector, RefreshConnectorState assignmentRefreshConnector) {

        ComponentInstance assignmentTable = initAssignmentHelperService.initTableWithCustomColumn("Assignments", null, "default-content-size assignment-table custom-col-table", null,
                null, 12, null, tableMainKPI, tableIdField, tableFields,
                initService.addDefaultMetadata(null),
                true, false, false, tableFixedFields, null, tableOrderField, true);

        assignmentAddConnector.setTarget(assignmentTable);
        assignmentReplaceConnector.setSource(assignmentTable);
        assignmentRefreshConnector.setTarget(assignmentTable);

        ComponentInstance assignmentIdCtrl = initService.form2().initFormNumber("ID", "id", null, false, false, true, -1d, null, null, null);

        ComponentInstance assignmentEmployeeCtrl = initService.form2().initFormDropdown("Employee*", "employee", null, true, false, false, null, employeeHierarchyDefinition, true, "absence-employee-control");
        ComponentInstance assignmentProjectCtrl = initService.form2().initFormDropdown("Project*", "project", null, true, false, false, null, projectHierarchyDefinition, true, "absence-project-control");

        ComponentInstance assignmentStartDateCtrl = initService.form2().initFormDateChooser("Start of operation*", "assignment-date start-date", "startDate", null, false, false, false, "", "", false);
        ComponentInstance assignmentEndDateCtrl = initService.form2().initFormDateChooser("End of operation*", "assignment-date end-date", "endDate", null, false, false, false, "", "", false);

        ComponentInstance assignmentWeeklyHoursCtrl = initFormNumber("Weekly Hours*", "weeklyHours", null, true, false, false, null, 0d, 40d, "", "weekly-hours");
        ComponentInstance assignmentDailyRateCtrl = initFormNumber("Daily Rate in €*", "dailyRate", null, true, false, false, null, 0d, null, "", "daily-rate");

        ComponentInstance assignmentMondayHoursCtrl = initFormNumber("Monday Hours*", "mondayHours", null, true, false, false, null, 0d, 24d, "", "daily-hours monday-hours");
        ComponentInstance assignmentTuesdayHoursCtrl = initFormNumber("Tuesday Hours*", "tuesdayHours", null, true, false, false, null, 0d, 24d, "", "daily-hours tuesday-hours");
        ComponentInstance assignmentWednesdayHoursCtrl = initFormNumber("Wednesday Hours*", "wednesdayHours", null, true, false, false, null, 0d, 24d, "", "daily-hours wednesday-hours");
        ComponentInstance assignmentThursdayHoursCtrl = initFormNumber("Thursday Hours*", "thursdayHours", null, true, false, false, null, 0d, 24d, "", "daily-hours thursday-hours");
        ComponentInstance assignmentFridayHoursCtrl = initFormNumber("Friday Hours*", "fridayHours", null, true, false, false, null, 0d, 24d, "", "daily-hours friday-hours");
        ComponentInstance assignmentDailyHoursFlowPanel = initService.panel().initFlowPanel("", "daily-hours-panel", false, false, false, assignmentMondayHoursCtrl, assignmentTuesdayHoursCtrl, assignmentWednesdayHoursCtrl, assignmentThursdayHoursCtrl, assignmentFridayHoursCtrl);

        ComponentInstance assignmentFormRow1 = initService.panel().initFlowPanel("", "flow-row", false, false, false, assignmentEmployeeCtrl);
        ComponentInstance assignmentFormRow2 = initService.panel().initFlowPanel("", "flow-row", false, false, false, assignmentProjectCtrl);
        ComponentInstance assignmentFormRow3 = initService.panel().initFlowPanel("", "flow-row", false, false, false, assignmentStartDateCtrl, assignmentEndDateCtrl);
        ComponentInstance assignmentFormRow4 = initService.panel().initFlowPanel("", "flow-row", false, false, false, assignmentWeeklyHoursCtrl, assignmentDailyRateCtrl);
        ComponentInstance assignmentFormRow5 = initService.panel().initFlowPanel("", "flow-row", false, false, false, assignmentDailyHoursFlowPanel);

        ComponentInstance assignmentFormControlFlowPanel = initService.panel().initFlowPanel("employeeFormControlFlowPanel", "v7-form-shadow", true, false, false,
                assignmentFormRow1,
                assignmentFormRow2,
                assignmentFormRow3,
                assignmentFormRow4,
                assignmentFormRow5);

        SynchronizationIdentifier assignmentFormGroupSyncId = initService.form2().initSyncId();
        AssignmentValidatorState validator = new AssignmentValidatorState();
        validatorRepo.save(validator);
        ComponentInstance assignmentFormGroup = initService.form2().initFormGroup("Assignment", "assignment", "assignment-form assignment-form", Arrays.asList(validator), null, false, assignmentFormGroupSyncId, "v7-assignment-assignment", true, assignmentFormControlFlowPanel);

        ComponentInstance saveButton = initService.initButton("Save", "Saves assignment", "btn btn-md btn-primary", null, "ctrl+s", false);
        initService.connector().initEnableConnector("Enable Save Button", assignmentFormGroup, saveButton, Boolean.TRUE, ConnectorState.TRIGGEREVENT_VALIDATED);
        initService.connector().initEnableConnector("Disable Save Button", assignmentFormGroup, saveButton, Boolean.FALSE, ConnectorState.TRIGGEREVENT_INVALIDATED);

        ConnectorState saveCloseDialogConnectorState = initService.connector().initShowConnector("close dialog after save", saveButton, overlayPanel, null, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        saveCloseDialogConnectorState.setExecutionOrder(0);
        ConnectorState saveFormConnectorState = initService.connector().initFormSaveConnectorState("save", saveButton, assignmentFormGroup, ConnectorState.TRIGGEREVENT_ACTION);
        saveFormConnectorState.setExecutionOrder(100);
        ConnectorState saveRefreshTableConnectorState = initService.connector().initRefreshConnector("refresh assignment table", saveButton, assignmentTable, ConnectorState.TRIGGEREVENT_ACTION);
        saveRefreshTableConnectorState.setExecutionOrder(200);
        ConnectorState saveResetFormConnectorState = initService.connector().initFormResetConnectorState("Reset assignment form group", saveButton, assignmentFormGroupSyncId, ConnectorState.TRIGGEREVENT_ACTION);
        saveResetFormConnectorState.setExecutionOrder(300);

        ComponentInstance cancelButton = initService.initButton("", "Cancel", "btn btn-md", "close", "esc");
        ConnectorState cancelCloseDialogState = initService.connector().initShowConnector("OK Save Dialog", cancelButton, overlayPanel, null, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        ConnectorState cancelResetFormConnectorState = initService.connector().initFormResetConnectorState("Reset assignment form group", cancelButton, assignmentFormGroupSyncId, ConnectorState.TRIGGEREVENT_ACTION);

        ComponentInstance assignmentDialogLabel = initService.initLabel("Assignment", "", "dialog-label v7-font-p-strong", "");
        ComponentInstance topBar = initService.panel().initFlowPanel("Top-ButtonBar", "panel-dialog-header", false, true, false, assignmentDialogLabel, cancelButton);
        ComponentInstance bottomBar = initService.panel().initFlowPanel("Bottom-ButtonBar", "panel-dialog-footer", false, true, false, saveButton);
        ComponentInstance rightContent = initService.panel().initFlowPanel("", "v7-background-center-color", true, false, false, topBar, assignmentFormGroup, bottomBar);
        ComponentInstance dialogPanel = initService.panel().initCenterPanel("Panel Dialog", "panel-dialog", rightContent);

        initService.connector().initShowConnector("Open Save Dialog", addAssignmentButton, overlayPanel, dialogPanel, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);

        initService.connector().initFetchConnector("Fetch assignment dialog components", assignmentTable,
                Arrays.asList(dialogPanel, assignmentIdCtrl,
                        assignmentEmployeeCtrl,
                        assignmentProjectCtrl,
                        assignmentStartDateCtrl,
                        assignmentEndDateCtrl,
                        assignmentFormRow1,
                        assignmentFormRow2,
                        assignmentFormRow3,
                        assignmentFormRow4,
                        assignmentFormRow5,
                        assignmentWeeklyHoursCtrl,
                        assignmentDailyRateCtrl,
                        assignmentMondayHoursCtrl,
                        assignmentTuesdayHoursCtrl,
                        assignmentWednesdayHoursCtrl,
                        assignmentThursdayHoursCtrl,
                        assignmentFridayHoursCtrl,
                        assignmentDailyHoursFlowPanel,
                        assignmentFormControlFlowPanel,
                        assignmentFormGroup,
                        cancelButton,
                        saveButton), ConnectorState.TRIGGEREVENT_COMPONENT_INITIALIZED);

        initService.connector().initFormSetValueByIdentifierAndServiceConnectorState("Assignment Form Values Connector", assignmentTable,
                assignmentFormGroupSyncId, "v7-assignment-assignment", "EDIT");
        initService.connector().initShowConnector("Open Save Dialog", assignmentTable, overlayPanel, dialogPanel, "OVERLAY", "EDIT");

        setupDeletion(assignmentTable, overlayPanel);

//        ComponentInstance lrPanel = initService.panel().initLeftRightPanel("test", "", dateChooserAssignment, null);
//        ComponentInstance flowPanel = initService.panel().initFlowPanel("test", "", true, false, false, lrPanel, assignmentTable);

        return initService.panel().initCenterPanel("Assignment Table Center Panel", "assignment-table-center-panel", assignmentTable);
    }

    public ComponentInstance createProjectPage(ComponentInstance overlayPanel, Field tableMainKPI, Field tableIdField, List<Field> tableFields, List<Field> tableFixedFields,
                                               HierarchyDefinition employeeHierarchyDefinition, HierarchyDefinition customerHierarchyDefinition) {

        ComponentInstance addProjectButton = initService.initButton("Add Project", "Add project", "btn btn-md btn-primary", "add", "", true);
        ComponentInstance addCustomerButtonFlowPanel = initService.panel().initFlowPanel("Add Project Flow Panel", "flex-nowrap table-view-button-bar", false, true, false, addProjectButton);
        ComponentInstance buttonsBar = initService.panel().initCenterPanel("Buttons Bar", "panel-buttonbar-center", addCustomerButtonFlowPanel);

        ComponentInstance projectTable = initAssignmentHelperService.initTableWithCustomColumn("Projects", null, "default-content-size project-table custom-col-table",
                null, null, 12, tableMainKPI, tableIdField, tableFields,
                initService.addDefaultMetadata(null),
                true, tableFixedFields);

        ComponentInstance projectKeyCtrl = initService.form2().initFormText("Project Key*", "projectKey", null, true, false, false, "", "CUSTOMER_COUNTRY_YEAR_PRODUCT_CONTRACTTYPE_NUMBER", "", 64, "project-key-control");
        ComponentInstance projectNameCtrl = initService.form2().initFormText("Project Name*", "projectName", null, true, false, false, "", "", "", 64, "project-name-control");
        ComponentInstance customerProjectNameCtrl = initService.form2().initFormText("Customer Project Name", "customerProjectName", null, false, false, false, "", "", "", 64, "customer-project-name-control");

        HierarchySimpleDefinition projectTypeHierarchy = initService.hierarchy().initHierarchySimpleDefinition("Project Type Hierarchy", new SimpleHierarchyItem("root", "root",
                new SimpleHierarchyItem("Extern", "Extern"),
                new SimpleHierarchyItem("Intern", "Intern"),
                new SimpleHierarchyItem("Offering", "Offering")));
        ComponentInstance projectTypeCtrl = initService.form2().initFormDropdown("Project Type*", "projectType", null, true, false, false, null, projectTypeHierarchy, true, "project-type-control");

        ComponentInstance benefitRecipientCtrl = initService.form2().initFormDropdown("Benefit Recipient*", "benefitRecipient", null, true, false, false, null, customerHierarchyDefinition, true, "project-benefit-recipient-control");
        ComponentInstance invoiceRecipientCtrl = initService.form2().initFormDropdown("Invoice Recipient*", "invoiceRecipient", null, true, false, false, null, customerHierarchyDefinition, true, "project-invoice-recipient-control");

        ComponentInstance startDateCtrl = initService.form2().initFormDateChooser("Start Date*", "project-start-date-control", "startDate", null, true, false, false, "", "", false);
        ComponentInstance endDateCtrl = initService.form2().initFormDateChooser("End Date*", "project-end-date-control", "endDate", null, true, false, false, "", "", false);
        ComponentInstance projectLeaderCtrl = initService.form2().initFormDropdown("Project Leader*", "projectLeader", null, true, false, false, null, employeeHierarchyDefinition, true, "project-leader-control");

        HierarchySimpleDefinition contractTypeHierarchy = initService.hierarchy().initHierarchySimpleDefinition("Project Contract Type Hierarchy", new SimpleHierarchyItem("root", "root",
                new SimpleHierarchyItem("Maintenance", "Maintenance"),
                new SimpleHierarchyItem("Service", "Service")));
        ComponentInstance contractTypeCtrl = initService.form2().initFormDropdown("Contract Type*", "contractType", null, true, false, false, null, contractTypeHierarchy, true, "project-contract-type-control");

        ComponentInstance salesCtrl = initService.form2().initFormNumber("Sales*", "sales", null, true, false, false, null, null, null, "");
        ComponentInstance notesCtrl = initService.form2().initFormTextarea("Notes", "notes", null, false, false, false, null, null, 256, 4, "project-notes-control");

        ComponentInstance projectFormRow1 = initService.panel().initFlowPanel("", "flow-row", false, false, false, projectKeyCtrl, projectNameCtrl, customerProjectNameCtrl, projectTypeCtrl);
        ComponentInstance projectFormRow2 = initService.panel().initFlowPanel("", "flow-row", false, false, false, benefitRecipientCtrl, invoiceRecipientCtrl, contractTypeCtrl);
        ComponentInstance projectFormRow3 = initService.panel().initFlowPanel("", "flow-row", false, false, false, projectLeaderCtrl, startDateCtrl, endDateCtrl, salesCtrl);
        ComponentInstance projectFormRow4 = initService.panel().initFlowPanel("", "flow-row", false, false, false, notesCtrl);

        ComponentInstance projectFormControlFlowPanel = initService.panel().initFlowPanel("projectFormControlFlowPanel", "v7-form-shadow", true, false, false,
                projectFormRow1,
                projectFormRow2,
                projectFormRow3,
                projectFormRow4
        );

        SynchronizationIdentifier projectFormGroupSyncId = initService.form2().initSyncId();
        ComponentInstance projectFormGroup = initService.form2().initFormGroup("Project", "project", "assignment-form project-form", null, null, false, projectFormGroupSyncId, "v7-assignment-project", true, projectFormControlFlowPanel);

        ComponentInstance saveButton = initService.initButton("Save", "Saves project", "btn btn-md btn-primary", null, "ctrl+s", false);
        initService.connector().initEnableConnector("Enable Save Button", projectFormGroup, saveButton, Boolean.TRUE, ConnectorState.TRIGGEREVENT_VALIDATED);
        initService.connector().initEnableConnector("Disable Save Button", projectFormGroup, saveButton, Boolean.FALSE, ConnectorState.TRIGGEREVENT_INVALIDATED);

        ConnectorState saveCloseDialogConnectorState = initService.connector().initShowConnector("close dialog after save", saveButton, overlayPanel, null, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        saveCloseDialogConnectorState.setExecutionOrder(0);
        ConnectorState saveFormConnectorState = initService.connector().initFormSaveConnectorState("save", saveButton, projectFormGroup, ConnectorState.TRIGGEREVENT_ACTION);
        saveFormConnectorState.setExecutionOrder(100);
        ConnectorState saveRefreshTableConnectorState = initService.connector().initRefreshConnector("refresh project table", saveButton, projectTable, ConnectorState.TRIGGEREVENT_ACTION);
        saveRefreshTableConnectorState.setExecutionOrder(200);
        ConnectorState saveResetFormConnectorState = initService.connector().initFormResetConnectorState("Reset form group", saveButton, projectFormGroupSyncId, ConnectorState.TRIGGEREVENT_ACTION);
        saveResetFormConnectorState.setExecutionOrder(300);

        ComponentInstance cancelButton = initService.initButton("", "Cancel", "btn btn-md", "close", "esc");
        ConnectorState cancelCloseDialogState = initService.connector().initShowConnector("OK Save Dialog", cancelButton, overlayPanel, null, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        initService.connector().initFormResetConnectorState("Reset form group", saveButton, projectFormGroupSyncId, ConnectorState.TRIGGEREVENT_ACTION);

        ComponentInstance projectDialogLabel = initService.initLabel("Project", "", "dialog-label v7-font-p-strong", "");
        ComponentInstance topBar = initService.panel().initFlowPanel("Top-ButtonBar", "panel-dialog-header", false, true, false, projectDialogLabel, cancelButton);
        ComponentInstance bottomBar = initService.panel().initFlowPanel("Bottom-ButtonBar", "panel-dialog-footer", false, true, false, saveButton);
        ComponentInstance rightContent = initService.panel().initFlowPanel("", "v7-background-center-color", true, false, false, topBar, projectFormGroup, bottomBar);
        ComponentInstance dialogPanel = initService.panel().initCenterPanel("Panel Dialog", "panel-dialog", rightContent);

        initService.connector().initShowConnector("Open Save Dialog", addProjectButton, overlayPanel, dialogPanel, "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);

        ComponentInstance page = initService.panel().initCenterPanel("Project table center panel", "v7-assignment-tool-full-page", projectTable);

        initService.connector().initFetchConnector("Fetch project dialog components", page,
                Arrays.asList(dialogPanel, projectKeyCtrl,
                        projectNameCtrl,
                        projectTypeCtrl,
                        benefitRecipientCtrl,
                        invoiceRecipientCtrl,
                        startDateCtrl,
                        endDateCtrl,
                        projectLeaderCtrl,
                        contractTypeCtrl,
                        salesCtrl,
                        notesCtrl,
                        projectFormGroup,
                        projectFormControlFlowPanel,
                        projectFormRow1,
                        projectFormRow2,
                        projectFormRow3,
                        projectFormRow4,
                        cancelButton,
                        saveButton), ConnectorState.TRIGGEREVENT_COMPONENT_INITIALIZED);

        initService.connector().initFormSetValueByIdentifierAndServiceConnectorState("Project Form Values Connector", projectTable,
                projectFormGroupSyncId, "v7-assignment-project", "EDIT");
        initService.connector().initShowConnector("Open Save Dialog", projectTable, overlayPanel, dialogPanel, "OVERLAY", "EDIT");

        setupDeletion(projectTable, overlayPanel);

        return initService.panel().initTopBottomPanel("Project Top Bottom Panel", "", page, buttonsBar);
    }

    public ComponentInstance createCompassReportViewPage(DataFormat<Number> defaultNumberFormat) {

        ComponentInstance compassReportsHistoryPool = initService.initHistoryPool("Reports", null, true, "compassreports", null, false, true);
        ComponentInstance historyPoolFlowPanel = initService.panel().initFlowPanel("Report tools", "panel-edge default-sidebar-height", true, false, true, compassReportsHistoryPool);

        CompassReport report = new CompassReport();
        List<CompassReportSheet> sheets = new ArrayList<>();
        report.setSheets(sheets);
        report.setCaption("Neuer Bericht");
        report.setDefaultNumberFormat(defaultNumberFormat);

        report.setReferenceDate(LocalDate.of(2017,12,31));
        report.setUseReportDate(Boolean.FALSE);

        CompassState compassState = new CompassState();
        compassState.setCaption("compass component for report view page");
        compassState.setShowDesignerElements(false);
        compassState.setCompassReport(report);
        compassState.setCssClass("default-content-size");

        ComponentInstance compassComponent = initService.initComponentState(compassState, "CompassComponent", "", null);

        initService.connector().initSetCompassReportConnector("Restore compass report from history pool", true, compassReportsHistoryPool, compassComponent, ConnectorState.TRIGGEREVENT_ACTION);

        ComponentInstance reportDesignerCenterPanel = initService.panel().initCenterPanel("Show compass report", null, compassComponent);
        ComponentInstance page = initService.panel().initCollapsibleLeftRightPanel("Left Right Panel cMDPage", "v7-compass-report-designer-page", true, historyPoolFlowPanel, reportDesignerCenterPanel);

        return page;
    }

    public ComponentInstance initFormNumber(String caption, String elementName, SynchronizationIdentifier syncId, boolean required, boolean disabled, boolean hidden, Double initValue, Double min, Double max, String placeholder, String cssClass) {
        DynamicFormControlNumberState state = new DynamicFormControlNumberState();
        state.setFormElementName(elementName);
        state.setRequired(required);
        state.setEnabled(!disabled);
        state.setHidden(hidden);
        state.setInitValue(initValue);
        state.setMinValue(min);
        state.setMaxValue(max);
        state.setPlaceholder(placeholder);
        state.setCssClass(cssClass);
        if (syncId != null) {
            state.setSyncId(syncId);
        }

        return initService.initComponentState(state, caption, (String)null, (Category)null);
    }

    public ComponentInstance createCompassPage(CompassReport report) {
        CompassState compassState = new CompassState();
        compassState.setCaption("Compass Component");
        compassState.setShowDesignerElements(false);
        compassState.setCompassReport(report);
        compassState.setCssClass("v7-assignment-compass-report");

        ComponentInstance compassComponent = initService.initComponentState(compassState, "Compass Component", "", null);

        ComponentInstance buttonBar = createSaveButtonBar(Arrays.asList(compassComponent), null, null, false, null, false);
        ComponentInstance compassPage = initService.panel().initTopBottomPanel("Compass Component Top Bottom Panel", "v7-compass-full-page", compassComponent, buttonBar);

        return compassPage;
    }

    public ComponentInstance createCompassPageWithFilters(List<ComponentInstance> compassFilterComponents, CompassReport report) {

        CompassState compassState = new CompassState();
        compassState.setCaption("CompassComponent");
        compassState.setShowDesignerElements(false);
        compassState.setCompassReport(report);
        compassState.setCssClass("default-content-size");

        ComponentInstance compassComponent = initService.initComponentState(compassState, "CompassComponent", "", null);

        ComponentInstance compassFlowPanel = initService.panel().initFlowPanel("CompassFlowPanel", "panel-edge default-sidebar-height", true, true, false, compassFilterComponents.toArray(new ComponentInstance[compassFilterComponents.size()]));
        for (ComponentInstance filterComponent : compassFilterComponents) {
            initService.connector().initAddConditionConnector("Apply filter on compass", filterComponent, compassComponent, ConnectorState.TRIGGEREVENT_ACTION);
        }

        ComponentInstance compassLeftRightPage = initService.panel().initCollapsibleLeftRightPanel("CompassChart", "", true, compassFlowPanel, compassComponent);

        ComponentInstance buttonBar = createSaveButtonBar(Arrays.asList(compassComponent), null, null, false, null, false);
        ComponentInstance compassPage = initService.panel().initTopBottomPanel("Kompass", null, compassLeftRightPage, buttonBar);

        return compassPage;
    }


    public ComponentInstance createCompass2Page(ComponentInstance report) {
        ComponentInstance buttonBar = createSaveButtonBar(Arrays.asList(report), null, null, false, null, false);
        ComponentInstance compassPage = initService.panel().initTopBottomPanel("Compass Component Top Bottom Panel", "v7-compass-full-page", report, buttonBar);

        return compassPage;
    }

    public ComponentInstance createCompass2PageWithFilters(List<ComponentInstance> compassFilterComponents, ComponentInstance report) {
        ComponentInstance compassFlowPanel = initService.panel().initFlowPanel("CompassFlowPanel", "panel-edge default-sidebar-height", true, true, false, compassFilterComponents.toArray(new ComponentInstance[compassFilterComponents.size()]));
        for (ComponentInstance filterComponent : compassFilterComponents) {
            initService.connector().initAddConditionConnector("Apply filter on compass", filterComponent, report, ConnectorState.TRIGGEREVENT_ACTION);
        }

        ComponentInstance compassLeftRightPage = initService.panel().initCollapsibleLeftRightPanel("CompassChart", "", true, compassFlowPanel, report);

        ArrayList<ComponentInstance> resetComponents = new ArrayList<>();
        resetComponents.add(report);
        resetComponents.addAll(compassFilterComponents);

        ComponentInstance buttonBar = createSaveButtonBar(resetComponents, null, null, false, null, false);
        ComponentInstance compassPage = initService.panel().initTopBottomPanel("Kompass", null, compassLeftRightPage, buttonBar);

        return compassPage;
    }

    private void setupDeletion(ComponentInstance table, ComponentInstance overlayPanel) {

        ComponentInstance closeButton = initService.initButton("", "Cancel", "btn btn-md", "close", "esc");
        ComponentInstance cancelButton = initService.initButton("Cancel", "", "btn btn-md btn-secondary", "", "esc");
        //TODO: icon?
        ComponentInstance confirmButton = initService.initButton("Delete", "", "btn btn-md btn-secondary", "", null);

        ComponentInstance panelButtons = panelInitializerService.initFlowPanel("", "mbd-fzg-delete-panel", false, true, false, confirmButton, cancelButton);

        ComponentInstance label = initService.initLabel("", null, "v7-font-h2 mbd-column-selection-hint-label", null);

        ComponentInstance dialogCaptionLabel = initService.initLabel("Confirm Deletion", "", "dialog-label v7-font-p-strong", "");
        ComponentInstance topBar = initService.panel().initFlowPanel("Top-ButtonBar", "panel-dialog-header", false, true, false, dialogCaptionLabel, closeButton);
        ComponentInstance bottomBar = initService.panel().initFlowPanel("Bottom-ButtonBar", "panel-dialog-footer", false, true, false, cancelButton, confirmButton);
        ComponentInstance rightContent = initService.panel().initFlowPanel("", "v7-background-center-color", true, false, false, topBar, label, bottomBar);
        ComponentInstance dialogPanel = initService.panel().initCenterPanel("Panel Dialog", "panel-dialog", rightContent);

        initAssignmentHelperService.initDynamicLabelMessageConnector("", table, label, "PASS_DELETION_INFO_TO_DIALOG");

//        ComponentInstance dialogPanel = panelInitializerService.initTopBottomPanel("Zuordnung löschen",
//                "col-auto panel-dialog mbd-scrollable-overlay-panel", label, panelButtons);

        connectorInitializerService.initShowConnector("Show Deletion Panel", table, overlayPanel, dialogPanel,
                "OVERLAY", "SHOW_DELETE_REQUEST_DIALOG");

        connectorInitializerService.initShowConnector("Close Deletion Panel", cancelButton, overlayPanel, null,
                "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        connectorInitializerService.initShowConnector("Close Deletion Panel", closeButton, overlayPanel, null,
                "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        initAssignmentHelperService.initDialogConfirmConnector("Confirm Deletion Dialog", confirmButton, table, ConnectorState.TRIGGEREVENT_ACTION);
        connectorInitializerService.initShowConnector("Close Deletion Panel", confirmButton, overlayPanel, null,
                "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);

/*        ComponentInstance successButton = initService.initButton("OK", "",
                "btn btn-md btn-secondary", "", "esc");
        ComponentInstance panelButtons2 = panelInitializerService.initFlowPanel("",
                "mbd-fzg-delete-panel", false, true, false, successButton);
        ComponentInstance successLabel = initService.initLabel("ADMA Zuordnung wurde erfolgreich gelöscht.",
                null, "v7-font-h2 mbd-column-selection-hint-label", null);
        ComponentInstance successPanel = panelInitializerService.initTopBottomPanel("",
                "col-auto panel-dialog mbd-scrollable-overlay-panel", successLabel, panelButtons2);*/
/*        connectorInitializerService.initShowConnector("Close Success Panel", successButton, overlayPanel, null,
                "OVERLAY", ConnectorState.TRIGGEREVENT_ACTION);
        connectorInitializerService.initShowConnector("Close Deletion Panel", table, overlayPanel, successPanel,
                "OVERLAY", "SHOW_ADMA_DELETION_SUCCESS_DIALOG");*/

        //}
    }

    public ComponentInstance createSaveButtonBar(List<ComponentInstance> resetComponents,
                                                 List<SynchronizationIdentifier> resetFormIdentifiers,
                                                 List<ComponentInstance> additionalButtons,
                                                 boolean resetWithSelection,
                                                 ConnectorState saveConnector,
                                                 boolean saveButtonEnabled
    ) {

        if (additionalButtons == null) {
            additionalButtons = new ArrayList<>();
        }
        if ((resetComponents != null && !resetComponents.isEmpty())
                || (resetFormIdentifiers != null && !resetFormIdentifiers.isEmpty())){
            ComponentInstance revert = initService.initButton("Reset", "Reset", "btn btn-md btn-secondary icon-first", "undo", null);
            if(resetComponents != null) {
                resetComponents.forEach((resetComponent) -> {
                    initService.connector().initResetConnector("Reset " + resetComponent.getCaption(), revert, resetComponent, resetWithSelection, ConnectorState.TRIGGEREVENT_ACTION);
                });
            }
            if(resetFormIdentifiers != null) {
                resetFormIdentifiers.forEach((resetFormIdentifier) -> {
                    initService.connector().initFormResetConnectorState("Reset Form Control" + resetFormIdentifier.getIdentifier(), revert, resetFormIdentifier, ConnectorState.TRIGGEREVENT_ACTION);
                });
            }
            additionalButtons.add(0, revert);
        }

        ComponentInstance buttons = initService.panel().initFlowPanel("Buttonbar", "flex-nowrap", false, true, false,
                initService.panel().initFlowPanel("", "panel-buttonbar-center", false, true, false,
                        additionalButtons.toArray(new ComponentInstance[additionalButtons.size()])));

        if (saveConnector != null) {
            String caption = (saveConnector.getCaption().isEmpty() ? "Speichern" : saveConnector.getCaption());
            String decription = (saveConnector.getDescription() == null || saveConnector.getDescription().isEmpty()
                    ? "Save"
                    : saveConnector.getDescription());
            String icon = (saveConnector.getIconClass() == null || saveConnector.getIconClass().isEmpty()
                    ? "chevron_right"
                    : saveConnector.getIconClass());
            ComponentInstance save = initService.initButton(caption, decription, "btn btn-md btn-primary", icon, null, saveButtonEnabled);
            saveConnector.setSource(save);
            ((FlowPanelState) buttons.getDefaultState()).addComponent(save);
        }
        return buttons;
    }
}
