package de.profect.inithelper;

import de.profect.component.dataview.compass.kpi.CompassKpi;
import de.profect.component.dataview.compass.report.CompassReport;
import de.profect.component.dataview.compass.report.CompassReportRepository;
import de.profect.component.dataview.compass.report.CompassReportSheet;
import de.profect.component.dataview.compass.reportelements.*;
import de.profect.data.hierarchy.databasehierarchy.HierarchyDatabaseDefinition;
import de.profect.data.hierarchy.databasehierarchy.HierarchyLevelDefinition;
import de.profect.data.query.field.dataformat.DataFormat;
import de.profect.data.query.timereference.Timereference;
import de.profect.data.query.timereference.elements.*;
import de.profect.util.InitHelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.*;

@Service
public class InitAssignmentReportsService {

    @Autowired
    private CompassReportRepository compassReportRepository;

    @Autowired
    private InitHelperService initService;

    public CompassReport createStaffWorkloadReport(
            CompassKpi employeeQuotaKpi, CompassKpi employeeActualKpi, CompassKpi employeeVacancyKpi,
            HierarchyDatabaseDefinition employeeHierarchy, HierarchyLevelDefinition employeeLevelDefinition,
            HierarchyDatabaseDefinition projectHierarchy, HierarchyLevelDefinition projectLevelDefinition,
            DataFormat<Number> defaultNumberFormat) {
        CompassRootElement headerRootElement = new CompassRootElement();
        headerRootElement.setCaption("Root");

        LocalDate week = LocalDate.of(2020, Month.JANUARY, 1);
        if(week.getDayOfWeek() != DayOfWeek.THURSDAY) {
            week = week.with(TemporalAdjusters.next(DayOfWeek.THURSDAY));
        }
        week = week.minusDays(3);
        List<CompassReportElement> quotaElements = new ArrayList<>();
        List<CompassReportElement> vacancyElements = new ArrayList<>();
        while(week.isBefore(LocalDate.of(2020, 12,31))) {
            TemporalField woy = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();
            int weekNumber = week.get(woy);

            FixDay startDay = new FixDay();
            startDay.setCaption("Monday CW " + weekNumber);
            startDay.setDay(week.getDayOfMonth());
            FixMonth startMonth = new FixMonth();
            startMonth.setCaption("" + week.getMonthValue());
            startMonth.setMonth(week.getMonthValue());
            FixYear startYear = new FixYear();
            startYear.setCaption("" + week.getYear());
            startYear.setYear(week.getYear());

            FixDay endDay = new FixDay();
            endDay.setCaption("Monday CW " + weekNumber);
            endDay.setDay(week.getDayOfMonth());
            FixMonth endMonth = new FixMonth();
            endMonth.setCaption("" + week.getMonthValue());
            endMonth.setMonth(week.getMonthValue());
            FixYear endYear = new FixYear();
            endYear.setCaption("" + week.getYear());
            endYear.setYear(week.getYear());

            String timeRefCaption = "CW " + weekNumber + " (" + week.getDayOfMonth() + "." + week.getMonthValue() + ". - " + week.plusDays(6).getDayOfMonth() + "." +  week.plusDays(6).getMonthValue() + "." + week.plusDays(6).getYear() + ")";
            Timereference cw = initService.timereference().initTimereference(timeRefCaption, startYear, startMonth, startDay, endYear, endMonth, endDay);

            CompassTimereferenceElement cwTimeReferenceElement = new CompassTimereferenceElement();
            cwTimeReferenceElement.setCaption(timeRefCaption);
            cwTimeReferenceElement.setTimereference(cw);
            headerRootElement.addChild(cwTimeReferenceElement);

//            CompassSpaceElement spaceElement = new CompassSpaceElement();
//            spaceElement.setCaption("");
//            headerRootElement.addChild(spaceElement);

            CompassKpiElement employeeQuotaKpiElement = new CompassKpiElement();
            employeeQuotaKpiElement.setCaption("QUOTA");
            employeeQuotaKpiElement.setCompassKpi(employeeQuotaKpi);
            cwTimeReferenceElement.addChild(employeeQuotaKpiElement);

            quotaElements.add(employeeQuotaKpiElement);

            CompassKpiElement employeeActualKpiElement = new CompassKpiElement();
            employeeActualKpiElement.setCaption("ACTUAL");
            employeeActualKpiElement.setCompassKpi(employeeActualKpi);
            cwTimeReferenceElement.addChild(employeeActualKpiElement);

            CompassKpiElement employeeVacancyKpiElement = new CompassKpiElement();
            employeeVacancyKpiElement.setCaption("VACANCY");
            employeeVacancyKpiElement.setCompassKpi(employeeVacancyKpi);
            cwTimeReferenceElement.addChild(employeeVacancyKpiElement);

            vacancyElements.add(employeeVacancyKpiElement);

            week = week.plusWeeks(1);
        }

        CompassRootElement treeRootElement = new CompassRootElement();
        treeRootElement.setCaption("Root");

        CompassTextElement totalElement = new CompassTextElement();
        totalElement.setCaption("Total");
        treeRootElement.addChild(totalElement);

        CompassCustomLoadingElement employeeLoadingElement = new CompassCustomLoadingElement();
        employeeLoadingElement.setCaption("Loading employees...");
        employeeLoadingElement.setHierarchyDatabaseDefinition(employeeHierarchy);
        employeeLoadingElement.setHierarchyLevelDefinition(employeeLevelDefinition);
        totalElement.addChild(employeeLoadingElement);

        CompassCustomLoadingElement projectLoadingElement = new CompassCustomLoadingElement();
        projectLoadingElement.setCaption("Loading projects...");
        projectLoadingElement.setHierarchyDatabaseDefinition(projectHierarchy);
        projectLoadingElement.setHierarchyLevelDefinition(projectLevelDefinition);
        employeeLoadingElement.addChild(projectLoadingElement);

        CompassReportSheet reportSheet = new CompassReportSheet();
        reportSheet.setCaption("Sheet 1");
        reportSheet.setHeaderRoot(headerRootElement);
        reportSheet.setTreeRoot(treeRootElement);

        CompassReport report = new CompassReport();
        List<CompassReportSheet> sheets = new ArrayList<>();
        sheets.add(reportSheet);
        report.setSheets(sheets);
        report.setCaption("Workload per staff");
        report.setUseReportDate(true);
        report.setReferenceDate(LocalDate.of(2017,12,31));
        report.setDefaultNumberFormat(defaultNumberFormat);
        report.setAllowLazyLoading(false);

        report.setMetadataContainer(initService.addDefaultMetadata(report.getMetadataContainer()));

        CompassReport savedReport = compassReportRepository.save(report);

        Set<String> disabledCells = new HashSet<>();
        for(CompassReportElement reportElement : quotaElements) {
            disabledCells.add(projectLoadingElement.getIdPath() + "|" + reportElement.getIdPath());
        }
        for(CompassReportElement reportElement : vacancyElements) {
            disabledCells.add(projectLoadingElement.getIdPath() + "|" + reportElement.getIdPath());
        }
        reportSheet.setDisabledCells(disabledCells);

        return compassReportRepository.save(savedReport);
    }

    public CompassReport createTeamWorkloadReport(
            CompassKpi employeeQuotaKpi, CompassKpi employeeActualKpi, CompassKpi employeeVacancyKpi,
            HierarchyDatabaseDefinition employeeHierarchy, HierarchyLevelDefinition employeeLevelDefinition,
            HierarchyDatabaseDefinition teamHierarchy, HierarchyLevelDefinition teamLevelDefinition,
            HierarchyDatabaseDefinition projectHierarchy, HierarchyLevelDefinition projectLevelDefinition,
            DataFormat<Number> defaultNumberFormat) {
        CompassRootElement headerRootElement = new CompassRootElement();
        headerRootElement.setCaption("Root");

        LocalDate week = LocalDate.of(2020, Month.JANUARY, 1);
        if(week.getDayOfWeek() != DayOfWeek.THURSDAY) {
            week = week.with(TemporalAdjusters.next(DayOfWeek.THURSDAY));
        }
        week = week.minusDays(3);
        List<CompassReportElement> quotaElements = new ArrayList<>();
        List<CompassReportElement> vacancyElements = new ArrayList<>();
        while(week.isBefore(LocalDate.of(2020, 12,31))) {
            TemporalField woy = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();
            int weekNumber = week.get(woy);

            FixDay startDay = new FixDay();
            startDay.setCaption("Monday CW " + weekNumber);
            startDay.setDay(week.getDayOfMonth());
            FixMonth startMonth = new FixMonth();
            startMonth.setCaption("" + week.getMonthValue());
            startMonth.setMonth(week.getMonthValue());
            FixYear startYear = new FixYear();
            startYear.setCaption("" + week.getYear());
            startYear.setYear(week.getYear());

            FixDay endDay = new FixDay();
            endDay.setCaption("Monday CW " + weekNumber);
            endDay.setDay(week.getDayOfMonth());
            FixMonth endMonth = new FixMonth();
            endMonth.setCaption("" + week.getMonthValue());
            endMonth.setMonth(week.getMonthValue());
            FixYear endYear = new FixYear();
            endYear.setCaption("" + week.getYear());
            endYear.setYear(week.getYear());

            String timeRefCaption = "CW " + weekNumber + " (" + week.getDayOfMonth() + "." + week.getMonthValue() + ". - " + week.plusDays(6).getDayOfMonth() + "." +  week.plusDays(6).getMonthValue() + "." + week.plusDays(6).getYear() + ")";
            Timereference cw = initService.timereference().initTimereference(timeRefCaption, startYear, startMonth, startDay, endYear, endMonth, endDay);

            CompassTimereferenceElement cwTimeReferenceElement = new CompassTimereferenceElement();
            cwTimeReferenceElement.setCaption(timeRefCaption);
            cwTimeReferenceElement.setTimereference(cw);
            headerRootElement.addChild(cwTimeReferenceElement);

//            CompassSpaceElement spaceElement = new CompassSpaceElement();
//            spaceElement.setCaption("");
//            headerRootElement.addChild(spaceElement);

            CompassKpiElement employeeQuotaKpiElement = new CompassKpiElement();
            employeeQuotaKpiElement.setCaption("QUOTA");
            employeeQuotaKpiElement.setCompassKpi(employeeQuotaKpi);
            cwTimeReferenceElement.addChild(employeeQuotaKpiElement);

            quotaElements.add(employeeQuotaKpiElement);

            CompassKpiElement employeeActualKpiElement = new CompassKpiElement();
            employeeActualKpiElement.setCaption("ACTUAL");
            employeeActualKpiElement.setCompassKpi(employeeActualKpi);
            cwTimeReferenceElement.addChild(employeeActualKpiElement);

            CompassKpiElement employeeVacancyKpiElement = new CompassKpiElement();
            employeeVacancyKpiElement.setCaption("VACANCY");
            employeeVacancyKpiElement.setCompassKpi(employeeVacancyKpi);
            cwTimeReferenceElement.addChild(employeeVacancyKpiElement);

            vacancyElements.add(employeeVacancyKpiElement);

            week = week.plusWeeks(1);
        }

        CompassRootElement treeRootElement = new CompassRootElement();
        treeRootElement.setCaption("Root");

        CompassTextElement totalElement = new CompassTextElement();
        totalElement.setCaption("Total");
        treeRootElement.addChild(totalElement);

        CompassCustomLoadingElement teamLoadingElement = new CompassCustomLoadingElement();
        teamLoadingElement.setCaption("Loading teams...");
        teamLoadingElement.setHierarchyDatabaseDefinition(teamHierarchy);
        teamLoadingElement.setHierarchyLevelDefinition(teamLevelDefinition);
        totalElement.addChild(teamLoadingElement);

        CompassCustomLoadingElement employeeLoadingElement = new CompassCustomLoadingElement();
        employeeLoadingElement.setCaption("Loading employees...");
        employeeLoadingElement.setHierarchyDatabaseDefinition(employeeHierarchy);
        employeeLoadingElement.setHierarchyLevelDefinition(employeeLevelDefinition);
        teamLoadingElement.addChild(employeeLoadingElement);

        CompassCustomLoadingElement projectLoadingElement = new CompassCustomLoadingElement();
        projectLoadingElement.setCaption("Loading projects...");
        projectLoadingElement.setHierarchyDatabaseDefinition(projectHierarchy);
        projectLoadingElement.setHierarchyLevelDefinition(projectLevelDefinition);
        employeeLoadingElement.addChild(projectLoadingElement);

        CompassReportSheet reportSheet = new CompassReportSheet();
        reportSheet.setCaption("Sheet 1");
        reportSheet.setHeaderRoot(headerRootElement);
        reportSheet.setTreeRoot(treeRootElement);

        CompassReport report = new CompassReport();
        List<CompassReportSheet> sheets = new ArrayList<>();
        sheets.add(reportSheet);
        report.setSheets(sheets);
        report.setCaption("Workload per team");
        report.setUseReportDate(true);
        report.setReferenceDate(LocalDate.of(2017,12,31));
        report.setDefaultNumberFormat(defaultNumberFormat);
        report.setAllowLazyLoading(false);

        report.setMetadataContainer(initService.addDefaultMetadata(report.getMetadataContainer()));

        CompassReport savedReport = compassReportRepository.save(report);

        Set<String> disabledCells = new HashSet<>();
        for(CompassReportElement reportElement : quotaElements) {
            disabledCells.add(projectLoadingElement.getIdPath() + "|" + reportElement.getIdPath());
        }
        for(CompassReportElement reportElement : vacancyElements) {
            disabledCells.add(projectLoadingElement.getIdPath() + "|" + reportElement.getIdPath());
        }
        reportSheet.setDisabledCells(disabledCells);

        return compassReportRepository.save(savedReport);
    }

    public CompassReport createLocationWorkloadReport(
            CompassKpi employeeQuotaKpi, CompassKpi employeeActualKpi, CompassKpi employeeVacancyKpi,
            HierarchyDatabaseDefinition employeeHierarchy, HierarchyLevelDefinition employeeLevelDefinition,
            HierarchyDatabaseDefinition teamHierarchy, HierarchyLevelDefinition teamLevelDefinition,
            HierarchyDatabaseDefinition locationHierarchy, HierarchyLevelDefinition locationLevelDefinition,
            HierarchyDatabaseDefinition projectHierarchy, HierarchyLevelDefinition projectLevelDefinition,
            DataFormat<Number> defaultNumberFormat) {
        CompassRootElement headerRootElement = new CompassRootElement();
        headerRootElement.setCaption("Root");

        LocalDate week = LocalDate.of(2020, Month.JANUARY, 1);
        if(week.getDayOfWeek() != DayOfWeek.THURSDAY) {
            week = week.with(TemporalAdjusters.next(DayOfWeek.THURSDAY));
        }
        week = week.minusDays(3);
        List<CompassReportElement> quotaElements = new ArrayList<>();
        List<CompassReportElement> vacancyElements = new ArrayList<>();
        while(week.isBefore(LocalDate.of(2020, 12,31))) {
            TemporalField woy = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();
            int weekNumber = week.get(woy);

            FixDay startDay = new FixDay();
            startDay.setCaption("Monday CW " + weekNumber);
            startDay.setDay(week.getDayOfMonth());
            FixMonth startMonth = new FixMonth();
            startMonth.setCaption("" + week.getMonthValue());
            startMonth.setMonth(week.getMonthValue());
            FixYear startYear = new FixYear();
            startYear.setCaption("" + week.getYear());
            startYear.setYear(week.getYear());

            FixDay endDay = new FixDay();
            endDay.setCaption("Monday CW " + weekNumber);
            endDay.setDay(week.getDayOfMonth());
            FixMonth endMonth = new FixMonth();
            endMonth.setCaption("" + week.getMonthValue());
            endMonth.setMonth(week.getMonthValue());
            FixYear endYear = new FixYear();
            endYear.setCaption("" + week.getYear());
            endYear.setYear(week.getYear());

            String timeRefCaption = "CW " + weekNumber + " (" + week.getDayOfMonth() + "." + week.getMonthValue() + ". - " + week.plusDays(6).getDayOfMonth() + "." +  week.plusDays(6).getMonthValue() + "." + week.plusDays(6).getYear() + ")";
            Timereference cw = initService.timereference().initTimereference(timeRefCaption, startYear, startMonth, startDay, endYear, endMonth, endDay);

            CompassTimereferenceElement cwTimeReferenceElement = new CompassTimereferenceElement();
            cwTimeReferenceElement.setCaption(timeRefCaption);
            cwTimeReferenceElement.setTimereference(cw);
            headerRootElement.addChild(cwTimeReferenceElement);

//            CompassSpaceElement spaceElement = new CompassSpaceElement();
//            spaceElement.setCaption("");
//            headerRootElement.addChild(spaceElement);

            CompassKpiElement employeeQuotaKpiElement = new CompassKpiElement();
            employeeQuotaKpiElement.setCaption("QUOTA");
            employeeQuotaKpiElement.setCompassKpi(employeeQuotaKpi);
            cwTimeReferenceElement.addChild(employeeQuotaKpiElement);

            quotaElements.add(employeeQuotaKpiElement);

            CompassKpiElement employeeActualKpiElement = new CompassKpiElement();
            employeeActualKpiElement.setCaption("ACTUAL");
            employeeActualKpiElement.setCompassKpi(employeeActualKpi);
            cwTimeReferenceElement.addChild(employeeActualKpiElement);

            CompassKpiElement employeeVacancyKpiElement = new CompassKpiElement();
            employeeVacancyKpiElement.setCaption("VACANCY");
            employeeVacancyKpiElement.setCompassKpi(employeeVacancyKpi);
            cwTimeReferenceElement.addChild(employeeVacancyKpiElement);

            vacancyElements.add(employeeVacancyKpiElement);

            week = week.plusWeeks(1);
        }

        CompassRootElement treeRootElement = new CompassRootElement();
        treeRootElement.setCaption("Root");

        CompassTextElement totalElement = new CompassTextElement();
        totalElement.setCaption("Total");
        treeRootElement.addChild(totalElement);

        CompassCustomLoadingElement locationLoadingElement = new CompassCustomLoadingElement();
        locationLoadingElement.setCaption("Loading locations...");
        locationLoadingElement.setHierarchyDatabaseDefinition(locationHierarchy);
        locationLoadingElement.setHierarchyLevelDefinition(locationLevelDefinition);
        totalElement.addChild(locationLoadingElement);

        CompassCustomLoadingElement teamLoadingElement = new CompassCustomLoadingElement();
        teamLoadingElement.setCaption("Loading teams...");
        teamLoadingElement.setHierarchyDatabaseDefinition(teamHierarchy);
        teamLoadingElement.setHierarchyLevelDefinition(teamLevelDefinition);
        locationLoadingElement.addChild(teamLoadingElement);

        CompassCustomLoadingElement employeeLoadingElement = new CompassCustomLoadingElement();
        employeeLoadingElement.setCaption("Loading employees...");
        employeeLoadingElement.setHierarchyDatabaseDefinition(employeeHierarchy);
        employeeLoadingElement.setHierarchyLevelDefinition(employeeLevelDefinition);
        teamLoadingElement.addChild(employeeLoadingElement);

        CompassCustomLoadingElement projectLoadingElement = new CompassCustomLoadingElement();
        projectLoadingElement.setCaption("Loading projects...");
        projectLoadingElement.setHierarchyDatabaseDefinition(projectHierarchy);
        projectLoadingElement.setHierarchyLevelDefinition(projectLevelDefinition);
        employeeLoadingElement.addChild(projectLoadingElement);

        CompassReportSheet reportSheet = new CompassReportSheet();
        reportSheet.setCaption("Sheet 1");
        reportSheet.setHeaderRoot(headerRootElement);
        reportSheet.setTreeRoot(treeRootElement);

        CompassReport report = new CompassReport();
        List<CompassReportSheet> sheets = new ArrayList<>();
        sheets.add(reportSheet);
        report.setSheets(sheets);
        report.setCaption("Workload per location");
        report.setUseReportDate(true);
        report.setReferenceDate(LocalDate.of(2017,12,31));
        report.setDefaultNumberFormat(defaultNumberFormat);
        report.setAllowLazyLoading(false);

        report.setMetadataContainer(initService.addDefaultMetadata(report.getMetadataContainer()));

        CompassReport savedReport = compassReportRepository.save(report);

        Set<String> disabledCells = new HashSet<>();
        for(CompassReportElement reportElement : quotaElements) {
            disabledCells.add(projectLoadingElement.getIdPath() + "|" + reportElement.getIdPath());
        }
        for(CompassReportElement reportElement : vacancyElements) {
            disabledCells.add(projectLoadingElement.getIdPath() + "|" + reportElement.getIdPath());
        }
        reportSheet.setDisabledCells(disabledCells);

        return compassReportRepository.save(savedReport);
    }

    public CompassReport createCustomerOverviewReport(
            CompassKpi manDaysKpi, CompassKpi dailyRatesKpi, CompassKpi projectSalesKpi, CompassKpi totalSalesKpi,
            HierarchyDatabaseDefinition customerHierarchy,
            HierarchyLevelDefinition customerLevelDefinition, HierarchyLevelDefinition customerDivisionLevelDefinition,
            HierarchyDatabaseDefinition projectHierarchy, HierarchyLevelDefinition projectLevelDefinition,
            HierarchyDatabaseDefinition employeeHierarchy, HierarchyLevelDefinition employeeLevelDefinition,
            DataFormat<Number> defaultNumberFormat) {

        CompassRootElement headerRootElement = new CompassRootElement();
        headerRootElement.setCaption("Root");

        List<CompassReportElement> disableColumnElementsForEmployee = new ArrayList<>(24);
        List<CompassReportElement> noSumColumnElements = new ArrayList<>(12);

        LocalDate date = LocalDate.of(2020, Month.JANUARY, 1);
        for(int i = 1; i <= 12; i++) {
            FixDay startDay = new FixDay();
            startDay.setCaption("First Day of month");
            startDay.setDay(1);
            FixMonth startMonth = new FixMonth();
            startMonth.setCaption(i+ ".");
            startMonth.setMonth(i);
            RelativeYear startYear = new RelativeYear();
            startYear.setCaption("[GJ]");
            startYear.setYearOffset(0);

            LastDayOfMonth lastDayOfMonth = new LastDayOfMonth();
            lastDayOfMonth.setCaption("Last day of month");
            FixMonth endMonth = new FixMonth();
            endMonth.setCaption(i + ".");
            endMonth.setMonth(i);
            RelativeYear endYear = new RelativeYear();
            endYear.setCaption("[GJ]");
            endYear.setYearOffset(0);

            String timeRefCaption = "[GJ] / "+i;
            Timereference month = initService.timereference().initTimereference(timeRefCaption, startYear, startMonth, startDay, endYear, endMonth, lastDayOfMonth);

            CompassTimereferenceElement monthTimeReferenceElement = new CompassTimereferenceElement();
            monthTimeReferenceElement.setCaption(timeRefCaption);
            monthTimeReferenceElement.setTimereference(month);
            headerRootElement.addChild(monthTimeReferenceElement);

            CompassKpiElement manDaysKpiElement = new CompassKpiElement();
            manDaysKpiElement.setCaption("man days");
            manDaysKpiElement.setCompassKpi(manDaysKpi);
            monthTimeReferenceElement.addChild(manDaysKpiElement);

            CompassKpiElement dailyRatesKpiElement = new CompassKpiElement();
            dailyRatesKpiElement.setCaption("daily rates");
            dailyRatesKpiElement.setCompassKpi(dailyRatesKpi);
            monthTimeReferenceElement.addChild(dailyRatesKpiElement);

            CompassKpiElement projectSalesKpiElement = new CompassKpiElement();
            projectSalesKpiElement.setCaption("project sales");
            projectSalesKpiElement.setCompassKpi(projectSalesKpi);
            monthTimeReferenceElement.addChild(projectSalesKpiElement);
            disableColumnElementsForEmployee.add(projectSalesKpiElement);

            CompassKpiElement totalSalesKpiElement = new CompassKpiElement();
            totalSalesKpiElement.setCaption("total sales");
            totalSalesKpiElement.setCompassKpi(totalSalesKpi);
            monthTimeReferenceElement.addChild(totalSalesKpiElement);
            disableColumnElementsForEmployee.add(totalSalesKpiElement);
            noSumColumnElements.add(totalSalesKpiElement);
        }

        CompassRootElement treeRootElement = new CompassRootElement();
        treeRootElement.setCaption("Root");

        CompassTextElement totalElement = new CompassTextElement();
        totalElement.setCaption("Total");
        treeRootElement.addChild(totalElement);

        CompassCustomLoadingElement customerLoadingElement = new CompassCustomLoadingElement();
        customerLoadingElement.setCaption("Loading customers...");
        customerLoadingElement.setHierarchyDatabaseDefinition(customerHierarchy);
        customerLoadingElement.setHierarchyLevelDefinition(customerLevelDefinition);
        totalElement.addChild(customerLoadingElement);

        CompassCustomLoadingElement divisionLoadingElement = new CompassCustomLoadingElement();
        divisionLoadingElement.setCaption("Loading customer divisions...");
        divisionLoadingElement.setHierarchyDatabaseDefinition(customerHierarchy);
        divisionLoadingElement.setHierarchyLevelDefinition(customerDivisionLevelDefinition);
        customerLoadingElement.addChild(divisionLoadingElement);

        CompassCustomLoadingElement projectLoadingElement = new CompassCustomLoadingElement();
        projectLoadingElement.setCaption("Loading projects...");
        projectLoadingElement.setHierarchyDatabaseDefinition(projectHierarchy);
        projectLoadingElement.setHierarchyLevelDefinition(projectLevelDefinition);
        divisionLoadingElement.addChild(projectLoadingElement);

        CompassCustomLoadingElement employeeLoadingElement = new CompassCustomLoadingElement();
        employeeLoadingElement.setCaption("Loading employees...");
        employeeLoadingElement.setHierarchyDatabaseDefinition(employeeHierarchy);
        employeeLoadingElement.setHierarchyLevelDefinition(employeeLevelDefinition);
        projectLoadingElement.addChild(employeeLoadingElement);

        CompassReportSheet reportSheet = new CompassReportSheet();
        reportSheet.setCaption("Sheet 1");
        reportSheet.setHeaderRoot(headerRootElement);
        reportSheet.setTreeRoot(treeRootElement);

        CompassReport report = new CompassReport();
        List<CompassReportSheet> sheets = new ArrayList<>();
        sheets.add(reportSheet);
        report.setSheets(sheets);
        report.setCaption("Customer Overview");
        report.setUseReportDate(true);
        report.setReferenceDate(LocalDate.of(2020,12,31));
        report.setDefaultNumberFormat(defaultNumberFormat);
        report.setAllowLazyLoading(false);

        report.setMetadataContainer(initService.addDefaultMetadata(report.getMetadataContainer()));

        CompassReport savedReport = compassReportRepository.save(report);

        Set<String> disabledCells = new HashSet<>();
        for (CompassReportElement disableColumnElement : disableColumnElementsForEmployee) {
            disabledCells.add(employeeLoadingElement.getIdPath() + "|" + disableColumnElement.getIdPath());
        }

        reportSheet.setDisabledCells(disabledCells);

        Set<String> noSumColumns = new HashSet<>();
        for (CompassReportElement noSumColumnElement : noSumColumnElements) {
            noSumColumns.add(noSumColumnElement.getIdPath());
        }
        reportSheet.setNoSumColumns(noSumColumns);

        return compassReportRepository.save(savedReport);
    }

    public CompassReport createDebugReport(CompassKpi employeeLocationKpi,
                                           HierarchyDatabaseDefinition employeeHierarchy, HierarchyLevelDefinition employeeLevelDefinition,
                                           DataFormat<Number> defaultNumberFormat) {

        CompassRootElement headerRootElement = new CompassRootElement();
        headerRootElement.setCaption("Root");

        CompassKpiElement locationKpiElement = new CompassKpiElement();
        locationKpiElement.setCaption("LOCATION");
        locationKpiElement.setCompassKpi(employeeLocationKpi);
        headerRootElement.addChild(locationKpiElement);

        CompassRootElement treeRootElement = new CompassRootElement();
        treeRootElement.setCaption("Root");

        CompassTextElement totalElement = new CompassTextElement();
        totalElement.setCaption("Total");
        treeRootElement.addChild(totalElement);

        CompassCustomLoadingElement employeeLoadingElement = new CompassCustomLoadingElement();
        employeeLoadingElement.setCaption("Loading employees...");
        employeeLoadingElement.setHierarchyDatabaseDefinition(employeeHierarchy);
        employeeLoadingElement.setHierarchyLevelDefinition(employeeLevelDefinition);
        totalElement.addChild(employeeLoadingElement);

        CompassReportSheet reportSheet = new CompassReportSheet();
        reportSheet.setCaption("Sheet 1");
        reportSheet.setHeaderRoot(headerRootElement);
        reportSheet.setTreeRoot(treeRootElement);

        CompassReport report = new CompassReport();
        List<CompassReportSheet> sheets = new ArrayList<>();
        sheets.add(reportSheet);
        report.setSheets(sheets);
        report.setCaption("Debug");
        report.setUseReportDate(true);
        report.setReferenceDate(LocalDate.of(2017,12,31));
        report.setDefaultNumberFormat(defaultNumberFormat);
        report.setAllowLazyLoading(false);

        report.setMetadataContainer(initService.addDefaultMetadata(report.getMetadataContainer()));

        return compassReportRepository.save(report);
    }

    public CompassReport createManDaysPerEmployeeReport(
            CompassKpi monthlyManDaysKpi,
            CompassKpi employeeLocationKpi, CompassKpi teamLeaderShorthandKpi, CompassKpi employeeContractTypeKpi, CompassKpi employeeWeeklyHoursKpi,
            CompassKpi invoiceRecipientNameKpi, CompassKpi invoiceRecipientDivisionKpi,
            HierarchyDatabaseDefinition projectHierarchy, HierarchyLevelDefinition projectLevelDefinition,
            HierarchyDatabaseDefinition employeeHierarchy, HierarchyLevelDefinition employeeLevelDefinition,
            DataFormat<Number> defaultNumberFormat) {

        CompassRootElement headerRootElement = new CompassRootElement();
        headerRootElement.setCaption("Root");

        CompassKpiElement locationKpiElement = new CompassKpiElement();
        locationKpiElement.setCaption("LOCATION");
        locationKpiElement.setCompassKpi(employeeLocationKpi);
        headerRootElement.addChild(locationKpiElement);

        CompassKpiElement teamLeaderShorthandKpiElement = new CompassKpiElement();
        teamLeaderShorthandKpiElement.setCaption("TEAM");
        teamLeaderShorthandKpiElement.setCompassKpi(teamLeaderShorthandKpi);
        headerRootElement.addChild(teamLeaderShorthandKpiElement);

        CompassKpiElement employeeContractTypeKpiElement = new CompassKpiElement();
        employeeContractTypeKpiElement.setCaption("CONTRACT TYPE");
        employeeContractTypeKpiElement.setCompassKpi(employeeContractTypeKpi);
        headerRootElement.addChild(employeeContractTypeKpiElement);

        CompassKpiElement employeeWeeklyHoursKpiElement = new CompassKpiElement();
        employeeWeeklyHoursKpiElement.setCaption("WEEKLY HOURS");
        employeeWeeklyHoursKpiElement.setCompassKpi(employeeWeeklyHoursKpi);
        headerRootElement.addChild(employeeWeeklyHoursKpiElement);

        CompassKpiElement customerKpiElement = new CompassKpiElement();
        customerKpiElement.setCaption("CUSTOMER");
        customerKpiElement.setCompassKpi(invoiceRecipientNameKpi);
        headerRootElement.addChild(customerKpiElement);

        CompassKpiElement customerDivisionKpiElement = new CompassKpiElement();
        customerDivisionKpiElement.setCaption("DIVISION");
        customerDivisionKpiElement.setCompassKpi(invoiceRecipientDivisionKpi);
        headerRootElement.addChild(customerDivisionKpiElement);

        CompassSpaceElement spaceElement = new CompassSpaceElement();
        spaceElement.setCaption("");
        headerRootElement.addChild(spaceElement);

        CompassKpiElement manDaysKpiElement = new CompassKpiElement();
        manDaysKpiElement.setCaption("man days");
        manDaysKpiElement.setCompassKpi(monthlyManDaysKpi);
        headerRootElement.addChild(manDaysKpiElement);

        LocalDate date = LocalDate.of(2020, Month.JANUARY, 1);
        for(int i = 1; i <= 12; i++) {
            LocalDate monthDate = LocalDate.of(2020, i, 1);
            FixDay startDay = new FixDay();
            startDay.setCaption("First Day of month");
            startDay.setDay(1);
            FixMonth startMonth = new FixMonth();
            startMonth.setCaption(i+ ".");
            startMonth.setMonth(i);
            FixYear startYear = new FixYear();
            startYear.setCaption("2020");
            startYear.setYear(2020);

            LastDayOfMonth lastDayOfMonth = new LastDayOfMonth();
            lastDayOfMonth.setCaption("Last day of month");
            FixMonth endMonth = new FixMonth();
            endMonth.setCaption(i + ".");
            endMonth.setMonth(i);
            FixYear endYear = new FixYear();
            endYear.setCaption("2020");
            endYear.setYear(2020);

            String timeRefCaption = "2020-"+i;
            Timereference month = initService.timereference().initTimereference(timeRefCaption, startYear, startMonth, startDay, endYear, endMonth, lastDayOfMonth);

            CompassTimereferenceElement monthTimeReferenceElement = new CompassTimereferenceElement();
            monthTimeReferenceElement.setCaption(timeRefCaption);
            monthTimeReferenceElement.setTimereference(month);
            manDaysKpiElement.addChild(monthTimeReferenceElement);
        }

        CompassRootElement treeRootElement = new CompassRootElement();
        treeRootElement.setCaption("Root");

        CompassTextElement totalElement = new CompassTextElement();
        totalElement.setCaption("Total");
        treeRootElement.addChild(totalElement);

        CompassCustomLoadingElement employeeLoadingElement = new CompassCustomLoadingElement();
        employeeLoadingElement.setCaption("Loading employees...");
        employeeLoadingElement.setHierarchyDatabaseDefinition(employeeHierarchy);
        employeeLoadingElement.setHierarchyLevelDefinition(employeeLevelDefinition);
        totalElement.addChild(employeeLoadingElement);

        CompassCustomLoadingElement projectLoadingElement = new CompassCustomLoadingElement();
        projectLoadingElement.setCaption("Loading projects...");
        projectLoadingElement.setHierarchyDatabaseDefinition(projectHierarchy);
        projectLoadingElement.setHierarchyLevelDefinition(projectLevelDefinition);
        employeeLoadingElement.addChild(projectLoadingElement);

        CompassReportSheet reportSheet = new CompassReportSheet();
        reportSheet.setCaption("Sheet 1");
        reportSheet.setHeaderRoot(headerRootElement);
        reportSheet.setTreeRoot(treeRootElement);

        CompassReport report = new CompassReport();
        List<CompassReportSheet> sheets = new ArrayList<>();
        sheets.add(reportSheet);
        report.setSheets(sheets);
        report.setCaption("Man Days per employee");
        report.setUseReportDate(true);
        report.setReferenceDate(LocalDate.of(2017,12,31));
        report.setDefaultNumberFormat(defaultNumberFormat);
        report.setAllowLazyLoading(false);

        report.setMetadataContainer(initService.addDefaultMetadata(report.getMetadataContainer()));

        CompassReport savedReport = compassReportRepository.save(report);

        Set<String> disabledCells = new HashSet<>();
        disabledCells.add(totalElement.getIdPath() + "|" + locationKpiElement.getIdPath());
        disabledCells.add(totalElement.getIdPath() + "|" + teamLeaderShorthandKpiElement.getIdPath());
        disabledCells.add(totalElement.getIdPath() + "|" + employeeContractTypeKpiElement.getIdPath());
        disabledCells.add(totalElement.getIdPath() + "|" + employeeWeeklyHoursKpiElement.getIdPath());
        disabledCells.add(totalElement.getIdPath() + "|" + customerKpiElement.getIdPath());
        disabledCells.add(totalElement.getIdPath() + "|" + customerDivisionKpiElement.getIdPath());

        disabledCells.add(employeeLoadingElement.getIdPath() + "|" + customerKpiElement.getIdPath());
        disabledCells.add(employeeLoadingElement.getIdPath() + "|" + customerDivisionKpiElement.getIdPath());

        disabledCells.add(projectLoadingElement.getIdPath() + "|" + locationKpiElement.getIdPath());
        disabledCells.add(projectLoadingElement.getIdPath() + "|" + teamLeaderShorthandKpiElement.getIdPath());
        disabledCells.add(projectLoadingElement.getIdPath() + "|" + employeeContractTypeKpiElement.getIdPath());
        disabledCells.add(projectLoadingElement.getIdPath() + "|" + employeeWeeklyHoursKpiElement.getIdPath());

        reportSheet.setDisabledCells(disabledCells);

        return compassReportRepository.save(savedReport);
    }

    public CompassReport createAssignmentsPerEmployeeReport(
            CompassKpi monthlyManDaysInternKpi, CompassKpi monthlyManDaysExternKpi, CompassKpi monthlyManDaysOfferingKpi, CompassKpi monthlyVacanyKpi,
            CompassKpi employeeLocationKpi, CompassKpi teamLeaderShorthandKpi, CompassKpi employeeContractTypeKpi, CompassKpi employeeWeeklyHoursKpi,
            CompassKpi invoiceRecipientNameKpi, CompassKpi invoiceRecipientDivisionKpi,
            HierarchyDatabaseDefinition projectHierarchy, HierarchyLevelDefinition projectLevelDefinition,
            HierarchyDatabaseDefinition employeeHierarchy, HierarchyLevelDefinition employeeLevelDefinition,
            DataFormat<Number> defaultNumberFormat) {

        CompassRootElement headerRootElement = new CompassRootElement();
        headerRootElement.setCaption("Root");

        CompassKpiElement locationKpiElement = new CompassKpiElement();
        locationKpiElement.setCaption("LOCATION");
        locationKpiElement.setCompassKpi(employeeLocationKpi);
        headerRootElement.addChild(locationKpiElement);

        CompassKpiElement teamLeaderShorthandKpiElement = new CompassKpiElement();
        teamLeaderShorthandKpiElement.setCaption("TEAM");
        teamLeaderShorthandKpiElement.setCompassKpi(teamLeaderShorthandKpi);
        headerRootElement.addChild(teamLeaderShorthandKpiElement);

        CompassKpiElement employeeContractTypeKpiElement = new CompassKpiElement();
        employeeContractTypeKpiElement.setCaption("CONTRACT TYPE");
        employeeContractTypeKpiElement.setCompassKpi(employeeContractTypeKpi);
        headerRootElement.addChild(employeeContractTypeKpiElement);

        CompassKpiElement employeeWeeklyHoursKpiElement = new CompassKpiElement();
        employeeWeeklyHoursKpiElement.setCaption("WEEKLY HOURS");
        employeeWeeklyHoursKpiElement.setCompassKpi(employeeWeeklyHoursKpi);
        headerRootElement.addChild(employeeWeeklyHoursKpiElement);

        CompassKpiElement customerKpiElement = new CompassKpiElement();
        customerKpiElement.setCaption("CUSTOMER");
        customerKpiElement.setCompassKpi(invoiceRecipientNameKpi);
        headerRootElement.addChild(customerKpiElement);

        CompassKpiElement customerDivisionKpiElement = new CompassKpiElement();
        customerDivisionKpiElement.setCaption("DIVISION");
        customerDivisionKpiElement.setCompassKpi(invoiceRecipientDivisionKpi);
        headerRootElement.addChild(customerDivisionKpiElement);

        CompassSpaceElement spaceElement = new CompassSpaceElement();
        spaceElement.setCaption("");
        headerRootElement.addChild(spaceElement);

        LocalDate date = LocalDate.of(2020, Month.JANUARY, 1);
        for(int i = 1; i <= 12; i++) {
            LocalDate monthDate = LocalDate.of(2020, i, 1);
            FixDay startDay = new FixDay();
            startDay.setCaption("First Day of month");
            startDay.setDay(1);
            FixMonth startMonth = new FixMonth();
            startMonth.setCaption(i+ ".");
            startMonth.setMonth(i);
            FixYear startYear = new FixYear();
            startYear.setCaption("2020");
            startYear.setYear(2020);

            LastDayOfMonth lastDayOfMonth = new LastDayOfMonth();
            lastDayOfMonth.setCaption("Last day of month");
            FixMonth endMonth = new FixMonth();
            endMonth.setCaption(i + ".");
            endMonth.setMonth(i);
            FixYear endYear = new FixYear();
            endYear.setCaption("2020");
            endYear.setYear(2020);

            String timeRefCaption = "2020-"+i;
            Timereference month = initService.timereference().initTimereference(timeRefCaption, startYear, startMonth, startDay, endYear, endMonth, lastDayOfMonth);

            CompassTimereferenceElement monthTimeReferenceElement = new CompassTimereferenceElement();
            monthTimeReferenceElement.setCaption(timeRefCaption);
            monthTimeReferenceElement.setTimereference(month);
            headerRootElement.addChild(monthTimeReferenceElement);

            CompassKpiElement manDaysInternKpiElement = new CompassKpiElement();
            manDaysInternKpiElement.setCaption("INTERN");
            manDaysInternKpiElement.setCompassKpi(monthlyManDaysInternKpi);
            monthTimeReferenceElement.addChild(manDaysInternKpiElement);

            CompassKpiElement manDaysExternKpiElement = new CompassKpiElement();
            manDaysExternKpiElement.setCaption("EXTERN");
            manDaysExternKpiElement.setCompassKpi(monthlyManDaysExternKpi);
            monthTimeReferenceElement.addChild(manDaysExternKpiElement);

            CompassKpiElement manDaysOfferingKpiElement = new CompassKpiElement();
            manDaysOfferingKpiElement.setCaption("OFFERING");
            manDaysOfferingKpiElement.setCompassKpi(monthlyManDaysOfferingKpi);
            monthTimeReferenceElement.addChild(manDaysOfferingKpiElement);

            CompassKpiElement monthlyAvailableKpiElement = new CompassKpiElement();
            monthlyAvailableKpiElement.setCaption("VACANCY");
            monthlyAvailableKpiElement.setCompassKpi(monthlyVacanyKpi);
            monthTimeReferenceElement.addChild(monthlyAvailableKpiElement);
        }

        CompassRootElement treeRootElement = new CompassRootElement();
        treeRootElement.setCaption("Root");

        CompassTextElement totalElement = new CompassTextElement();
        totalElement.setCaption("Total");
        treeRootElement.addChild(totalElement);

        CompassCustomLoadingElement employeeLoadingElement = new CompassCustomLoadingElement();
        employeeLoadingElement.setCaption("Loading employees...");
        employeeLoadingElement.setHierarchyDatabaseDefinition(employeeHierarchy);
        employeeLoadingElement.setHierarchyLevelDefinition(employeeLevelDefinition);
        totalElement.addChild(employeeLoadingElement);

        CompassCustomLoadingElement projectLoadingElement = new CompassCustomLoadingElement();
        projectLoadingElement.setCaption("Loading projects...");
        projectLoadingElement.setHierarchyDatabaseDefinition(projectHierarchy);
        projectLoadingElement.setHierarchyLevelDefinition(projectLevelDefinition);
        employeeLoadingElement.addChild(projectLoadingElement);

        CompassReportSheet reportSheet = new CompassReportSheet();
        reportSheet.setCaption("Sheet 1");
        reportSheet.setHeaderRoot(headerRootElement);
        reportSheet.setTreeRoot(treeRootElement);

        CompassReport report = new CompassReport();
        List<CompassReportSheet> sheets = new ArrayList<>();
        sheets.add(reportSheet);
        report.setSheets(sheets);
        report.setCaption("ASSIGNMENTS PER MONTH");
        report.setUseReportDate(true);
        report.setReferenceDate(LocalDate.of(2017,12,31));
        report.setDefaultNumberFormat(defaultNumberFormat);
        report.setAllowLazyLoading(false);

        report.setMetadataContainer(initService.addDefaultMetadata(report.getMetadataContainer()));

        CompassReport savedReport = compassReportRepository.save(report);

        Set<String> disabledCells = new HashSet<>();
        disabledCells.add(totalElement.getIdPath() + "|" + locationKpiElement.getIdPath());
        disabledCells.add(totalElement.getIdPath() + "|" + teamLeaderShorthandKpiElement.getIdPath());
        disabledCells.add(totalElement.getIdPath() + "|" + employeeContractTypeKpiElement.getIdPath());
        disabledCells.add(totalElement.getIdPath() + "|" + employeeWeeklyHoursKpiElement.getIdPath());
        disabledCells.add(totalElement.getIdPath() + "|" + customerKpiElement.getIdPath());
        disabledCells.add(totalElement.getIdPath() + "|" + customerDivisionKpiElement.getIdPath());

        disabledCells.add(employeeLoadingElement.getIdPath() + "|" + customerKpiElement.getIdPath());
        disabledCells.add(employeeLoadingElement.getIdPath() + "|" + customerDivisionKpiElement.getIdPath());

        disabledCells.add(projectLoadingElement.getIdPath() + "|" + locationKpiElement.getIdPath());
        disabledCells.add(projectLoadingElement.getIdPath() + "|" + teamLeaderShorthandKpiElement.getIdPath());
        disabledCells.add(projectLoadingElement.getIdPath() + "|" + employeeContractTypeKpiElement.getIdPath());
        disabledCells.add(projectLoadingElement.getIdPath() + "|" + employeeWeeklyHoursKpiElement.getIdPath());

        reportSheet.setDisabledCells(disabledCells);

        return compassReportRepository.save(savedReport);
    }
}
