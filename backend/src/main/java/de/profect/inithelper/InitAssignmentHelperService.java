package de.profect.inithelper;

import de.profect.Category;
import de.profect.assignmenttool.assignment.Assignment;
import de.profect.assignmenttool.assignment.AssignmentRepository;
import de.profect.assignmenttool.customer.Customer;
import de.profect.assignmenttool.customer.CustomerRepository;
import de.profect.assignmenttool.customer.hierarchy.CustomerHierarchyDefinition;
import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.employee.EmployeeFormSaveConnectorState;
import de.profect.assignmenttool.employee.EmployeeRepository;
import de.profect.assignmenttool.employee.EmployeeTeamleaderConnectorState;
import de.profect.assignmenttool.employee.hierarchy.EmployeeHierarchyDefinition;
import de.profect.assignmenttool.msgraph.graphupdateconnector.GraphUpdateConnectorState;
import de.profect.assignmenttool.project.Project;
import de.profect.assignmenttool.project.ProjectFormSaveConnectorState;
import de.profect.assignmenttool.project.ProjectRepository;
import de.profect.assignmenttool.project.hierarchy.ProjectHierarchyDefinition;
import de.profect.assignmenttool.tableviewwithcustomcolumn.TableViewWithCustomColumnState;
import de.profect.assignmenttool.tableviewwithcustomcolumn.dialogconfirmconnector.DialogConfirmConnectorState;
import de.profect.assignmenttool.tableviewwithcustomcolumn.dynamiclabelmessageconnector.DynamicLabelMessageConnectorState;
import de.profect.component.ComponentInstance;
import de.profect.connector.ConnectorInitializerService;
import de.profect.connector.ConnectorState;
import de.profect.connector.ConnectorStateService;
import de.profect.connector.target.form.save_form.FormSaveConnectorState;
import de.profect.data.hierarchy.HierarchyDefinitionRepository;
import de.profect.data.metadata.Metadata;
import de.profect.data.metadata.MetadataContainer;
import de.profect.data.metadata.MetadataDefinition;
import de.profect.data.query.condition.Condition;
import de.profect.data.query.field.Field;
import de.profect.data.query.field.constant.ConstantField;
import de.profect.util.InitHelperService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
@Transactional
@Log4j2
public class InitAssignmentHelperService {

    @Autowired
    InitHelperService initService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private AssignmentRepository assignmentRepository;

    @Autowired
    private HierarchyDefinitionRepository hierarchyDefinitionRepo;

    @Autowired
    private ConnectorStateService connectorService;

    public ComponentInstance initTableWithCustomColumn(String caption, String description, String css, String iconClass,
                                       Category category, Integer limit, Field mainKPIField, Field idField, List<Field> fields,
                                       MetadataContainer metadata, Boolean displayHeader, List<Field> fixedFields) {
        return initTableWithCustomColumn(caption, description, css, iconClass, category, limit, null, mainKPIField, idField, fields,
                metadata, displayHeader, true, false, fixedFields, null, idField, true);
    }

    public ComponentInstance initTableWithCustomColumn(String caption, String description, String css, String iconCss,
                                       Category category, Integer limit, Field mainKPIField, Field idField, List<Field> fields,
                                       MetadataContainer metadata, boolean displayHeader, boolean selectableRows, boolean groupById,
                                       List<Field> fixedFields) {
        return initTableWithCustomColumn(caption, description, css, iconCss, category, limit, null, mainKPIField, idField, fields,
                metadata, displayHeader, selectableRows, groupById, fixedFields, null, idField, true);
    }

    public ComponentInstance initTableWithCustomColumn(String caption, String description, String css, String iconClass,
                                       Category category, Integer limit, Integer offset, Field mainKPIField, Field idField, List<Field> fields,
                                       MetadataContainer metadata, boolean displayHeader, boolean selectableRows, boolean groupById,
                                       List<Field> fixedFields, Condition defaultCondition, Field orderField, boolean orderDirection) {
        return initTableWithCustomColumn(caption, description, css, iconClass, category, limit, offset, mainKPIField, idField, fields, metadata, displayHeader, selectableRows,
                groupById, fixedFields, defaultCondition, orderField, orderDirection, 0);
    }

    public ComponentInstance initTableWithCustomColumn(String caption, String description, String css, String iconClass,
                                                       Category category, Integer limit, Integer offset, Field mainKPIField, Field idField, List<Field> fields,
                                                       MetadataContainer metadata, boolean displayHeader, boolean selectableRows, boolean groupById,
                                                       List<Field> fixedFields, Condition defaultCondition, Field orderField, boolean orderDirection, Integer filterMinLength) {
        TableViewWithCustomColumnState state = new TableViewWithCustomColumnState();
        state.setLimit(limit);
        state.setOffset(offset);
        state.setCssClass(css);
        state.setIconClass(iconClass);
        state.setFilterMinlength(filterMinLength);
        state.setMainKPI(mainKPIField);
        state.setIdField(idField);
        if (idField != null && orderField == null) {
            orderField = idField;
        }
        state.setDisplayHeader(displayHeader);
        state.setSelectableRows(selectableRows);
        state.setSelectedRowIndex(new BigDecimal(-1));
        state.setGroupById(groupById);
        state.setCondition(defaultCondition);
        state.setTableViewFilters(new HashSet<>());
        if (fixedFields != null) {
            state.setFixedSelectedFields(new HashSet<>(fixedFields));
        }
        if (fields != null) {

            // should probably throw an error if a field has "edit" as caption

            // this field is needed to add another column in the view
            ConstantField editField = initService.field().initStringConstant("edit", "blub", initService.getDefaultStringFormat(), null);
            ConstantField deleteField = initService.field().initStringConstant("delete", "blub", initService.getDefaultStringFormat(), null);
            List<Field> actualFields = new ArrayList<>(fields);
            actualFields.add(editField);
            actualFields.add(deleteField);
            fields = actualFields;
            state.setSelectedFields(fields);
            if (orderField == null) {
                orderField = fields.get(0);
            }
        } else {
            log.warn("You are initializing a table without fields. Are you sure about this?");
        }

        if (orderField != null) {
            state.setOrderStates(Arrays.asList(initService.initOrderState(orderField, orderDirection)));
        }
        state.setMetadataContainer(metadata);
        return initService.initComponentState(state, caption, description, category);
    }

    public CustomerHierarchyDefinition initCustomerHierarchy(String caption, MetadataContainer filter) {
        CustomerHierarchyDefinition hierarchy = new CustomerHierarchyDefinition();
        hierarchy.setCaption(caption);
        hierarchy.setFilter(filter);
        hierarchy.setMetadataContainer(initService.addDefaultMetadata(hierarchy.getMetadataContainer()));
        return hierarchyDefinitionRepo.save(hierarchy);
    }

    public EmployeeHierarchyDefinition initEmployeeHierarchy(String caption, MetadataContainer filter) {
        EmployeeHierarchyDefinition hierarchy = new EmployeeHierarchyDefinition();
        hierarchy.setCaption(caption);
        hierarchy.setFilter(filter);
        hierarchy.setMetadataContainer(initService.addDefaultMetadata(hierarchy.getMetadataContainer()));
        return hierarchyDefinitionRepo.save(hierarchy);
    }

    public ProjectHierarchyDefinition initProjectHierarchy(String caption, MetadataContainer filter) {
        ProjectHierarchyDefinition hierarchy = new ProjectHierarchyDefinition();
        hierarchy.setCaption(caption);
        hierarchy.setFilter(filter);
        hierarchy.setMetadataContainer(initService.addDefaultMetadata(hierarchy.getMetadataContainer()));
        return hierarchyDefinitionRepo.save(hierarchy);
    }

    public EmployeeTeamleaderConnectorState initEmployeeTeamleaderConnector(String caption, ComponentInstance source, ComponentInstance target, String triggerEvent) {
        EmployeeTeamleaderConnectorState connector = new EmployeeTeamleaderConnectorState();
        connector.setCaption(caption);
        connector.setTriggerEvent(triggerEvent);
        connector.setTarget(target);
        connector.setSource(source);
        connector.setMetadataContainer(initService.addDefaultMetadata(connector.getMetadataContainer()));
        return (EmployeeTeamleaderConnectorState) connectorService.save(connector);
	}
	
    public DialogConfirmConnectorState initDialogConfirmConnector(String caption, ComponentInstance source,
                                                                  ComponentInstance target, String triggerEvent) {
        DialogConfirmConnectorState connector = new DialogConfirmConnectorState();
        connector.setCaption(caption);
        connector.setTriggerEvent(triggerEvent);
        connector.setSource(source);
        connector.setTarget(target);
        connector.setMetadataContainer(initService.addDefaultMetadata(connector.getMetadataContainer()));
        return (DialogConfirmConnectorState) connectorService.save(connector);
    }

    public DynamicLabelMessageConnectorState initDynamicLabelMessageConnector(String caption, ComponentInstance source,
                                                                          ComponentInstance target, String triggerEvent) {
        DynamicLabelMessageConnectorState connector = new DynamicLabelMessageConnectorState();
        connector.setCaption(caption);
        connector.setTriggerEvent(triggerEvent);
        connector.setSource(source);
        connector.setTarget(target);
        connector.setMetadataContainer(initService.addDefaultMetadata(connector.getMetadataContainer()));
        return (DynamicLabelMessageConnectorState) connectorService.save(connector);
    }

    public void createObjectsForDebugging() {
        Employee employee1 = new Employee();
        employee1.setIsTeamLeader(true);
        employee1.setStaffNumber("1111");
        employee1.setCaption("Dirk Arndt");
        employee1.setShorthand("dia");
        employee1.setEmail("dirk.arndt@profect.de");
        employee1.setFirstName("Dirk");
        employee1.setLastName("Arndt");
        employee1.setLocation("Profect DE");
        employee1.setContractType("salaried employee");
        employee1.setWeeklyHours(40d);
        employee1.setMondayHours(8d);
        employee1.setTuesdayHours(8d);
        employee1.setWednesdayHours(8d);
        employee1.setThursdayHours(8d);
        employee1.setFridayHours(8d);
        employee1.setEntryDate(LocalDate.of(2020, 1, 1));
        employee1.setRetirementDate(LocalDate.of(2040, 12, 31));
        employee1.setComment("");
        employee1.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        employee1 = employeeRepository.save(employee1);

        Employee employee2 = new Employee();
        employee2.setIsTeamLeader(true);
        employee2.setStaffNumber("2222");
        employee2.setCaption("Achim Weiß");
        employee2.setShorthand("acw");
        employee2.setEmail("achim.weiß@profect.de");
        employee2.setFirstName("Achim");
        employee2.setLastName("Weiß");
        employee2.setLocation("Profect DE");
        employee2.setContractType("salaried employee");
        employee2.setWeeklyHours(40d);
        employee2.setMondayHours(8d);
        employee2.setTuesdayHours(8d);
        employee2.setWednesdayHours(8d);
        employee2.setThursdayHours(8d);
        employee2.setFridayHours(8d);
        employee2.setEntryDate(LocalDate.of(2020, 1, 1));
        employee2.setRetirementDate(LocalDate.of(2040, 12, 31));
        employee2.setComment("");
        employee2.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        employee2 = employeeRepository.save(employee2);

        Employee employee3 = new Employee();
        employee3.setIsTeamLeader(true);
        employee3.setStaffNumber("3333");
        employee3.setCaption("Benjamin Söll");
        employee3.setShorthand("bso");
        employee3.setEmail("benjamin.soell@profect.de");
        employee3.setFirstName("Benjamin");
        employee3.setLastName("Söll");
        employee3.setLocation("Profect DE");
        employee3.setContractType("salaried employee");
        employee3.setWeeklyHours(40d);
        employee3.setMondayHours(8d);
        employee3.setTuesdayHours(8d);
        employee3.setWednesdayHours(8d);
        employee3.setThursdayHours(8d);
        employee3.setFridayHours(8d);
        employee3.setEntryDate(LocalDate.of(2020, 1, 1));
        employee3.setRetirementDate(LocalDate.of(2040, 12, 31));
        employee3.setComment("");
        employee3.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        employee3 = employeeRepository.save(employee3);

        Employee employee4 = new Employee();
        employee4.setIsTeamLeader(true);
        employee4.setStaffNumber("4444");
        employee4.setCaption("Marco Sauter");
        employee4.setShorthand("mas");
        employee4.setEmail("marco.sauter@profect.de");
        employee4.setFirstName("Marco");
        employee4.setLastName("Sauter");
        employee4.setLocation("Profect DE");
        employee4.setContractType("salaried employee");
        employee4.setWeeklyHours(40d);
        employee4.setMondayHours(8d);
        employee4.setTuesdayHours(8d);
        employee4.setWednesdayHours(8d);
        employee4.setThursdayHours(8d);
        employee4.setFridayHours(8d);
        employee4.setEntryDate(LocalDate.of(2020, 1, 1));
        employee4.setRetirementDate(LocalDate.of(2040, 12, 31));
        employee4.setComment("");
        employee4.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        employee4 = employeeRepository.save(employee4);

        Employee employee5 = new Employee();
        employee5.setIsTeamLeader(false);
        employee5.setStaffNumber("5555");
        employee5.setCaption("Kevin Fechner");
        employee5.setShorthand("kfe");
        employee5.setEmail("kevin.fechner@profect.de");
        employee5.setFirstName("Kevin");
        employee5.setLastName("Fechner");
        employee5.setLocation("Profect DE");
        employee5.setTeamLeader(employee2);
        employee5.setContractType("salaried employee");
        employee5.setWeeklyHours(40d);
        employee5.setMondayHours(8d);
        employee5.setTuesdayHours(8d);
        employee5.setWednesdayHours(8d);
        employee5.setThursdayHours(8d);
        employee5.setFridayHours(8d);
        employee5.setEntryDate(LocalDate.of(2020, 1, 1));
        employee5.setRetirementDate(LocalDate.of(2055, 2, 28));
        employee5.setComment("");
        employee5.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        employee5 = employeeRepository.save(employee5);

        Employee employee6 = new Employee();
        employee6.setIsTeamLeader(false);
        employee6.setStaffNumber("6666");
        employee6.setCaption("Laura Zakowski");
        employee6.setShorthand("lza");
        employee6.setEmail("laura.zakowski@profect.de");
        employee6.setFirstName("Laura");
        employee6.setLastName("Zakowski");
        employee6.setLocation("Profect DE");
        employee6.setTeamLeader(employee1);
        employee6.setContractType("salaried employee");
        employee6.setWeeklyHours(40d);
        employee6.setMondayHours(8d);
        employee6.setTuesdayHours(8d);
        employee6.setWednesdayHours(8d);
        employee6.setThursdayHours(8d);
        employee6.setFridayHours(8d);
        employee6.setEntryDate(LocalDate.of(2020, 1, 1));
        employee6.setRetirementDate(LocalDate.of(2060, 12, 31));
        employee6.setComment("");
        employee6.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        employee6 = employeeRepository.save(employee6);

        Employee employee7 = new Employee();
        employee7.setIsTeamLeader(false);
        employee7.setStaffNumber("7777");
        employee7.setCaption("Steffen Merker");
        employee7.setShorthand("sme");
        employee7.setEmail("steffen.merker@profect.de");
        employee7.setFirstName("Steffen");
        employee7.setLastName("Merker");
        employee7.setLocation("Profect DE");
        employee7.setTeamLeader(employee4);
        employee7.setContractType("student");
        employee7.setWeeklyHours(18d);
        employee7.setMondayHours(8d);
        employee7.setTuesdayHours(8d);
        employee7.setWednesdayHours(8d);
        employee7.setThursdayHours(8d);
        employee7.setFridayHours(8d);
        employee7.setEntryDate(LocalDate.of(2020, 1, 1));
        employee7.setRetirementDate(LocalDate.of(2022, 12, 31));
        employee7.setComment("");
        employee7.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        employee7 = employeeRepository.save(employee7);

        Employee employee8 = new Employee();
        employee8.setIsTeamLeader(false);
        employee8.setStaffNumber("8888");
        employee8.setCaption("Büsra Kültür");
        employee8.setShorthand("byu");
        employee8.setEmail("buesra.kueltuer@profect.de");
        employee8.setFirstName("Büsra");
        employee8.setLastName("Kültür");
        employee8.setLocation("Profect DE");
        employee8.setTeamLeader(employee3);
        employee8.setContractType("salaried employee");
        employee8.setWeeklyHours(32d);
        employee8.setMondayHours(8d);
        employee8.setTuesdayHours(8d);
        employee8.setWednesdayHours(8d);
        employee8.setThursdayHours(8d);
        employee8.setFridayHours(0d);
        employee8.setEntryDate(LocalDate.of(2020, 1, 1));
        employee8.setRetirementDate(LocalDate.of(2040, 12, 31));
        employee8.setComment("");
        employee8.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        employee8 = employeeRepository.save(employee8);

        Customer customer = new Customer();
        customer.setCustomerName("Wüstenrot & Württembergische");
        customer.setDivision("Wüstenrot & Württembergische");
        customer.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        customerRepository.save(customer);

        Customer customer2 = new Customer();
        customer2.setCustomerName("Wüstenrot & Württembergische");
        customer2.setDivision("Württembergische Versicherung AG");
        customer2.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        customerRepository.save(customer2);

        Customer customer3 = new Customer();
        customer3.setCustomerName("Wüstenrot & Württembergische");
        customer3.setDivision("Wüstenrot Bauspar AG");
        customer3.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        customerRepository.save(customer3);

        Customer customer4 = new Customer();
        customer4.setCustomerName("Profect");
        customer4.setDivision("Profect DE GmbH");
        customer4.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        customerRepository.save(customer4);

        Project project = new Project();
        project.setProjectName("V7 Migration");
        project.setProjectKey("WST_2019_BRA_DL_002");
        project.setBenefitRecipient(customer);
        project.setInvoiceRecipient(customer2);
        project.setProjectType("Extern");
        project.setStartDate(LocalDate.of(2020, 6, 1));
        project.setEndDate(LocalDate.of(2020, 12, 31));
        project.setProjectLeader(employee2);
        project.setContractType("Service contract");
        project.setSales(950000d);
        project.setNotes("quarterly bills");
        project.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        projectRepository.save(project);

        Project project2 = new Project();
        project2.setProjectName("BR6 Wartung");
        project2.setProjectKey("WST_2019_BRA_WA_001");
        project2.setBenefitRecipient(customer);
        project2.setInvoiceRecipient(customer);
        project2.setProjectType("Offering");
        project2.setStartDate(LocalDate.of(2020, 9, 1));
        project2.setEndDate(LocalDate.of(2020, 12, 31));
        project2.setProjectLeader(employee2);
        project2.setContractType("Maintenance contract");
        project2.setSales(950000d);
        project2.setNotes("quarterly bills");
        project2.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        projectRepository.save(project2);

        Project project3 = new Project();
        project3.setProjectName("Internes Projekt");
        project3.setProjectKey("PRF_2020_INTERN_001");
        project3.setBenefitRecipient(customer4);
        project3.setInvoiceRecipient(customer4);
        project3.setProjectType("Intern");
        project3.setStartDate(LocalDate.of(2020, 1, 1));
        project3.setEndDate(LocalDate.of(2020, 12, 31));
        project3.setProjectLeader(employee2);
        project3.setContractType("Maintenance contract");
        project3.setSales(200000d);
        project3.setNotes("");
        project3.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        projectRepository.save(project3);

        Assignment assignment = new Assignment();
        assignment.setEmployee(employee5);
        assignment.setProject(project);
        assignment.setStartDate(LocalDate.of(2020, 6, 1));
        assignment.setEndDate(LocalDate.of(2020, 12, 31));
        assignment.setDailyRate(1000d);
        assignment.setWeeklyHours(16d);
        assignment.setMondayHours(0d);
        assignment.setTuesdayHours(0d);
        assignment.setWednesdayHours(0d);
        assignment.setThursdayHours(8d);
        assignment.setFridayHours(8d);
        assignment.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        assignmentRepository.save(assignment);

        Assignment assignment2 = new Assignment();
        assignment2.setEmployee(employee5);
        assignment2.setProject(project2);
        assignment2.setStartDate(LocalDate.of(2020, 9, 1));
        assignment2.setEndDate(LocalDate.of(2020, 12, 31));
        assignment2.setDailyRate(800d);
        assignment2.setWeeklyHours(16d);
        assignment2.setMondayHours(0d);
        assignment2.setTuesdayHours(8d);
        assignment2.setWednesdayHours(8d);
        assignment2.setThursdayHours(0d);
        assignment2.setFridayHours(0d);
        assignment2.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        assignmentRepository.save(assignment2);

        Assignment assignment3 = new Assignment();
        assignment3.setEmployee(employee5);
        assignment3.setProject(project3);
        assignment3.setStartDate(LocalDate.of(2020, 1, 1));
        assignment3.setEndDate(LocalDate.of(2020, 12, 31));
        assignment3.setDailyRate(800d);
        assignment3.setWeeklyHours(8d);
        assignment3.setMondayHours(8d);
        assignment3.setTuesdayHours(0d);
        assignment3.setWednesdayHours(0d);
        assignment3.setThursdayHours(0d);
        assignment3.setFridayHours(0d);
        assignment3.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
        assignmentRepository.save(assignment3);

        for (int i = 0; i<15; i++) {
            Employee employee = new Employee();
            employee.setIsTeamLeader(false);
            employee.setStaffNumber((2000 + i) + "");
            employee.setCaption("Dummy Employee");
            employee.setShorthand("dem");
            employee.setEmail("dummy@profect.de");
            employee.setFirstName("Dum" + i);
            employee.setLastName("Emp" + i);
            employee.setLocation("Profect DE");
            employee.setTeamLeader(null);
            employee.setContractType("salaried employee");
            employee.setWeeklyHours(32d);
            employee.setMondayHours(8d);
            employee.setTuesdayHours(8d);
            employee.setWednesdayHours(8d);
            employee.setThursdayHours(8d);
            employee.setFridayHours(0d);
            employee.setEntryDate(LocalDate.of(2020, 1, 1));
            employee.setRetirementDate(LocalDate.of(2040, 12, 31));
            employee.setComment("");
            employee.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
            employee = employeeRepository.save(employee);
        }

        for (int i = 0; i<15; i++) {
            Customer customer_ = new Customer();
            customer_.setCustomerName("Dummy Customer " + i);
            customer_.setDivision("Dummy Division");
            customer_.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
            customerRepository.save(customer_);
        }
        for (int i = 0; i<15; i++) {
            Project project_ = new Project();
            project_.setProjectName("Dummy Project " + i);
            project_.setProjectKey("PRF_2020_INTERN_" + i);
            project_.setBenefitRecipient(customer4);
            project_.setInvoiceRecipient(customer4);
            project_.setProjectType("Intern");
            project_.setStartDate(LocalDate.of(2020, 1, 1));
            project_.setEndDate(LocalDate.of(2020, 12, 31));
            project_.setProjectLeader(employee2);
            project_.setContractType("Maintenance contract");
            project_.setSales(200000d);
            project_.setNotes("");
            project_.setMetadataContainer(initService.initMetadataContainer(new Metadata(MetadataDefinition.CREATION_USER, "user")));
            projectRepository.save(project_);
        }

    }

    public FormSaveConnectorState initEmployeeFormSaveConnectorState(String caption, ComponentInstance source, ComponentInstance target, String triggerEvent) {
        EmployeeFormSaveConnectorState connector = new EmployeeFormSaveConnectorState();
        connector.setCaption(caption);
        connector.setTriggerEvent(triggerEvent);
        connector.setSource(source);
        connector.setTarget(target);
        connector.setMetadataContainer(this.initService.addDefaultMetadata(connector.getMetadataContainer()));
        return (EmployeeFormSaveConnectorState)this.connectorService.save(connector);
    }

    public FormSaveConnectorState initProjectFormSaveConnectorState(String caption, ComponentInstance source, ComponentInstance target, String triggerEvent) {
        ProjectFormSaveConnectorState connector = new ProjectFormSaveConnectorState();
        connector.setCaption(caption);
        connector.setTriggerEvent(triggerEvent);
        connector.setSource(source);
        connector.setTarget(target);
        connector.setMetadataContainer(this.initService.addDefaultMetadata(connector.getMetadataContainer()));
        return (ProjectFormSaveConnectorState)this.connectorService.save(connector);
    }

    public GraphUpdateConnectorState initGraphUpdateConnector(String caption, ComponentInstance source,
                                                              ComponentInstance target, String triggerEvent) {
        GraphUpdateConnectorState connector = new GraphUpdateConnectorState();
        connector.setCaption(caption);
        connector.setTriggerEvent(triggerEvent);
        connector.setSource(source);
        connector.setTarget(target);
        connector.setMetadataContainer(initService.addDefaultMetadata(connector.getMetadataContainer()));
        return (GraphUpdateConnectorState) connectorService.save(connector);
    }

}