package de.profect.inithelper;

import de.profect.assignmenttool.employee.hierarchy.EmployeeHierarchyDefinition;
import de.profect.assignmenttool.project.hierarchy.ProjectHierarchyDefinition;
import de.profect.component.ComponentInstance;
import de.profect.component.dataview.compass.report.CompassReport;
import de.profect.component.input.workflow.WorkflowGraphState;
import de.profect.component.input.workflow.WorkflowLevel;
import de.profect.component.input.workflow.WorkflowNode;
import de.profect.connector.ConnectorState;
import de.profect.data.hierarchy.HierarchyDefinition;
import de.profect.data.query.field.Field;
import de.profect.util.InitHelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class InitAssignmentWorkflowsService {

    @Autowired
    private InitHelperService initService;

    @Autowired
    private InitAssignmentPagesService initAssignmentPageService;

    public WorkflowGraphState createCustomerWorkflow(Integer pageId, Integer targetId, List<ComponentInstance> headerComponents, ComponentInstance overlayPanel, Field tableMainKPI, Field tableIdField, List<Field> tableFields, List<Field> tableFixedFields) {

        //Init workflow
        ComponentInstance workflow = initService.initWorkflowGraph("Customers", "order-2", "person", pageId, targetId, "MAIN");
        ComponentInstance[] headerArr = new ComponentInstance[headerComponents.size() + 1];
        headerArr = headerComponents.toArray(headerArr);
        headerArr[headerArr.length - 1] = workflow;
        ComponentInstance header = initService.panel().initFlowPanel("Header", "v7-header", false, true, false, headerArr);
        WorkflowGraphState graph = (WorkflowGraphState) workflow.getDefaultState();

        WorkflowLevel tableAndFieldAdministrationLevel = initService.initWorkflowLevel("Customers", 0);

        ComponentInstance customerAdministrationPage = initAssignmentPageService.createCustomerPage(overlayPanel, tableMainKPI, tableIdField, tableFields, tableFixedFields);

        ComponentInstance tableAndFieldAdministrationFullPage = initService.panel().initTopBottomPanel(customerAdministrationPage.getDefaultState().getCaption(), null, header, customerAdministrationPage);
        WorkflowNode formulaMetadataNode = initService.initWorkflowNode(tableAndFieldAdministrationFullPage);
        tableAndFieldAdministrationLevel.setNodes(Arrays.asList(formulaMetadataNode));

        graph.addLevel(tableAndFieldAdministrationLevel);

        return graph;
    }

    public WorkflowGraphState createEmployeeWorkflow(Integer pageId, Integer targetId, List<ComponentInstance> headerComponents, ComponentInstance overlayPanel,
                                                     Field employeeTableMainKPI, Field employeeTableIdField, Field employeeOrderByField, List<Field> employeeTableFields, List<Field> employeeTableFixedFields,
                                                     Field absenceTableMainKPI, Field absenceTableIdField, Field absenceOrderByField, List<Field> absenceTableFields, List<Field> absenceTableFixedFields,
                                                     Field assignmentTableMainKPI, Field assignmentTableIdField, Field assignmentOrderByField, List<Field> assignmentTableFields, List<Field> assignmentTableFixedFields,
                                                     EmployeeHierarchyDefinition employeeHierarchyDefinition, ProjectHierarchyDefinition projectHierarchyDefinition, EmployeeHierarchyDefinition teamleaderHierarchyDefinition,
                                                     Field assignmentStartDateField, Field absenceStartDateField) {

        //Init workflow
        ComponentInstance workflow = initService.initWorkflowGraph("Employees", "order-2", "face", pageId, targetId, "MAIN");
        ComponentInstance[] headerArr = new ComponentInstance[headerComponents.size() + 1];
        headerArr = headerComponents.toArray(headerArr);
        headerArr[headerArr.length - 1] = workflow;
        ComponentInstance header = initService.panel().initFlowPanel("Header", "v7-header", false, true, false, headerArr);
        WorkflowGraphState graph = (WorkflowGraphState) workflow.getDefaultState();

        WorkflowLevel tableAndFieldAdministrationLevel = initService.initWorkflowLevel("Employees", 0);

        ComponentInstance employeeAdministrationPage = initAssignmentPageService.createEmployeePage(overlayPanel,
                employeeTableMainKPI, employeeTableIdField, employeeOrderByField, employeeTableFields, employeeTableFixedFields,
                absenceTableMainKPI, absenceTableIdField, absenceOrderByField, absenceTableFields, absenceTableFixedFields,
                assignmentTableMainKPI, assignmentTableIdField, assignmentOrderByField, assignmentTableFields, assignmentTableFixedFields,
                employeeHierarchyDefinition, projectHierarchyDefinition, teamleaderHierarchyDefinition, assignmentStartDateField, absenceStartDateField);

        ComponentInstance tableAndFieldAdministrationFullPage = initService.panel().initTopBottomPanel(employeeAdministrationPage.getDefaultState().getCaption(), null, header, employeeAdministrationPage);
        WorkflowNode formulaMetadataNode = initService.initWorkflowNode(tableAndFieldAdministrationFullPage);
        tableAndFieldAdministrationLevel.setNodes(Arrays.asList(formulaMetadataNode));

        graph.addLevel(tableAndFieldAdministrationLevel);

        return graph;
    }

    public WorkflowGraphState createProjectWorkflow(Integer pageId, Integer targetId, List<ComponentInstance> headerComponents, ComponentInstance overlayPanel,
                                                    Field tableMainKPI, Field tableIdField, List<Field> tableFields, List<Field> tableFixedFields,
                                                    HierarchyDefinition employeeHierarchyDefinition, HierarchyDefinition customerHierarchyDefinition) {

        //Init workflow
        ComponentInstance workflow = initService.initWorkflowGraph("Projects", "order-2", "work", pageId, targetId, "MAIN");
        ComponentInstance[] headerArr = new ComponentInstance[headerComponents.size() + 1];
        headerArr = headerComponents.toArray(headerArr);
        headerArr[headerArr.length - 1] = workflow;
        ComponentInstance header = initService.panel().initFlowPanel("Header", "v7-header", false, true, false, headerArr);
        WorkflowGraphState graph = (WorkflowGraphState) workflow.getDefaultState();

        WorkflowLevel tableAndFieldAdministrationLevel = initService.initWorkflowLevel("Projects", 0);

        ComponentInstance projectAdministrationPage = initAssignmentPageService.createProjectPage(overlayPanel, tableMainKPI, tableIdField, tableFields, tableFixedFields, employeeHierarchyDefinition, customerHierarchyDefinition);

        ComponentInstance tableAndFieldAdministrationFullPage = initService.panel().initTopBottomPanel(projectAdministrationPage.getDefaultState().getCaption(), null, header, projectAdministrationPage);
        WorkflowNode formulaMetadataNode = initService.initWorkflowNode(tableAndFieldAdministrationFullPage);
        tableAndFieldAdministrationLevel.setNodes(Arrays.asList(formulaMetadataNode));

        graph.addLevel(tableAndFieldAdministrationLevel);

        return graph;
    }

    public WorkflowGraphState createCompassReportViewWorkflow(Integer pageId,
                                                              Integer targetId,
                                                              List<ComponentInstance> headerComponents,
                                                              CompassReport report) {

        ComponentInstance workflow = initService.initWorkflowGraph(report.getCaption(), "order-2", "table_chart", pageId, targetId, "MAIN");
        ComponentInstance[] headerArr = new ComponentInstance[headerComponents.size() + 1];
        headerArr = headerComponents.toArray(headerArr);
        headerArr[headerArr.length - 1] = workflow;
        ComponentInstance header = initService.initFlowPanel("Header", "v7-header", false, true, false, null, headerArr);
        WorkflowGraphState graph = (WorkflowGraphState) workflow.getDefaultState();

        WorkflowLevel compassReportViewLevel = initService.initWorkflowLevel(report.getCaption(), 1);
        ComponentInstance compassReportViewPage = initAssignmentPageService.createCompassPage(report);
        ComponentInstance compassReportViewFullPage = initService.panel().initTopBottomPanel(compassReportViewPage.getDefaultState().getCaption(), null, header, compassReportViewPage);
        WorkflowNode compassReportViewNode = initService.initWorkflowNode(compassReportViewFullPage);
        compassReportViewLevel.setNodes(Arrays.asList(compassReportViewNode));

        graph.addLevel(compassReportViewLevel);

        return graph;
    }

    public WorkflowGraphState createCompassReportViewWithFiltersWorkflow(Integer pageId,
                                                                         Integer targetId,
                                                                         List<ComponentInstance> headerComponents,
                                                                         List<ComponentInstance> filterComponents,
                                                                         CompassReport report) {

        ComponentInstance workflow = initService.initWorkflowGraph(report.getCaption(), "order-2", "table_chart", pageId, targetId, "MAIN");
        ComponentInstance[] headerArr = new ComponentInstance[headerComponents.size() + 1];
        headerArr = headerComponents.toArray(headerArr);
        headerArr[headerArr.length - 1] = workflow;
        ComponentInstance header = initService.initFlowPanel("Header", "v7-header", false, true, false, null, headerArr);
        WorkflowGraphState graph = (WorkflowGraphState) workflow.getDefaultState();

        WorkflowLevel compassReportViewLevel = initService.initWorkflowLevel(report.getCaption(), 1);
        ComponentInstance compassReportViewPage = initAssignmentPageService.createCompassPageWithFilters(filterComponents, report);
        ComponentInstance compassReportViewFullPage = initService.panel().initTopBottomPanel(compassReportViewPage.getDefaultState().getCaption(), null, header, compassReportViewPage);
        WorkflowNode compassReportViewNode = initService.initWorkflowNode(compassReportViewFullPage);
        compassReportViewLevel.setNodes(Arrays.asList(compassReportViewNode));

        graph.addLevel(compassReportViewLevel);

        return graph;
    }

    public WorkflowGraphState createCompassReport2ViewWorkflow(Integer pageId,
                                                               Integer targetId,
                                                               List<ComponentInstance> headerComponents,
                                                               ComponentInstance report) {

        initService.connector().initRefreshConnector("Refresh and calc", report, report, ConnectorState.TRIGGEREVENT_COMPONENT_INITIALIZED);

        ComponentInstance workflow = initService.initWorkflowGraph(report.getDefaultState().getCaption(), "order-2", "table_chart", pageId, targetId, "MAIN");
        ComponentInstance[] headerArr = new ComponentInstance[headerComponents.size() + 1];
        headerArr = headerComponents.toArray(headerArr);
        headerArr[headerArr.length - 1] = workflow;
        ComponentInstance header = initService.initFlowPanel("Header", "v7-header", false, true, false, null, headerArr);
        WorkflowGraphState graph = (WorkflowGraphState) workflow.getDefaultState();

        WorkflowLevel compassReportViewLevel = initService.initWorkflowLevel(report.getDefaultState().getCaption(), 1);
        ComponentInstance compassReportViewPage = initAssignmentPageService.createCompass2Page(report);
        ComponentInstance compassReportViewFullPage = initService.panel().initTopBottomPanel(compassReportViewPage.getDefaultState().getCaption(), null, header, compassReportViewPage);
        WorkflowNode compassReportViewNode = initService.initWorkflowNode(compassReportViewFullPage);
        compassReportViewLevel.setNodes(Arrays.asList(compassReportViewNode));

        graph.addLevel(compassReportViewLevel);

        return graph;
    }

    public WorkflowGraphState createCompassReport2ViewWithFiltersWorkflow(Integer pageId,
                                                                          Integer targetId,
                                                                          List<ComponentInstance> headerComponents,
                                                                          List<ComponentInstance> filterComponents,
                                                                          ComponentInstance report) {

        initService.connector().initRefreshConnector("Refresh and calc", report, report, ConnectorState.TRIGGEREVENT_COMPONENT_INITIALIZED);

        ComponentInstance workflow = initService.initWorkflowGraph(report.getDefaultState().getCaption(), "order-2", "table_chart", pageId, targetId, "MAIN");
        ComponentInstance[] headerArr = new ComponentInstance[headerComponents.size() + 1];
        headerArr = headerComponents.toArray(headerArr);
        headerArr[headerArr.length - 1] = workflow;
        ComponentInstance header = initService.initFlowPanel("Header", "v7-header", false, true, false, null, headerArr);
        WorkflowGraphState graph = (WorkflowGraphState) workflow.getDefaultState();

        WorkflowLevel compassReportViewLevel = initService.initWorkflowLevel(report.getDefaultState().getCaption(), 1);
        ComponentInstance compassReportViewPage = initAssignmentPageService.createCompass2PageWithFilters(filterComponents, report);
        ComponentInstance compassReportViewFullPage = initService.panel().initTopBottomPanel(compassReportViewPage.getDefaultState().getCaption(), null, header, compassReportViewPage);
        WorkflowNode compassReportViewNode = initService.initWorkflowNode(compassReportViewFullPage);
        compassReportViewLevel.setNodes(Arrays.asList(compassReportViewNode));

        graph.addLevel(compassReportViewLevel);

        return graph;
    }
}
