package de.profect.assignmenttool.monthlyavailability;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.profect.assignmenttool.employee.Employee;
import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "MONTHLY_AVAILABILITY")
@Data
public class MonthlyAvailability implements Serializable {

    private static final long serialVersionUID = 245599543205835817L;

    public MonthlyAvailability() {

    };

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @JsonProperty
    @NonNull
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMPLOYEE_ID")
    @JsonProperty
    private Employee employee;

    @Column(name = "AVAILABILITY")
    @JsonProperty
    private Double availability;

    @Column(name = "AVAILABILITY_DATE")
    @JsonProperty
    private LocalDate availabilityDate;

    @Override
    public String toString() {
        return id + "," + employee + "," + availability + "," + availabilityDate;
    }
}
