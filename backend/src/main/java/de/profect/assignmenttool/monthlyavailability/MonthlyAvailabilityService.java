package de.profect.assignmenttool.monthlyavailability;

import de.profect.assignmenttool.absence.Absence;
import de.profect.assignmenttool.absence.AbsenceRepository;
import de.profect.assignmenttool.employee.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class MonthlyAvailabilityService {

    @Autowired
    @Lazy
    private MonthlyAvailabilityRepository monthlyAvailabilityRepository;

    @Autowired
    @Lazy
    private AbsenceRepository absenceRepository;

    public void updateWeeklyHoursForEmployee(Employee employee) {

        LocalDate currentDate = employee.getEntryDate();
        monthlyAvailabilityRepository.deleteByEmployeeId(employee.getId());

        List<Absence> absences = absenceRepository.findByEmployeeAndEndDateAfter(employee, currentDate);
        Collections.sort(absences, Comparator.comparing(Absence::getStartDate));

        List<MonthlyAvailability> monthlyAvailabilityList = new ArrayList<>();

        while(currentDate.isBefore(employee.getRetirementDate())) {
            MonthlyAvailability monthlyAvailability = new MonthlyAvailability();
            monthlyAvailability.setEmployee(employee);

            monthlyAvailability.setAvailability(calculateMonthlyAvailability(employee, currentDate, absences));
            monthlyAvailability.setAvailabilityDate(currentDate);
            monthlyAvailabilityList.add(monthlyAvailability);

            currentDate = currentDate.plusMonths(1).withDayOfMonth(1);
        }

        monthlyAvailabilityRepository.saveAll(monthlyAvailabilityList);
    }

    private Double calculateMonthlyAvailability(Employee employee, LocalDate firstOfMonth, List<Absence> absences) {
        LocalDate lastOfMonth = firstOfMonth.withDayOfMonth(firstOfMonth.lengthOfMonth());
        Double monthlyHours = getHoursForMonth(firstOfMonth, employee);
        LocalDate currentDay = firstOfMonth;

        for(Absence absence : absences) {
            if((absence.getStartDate().isBefore(lastOfMonth) || absence.getStartDate().isEqual(lastOfMonth))
                    && (absence.getEndDate().isAfter(currentDay) || absence.getEndDate().isEqual(currentDay))) {
                LocalDate absentDay = absence.getStartDate().isBefore(currentDay) ? currentDay : absence.getStartDate();
                while((absentDay.isBefore(lastOfMonth) || absentDay.isEqual(lastOfMonth))
                        && (absentDay.isBefore(absence.getEndDate()) || absentDay.isEqual(absence.getEndDate()))) {
                    monthlyHours -= employee.getHoursForWeekDay(absentDay.getDayOfWeek());
                    absentDay = absentDay.plusDays(1);
                    currentDay = currentDay.plusDays(1);
                }
            }
        }
        return monthlyHours / 8;
    }

    private Double getHoursForMonth(LocalDate firstOfMonth, Employee employee) {
        LocalDate day = firstOfMonth;
        Double hours = 0d;
        while(day.isBefore(firstOfMonth.plusMonths(1))) {
            if(day.getDayOfWeek() != DayOfWeek.SATURDAY && day.getDayOfWeek() != DayOfWeek.SUNDAY) {
                hours += employee.getHoursForWeekDay(day.getDayOfWeek());
            }
            day = day.plusDays(1);
        }
        return hours;
    }
}
