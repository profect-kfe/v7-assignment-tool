package de.profect.assignmenttool.monthlyavailability;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import de.profect.assignmenttool.employee.Employee;
import org.springframework.data.repository.CrudRepository;

public interface MonthlyAvailabilityRepository extends CrudRepository<MonthlyAvailability, Integer>{

    @Override
    public void deleteById(Integer id);

    @Override
    public List<MonthlyAvailability> findAll();

    @Override
    public Optional<MonthlyAvailability> findById(Integer id);

    public List<MonthlyAvailability> findByEmployeeAndAvailabilityDateAfter(Employee employee, LocalDate availabilityDate);

    public void deleteByEmployeeAndAvailabilityDateAfter(Employee employee, LocalDate availabilityDate);

    @Override
    public <P extends MonthlyAvailability> P save(P persist);

    @Override
    public <P extends MonthlyAvailability> Iterable<P>  saveAll(Iterable<P> WeeklyHoursPerEmployeeList);

    public void deleteByEmployeeId(int employeeId);
}