package de.profect.assignmenttool.deltatoken;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.AbstractGUIState;
import de.profect.assignmenttool.employee.Employee;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "DELTATOKEN")
@JsonTypeName(value = "v7-assignment-delta-token")
@Data
@EqualsAndHashCode(callSuper = true)
public class DeltaToken extends AbstractGUIState {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMPLOYEE_ID")
    @JsonProperty
    private Employee employee;

    @Column(name = "DELTA_TOKEN")
    @JsonProperty
    private String deltaToken;

    @Override
    public String getControllerPath() {
        return "DELTATOKEN";
    }
}
