package de.profect.assignmenttool.deltatoken;

import de.profect.authentication.secured.SecuredService;
import org.springframework.stereotype.Service;

/**
 *
 * @author nsc
 */
@Service
public class DeltaTokenService extends SecuredService<DeltaToken> {

    @Override
    public String getPrivilegePrefix() {
        return "DELTATOKEN";
    }

}
