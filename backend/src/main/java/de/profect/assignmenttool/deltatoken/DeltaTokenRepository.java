package de.profect.assignmenttool.deltatoken;

import de.profect.assignmenttool.employee.Employee;
import de.profect.authentication.secured.SecuredRepository;
import de.profect.data.metadata.MetadataContainer;
import de.profect.data.metadata.MetadataDefinition;
import de.profect.data.metadata.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author nsc
 */
@Repository
public class DeltaTokenRepository extends SecuredRepository<DeltaToken> {

    @Autowired
    @Lazy
    private MetadataService metadataService;

    @Override
    public <P extends DeltaToken> P save(P entity) {
        if (entity.getCaption().equals("")) { entity.setCaption("deltatoken_vacation"); }
        entity.setMetadataContainer(addDefaultMetadata(entity.getMetadataContainer()));
        entity = super.save(entity);
        return entity;
    }

/*    @Override
    public <P extends Absence> P merge(P entity) {
        entity.setCaption(entity.getReason());
        entity = super.merge(entity);
        weeklyQuotaPerEmployeeService.updateWeeklyHoursForEmployee(entity.getEmployee());
        monthlyAvailabilityService.updateWeeklyHoursForEmployee(entity.getEmployee());
        return entity;
    }*/

    public List<DeltaToken> findByEmployee(Employee employee) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.employee = :employee AND a.caption = :caption", getEntityClass())
                .setParameter("employee", employee).setParameter("caption", "deltatoken_Vacation")
                .getResultList();
    }

    public List<DeltaToken> findHolidayTokenByEmployee(Employee employee) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.employee = :employee AND a.caption = :caption", getEntityClass())
                .setParameter("employee", employee).setParameter("caption", "deltatoken_Holiday")
                .getResultList();
    }

    public MetadataContainer addDefaultMetadata(MetadataContainer metadataContainer) {
        metadataContainer = metadataService.addSystemMetadata(metadataContainer);
        if (metadataContainer.getMetaValue(MetadataDefinition.READ_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.READ_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.WRITE_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.WRITE_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.IS_CACHEABLE) == null) {
            metadataContainer.addMetadata(MetadataDefinition.IS_CACHEABLE, Boolean.FALSE);
        }
        return metadataService.save(metadataContainer);
    }

}
