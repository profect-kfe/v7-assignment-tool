package de.profect.assignmenttool.project.hierarchy;

import de.profect.assignmenttool.project.Project;
import de.profect.data.hierarchy.AbstractGUIStateItem;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author kfe
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProjectHierarchyItem extends AbstractGUIStateItem<Project>{

    private static final long serialVersionUID = -5416832138454896681L;

}
