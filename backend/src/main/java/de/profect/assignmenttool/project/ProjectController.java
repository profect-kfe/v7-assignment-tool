package de.profect.assignmenttool.project;

import de.profect.authentication.secured.SecuredController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/project")
public class ProjectController extends SecuredController<Project> {

}
