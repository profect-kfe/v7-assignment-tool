package de.profect.assignmenttool.project;

import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.component.formbuilder.validators.AbstractAsyncValidatorState;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("v7-project-validator")
@JsonTypeName("v7-project-validator")
@Data
@EqualsAndHashCode(callSuper = true)
public class ProjectValidatorState extends AbstractAsyncValidatorState {

}

