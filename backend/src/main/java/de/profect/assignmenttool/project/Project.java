package de.profect.assignmenttool.project;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.AbstractGUIState;
import de.profect.assignmenttool.customer.Customer;
import de.profect.assignmenttool.employee.Employee;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "PROJECTS")
@JsonTypeName(value = "v7-assignment-project")
@Data
@EqualsAndHashCode(callSuper = true)
public class Project extends AbstractGUIState {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVOICE_RECIPIENT_CUSTOMER_ID")
    @JsonProperty
    private Customer invoiceRecipient;

    @Column(name = "PROJECT_KEY")
    @JsonProperty
    private String projectKey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BENEFIT_RECIPIENT_CUSTOMER_ID")
    @JsonProperty
    private Customer benefitRecipient;

    @Column(name = "PROJECT_NAME")
    @JsonProperty
    private String projectName;

    @Column(name = "CUSTOMER_PROJECT_NAME")
    @JsonProperty
    private String customerProjectName;

    @Column(name = "PROJECT_TYPE")
    @JsonProperty
    private String projectType;

    @Column(name = "START_DATE")
    @JsonProperty
    private LocalDate startDate;

    @Column(name = "END_DATE")
    @JsonProperty
    private LocalDate endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_LEADER_EMPLOYEE_ID")
    @JsonProperty
    private Employee projectLeader;

    @Column(name = "CONTRACT_TYPE")
    @JsonProperty
    private String contractType;

    @Column(name = "SALES")
    @JsonProperty
    private Double sales;

    @Column(name = "NOTES")
    @JsonProperty
    private String notes;

    @Override
    public String getControllerPath() {
        return "PROJECT";
    }
}
