package de.profect.assignmenttool.project.hierarchy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.profect.assignmenttool.project.Project;
import de.profect.assignmenttool.project.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.profect.data.hierarchy.HierarchyDefinitionService;
import de.profect.data.hierarchy.HierarchyItem;
import lombok.extern.log4j.Log4j2;

/**
 * ProjectHierarchyService retrieves HierarchyData from state.
 *
 * @author kfe
 */
@Log4j2
@Service
public class ProjectHierarchyService extends HierarchyDefinitionService<ProjectHierarchyDefinition> {

    @Autowired
    private ProjectService service;

    @Override
    protected String getPrivilegePrefix() {
        return "PROJECT_HIERARCHY_DEFINITION";
    }

    @Override
    public List<List<HierarchyItem<?, ?>>> getDataForState(ProjectHierarchyDefinition state) {
        List<List<HierarchyItem<?, ?>>> result = new ArrayList<>();
        List<Project> entities = service.fetchFiltered(state.getFilter());
        Collections.sort(entities, (o1, o2) -> o1.getProjectName().toLowerCase().compareTo(o2.getProjectName().toLowerCase()));
        for (Project entity : entities) {
            List<HierarchyItem<?, ?>> path = new ArrayList<>();
            addGroupPath(entity.getCategory(), path);
            path.add(getItemFromEntity(entity));
            result.add(path);
        }
        return result;
    }

    protected ProjectHierarchyItem getItemFromEntity(Project entity) {
        if (entity != null) {
            ProjectHierarchyItem item = new ProjectHierarchyItem();
            item.setId(entity.getId());
            item.setCaption(entity.getProjectName() + "  (" + entity.getProjectKey() + ")");
            item.setOriginalObject(entity);
            return item;
        }
        return null;
    }
}
