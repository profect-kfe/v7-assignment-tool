package de.profect.assignmenttool.project.hierarchy;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import de.profect.data.hierarchy.HierarchyDefinition;
import de.profect.data.metadata.MetadataContainer;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author kfe
 */
@Entity
@DiscriminatorValue("v7-project-hierarchy-definition")
@JsonTypeName("v7-project-hierarchy-definition")
@Data
@EqualsAndHashCode(callSuper = true)
public class ProjectHierarchyDefinition extends HierarchyDefinition {

    private static final long serialVersionUID = 5820267963576263651L;

    /**
     * The {@link de.profect.data.metadata.MetadataContainer} represents the filter
     * that should be used on the ProjectHierarchyDefinition.
     */
    @ManyToOne
    @JoinColumn(name = "FILTER_ID")
    @JsonProperty
    protected MetadataContainer filter;

    @Override
    public String getControllerPath() {
        return "PROJECTHIERARCHYDEFINITION";
    }

}
