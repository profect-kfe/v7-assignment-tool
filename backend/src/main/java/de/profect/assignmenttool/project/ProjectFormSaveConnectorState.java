package de.profect.assignmenttool.project;

import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.connector.target.form.save_form.FormSaveConnectorState;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("v7-project-form-save-connector")
@JsonTypeName("v7-project-form-save-connector")
public class ProjectFormSaveConnectorState extends FormSaveConnectorState {
}
