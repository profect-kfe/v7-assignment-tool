package de.profect.assignmenttool.project.hierarchy;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.profect.data.hierarchy.HierarchyDefinitionController;

/**
 * The ProjectHierarchyController handles request from the client concerning the ProjectHierarchyDefinition
 * @author kfe
 */
@RestController
@RequestMapping(value = "/projecthierarchydefinition")
public class ProjectHierarchyController extends HierarchyDefinitionController<ProjectHierarchyDefinition, ProjectHierarchyService> {

}
