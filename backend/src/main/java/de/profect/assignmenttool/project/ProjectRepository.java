package de.profect.assignmenttool.project;

import de.profect.assignmenttool.absence.Absence;
import de.profect.assignmenttool.assignment.Assignment;
import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.monthlyprojectsales.MonthlyProjectSalesService;
import de.profect.assignmenttool.weeklyquotaperemployee.WeeklyQuotaPerEmployee;
import de.profect.authentication.secured.SecuredRepository;
import de.profect.data.metadata.MetadataContainer;
import de.profect.data.metadata.MetadataDefinition;
import de.profect.data.metadata.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class ProjectRepository extends SecuredRepository<Project> {

    @Autowired
    private MetadataService metadataService;

    @Autowired
    @Lazy
    private MonthlyProjectSalesService monthlyProjectSalesService;

    @Override
    public <P extends Project> P save(P entity) {
        entity.setCaption(entity.getProjectKey());
        entity.setMetadataContainer(addDefaultMetadata(entity.getMetadataContainer()));
        entity = super.save(entity);

        monthlyProjectSalesService.updateMonthlyProjectSales(entity);

        return entity;
    }

    @Override
    public <P extends Project> P merge(P entity) {
        entity.setCaption(entity.getProjectKey());
        entity = super.merge(entity);

        monthlyProjectSalesService.updateMonthlyProjectSales(entity);

        return entity;
    }

    public List<Project> findByProjectLeader(Employee projectLeader) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.projectLeader = :projectLeader", getEntityClass())
                .setParameter("projectLeader", projectLeader)
                .getResultList();
    };

    public MetadataContainer addDefaultMetadata(MetadataContainer metadataContainer) {
        metadataContainer = metadataService.addSystemMetadata(metadataContainer);
        if (metadataContainer.getMetaValue(MetadataDefinition.READ_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.READ_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.WRITE_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.WRITE_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.IS_CACHEABLE) == null) {
            metadataContainer.addMetadata(MetadataDefinition.IS_CACHEABLE, Boolean.FALSE);
        }
        return metadataService.save(metadataContainer);
    }

    @Override
    public void delete(Project entity) {
        monthlyProjectSalesService.deleteMonthlyProjectSales(entity);
        super.delete(entity);
    }

    @Override
    public void deleteById(Integer id) {
        Optional<Project> optionalProject = findById(id);
        if(optionalProject.isPresent()) {
            Project project = optionalProject.get();
            monthlyProjectSalesService.deleteMonthlyProjectSales(project);
        }
        super.deleteById(id);
    }

    @Override
    public void deleteAll(Iterable<? extends Project> entities) {
        for(Project project : entities) {
            monthlyProjectSalesService.deleteMonthlyProjectSales(project);
        }
        super.deleteAll(entities);
    }


}