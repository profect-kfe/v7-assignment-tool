package de.profect.assignmenttool.project;

import de.profect.assignmenttool.weeklyassignmenthoursperemployee.WeeklyAssignmentsRepository;
import de.profect.assignmenttool.assignment.AssignmentService;
import de.profect.assignmenttool.weeklyquotaperemployee.WeeklyQuotaPerEmployeeRepository;
import de.profect.authentication.secured.SecuredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kfe
 */
@Service
public class ProjectService extends SecuredService<Project> {

    @Lazy
    @Autowired
    ProjectService projectService;

    @Lazy
    @Autowired
    WeeklyAssignmentsRepository weeklyAssignmentsRepository;

    @Lazy
    @Autowired
    WeeklyQuotaPerEmployeeRepository weeklyQuotaPerEmployeeRepository;

    @Lazy
    @Autowired
    AssignmentService assignmentService;

    @Override
    public String getPrivilegePrefix() {
        return "PROJECT";
    }

    @Transactional
    public List<Project> findByProjectLeaderEmployeeId(int projectLeaderEmployeeId) {
        List<Project> allProjects = repo.findAll();
        List<Project> filteredProjects = new ArrayList<>();
        for (Project project : allProjects) {
            if (project.getProjectLeader().getId() == projectLeaderEmployeeId) {
                filteredProjects.add(project);
            }
        }
        return filteredProjects;
    }

    /***
     * Returns a list of projects that have the customer either as invoice recipient and/or benefit recipient.
     * @param customerId
     * @return
     */
    @Transactional
    public List<Project> findByCustomer(int customerId) {
        List<Project> allProjects = repo.findAll();
        List<Project> filteredProjects = new ArrayList<>();
        for (Project project : allProjects) {
            if (project.getBenefitRecipient().getId() == customerId || project.getInvoiceRecipient().getId() == customerId) {
                filteredProjects.add(project);
            }
        }
        return filteredProjects;
    }

    @Transactional
    public String delete(int id) {
        assignmentService.deleteByProjectId(id);
        weeklyAssignmentsRepository.deleteByProjectId(id);
        repo.deleteById(id);
        return null;
    }
}
