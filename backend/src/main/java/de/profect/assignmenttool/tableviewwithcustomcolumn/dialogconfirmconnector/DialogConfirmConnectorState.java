package de.profect.assignmenttool.tableviewwithcustomcolumn.dialogconfirmconnector;

import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.connector.target.content.ContentConnectorState;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@DiscriminatorValue("v7-dialog-confirm-connector")
@JsonTypeName("v7-dialog-confirm-connector")
@Data
@EqualsAndHashCode(callSuper = true)
public class DialogConfirmConnectorState extends ContentConnectorState {

}
