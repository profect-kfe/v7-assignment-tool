package de.profect.assignmenttool.tableviewwithcustomcolumn.dynamiclabelmessageconnector;

import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.connector.target.content.ContentConnectorState;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@DiscriminatorValue("v7-dynamic-label-message-connector")
@JsonTypeName("v7-dynamic-label-message-connector")
@Data
@EqualsAndHashCode(callSuper = true)
public class DynamicLabelMessageConnectorState extends ContentConnectorState {

}
