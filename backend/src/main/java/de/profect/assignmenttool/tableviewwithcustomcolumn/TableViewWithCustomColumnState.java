package de.profect.assignmenttool.tableviewwithcustomcolumn;

import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.component.dataview.tableview.TableViewState;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("v7-tableview-cuco-component")
@JsonTypeName("v7-tableview-cuco-component")
@Data
@EqualsAndHashCode(callSuper = true)
public class TableViewWithCustomColumnState extends TableViewState {

}
