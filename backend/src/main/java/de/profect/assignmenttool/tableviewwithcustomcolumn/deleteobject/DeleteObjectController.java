package de.profect.assignmenttool.tableviewwithcustomcolumn.deleteobject;

import de.profect.assignmenttool.absence.AbsenceRepository;
import de.profect.assignmenttool.absence.AbsenceService;
import de.profect.assignmenttool.assignment.AssignmentRepository;
import de.profect.assignmenttool.assignment.AssignmentService;
import de.profect.assignmenttool.customer.CustomerRepository;
import de.profect.assignmenttool.customer.CustomerService;
import de.profect.assignmenttool.employee.EmployeeRepository;
import de.profect.assignmenttool.employee.EmployeeService;
import de.profect.assignmenttool.project.ProjectRepository;
import de.profect.assignmenttool.project.ProjectService;
import de.profect.assignmenttool.tableviewwithcustomcolumn.TableViewWithCustomColumnState;
import de.profect.authentication.secured.SecuredController;
import de.profect.component.dataview.tableview.TableViewState;
import liquibase.pro.packaged.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author nsc
 */
@RestController
@RequestMapping(value = "/deleteservice")
@ControllerAdvice
public class DeleteObjectController extends SecuredController<TableViewState> {

    @Lazy
    @Autowired
    AssignmentRepository assignmentRepository;

    @Lazy
    @Autowired
    AbsenceRepository absenceRepository;

    @Lazy
    @Autowired
    CustomerService customerService;

    @Lazy
    @Autowired
    EmployeeService employeeService;

    @Lazy
    @Autowired
    ProjectService projectService;

    @Transactional
    @RequestMapping(value = {"/deleteobject"}, method = RequestMethod.GET)
    public String deleteObject(@RequestParam("identifier") String identifier, @RequestParam("objectType") String objectType) {

        int id;

        try {
            id = Integer.parseInt(identifier);
        } catch (NumberFormatException e) {
            return null;
        }

        switch (objectType) {
            case "Scheduled Absences":
                absenceRepository.deleteById(id);
                return null;
            case "Assignments":
                assignmentRepository.deleteById(id);
                return null;
            case "Customers":
                customerService.delete(id);
                return null;
            case "Employees":
                return employeeService.delete(id);
            case "Projects":
                projectService.delete(id);
                return null;
            default:
                return null;
        }
    }
}