package de.profect.assignmenttool.weeklyassignmenthoursperemployee;

import de.profect.assignmenttool.employee.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface WeeklyAssignmentsRepository extends CrudRepository<WeeklyAssignments, Integer>{

    @Override
    public void deleteById(Integer id);

    @Override
    public List<WeeklyAssignments> findAll();

    @Override
    public Optional<WeeklyAssignments> findById(Integer id);

    public List<WeeklyAssignments> findByEmployee(Employee employee);

    public void deleteByEmployee(Employee employee);

    @Override
    public <P extends WeeklyAssignments> P save(P persist);

    @Override
    public <P extends WeeklyAssignments> Iterable<P>  saveAll(Iterable<P> WeeklyHoursPerEmployeeList);

    public void deleteByEmployeeId(int employeeId);

    public void deleteByProjectId(int projectId);

}
