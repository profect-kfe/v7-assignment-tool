package de.profect.assignmenttool.weeklyassignmenthoursperemployee;


import com.fasterxml.jackson.annotation.JsonProperty;
import de.profect.assignmenttool.customer.Customer;
import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.project.Project;
import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "WEEKLY_ASSIGNMENTS")
@Data
public class WeeklyAssignments implements Serializable {

    private static final long serialVersionUID = -7157308925115158556L;

    public WeeklyAssignments() {

    };

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @JsonProperty
    @NonNull
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMPLOYEE_ID")
    @JsonProperty
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_ID")
    @JsonProperty
    private Project project;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID")
    @JsonProperty
    private Customer customer;

    @Column(name = "HOURS")
    @JsonProperty
    private Double hours;

    @Column(name = "MAN_DAYS")
    @JsonProperty
    private Double manDays;

    @Column(name = "DAILY_RATES")
    @JsonProperty
    private Double dailyRates;

    @Column(name = "WEEK_DATE")
    @JsonProperty
    private LocalDate weekDate;

    @Override
    public String toString() {
        return id + "," + employee + "," + project+ "," + hours + "," + weekDate;
    }
}
