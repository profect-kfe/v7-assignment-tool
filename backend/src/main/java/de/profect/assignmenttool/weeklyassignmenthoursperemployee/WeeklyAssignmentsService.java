package de.profect.assignmenttool.weeklyassignmenthoursperemployee;

import de.profect.assignmenttool.assignment.Assignment;
import de.profect.assignmenttool.assignment.AssignmentRepository;
import de.profect.assignmenttool.employee.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.*;

@Service
public class WeeklyAssignmentsService {

    @Autowired
    @Lazy
    private WeeklyAssignmentsRepository weeklyAssignmentsRepository;

    @Autowired
    @Lazy
    private AssignmentRepository assignmentRepository;

    public void updateWeeklyAssignments(Employee employee) {
        weeklyAssignmentsRepository.deleteByEmployee(employee);

        List<Assignment> assignments = assignmentRepository.findByEmployee(employee);
        List<WeeklyAssignments> weeklyAssignmentsList = new ArrayList<>();

        for(Assignment assignment : assignments) {
            LocalDate mondayOfWeek = assignment.getStartDate().minusDays(assignment.getStartDate().getDayOfWeek().getValue() - 1);
            Double dailyRate = assignment.getDailyRate();
            while(mondayOfWeek.isBefore(assignment.getEndDate()) || mondayOfWeek.isEqual(assignment.getEndDate())) {
                Double hours = calculateWeeklyHours(assignment, mondayOfWeek);
                Double manDays = hours / 8d;
                WeeklyAssignments weeklyAssignments = new WeeklyAssignments();
                weeklyAssignments.setEmployee(employee);
                weeklyAssignments.setProject(assignment.getProject());
                weeklyAssignments.setCustomer((assignment.getProject().getInvoiceRecipient()));
                weeklyAssignments.setHours(hours);
                weeklyAssignments.setManDays(manDays);
                weeklyAssignments.setDailyRates(manDays * dailyRate);
                weeklyAssignments.setWeekDate(mondayOfWeek);

                weeklyAssignmentsList.add(weeklyAssignments);

                mondayOfWeek = mondayOfWeek.plusWeeks(1);
            }
        }

        weeklyAssignmentsRepository.saveAll(weeklyAssignmentsList);
    }

    private Double calculateWeeklyHours(Assignment assignment, LocalDate mondayOfWeek) {
        if(mondayOfWeek.isBefore(assignment.getStartDate()) || assignment.getEndDate().isBefore(mondayOfWeek.plusDays(4))) {
            Double weeklyHours = 0d;
            LocalDate start = mondayOfWeek.isBefore(assignment.getStartDate()) ? assignment.getStartDate() : mondayOfWeek;
            LocalDate end = assignment.getEndDate().isBefore(mondayOfWeek.plusDays(4)) ? assignment.getEndDate() : mondayOfWeek.plusDays(4);
            while(start.isBefore(end) || start.isEqual(end)) {
                weeklyHours += assignment.getHoursForWeekDay(start.getDayOfWeek());
                start = start.plusDays(1);
            }
            return weeklyHours;
        }

        return assignment.getWeeklyHours();
    }

}
