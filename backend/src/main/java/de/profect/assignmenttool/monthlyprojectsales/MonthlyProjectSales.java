package de.profect.assignmenttool.monthlyprojectsales;


import com.fasterxml.jackson.annotation.JsonProperty;
import de.profect.assignmenttool.customer.Customer;
import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.project.Project;
import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "MONTHLY_PROJECT_SALES")
@Data
public class MonthlyProjectSales implements Serializable {

    private static final long serialVersionUID = 6103113037287340928L;

    public MonthlyProjectSales() {

    };

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @JsonProperty
    @NonNull
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_ID")
    @JsonProperty
    private Project project;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID")
    @JsonProperty
    private Customer customer;

    @Column(name = "SALES")
    @JsonProperty
    private Double sales;

    @Column(name = "SALES_DATE")
    @JsonProperty
    private LocalDate date;

    @Override
    public String toString() {
        return id + "," + customer + "," + project+ ","  + date;
    }
}