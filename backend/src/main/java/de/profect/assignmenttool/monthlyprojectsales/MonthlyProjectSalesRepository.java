package de.profect.assignmenttool.monthlyprojectsales;

import de.profect.assignmenttool.project.Project;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface MonthlyProjectSalesRepository extends CrudRepository<MonthlyProjectSales, Integer> {

    @Override
    public void deleteById(Integer id);

    @Override
    public List<MonthlyProjectSales> findAll();

    @Override
    public Optional<MonthlyProjectSales> findById(Integer id);

    public void deleteByProject(Project project);

    @Override
    public <P extends MonthlyProjectSales> P save(P persist);

    @Override
    public <P extends MonthlyProjectSales> Iterable<P>  saveAll(Iterable<P> MonthlyProjectSales);

    public void deleteByProjectId(int projectId);

}
