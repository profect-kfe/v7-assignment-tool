package de.profect.assignmenttool.monthlyprojectsales;

import de.profect.assignmenttool.project.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class MonthlyProjectSalesService {

    @Autowired
    @Lazy
    private MonthlyProjectSalesRepository monthlyProjectSalesRepository;

    public void updateMonthlyProjectSales(Project project) {

        monthlyProjectSalesRepository.deleteByProject(project);

        LocalDate month = project.getStartDate();
        int months = 0;
        while(month.isBefore(project.getEndDate())) {
            month = month.withDayOfMonth(1);
            months += 1;
            month = month.plusMonths(1);
        }
        month = project.getStartDate();
        Double salesPerMonth = project.getSales() / months;

        List<MonthlyProjectSales> monthlyProjectSalesList = new ArrayList<>();
        while(month.isBefore(project.getEndDate())) {
            month = month.withDayOfMonth(1);

            MonthlyProjectSales monthlyProjectSales = new MonthlyProjectSales();
            monthlyProjectSales.setProject(project);
            monthlyProjectSales.setCustomer(project.getInvoiceRecipient());
            monthlyProjectSales.setSales(salesPerMonth);
            monthlyProjectSales.setDate(month);
            monthlyProjectSalesList.add(monthlyProjectSales);

            month = month.plusMonths(1);
        }

        monthlyProjectSalesRepository.saveAll(monthlyProjectSalesList);
    }

    public void deleteMonthlyProjectSales(Project project) {
        monthlyProjectSalesRepository.deleteByProject(project);
    }
}
