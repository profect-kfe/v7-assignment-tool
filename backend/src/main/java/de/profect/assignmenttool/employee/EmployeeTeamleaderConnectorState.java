package de.profect.assignmenttool.employee;

import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.connector.target.TargetConnectorState;
import lombok.Data;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("v7-employee-teamleader-connector")
@JsonTypeName("v7-employee-teamleader-connector")
@Data
public class EmployeeTeamleaderConnectorState extends TargetConnectorState {

}

