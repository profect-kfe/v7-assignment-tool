package de.profect.assignmenttool.employee;

import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.component.formbuilder.validators.AbstractAsyncValidatorState;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("v7-employee-validator")
@JsonTypeName("v7-employee-validator")
@Data
@EqualsAndHashCode(callSuper = true)
public class EmployeeValidatorState extends AbstractAsyncValidatorState {

}

