package de.profect.assignmenttool.employee.hierarchy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.profect.data.hierarchy.HierarchyDefinitionService;
import de.profect.data.hierarchy.HierarchyItem;
import lombok.extern.log4j.Log4j2;

/**
 * EmployeeHierarchyService retrieves HierarchyData from state.
 *
 * @author kfe
 */
@Log4j2
@Service
public class EmployeeHierarchyService extends HierarchyDefinitionService<EmployeeHierarchyDefinition> {

    @Autowired
    private EmployeeService service;

    @Override
    protected String getPrivilegePrefix() {
        return "EMPLOYEE_HIERARCHY_DEFINITION";
    }

    @Override
    public List<List<HierarchyItem<?, ?>>> getDataForState(EmployeeHierarchyDefinition state) {
        List<List<HierarchyItem<?, ?>>> result = new ArrayList<>();
        List<Employee> entities = service.fetchFiltered(state.getFilter());
        Collections.sort(entities, (o1, o2) -> o1.getCaption().compareTo(o2.getCaption()));
        for (Employee entity : entities) {
            List<HierarchyItem<?, ?>> path = new ArrayList<>();
            addGroupPath(entity.getCategory(), path);
            path.add(getItemFromEntity(entity));
            result.add(path);
        }
        return result;
    }

    protected EmployeeHierarchyItem getItemFromEntity(Employee entity) {
        if (entity != null) {
            EmployeeHierarchyItem item = new EmployeeHierarchyItem();
            item.setId(entity.getId());
            item.setCaption(entity.getCaption());
            item.setOriginalObject(entity);
            return item;
        }
        return null;
    }

}
