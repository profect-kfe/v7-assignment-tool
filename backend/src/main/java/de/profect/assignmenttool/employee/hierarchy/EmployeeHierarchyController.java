package de.profect.assignmenttool.employee.hierarchy;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.profect.data.hierarchy.HierarchyDefinitionController;

/**
 * The EmployeeHierarchyController handles request from the client concerning the EmployeeHierarchyDefinition
 * @author kfe
 */
@RestController
@RequestMapping(value = "/employeehierarchydefinition")
public class EmployeeHierarchyController extends HierarchyDefinitionController<EmployeeHierarchyDefinition, EmployeeHierarchyService> {

}
