package de.profect.assignmenttool.employee;

import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.connector.target.form.save_form.FormSaveConnectorState;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("v7-employee-form-save-connector")
@JsonTypeName("v7-employee-form-save-connector")
public class EmployeeFormSaveConnectorState extends FormSaveConnectorState {
}
