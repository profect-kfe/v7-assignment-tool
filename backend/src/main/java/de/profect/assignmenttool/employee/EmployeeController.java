package de.profect.assignmenttool.employee;

import de.profect.authentication.secured.SecuredController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController extends SecuredController<Employee> {

    @Autowired
    EmployeeService employeeService;

    @RequestMapping(value = {"/getstaffnumbers"}, method = RequestMethod.GET)
    public List<String> getStaffNumbers() {

        return employeeService.getEmployeeStaffNumbers();
    }
}
