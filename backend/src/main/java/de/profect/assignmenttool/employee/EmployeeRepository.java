package de.profect.assignmenttool.employee;

import de.profect.assignmenttool.absence.AbsenceRepository;
import de.profect.assignmenttool.assignment.Assignment;
import de.profect.assignmenttool.monthlyavailability.MonthlyAvailability;
import de.profect.assignmenttool.monthlyavailability.MonthlyAvailabilityService;
import de.profect.assignmenttool.weeklyquotaperemployee.WeeklyQuotaPerEmployeeService;
import de.profect.authentication.secured.SecuredRepository;
import de.profect.data.metadata.MetadataContainer;
import de.profect.data.metadata.MetadataDefinition;
import de.profect.data.metadata.MetadataService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.util.List;

@Log4j2
@Repository
public class EmployeeRepository extends SecuredRepository<Employee> {

    @Autowired
    @Lazy
    private MetadataService metadataService;

    @Autowired
    @Lazy
    private WeeklyQuotaPerEmployeeService weeklyQuotaPerEmployeeService;

    @Autowired
    @Lazy
    private MonthlyAvailabilityService monthlyAvailabilityService;


    @Override
    public <P extends Employee> P save(P entity) {
        if (entity.getIsTeamLeader() == null) {
            entity.setIsTeamLeader(false);
        }
        if(entity.getShorthand() != null) {
            entity.setShorthand(entity.getShorthand().toUpperCase());
        }
        replaceNullValues(entity);
        entity.setCaption(entity.getLastName()+ ", " + entity.getFirstName());
        entity.setMetadataContainer(addDefaultMetadata(entity, entity.getMetadataContainer()));

        if (entity.getIsTeamLeader()) {
            entity.setTeamLeaderShorthand(entity.getShorthand());
        } else if (entity.getTeamLeader() != null && entity.getTeamLeader().getShorthand() != null){
            entity.setTeamLeaderShorthand(entity.getTeamLeader().getShorthand().toUpperCase());
        }
        entity = super.save(entity);

        updateDerivedData(entity);

        return entity;
    }

    @Override
    public <P extends Employee> P merge(P entity) {
        if (entity.getIsTeamLeader() == null) {
            entity.setIsTeamLeader(false);
        }
        if(entity.getShorthand() != null) {
            entity.setShorthand(entity.getShorthand().toUpperCase());
        }
        replaceNullValues(entity);
        entity.setCaption(entity.getLastName()+ ", " + entity.getFirstName());
        entity.setMetadataContainer(addDefaultMetadata(entity, entity.getMetadataContainer()));

        if (entity.getIsTeamLeader()) {
            entity.setTeamLeaderShorthand(entity.getShorthand());
        } else if (entity.getTeamLeader() != null && entity.getTeamLeader().getShorthand() != null){
            entity.setTeamLeaderShorthand(entity.getTeamLeader().getShorthand().toUpperCase());
        }
        entity.setCaption(entity.getLastName()+ ", " + entity.getFirstName());

        entity = super.merge(entity);

        updateDerivedData(entity);

        return entity;
    }

    public List<Employee> findByTeamLeader(Employee teamLeader) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.teamLeader = :teamLeader", getEntityClass())
                .setParameter("teamLeader", teamLeader)
                .getResultList();
    };

    public List<Employee> findByLocation(String location) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.location = :location", getEntityClass())
                .setParameter("location", location)
                .getResultList();
    };

    private void updateDerivedData(Employee employee) {
        weeklyQuotaPerEmployeeService.updateWeeklyHoursForEmployee(employee);
        monthlyAvailabilityService.updateWeeklyHoursForEmployee(employee);
    }

    public MetadataContainer addDefaultMetadata(Employee entity, MetadataContainer metadataContainer) {
        metadataContainer = metadataService.addSystemMetadata(metadataContainer);
        if (metadataContainer.getMetaValue(MetadataDefinition.READ_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.READ_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.WRITE_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.WRITE_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.IS_CACHEABLE) == null) {
            metadataContainer.addMetadata(MetadataDefinition.IS_CACHEABLE, Boolean.FALSE);
        }
        metadataContainer.addMetadata("IS_TEAM_LEADER", entity.getIsTeamLeader());
        return metadataService.save(metadataContainer);
    }

    private void replaceNullValues(Employee entity) {
        if(entity.getMondayHours() == null) {
            entity.setMondayHours(0d);
        }
        if(entity.getTuesdayHours() == null) {
            entity.setTuesdayHours(0d);
        }
        if(entity.getWednesdayHours() == null) {
            entity.setWednesdayHours(0d);
        }
        if(entity.getThursdayHours() == null) {
            entity.setThursdayHours(0d);
        }
        if(entity.getFridayHours() == null) {
            entity.setFridayHours(0d);
        }
    }
}