package de.profect.assignmenttool.employee;

import de.profect.assignmenttool.absence.AbsenceService;
import de.profect.assignmenttool.assignment.AssignmentService;
import de.profect.assignmenttool.monthlyavailability.MonthlyAvailabilityRepository;
import de.profect.assignmenttool.monthlymandays.MonthlyManDaysRepository;
import de.profect.assignmenttool.project.Project;
import de.profect.assignmenttool.project.ProjectRepository;
import de.profect.assignmenttool.project.ProjectService;
import de.profect.assignmenttool.weeklyassignmenthoursperemployee.WeeklyAssignmentsRepository;
import de.profect.assignmenttool.weeklyquotaperemployee.WeeklyQuotaPerEmployeeRepository;
import de.profect.authentication.secured.SecuredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author kfe
 */
@Service
public class EmployeeService extends SecuredService<Employee> {

    @Autowired
    @Lazy
    ProjectRepository projectRepository;

    @Autowired
    @Lazy
    WeeklyAssignmentsRepository weeklyAssignmentsRepository;

    @Autowired
    @Lazy
    WeeklyQuotaPerEmployeeRepository weeklyQuotaPerEmployeeRepository;

    @Autowired
    @Lazy
    MonthlyManDaysRepository monthlyManDaysRepository;

    @Autowired
    @Lazy
    MonthlyAvailabilityRepository monthlyAvailabilityRepository;

    @Autowired
    @Lazy
    AssignmentService assignmentService;

    @Autowired
    @Lazy
    AbsenceService absenceService;

    @Override
    public String getPrivilegePrefix() {
        return "EMPLOYEE";
    }

    /***
     * Checks whether the employee with given id {@paramref id} is referenced as the project lead for any projects. If so,
     * returns a list with the ids of the projects, and otherwise deletes the employee and returns null.
     * @param id id of the employee
     * @return List of projects ids or null.
     */
    @Transactional
    public String delete(int id) {
        Optional<Employee> optionalEmployee = repo.findById(id);
        if (optionalEmployee.isPresent()) {
            Employee deleteEmployee = optionalEmployee.get();
            List<Project> projects = projectRepository.findByProjectLeader(deleteEmployee);
            List<Employee> employees = ((EmployeeRepository) repo).findByTeamLeader(deleteEmployee);
            List<String> projectKeys = new ArrayList<>();
            for (Project project : projects) {
                projectKeys.add(project.getProjectKey());
            }
            List<String> employeeNames = new ArrayList<>();
            for (Employee employee : employees) {
                if (employee.getId() == deleteEmployee.getId()) continue;
                employeeNames.add(employee.getCaption());
            }
            if (projectKeys.isEmpty() && employeeNames.isEmpty()) {
                assignmentService.deleteByEmployeeId(id);
                absenceService.deleteByEmployeeId(id);
                weeklyAssignmentsRepository.deleteByEmployeeId(id);
                weeklyQuotaPerEmployeeRepository.deleteByEmployeeId(id);
                monthlyManDaysRepository.deleteByEmployeeId(id);
                monthlyAvailabilityRepository.deleteByEmployeeId(id);
                repo.deleteById(id);
                return null;
            } else {
                String response = "";
                if (!projectKeys.isEmpty()) {
                    response += getReferencedProjectsString(projectKeys, deleteEmployee);
                }
                if (!employeeNames.isEmpty()) {
                    response += getReferencedEmployeesString(employeeNames, deleteEmployee);
                }
                return response;
            }
        }
        return null;
    }

    private String getReferencedProjectsString(List<String> projectKeys, Employee deleteEmployee) {
        StringBuilder sb = new StringBuilder();
        sb.append(projectKeys.remove(0));
        for (String projectKey : projectKeys) {
            sb.append(", " + projectKey);
        }
        return deleteEmployee.getFirstName() + " " + deleteEmployee.getLastName() + " is project leader for " + sb.toString()
                + ". Please assign a new project leader first. ";
    }

    private String getReferencedEmployeesString(List<String> employeeNames, Employee deleteEmployee) {
        StringBuilder sb = new StringBuilder();
        sb.append(employeeNames.remove(0));
        for (String employeeName : employeeNames) {
            sb.append(", " + employeeName);
        }
        return deleteEmployee.getFirstName() + " " + deleteEmployee.getLastName() + " is team leader for " + sb.toString()
                + ". Please assign a new team leader first.";
    }

    @Transactional
    public List<Employee> findByTeamLeader(Employee teamLeader) {
        return ((EmployeeRepository) repo).findByTeamLeader(teamLeader);
    }

    public List<String> getEmployeeStaffNumbers() {
        List<String> employeeStaffNumbers = new ArrayList<>();
        List<Employee> employees = repo.findAll();

        for (Employee employee : employees) {
            employeeStaffNumbers.add(employee.getStaffNumber());
        }
        return employeeStaffNumbers;
    }
}
