package de.profect.assignmenttool.employee;

import com.fasterxml.jackson.annotation.*;
import de.profect.AbstractGUIState;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalDate;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "EMPLOYEES")
@JsonTypeName(value = "v7-assignment-employee")
@Data
@EqualsAndHashCode(callSuper = true)
public class Employee extends AbstractGUIState {

    @Column(name = "STAFF_NUMBER")
    @JsonProperty
    private String staffNumber;

    @Column(name = "FIRST_NAME")
    @JsonProperty
    private String firstName;

    @Column(name = "LAST_NAME")
    @JsonProperty
    private String lastName;

    @Column(name = "LOCATION")
    @JsonProperty
    private String location;

    @Column(name = "CONTRACT_TYPE")
    @JsonProperty
    private String contractType;

    @Column(name = "WEEKLY_HOURS")
    @JsonProperty
    private Double weeklyHours;

    @Column(name = "MONDAY_HOURS")
    @JsonProperty
    private Double mondayHours;

    @Column(name = "TUESDAY_HOURS")
    @JsonProperty
    private Double tuesdayHours;

    @Column(name = "WEDNESDAY_HOURS")
    @JsonProperty
    private Double wednesdayHours;

    @Column(name = "THURSDAY_HOURS")
    @JsonProperty
    private Double thursdayHours;

    @Column(name = "FRIDAY_HOURS")
    @JsonProperty
    private Double fridayHours;

    @Column(name = "ENTRY_DATE")
    @JsonProperty
    private LocalDate entryDate;

    @Column(name = "RETIREMENT_DATE")
    @JsonProperty
    private LocalDate retirementDate;

    @Column(name = "COMMENT")
    @JsonProperty
    private String comment;

    @Column(name = "SHORTHAND")
    @JsonProperty
    private String shorthand;

    @Column(name = "EMAIL")
    @JsonProperty
    private String email;

    @Column(name = "IS_TEAM_LEADER")
    @JsonProperty
    private Boolean isTeamLeader = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TEAM_LEADER_EMPLOYEE_ID")
    @JsonProperty
    private Employee teamLeader;

    @Column(name = "TEAM_LEADER_SHORTHAND")
    private String teamLeaderShorthand;

    @Override
    public String getControllerPath() {
        return "EMPLOYEE";
    }

    public Double getHoursForWeekDay(DayOfWeek dayOfWeek) {
        switch(dayOfWeek) {
            case MONDAY:
                return mondayHours;
            case TUESDAY:
                return tuesdayHours;
            case WEDNESDAY:
                return wednesdayHours;
            case THURSDAY:
                return thursdayHours;
            case FRIDAY:
                return fridayHours;
            default:
                return 0d;
        }
    }
}
