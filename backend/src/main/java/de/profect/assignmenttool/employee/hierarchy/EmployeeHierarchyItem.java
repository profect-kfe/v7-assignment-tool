package de.profect.assignmenttool.employee.hierarchy;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.profect.assignmenttool.employee.Employee;
import de.profect.data.hierarchy.AbstractGUIStateItem;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author kfe
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Data
@EqualsAndHashCode(callSuper = true)
public class EmployeeHierarchyItem extends AbstractGUIStateItem<Employee>{

    private static final long serialVersionUID = -2677498592439477224L;


}
