package de.profect.assignmenttool.msgraph;

public class MaxRetryException extends Exception {
    public MaxRetryException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
