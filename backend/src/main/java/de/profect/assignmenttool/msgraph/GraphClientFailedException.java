package de.profect.assignmenttool.msgraph;

public class GraphClientFailedException extends Exception {
    public GraphClientFailedException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}