package de.profect.assignmenttool.msgraph;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/graph")
@Log4j2
public class GraphController {

    @Autowired
    GraphService graphService;

    @RequestMapping(value = {"/updateabsences"}, method = RequestMethod.GET)
    public boolean updateAbsences() {

        try {
            return graphService.updateAllAbsences(false);
        } catch (MaxRetryException e) {
            log.error(e);
            return false;
        }
    }
}
