package de.profect.assignmenttool.msgraph;

import java.net.MalformedURLException;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.*;
import java.util.function.Consumer;

import com.microsoft.aad.msal4j.*;
import com.microsoft.aad.msal4j.IAccount;
import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.microsoft.aad.msal4j.IntegratedWindowsAuthenticationParameters;
import com.microsoft.aad.msal4j.MsalException;
import com.microsoft.aad.msal4j.PublicClientApplication;
import com.microsoft.aad.msal4j.SilentParameters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GraphAuthentication {

    @Value("${ms.graph.client_secret}")
    private String clientSecret = "";

    @Value("${ms.graph.id}")
    private String applicationId = "";

    @Value("${ms.graph.client_credential_authority}")
    private String client_credential_authority ="";

    @Value("${ms.graph.client_credential_scope}")
    private String client_credential_scope = "";

    // Set authority to allow only organizational accounts
    // Device code flow only supports organizational accounts
    //private final static String authority = "https://login.microsoftonline.com/common/";
    //private final static String AUTHORITY = "https://login.microsoftonline.com/organizations/";

    //private final static String tenantId = "46390799-6bed-4d68-98bf-fbf4c15a073b";
    //private final String CLIENT_CREDENTIAL_AUTHORITY = "https://login.microsoftonline.com/46390799-6bed-4d68-98bf-fbf4c15a073b/";
    //private final Set<String> CLIENT_CREDENTIAL_SCOPE = Collections.singleton("https://graph.microsoft.com/.default");

/*    public void initialize() {

    }*/

    public IAuthenticationResult getAccessToken() {
        return getUserAccessWithClientSecret();
    }

    private IAuthenticationResult getUserAccessWithClientSecret() {

        if (applicationId == null) {
            System.out.println("You must initialize Authentication before calling getUserAccessToken");
            return null;
        }

        //String accessToken;
        IAuthenticationResult result;
        try {
            //accessToken = acquireTokenWithClientSecret(clientSecret);
            result = acquireTokenWithClientSecret(clientSecret);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        //return accessToken;
        return result;
    }

    private IAuthenticationResult acquireTokenWithClientSecret(String clientSecret) throws Exception {

        Set<String> clientCredentialScope = Collections.singleton(client_credential_scope);

        // i have no idea what the tokenCache is used (/useful) for when using client secret.
        // nothing is actually written into the .json file.
        // i just got it from the example code (can't find link currently :/ ).

        // Load token cache from file and initialize token cache aspect. The token cache will have
        // dummy data, so the acquireTokenSilently call will fail.
        TokenCacheAspect tokenCacheAspect = new TokenCacheAspect("sample_cache.json");

        // This is the secret that is created in the Azure portal when registering the application
        IClientCredential credential = ClientCredentialFactory.createFromSecret(clientSecret);
        ConfidentialClientApplication cca =
                ConfidentialClientApplication
                        .builder(applicationId, credential)
                        .authority(client_credential_authority)
                        .setTokenCacheAccessAspect(tokenCacheAspect)
                        .build();

        IAuthenticationResult result;
        try {
            SilentParameters silentParameters =
                    SilentParameters
                            .builder(clientCredentialScope)
                            .build();

            // try to acquire token silently. This call will fail since the token cache does not
            // have a token for the application you are requesting an access token for
            result = cca.acquireTokenSilently(silentParameters).join();
        } catch (Exception ex) {
            if (ex.getCause() instanceof MsalException) {

                ClientCredentialParameters parameters =
                        ClientCredentialParameters
                                .builder(clientCredentialScope)
                                .build();

                // Try to acquire a token. If successful, you should see
                // the token information printed out to console
                result = cca.acquireToken(parameters).join();
            } else {
                // Handle other exceptions accordingly
                throw ex;
            }
        }
        //return result.accessToken();
        return result;
    }
}
