package de.profect.assignmenttool.msgraph;

import com.microsoft.graph.models.extensions.DateTimeTimeZone;
import com.microsoft.graph.models.extensions.Event;
import com.microsoft.graph.models.extensions.User;
import de.profect.assignmenttool.absence.Absence;
import de.profect.assignmenttool.absence.AbsenceRepository;
import de.profect.assignmenttool.absence.AbsenceService;
import de.profect.assignmenttool.deltatoken.DeltaTokenService;
import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.employee.EmployeeRepository;
import de.profect.assignmenttool.deltatoken.DeltaToken;
import de.profect.assignmenttool.deltatoken.DeltaTokenRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j2
public class GraphService {

    @Autowired
    GraphAuthentication graphAuthentication;

    @Autowired
    GraphAPIService graphService;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    AbsenceService absenceService;

    @Autowired
    AbsenceRepository absenceRepository;

    @Autowired
    DeltaTokenRepository deltaTokenRepository;

    @Autowired
    DeltaTokenService deltaTokenService;

    private int retryCount = 0;

    public boolean updateAllAbsences(boolean reset) throws MaxRetryException {

        List<User> graphUsers = new ArrayList<>();
        try {
            graphUsers = graphService.getUsers(false);
        } catch (GraphClientFailedException e) {
            return false;
        } catch (Exception e) {
            retryCount++;
            if (retryCount > 3) { throw new MaxRetryException("3 retries were exceeded.", e);}
            updateAllAbsences(true);
        }
        retryCount = 0;

        List<Employee> employees = employeeRepository.findAll();

        for (Employee employee : employees) {
            try {
                DeltaEvents deltaEventsHoliday = updateHolidaysForEmployee(getUserByEmail(graphUsers, "nikolai.schuster@profect.de"), employee, false);
                DeltaEvents deltaEventsVacation = updateEmployeeVacation(getUserByEmail(graphUsers, employee.getEmail()), employee);
                if (deltaEventsHoliday != null) { updateDeltaToken(employee, deltaEventsHoliday, "Holiday"); }
                if (deltaEventsVacation != null) { updateDeltaToken(employee, deltaEventsVacation, "Vacation"); }
            } catch (MaxRetryException | GraphClientFailedException e) {
                log.error(e.getMessage(), e);
                return false;
            }
            //updateEmployeeVacation(getUserByEmail(graphUsers, "benjamin.soell@profect.de"), employee, accessToken);
        }
        return true;
    }

    private void updateDeltaToken(Employee employee, DeltaEvents deltaEvents, String eventType) {
        // if a deltaToken already exists, it has to be update, else a new deltaToken for the employee is created
        if (deltaEvents.getOldDeltaToken() != null) {
            deltaEvents.getOldDeltaToken().setDeltaToken(deltaEvents.getDeltaToken());
        } else {
            DeltaToken newDeltaToken = new DeltaToken();
            newDeltaToken.setDeltaToken(deltaEvents.getDeltaToken());
            newDeltaToken.setEmployee(employee);
            newDeltaToken.setCaption("deltatoken_" + eventType);
            deltaTokenService.save(newDeltaToken);
        }
    }

    /**
     * Updates (and adds new) Vacations for the employee
     * @param graphUser graph user object of the employee
     * @param employee current employee
     * @throws MaxRetryException
     * @throws GraphClientFailedException
     */
    private DeltaEvents updateEmployeeVacation(User graphUser, Employee employee) throws MaxRetryException, GraphClientFailedException {
        if (graphUser == null) { return null; }
        return updateAbsencesUrlaubForUser(employee, graphUser, false);
    }

    private DeltaToken getEmployeeDeltaToken(Employee employee) {
        List<DeltaToken> deltaTokenOpt = deltaTokenRepository.findByEmployee(employee);
        if (deltaTokenOpt.size() == 0) {
            return null;
        } else {
            return deltaTokenOpt.get(0);
        }
    }

    private DeltaToken getEmployeeHolidayDeltaToken(Employee employee) {
        List<DeltaToken> deltaTokenOpt = deltaTokenRepository.findHolidayTokenByEmployee(employee);
        if (deltaTokenOpt.size() == 0) {
            return null;
        } else {
            return deltaTokenOpt.get(0);
        }
    }

    private LocalDate formatDateTimeTimeZoneToLocalDate(DateTimeTimeZone date) {

        LocalDateTime dateTime = LocalDateTime.parse(date.dateTime);
        //dateTime = dateTime.plusSeconds(getTimeZoneUTCDifference());

        dateTime = dateTime.plusHours(2);

        //return LocalDate.parse(date.dateTime);
/*        String tmp = date.dateTime;
        return LocalDate.of(Integer.parseInt(tmp.substring(0, 4)), Integer.parseInt(tmp.substring(5, 7)), Integer.parseInt(tmp.substring(8, 10)));*/
        return dateTime.toLocalDate();
    }

    private LocalDate formatDateTimetoLocalDateEnd(DateTimeTimeZone date) {
        // end date for vacations is last day 24:00 which gets rolled over to next day 00:00
        //TODO: handling für untertägige abwesenheiten
        LocalDateTime dateTime = LocalDateTime.parse(date.dateTime);

        if (dateTime.toLocalTime().equals(LocalTime.of(0,0,0,0))) {
            dateTime = dateTime.minusDays(1);
        } else {
            dateTime = dateTime.plusHours(2);
        }

        return dateTime.toLocalDate();

/*        String test = dateTime.format(
                DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)) +
                " (" + date.timeZone + ")";

        String tmp = date.dateTime;
        LocalDate tmpDate = LocalDate.of(Integer.parseInt(tmp.substring(0, 4)), Integer.parseInt(tmp.substring(5, 7)), Integer.parseInt(tmp.substring(8, 10)));
        return tmpDate.minusDays(1);*/
    }

    private int getTimeZoneUTCDifference() {
        Instant instant = Instant.ofEpochMilli(1484063246L * 1000L);

// You can use this if you already have a Date object
// Instant instant = dateInstance.toInstant();

// You can use this for current offset
// Instant instant = Instant.now();

        return ZoneId.systemDefault().getRules().getOffset(instant).getTotalSeconds();
    }

    private String formatDateTimeTimeZone(DateTimeTimeZone date) {
        LocalDateTime dateTime = LocalDateTime.parse(date.dateTime);

        return dateTime.format(
                DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)) +
                " (" + date.timeZone + ")";
    }

    @Transactional
    private DeltaEvents updateAbsencesUrlaubForUser(Employee employee, User user, boolean reset) throws MaxRetryException, GraphClientFailedException {
        //List<Event> events = graphService.getUserEvents(accessToken, user, "Urlaub");

        DeltaEvents deltaEvents = new DeltaEvents();
        //List<Event> events = new ArrayList<>();
        try {
            deltaEvents = graphService.getDeltaEventsForUser(user, employee, getEmployeeDeltaToken(employee), "vacation", false);
            deltaEvents.setEvents(filterUrlaubEvents(deltaEvents.getEvents()));
            //events = filterUrlaubEvents(events);
        } catch (GraphClientFailedException e) {
            throw e;
        } catch (Exception e) {
            retryCount++;
            if (retryCount > 3) { throw new MaxRetryException("3 retries were exceeded.", e);}
            updateAbsencesUrlaubForUser(employee, user,true);
        }
        retryCount = 0;

        List<Absence> existingAbsences = absenceRepository.findByEmployeeAndReason(employee, "Vacation");
        for (Event event : deltaEvents.getEvents()) {
            if (event.getRawObject().get("@removed") != null) {
                deleteAbsence(employee, event);
            } else {
                // there seems to be no indication whether an event is new or a changed old event
                // thus, it is required to check existing absences ("Vacation") if anyone matches the id
                // and, if so, update it.
                Absence existingAbsence = checkForChangedAbsence(event, existingAbsences);
                if (existingAbsence != null) {
                    updateAbsence(existingAbsence, event);
                } else {
                    createNewAbsence(employee, event, "Vacation");
                }
            }
        }
        return deltaEvents;
    }

    private Absence checkForChangedAbsence(Event currentEvent, List<Absence> existingAbsences) {
        return existingAbsences.stream()
                .filter(absence -> absence.getGraphId().equals(currentEvent.id))
                .findFirst()
                .orElse(null);

    }

    private void updateAbsence(Absence absence, Event event) {
        absence.setStartDate(formatDateTimeTimeZoneToLocalDate(event.start));
        absence.setEndDate((formatDateTimetoLocalDateEnd(event.end)));
    }

    private void deleteAbsence(Employee employee, Event event) {
        // If an event has the @removed tag, it was removed from the calendar and needs to be removed from the database
        // the graph_id uniquely identifies the event

        //absenceRepository.deleteByGraphId(event.id);

        List<Absence> absenceToBeDeleted = absenceRepository.findByEmployeeAndGraphId(employee, event.id);
        if (absenceToBeDeleted.size() > 0) {
            for (Absence absence : absenceToBeDeleted) {
                absenceService.delete(absence.getId());
            }
        }
    }

    private List<Event> filterUrlaubEvents(List<Event> events) {
        List<Event> filteredEvents = new ArrayList<>();
// event.organizer.emailAddress.address.equals("nikolai.schuster@profect.de") ||
        try {
            filteredEvents = events.stream()
                    .filter(event -> (event.organizer != null && event.organizer.emailAddress != null && (event.organizer.emailAddress.name.equals("Urlaub"))) || event.getRawObject().get("@removed") != null)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Filtering events for 'Urlaub' failed.", e);
        }
        return filteredEvents;
    }

    @Transactional
    private DeltaEvents updateHolidaysForEmployee(User holidayDates, Employee employee, boolean reset) throws MaxRetryException, GraphClientFailedException {

        DeltaEvents deltaEvents = new DeltaEvents();
        //List<Event> holidays = new ArrayList<>();
        try {
            deltaEvents = graphService.getDeltaEventsForUser(holidayDates, employee, getEmployeeHolidayDeltaToken(employee), "holiday", false);
        } catch (GraphClientFailedException e) {
            throw e;
        } catch (Exception e) {
            retryCount++;
            if (retryCount > 3) { throw new MaxRetryException("3 retries were exceeded.", e);}
            updateHolidaysForEmployee(holidayDates, employee, true);
        }
        retryCount = 0;

        switch (employee.getLocation()) {
            case "Profect DE":
                deltaEvents.setEvents(filterHolidaysByLocation(deltaEvents.getEvents(), "Deutschland"));
                break;
            case "Profect CH":
                deltaEvents.setEvents(filterHolidaysByLocation(deltaEvents.getEvents(), "Schweiz"));
                break;
            case "Profect SK":
                deltaEvents.setEvents(filterHolidaysByLocation(deltaEvents.getEvents(), "Slowakei"));
                break;
            default:
                return null;
        }

        for (Event holiday : deltaEvents.getEvents()) {
            createNewAbsence(employee, holiday, "Official Holiday");
        }
        return deltaEvents;
    }

    private void createNewAbsence(Employee employee, Event event, String reason) {
        Absence newAbsence = new Absence();
        newAbsence.setEmployee(employee);
        //TODO: Formatting?
        //newAbsence.setStartDate(parseEventDateToLocalDate(event.start).atStartOfDay().toLocalDate());
        //newAbsence.setEndDate(parseEventDateToLocalDate(event.end));
        newAbsence.setStartDate(formatDateTimeTimeZoneToLocalDate(event.start));
        newAbsence.setEndDate((formatDateTimetoLocalDateEnd(event.end)));
        //newAbsence.setReason(reason + event.subject);
        newAbsence.setReason(reason);
        newAbsence.setGraphId(event.id);
        absenceService.save(newAbsence);
    }

    /**
     * Returns a list of events that are a holiday in the given location.
     * @param events
     * @param location
     * @return
     */
    private List<Event> filterHolidaysByLocation(List<Event> events, String location) {
        List<Event> filteredEvents = new ArrayList<>();

        try {
            filteredEvents = events.stream()
                    .filter(event -> event.location != null && event.location.displayName != null && event.location.displayName.equals(location) && (event.categories != null && event.categories.contains("Feiertag")))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Filtering events for location '" + location + "' and 'Feiertag' failed.", e);
        }

        return filteredEvents;
    }

/*    private void listCalendarEvents(String accessToken, User user, boolean reset) throws MaxRetryException {
        // Get the user's events
        //List<Event> events = Graph.getEvents(accessToken);

        List<Event> events = new ArrayList<>();
        try {
            events = graphService.getUserEvents(accessToken, user, null, false);
        } catch (Exception e) {
            retryCount++;
            if (retryCount > 3) { throw new MaxRetryException("3 retries were exceeded.", e);}
            listCalendarEvents(accessToken, user, true);
        }
        retryCount = 0;
        StringBuilder sb = new StringBuilder();

        System.out.println("Events:");
        sb.append("Events:\n");

        for (Event event : events) {
            System.out.println("Subject: " + event.subject);
            sb.append("Subject: " + event.subject);
            System.out.println("  Organizer: " + event.organizer.emailAddress.name);
            sb.append("  Organizer: " + event.organizer.emailAddress.name);
            System.out.println("  Start: " + formatDateTimeTimeZone(event.start));
            sb.append("  Start: " + formatDateTimeTimeZone(event.start));
            System.out.println("  End: " + formatDateTimeTimeZone(event.end));
            sb.append("  End: " + formatDateTimeTimeZone(event.end));
            System.out.println("  Categories: " + event.categories);
            sb.append("  Categories: " + event.categories);
        }

        System.out.println();
        log.debug(sb.toString());
    }*/

    /***
     * Searches users for a specific user with given firstAndLastName.
     * @param firstAndLastName "Firstname Lastname", e.g. "Nikolai Schuster"
     * @return The searched user object, if it exists, otherwise null.
     */
    private User getUser(List<User> users, String firstAndLastName) {
        if (users == null || users.size() == 0) {
            return null;
        }

        return users.stream()
                .filter(user -> firstAndLastName.equals(user.displayName))
                .findAny()
                .orElse(null);
    }

    /***
     * Searches users for a specific user with given email (should uniquely identify user).
     * @param email e.g. nikolai.schuster@profect.de
     * @return The searched user object, if it exists, otherwise null.
     */
    private User getUserByEmail(List<User> users, String email) {
        if (users == null || users.size() == 0) {
            return null;
        }

        return users.stream()
                .filter(user -> email.equals(user.userPrincipalName))
                .findAny()
                .orElse(null);
    }

}
