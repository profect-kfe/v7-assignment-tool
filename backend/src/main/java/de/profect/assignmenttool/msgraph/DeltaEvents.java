package de.profect.assignmenttool.msgraph;

import com.microsoft.graph.models.extensions.Event;
import de.profect.assignmenttool.deltatoken.DeltaToken;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DeltaEvents {
    private String deltaToken;
    private List<Event> events = new ArrayList<>();
    private DeltaToken oldDeltaToken = null;
}
