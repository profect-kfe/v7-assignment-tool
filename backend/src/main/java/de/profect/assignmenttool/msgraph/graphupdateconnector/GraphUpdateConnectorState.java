package de.profect.assignmenttool.msgraph.graphupdateconnector;

import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.connector.target.TargetConnectorState;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@DiscriminatorValue("v7-graph-update-connector")
@JsonTypeName("v7-graph-update-connector")
@Data
@EqualsAndHashCode(callSuper = true)
public class GraphUpdateConnectorState extends TargetConnectorState {

}
