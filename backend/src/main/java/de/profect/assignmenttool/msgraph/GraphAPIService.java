package de.profect.assignmenttool.msgraph;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.JsonElement;
import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.microsoft.graph.core.BaseClient;
import com.microsoft.graph.core.DefaultConnectionConfig;
import com.microsoft.graph.core.IConnectionConfig;
import com.microsoft.graph.http.CoreHttpProvider;
import com.microsoft.graph.httpcore.HttpClients;
import com.microsoft.graph.logger.DefaultLogger;
import com.microsoft.graph.logger.LoggerLevel;
import com.microsoft.graph.models.extensions.Event;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.models.extensions.User;
import com.microsoft.graph.options.HeaderOption;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.options.QueryOption;
import com.microsoft.graph.requests.extensions.GraphServiceClient;
import com.microsoft.graph.requests.extensions.IEventCollectionPage;
import com.microsoft.graph.requests.extensions.IEventDeltaCollectionPage;
import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.deltatoken.DeltaToken;
import de.profect.assignmenttool.deltatoken.DeltaTokenService;
import lombok.extern.log4j.Log4j2;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Graph
 */
@Service
@Log4j2
public class GraphAPIService {

    @Autowired
    DeltaTokenService deltaTokenService;

    @Autowired
    GraphAuthentication graphAuthentication;

    private static IGraphServiceClient graphClient = null;
    private static SimpleAuthProvider authProvider = null;

    private static String accessToken = "";
    private static int retryCount = 0;

    private Date accessTokenExpirationDate = null;

/*    private void ensureGraphClient2(String accessToken) {
        ensureGraphClient(accessToken, false);
    }*/

    private void ensureGraphClient() throws GraphClientFailedException {
        ensureGraphClient(true);
    }

    private void ensureGraphClient(boolean reset) throws GraphClientFailedException {
        if (graphClient == null || reset || (accessTokenExpirationDate != null && accessTokenExpirationDate.getTime() - new Date().getTime() < 300)) {
            // Create the auth provider
            try {
                //if (accessToken.equals("")) {
                    //accessToken = graphAuthentication.getAccessToken();
                IAuthenticationResult result = graphAuthentication.getAccessToken();
                accessToken = result.accessToken();
                accessTokenExpirationDate = result.expiresOnDate();
                //}
                authProvider = new SimpleAuthProvider(accessToken);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                retryCount++;
                if (retryCount > 1) {
                    throw new GraphClientFailedException("Ensuring graph client failed.", e);
                }
                accessToken = "";
                ensureGraphClient(true);
            }
            retryCount = 0;

            // Create default logger to only log errors
            DefaultLogger logger = new DefaultLogger();
            logger.setLoggingLevel(LoggerLevel.ERROR);

            // Build a Graph client
            graphClient = GraphServiceClient.builder()
                    .authenticationProvider(authProvider)
                    .logger(logger)
                    .buildClient();

            //this.graphServiceClient = GraphServiceClient.builder().authenticationProvider(authenticationProvider).buildClient()
            try {
                /*
                 * Reflection to access the httpProvider
                 */
                Field httpProviderField = BaseClient.class.getDeclaredField("httpProvider");
                httpProviderField.setAccessible(true);
                CoreHttpProvider httpProvider = (CoreHttpProvider) httpProviderField.get(graphClient);

                /*
                 * Reflection to access the coreHttpProvider's coreHttpClient
                 */
                Field coreHttpClientField = CoreHttpProvider.class.getDeclaredField("corehttpClient");
                coreHttpClientField.setAccessible(true);
                OkHttpClient coreHttpClient = (OkHttpClient) coreHttpClientField.get(httpProvider);

                /*
                 * Building an OKHttpClient without HTTP/2
                 */
                if(coreHttpClient == null) {
                    /*
                     * Reflection to get the coreHttpProvider's connectionConfig
                     */
                    Field connectionConfigField = CoreHttpProvider.class.getDeclaredField("connectionConfig");
                    connectionConfigField.setAccessible(true);
                    IConnectionConfig connectionConfig = (IConnectionConfig) connectionConfigField.get(httpProvider);

                    /*
                     * Initing connection config to default if null
                     */
                    if(connectionConfig == null)
                        connectionConfig = new DefaultConnectionConfig();

                    /*
                     * Reflection to set the coreHttpProviver's coreHttpProvider to an HTTP1.1 only OKHttpClient
                     * The code is copypasted from CoreHttpProvider. I have just added the line
                     * .protocols(Arrays.asList(Protocol.HTTP_1_1)) so that the OKHttpClient cannot negotiate
                     * HTTP/2
                     */
                    coreHttpClientField.set(httpProvider, HttpClients
                            .createDefault(request -> request)
                            .newBuilder()
                            .protocols(Arrays.asList(Protocol.HTTP_1_1))
                            .connectTimeout(connectionConfig.getConnectTimeout(), TimeUnit.MILLISECONDS)
                            .readTimeout(connectionConfig.getReadTimeout(), TimeUnit.MILLISECONDS)
                            .followRedirects(false)
                            .retryOnConnectionFailure(false)
                            .build());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<User> getUsers(boolean reset) throws Exception {
        ensureGraphClient(reset);

        // GET /me to get authenticated user
        List<User> users;
        try {
            users = graphClient
                    .users()
                    .buildRequest()
                    .get()
                    .getCurrentPage();
        } catch (Exception e) {
            throw e;
        }
        return users;
    }



    private List<Event> getAllEvents(IEventCollectionPage eventPage) {
        if (eventPage == null) {return null;}
        if (eventPage.getRawObject().get("@odata.nextLink") != null) {
            return Stream.concat(eventPage.getCurrentPage().stream(),
                    getAllEvents(eventPage.getNextPage().buildRequest().get()).stream())
                    .collect(Collectors.toList());
        } else {
            return eventPage.getCurrentPage();
        }
    }

    public DeltaEvents getDeltaEventsForUser(User user, Employee employee, DeltaToken deltaToken, String eventType, boolean reset) throws Exception {
        List<Event> events;
        List<Option> queryOptions = getDefaultQueryOptions(eventType);
        DeltaEvents deltaEvents = new DeltaEvents();
        try {
            IEventDeltaCollectionPage firstDelta = getDeltaSubsetForUser(user, queryOptions, null, deltaToken == null ? null : deltaToken.getDeltaToken(), reset);
            events = firstDelta.getCurrentPage();

            IEventDeltaCollectionPage nextDelta = firstDelta;

            while (nextDelta.getRawObject().get("@odata.nextLink") != null) {
                nextDelta = getDeltaSubsetForUser(user, queryOptions, extractSkipTokenFromNextLink(nextDelta.getRawObject().get("@odata.nextLink")), null, reset);
                events = Stream.concat(events.stream(),
                        nextDelta.getCurrentPage().stream())
                        .collect(Collectors.toList());
            }
            String newDeltaToken = "";
            if (nextDelta.getRawObject().get("@odata.deltaLink") != null) {
                deltaEvents.setDeltaToken(extractDeltaTokenFromNextLink(nextDelta.getRawObject().get("@odata.deltaLink")));
                deltaEvents.setOldDeltaToken(deltaToken);
                // if a deltaToken already exists, it has to be update, else a new deltaToken for the employee is created
/*                if (deltaToken != null) {
                    deltaToken.setDeltaToken(extractDeltaTokenFromNextLink(nextDelta.getRawObject().get("@odata.deltaLink")));
                } else {
                    DeltaToken newDeltaToken = new DeltaToken();
                    newDeltaToken.setDeltaToken(extractDeltaTokenFromNextLink(nextDelta.getRawObject().get("@odata.deltaLink")));
                    newDeltaToken.setEmployee(employee);
                    newDeltaToken.setCaption("deltatoken_" + eventType);
                    deltaTokenService.save(newDeltaToken);
                }*/
            }
            deltaEvents.setEvents(events);
        } catch (Exception e) {
            throw e;
        }

        return deltaEvents;
    }

    private List<Option> getDefaultQueryOptions(String eventType) {
        List<Option> queryOptions = new ArrayList<>();
        switch (eventType) {
            case "vacation":
                queryOptions.add(new QueryOption("startdatetime", today()));
                queryOptions.add(new QueryOption("enddatetime", todayNextYear()));
                break;
            case "holiday":
                queryOptions.add(new QueryOption("startdatetime", today()));
                queryOptions.add(new QueryOption("enddatetime", todayNextYear()));
                break;
            default:
                break;
        }
        return queryOptions;
    }

    private IEventDeltaCollectionPage getDeltaSubsetForUser(User user, List<Option> queryOptions, String skipToken, String deltaToken, boolean reset) throws GraphClientFailedException {
        // https://docs.microsoft.com/en-us/graph/delta-query-events?tabs=java#step-1-sample-initial-request

        ensureGraphClient(reset);

        List<Option> options = new ArrayList<>(queryOptions);

        options.add(new HeaderOption("Prefer", "odata.maxpagesize=20"));

        if (skipToken != null && !skipToken.equals("")) {
            options.add(new QueryOption("$skiptoken", skipToken));
        } else if (deltaToken != null && !deltaToken.equals("")) {
            options.add(new QueryOption("$deltatoken", deltaToken));
        }

        IEventDeltaCollectionPage delta;
        try {
            delta = graphClient.users(user.id).calendarView()
                    .delta()
                    .buildRequest(options)
                    .get();
        } catch (Exception e) {
            throw e;
        }

        return delta;
    }

    private String extractSkipTokenFromNextLink(JsonElement nextlink) {
        String tmp = nextlink.toString().split("skiptoken=")[1];
        return tmp.substring(0, tmp.length() - 1);
    }

    private String extractDeltaTokenFromNextLink(JsonElement nextlink) {
        String tmp = nextlink.toString().split("deltatoken=")[1];
        return tmp.substring(0, tmp.length() - 1);
    }

    private String today() {
        LocalDate now = LocalDate.now();
        StringBuilder sb = new StringBuilder();
        sb.append(now.getYear());
        sb.append("-");
        sb.append(now.getMonthValue());
        sb.append("-");
        sb.append(now.getDayOfMonth());

        return sb.toString();
    }

    private String todayNextYear() {
        LocalDate now = LocalDate.now();
        now = now.plusYears(1);
        StringBuilder sb = new StringBuilder();
        sb.append(now.getYear());
        sb.append("-");
        sb.append(now.getMonthValue());
        sb.append("-");
        sb.append(now.getDayOfMonth());

        return sb.toString();
    }
}
