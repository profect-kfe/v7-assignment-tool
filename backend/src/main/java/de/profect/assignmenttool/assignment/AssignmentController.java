package de.profect.assignmenttool.assignment;

import de.profect.authentication.secured.SecuredController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kfe
 */
@RestController
@RequestMapping(value = "/assignment")
public class AssignmentController extends SecuredController<Assignment> {

}
