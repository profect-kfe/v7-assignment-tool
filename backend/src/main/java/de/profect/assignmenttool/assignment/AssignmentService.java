package de.profect.assignmenttool.assignment;

import de.profect.authentication.secured.SecuredService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kfe
 */
@Service
public class AssignmentService extends SecuredService<Assignment> {

    @Override
    public String getPrivilegePrefix() {
        return "ASSIGNMENT";
    }

    public void deleteByEmployeeId(int employeeId) {
        List<Assignment> assignments = repo.findAll();
        List<Assignment> deleteAssignments = new ArrayList<>();
        for (Assignment assignment : assignments) {
            if (assignment.getEmployee().getId() == employeeId) {
                deleteAssignments.add(assignment);
            }
        }
        repo.deleteAll(deleteAssignments);
    }

    public void deleteByProjectId(int projectId) {
        List<Assignment> assignments = repo.findAll();
        List<Assignment> deleteAssignments = new ArrayList<>();
        for (Assignment assignment : assignments) {
            if (assignment.getProject().getId() == projectId) {
                deleteAssignments.add(assignment);
            }
        }
        repo.deleteAll(deleteAssignments);
    }
}
