package de.profect.assignmenttool.assignment;

import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.monthlymandays.MonthlyManDaysService;
import de.profect.assignmenttool.weeklyassignmenthoursperemployee.WeeklyAssignmentsService;
import de.profect.authentication.secured.SecuredRepository;
import de.profect.data.metadata.MetadataContainer;
import de.profect.data.metadata.MetadataDefinition;
import de.profect.data.metadata.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 *
 * @author kfe
 */
@Repository
public class AssignmentRepository extends SecuredRepository<Assignment> {

    @Autowired
    @Lazy
    private WeeklyAssignmentsService weeklyAssignmentsService;

    @Autowired
    @Lazy
    private MonthlyManDaysService monthlyManDaysService;

    @Autowired
    @Lazy
    private MetadataService metadataService;

    @Override
    public Assignment save(Assignment entity) {
        entity.setCaption(entity.getProject().getProjectKey());
        entity.setMetadataContainer(addDefaultMetadata(entity.getMetadataContainer()));
        replaceNullValues(entity);
        entity = super.save(entity);
        weeklyAssignmentsService.updateWeeklyAssignments(entity.getEmployee());
        monthlyManDaysService.updateMonthlyManDays(entity.getEmployee());
        return entity;
    }

    @Override
    public <P extends Assignment> P merge(P entity) {
        entity.setCaption(entity.getProject().getProjectKey());
        replaceNullValues(entity);
        entity = super.merge(entity);
        weeklyAssignmentsService.updateWeeklyAssignments(entity.getEmployee());
        monthlyManDaysService.updateMonthlyManDays(entity.getEmployee());
        return entity;
    }

    @Override
    public void delete(Assignment entity) {
        Employee employee = entity.getEmployee();
        super.delete(entity);
        updateDerivedData(employee);
    }

    @Override
    public void deleteById(Integer id) {
        Optional<Assignment> optionalAssignment = findById(id);
        if(optionalAssignment.isPresent()) {
            Assignment assignment = optionalAssignment.get();
            Employee employee = assignment.getEmployee();
            super.deleteById(id);
            updateDerivedData(employee);
        }
    }

    @Override
    public void deleteAll(Iterable<? extends Assignment> entities) {
        Set<Employee> effectedEmployees = new HashSet<>();
        for(Assignment entity : entities) {
            effectedEmployees.add(entity.getEmployee());
        }
        super.deleteAll(entities);
        for(Employee employee : effectedEmployees) {
            updateDerivedData(employee);
        }
    }

    public List<Assignment> findByEmployee(Employee employee) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.employee = :employee", getEntityClass())
                .setParameter("employee", employee)
                .getResultList();
    };

    private void updateDerivedData(Employee employee) {
        weeklyAssignmentsService.updateWeeklyAssignments(employee);
        monthlyManDaysService.updateMonthlyManDays(employee);
    }

    public MetadataContainer addDefaultMetadata(MetadataContainer metadataContainer) {
        metadataContainer = metadataService.addSystemMetadata(metadataContainer);
        if (metadataContainer.getMetaValue(MetadataDefinition.READ_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.READ_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.WRITE_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.WRITE_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.IS_CACHEABLE) == null) {
            metadataContainer.addMetadata(MetadataDefinition.IS_CACHEABLE, Boolean.FALSE);
        }
        return metadataService.save(metadataContainer);
    }

    private void replaceNullValues(Assignment entity) {
        if(entity.getMondayHours() == null) {
            entity.setMondayHours(0d);
        }
        if(entity.getTuesdayHours() == null) {
            entity.setTuesdayHours(0d);
        }
        if(entity.getWednesdayHours() == null) {
            entity.setWednesdayHours(0d);
        }
        if(entity.getThursdayHours() == null) {
            entity.setThursdayHours(0d);
        }
        if(entity.getFridayHours() == null) {
            entity.setFridayHours(0d);
        }
    }
}
