package de.profect.assignmenttool.assignment;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import de.profect.component.formbuilder.validators.AbstractAsyncValidatorState;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@DiscriminatorValue("v7-assignment-validator")
@JsonTypeName("v7-assignment-validator")
@Data
@EqualsAndHashCode(callSuper = true)
public class AssignmentValidatorState extends AbstractAsyncValidatorState {

}

