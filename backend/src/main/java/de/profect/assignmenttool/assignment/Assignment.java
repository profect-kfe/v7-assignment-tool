package de.profect.assignmenttool.assignment;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.AbstractGUIState;
import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.project.Project;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalDate;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "ASSIGNMENTS")
@JsonTypeName(value = "v7-assignment-assignment")
@Data
@EqualsAndHashCode(callSuper = true)
public class Assignment extends AbstractGUIState {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMPLOYEE_ID")
    @JsonProperty
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_ID")
    @JsonProperty
    private Project project;

    @Column(name = "START_DATE")
    @JsonProperty
    private LocalDate startDate;

    @Column(name = "END_DATE")
    @JsonProperty
    private LocalDate endDate;

    @Column(name = "DAILY_RATE")
    @JsonProperty
    private Double dailyRate;

    @Column(name = "WEEKLY_HOURS")
    @JsonProperty
    private Double weeklyHours;

    @Column(name = "MONDAY_HOURS")
    @JsonProperty
    private Double mondayHours;

    @Column(name = "TUESDAY_HOURS")
    @JsonProperty
    private Double tuesdayHours;

    @Column(name = "WEDNESDAY_HOURS")
    @JsonProperty
    private Double wednesdayHours;

    @Column(name = "THURSDAY_HOURS")
    @JsonProperty
    private Double thursdayHours;

    @Column(name = "FRIDAY_HOURS")
    @JsonProperty
    private Double fridayHours;

    @Override
    public String getControllerPath() {
        return "ASSIGNMENT";
    }

    public Double getHoursForWeekDay(DayOfWeek dayOfWeek) {
        switch(dayOfWeek) {
            case MONDAY:
                return mondayHours;
            case TUESDAY:
                return tuesdayHours;
            case WEDNESDAY:
                return wednesdayHours;
            case THURSDAY:
                return thursdayHours;
            case FRIDAY:
                return fridayHours;
            default:
                return 0d;
        }
    }
}
