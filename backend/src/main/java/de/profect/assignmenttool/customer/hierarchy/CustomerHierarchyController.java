package de.profect.assignmenttool.customer.hierarchy;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.profect.data.hierarchy.HierarchyDefinitionController;

/**
 * The CustomerHierarchyController handles request from the client concerning the CustomerHierarchyDefinition
 * @author kfe
 */
@RestController
@RequestMapping(value = "/customerhierarchydefinition")
public class CustomerHierarchyController extends HierarchyDefinitionController<CustomerHierarchyDefinition, CustomerHierarchyService> {

}
