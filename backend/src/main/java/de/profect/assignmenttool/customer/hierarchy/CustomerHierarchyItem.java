package de.profect.assignmenttool.customer.hierarchy;

import de.profect.assignmenttool.customer.Customer;
import de.profect.data.hierarchy.AbstractGUIStateItem;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author kfe
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerHierarchyItem extends AbstractGUIStateItem<Customer> {

    private static final long serialVersionUID = -3767352318393141169L;


}
