package de.profect.assignmenttool.customer;

import de.profect.authentication.secured.SecuredController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController extends SecuredController<Customer> {

}

