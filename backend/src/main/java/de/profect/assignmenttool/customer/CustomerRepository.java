package de.profect.assignmenttool.customer;

import de.profect.authentication.secured.SecuredRepository;
import de.profect.data.metadata.MetadataContainer;
import de.profect.data.metadata.MetadataDefinition;
import de.profect.data.metadata.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRepository extends SecuredRepository<Customer> {

    @Autowired
    @Lazy
    private MetadataService metadataService;

    @Override
    public <P extends Customer> P save(P entity) {
        entity.setCaption(entity.getDivision());
        entity.setMetadataContainer(addDefaultMetadata(entity.getMetadataContainer()));
        return super.save(entity);
    }

    @Override
    public <P extends Customer> P merge(P entity) {
        entity.setCaption(entity.getDivision());
        return super.merge(entity);
    }

    public MetadataContainer addDefaultMetadata(MetadataContainer metadataContainer) {
        metadataContainer = metadataService.addSystemMetadata(metadataContainer);
        if (metadataContainer.getMetaValue(MetadataDefinition.READ_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.READ_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.WRITE_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.WRITE_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.IS_CACHEABLE) == null) {
            metadataContainer.addMetadata(MetadataDefinition.IS_CACHEABLE, Boolean.FALSE);
        }
        return metadataService.save(metadataContainer);
    }
}
