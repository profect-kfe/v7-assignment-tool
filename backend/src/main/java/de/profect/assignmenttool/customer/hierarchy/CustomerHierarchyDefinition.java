package de.profect.assignmenttool.customer.hierarchy;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import de.profect.data.hierarchy.HierarchyDefinition;
import de.profect.data.metadata.MetadataContainer;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author kfe
 */
@Entity
@DiscriminatorValue("v7-customer-hierarchy-definition")
@JsonTypeName("v7-customer-hierarchy-definition")
@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerHierarchyDefinition extends HierarchyDefinition {

    private static final long serialVersionUID = 839380945549521417L;

    /**
     * The {@link de.profect.data.metadata.MetadataContainer} represents the filter
     * that should be used on the CustomerHierarchyDefinition.
     */
    @ManyToOne
    @JoinColumn(name = "FILTER_ID")
    @JsonProperty
    protected MetadataContainer filter;

    @Override
    public String getControllerPath() {
        return "CUSTOMERHIERARCHYDEFINITION";
    }

}
