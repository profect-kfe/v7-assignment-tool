package de.profect.assignmenttool.customer;

import de.profect.assignmenttool.employee.EmployeeRepository;
import de.profect.assignmenttool.project.Project;
import de.profect.assignmenttool.project.ProjectService;
import de.profect.assignmenttool.weeklyassignmenthoursperemployee.WeeklyAssignmentsRepository;
import de.profect.assignmenttool.weeklyquotaperemployee.WeeklyQuotaPerEmployeeRepository;
import de.profect.authentication.secured.SecuredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kfe
 */
@Service
public class CustomerService extends SecuredService<Customer> {

    @Lazy
    @Autowired
    private ProjectService projectService;

    @Lazy
    @Autowired
    private WeeklyAssignmentsRepository weeklyAssignmentsRepository;

    @Lazy
    @Autowired
    private WeeklyQuotaPerEmployeeRepository weeklyQuotaPerEmployeeRepository;

    @Lazy
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public String getPrivilegePrefix() {
        return "CUSTOMER";
    }

    @Transactional
    public String delete(int id) {
        List<Project> projects = projectService.findByCustomer(id);

        for (Project project : projects) {
            projectService.delete(project.getId());
        }
        repo.deleteById(id);
        return null;
    }
}
