package de.profect.assignmenttool.customer.hierarchy;


import de.profect.assignmenttool.customer.Customer;
import de.profect.assignmenttool.customer.CustomerService;
import de.profect.data.hierarchy.HierarchyDefinitionService;
import de.profect.data.hierarchy.HierarchyItem;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * CustomerHierarchyService retrieves HierarchyData from state.
 *
 * @author kfe
 */
@Log4j2
@Service
public class CustomerHierarchyService extends HierarchyDefinitionService<CustomerHierarchyDefinition> {

    @Autowired
    private CustomerService service;

    @Override
    protected String getPrivilegePrefix() {
        return "CUSTOMER_HIERARCHY_DEFINITION";
    }

    @Override
    public List<List<HierarchyItem<?, ?>>> getDataForState(CustomerHierarchyDefinition state) {
        List<List<HierarchyItem<?, ?>>> result = new ArrayList<>();
        List<Customer> entities = service.fetchFiltered(state.getFilter());
        Collections.sort(entities, (o1, o2) -> o1.getCaption().compareTo(o2.getCaption()));
        for (Customer entity : entities) {
            List<HierarchyItem<?, ?>> path = new ArrayList<>();
            addGroupPath(entity.getCategory(), path);
            path.add(getItemFromEntity(entity));
            result.add(path);
        }
        return result;
    }

    protected CustomerHierarchyItem getItemFromEntity(Customer entity) {
        if (entity != null) {
            CustomerHierarchyItem item = new CustomerHierarchyItem();
            item.setId(entity.getId());
            item.setCaption(entity.getCaption());
            item.setOriginalObject(entity);
            return item;
        }
        return null;
    }

}

