package de.profect.assignmenttool.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.AbstractGUIState;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "CUSTOMERS")
@JsonTypeName(value = "v7-assignment-customer")
@Data
@EqualsAndHashCode(callSuper = true)
public class Customer extends AbstractGUIState {

    @Column(name = "CUSTOMER_NAME")
    @JsonProperty
    private String customerName;

    @Column(name = "DIVISION")
    @JsonProperty
    private String division;

    @Override
    public String getControllerPath() {
        return "CUSTOMER";
    }


}
