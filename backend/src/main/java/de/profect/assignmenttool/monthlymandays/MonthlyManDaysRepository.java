package de.profect.assignmenttool.monthlymandays;

import de.profect.assignmenttool.employee.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface MonthlyManDaysRepository extends CrudRepository<MonthlyManDays, Integer>{

    @Override
    public void deleteById(Integer id);

    @Override
    public List<MonthlyManDays> findAll();

    @Override
    public Optional<MonthlyManDays> findById(Integer id);

    public List<MonthlyManDays> findByEmployee(Employee employee);

    public void deleteByEmployee(Employee employee);

    @Override
    public <P extends MonthlyManDays> P save(P persist);

    @Override
    public <P extends MonthlyManDays> Iterable<P>  saveAll(Iterable<P> WeeklyHoursPerEmployeeList);

    public void deleteByEmployeeId(int employeeId);

    public void deleteByProjectId(int projectId);

}
