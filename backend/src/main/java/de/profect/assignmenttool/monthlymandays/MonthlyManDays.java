package de.profect.assignmenttool.monthlymandays;


import com.fasterxml.jackson.annotation.JsonProperty;
import de.profect.assignmenttool.customer.Customer;
import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.project.Project;
import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "MONTHLY_MAN_DAYS")
@Data
public class MonthlyManDays implements Serializable {

    private static final long serialVersionUID = -2794767422946521182L;

    public MonthlyManDays() {

    };

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @JsonProperty
    @NonNull
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMPLOYEE_ID")
    @JsonProperty
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_ID")
    @JsonProperty
    private Project project;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID")
    @JsonProperty
    private Customer customer;

    @Column(name = "PROJECT_TYPE")
    @JsonProperty
    private String projectType;

    @Column(name = "MAN_DAYS")
    @JsonProperty
    private Double manDays;

    @Column(name = "DAILY_RATES")
    @JsonProperty
    private Double dailyRates;

    @Column(name = "DATE")
    @JsonProperty
    private LocalDate date;

    @Override
    public String toString() {
        return id + "," + employee + "," + project+ ","  + date;
    }
}
