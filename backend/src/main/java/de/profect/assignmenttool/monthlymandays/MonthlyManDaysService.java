package de.profect.assignmenttool.monthlymandays;

import de.profect.assignmenttool.assignment.Assignment;
import de.profect.assignmenttool.assignment.AssignmentRepository;
import de.profect.assignmenttool.employee.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class MonthlyManDaysService {

    @Autowired
    @Lazy
    private MonthlyManDaysRepository monthlyManDaysRepository;

    @Autowired
    @Lazy
    private AssignmentRepository assignmentRepository;

    public void updateMonthlyManDays(Employee employee) {
        monthlyManDaysRepository.deleteByEmployee(employee);

        List<Assignment> assignments = assignmentRepository.findByEmployee(employee);
        List<MonthlyManDays> monthlyManDaysList = new ArrayList<>();

        for(Assignment assignment : assignments) {
            LocalDate firstOfMonth = assignment.getStartDate().withDayOfMonth(1);

            while(firstOfMonth.isBefore(assignment.getEndDate()) || firstOfMonth.isEqual(assignment.getEndDate())) {
                Double manDays = calculateManDays(assignment, firstOfMonth);
                MonthlyManDays monthlyManDays = new MonthlyManDays();
                monthlyManDays.setEmployee(employee);
                monthlyManDays.setProject(assignment.getProject());
                monthlyManDays.setProjectType(assignment.getProject().getProjectType());
                monthlyManDays.setCustomer((assignment.getProject().getInvoiceRecipient()));
                monthlyManDays.setManDays(manDays);
                monthlyManDays.setDailyRates(manDays * assignment.getDailyRate());
                monthlyManDays.setDate(firstOfMonth.withDayOfMonth(firstOfMonth.lengthOfMonth()));

                monthlyManDaysList.add(monthlyManDays);

                firstOfMonth = firstOfMonth.plusMonths(1);
            }
        }

        monthlyManDaysRepository.saveAll(monthlyManDaysList);
    }

    private Double calculateManDays(Assignment assignment, LocalDate firstOfMonth) {
        LocalDate endOfMonth =  firstOfMonth.withDayOfMonth(firstOfMonth.lengthOfMonth());

        LocalDate start = firstOfMonth.isBefore(assignment.getStartDate()) ? assignment.getStartDate() : firstOfMonth;
        LocalDate end = assignment.getEndDate().isBefore(endOfMonth) ? assignment.getEndDate() : endOfMonth;

        Double hours = 0d;
        while(start.isBefore(end) || start.isEqual(end)) {
            hours += assignment.getHoursForWeekDay(start.getDayOfWeek());
            start = start.plusDays(1);
        }
        return hours / 8d;
    }
}
