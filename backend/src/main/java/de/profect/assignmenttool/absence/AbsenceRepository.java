package de.profect.assignmenttool.absence;

import de.profect.assignmenttool.employee.Employee;
import de.profect.assignmenttool.monthlyavailability.MonthlyAvailabilityService;
import de.profect.assignmenttool.weeklyquotaperemployee.WeeklyQuotaPerEmployeeService;
import de.profect.authentication.secured.SecuredRepository;
import de.profect.data.metadata.MetadataContainer;
import de.profect.data.metadata.MetadataDefinition;
import de.profect.data.metadata.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author kfe
 */
@Repository
public class AbsenceRepository extends SecuredRepository<Absence> {

    @Autowired
    @Lazy
    private MetadataService metadataService;

    @Autowired
    @Lazy
    private WeeklyQuotaPerEmployeeService weeklyQuotaPerEmployeeService;

    @Autowired
    @Lazy
    private MonthlyAvailabilityService monthlyAvailabilityService;

    @Override
    public <P extends Absence> P save(P entity) {
        entity.setCaption(entity.getReason());
        entity.setMetadataContainer(addDefaultMetadata(entity.getMetadataContainer()));
        entity = super.save(entity);
        weeklyQuotaPerEmployeeService.updateWeeklyHoursForEmployee(entity.getEmployee());
        monthlyAvailabilityService.updateWeeklyHoursForEmployee(entity.getEmployee());
        return entity;
    }

    @Override
    public <P extends Absence> P merge(P entity) {
        entity.setCaption(entity.getReason());
        entity = super.merge(entity);
        weeklyQuotaPerEmployeeService.updateWeeklyHoursForEmployee(entity.getEmployee());
        monthlyAvailabilityService.updateWeeklyHoursForEmployee(entity.getEmployee());
        return entity;
    }

    public List<Absence> findByEmployee(Employee employee) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.employee = :employee", getEntityClass())
                .setParameter("employee", employee)
                .getResultList();
    };

    public List<Absence> findByEmployeeAndStartDateAfter(Employee employee, LocalDate date) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.employee = :employee AND a.startDate > :start_date", getEntityClass())
                .setParameter("employee", employee).setParameter("start_date", date)
                .getResultList();
    };

    public List<Absence> findByEmployeeAndEndDateAfter(Employee employee, LocalDate date) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.employee = :employee AND a.endDate > :endDate", getEntityClass())
                .setParameter("employee", employee).setParameter("endDate", date)
                .getResultList();
    };

    public List<Absence> findByEmployeeAndGraphId(Employee employee, String graphId) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.employee = :employee AND a.graphId = :graphId", getEntityClass())
                .setParameter("employee", employee).setParameter("graphId", graphId)
                .getResultList();
    }

    public List<Absence> findByEmployeeAndReason(Employee employee, String reason) {
        return em.createQuery(
                "SELECT a FROM " + getEntityClass().getSimpleName() + " a WHERE a.employee = :employee AND a.reason = :reason", getEntityClass())
                .setParameter("employee", employee).setParameter("reason", reason)
                .getResultList();
    }

/*    public void deleteByGraphId(String graphId) {
        em.createQuery(
                "DELETE FROM " + getEntityClass().getSimpleName() + " a WHERE a.graphId = :graphId", getEntityClass())
                .setParameter("graphId", graphId)
                .getResultList();
    }*/

    public MetadataContainer addDefaultMetadata(MetadataContainer metadataContainer) {
        metadataContainer = metadataService.addSystemMetadata(metadataContainer);
        if (metadataContainer.getMetaValue(MetadataDefinition.READ_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.READ_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.WRITE_PUBLIC) == null) {
            metadataContainer.addMetadata(MetadataDefinition.WRITE_PUBLIC, Boolean.TRUE);
        }
        if (metadataContainer.getMetaValue(MetadataDefinition.IS_CACHEABLE) == null) {
            metadataContainer.addMetadata(MetadataDefinition.IS_CACHEABLE, Boolean.FALSE);
        }
        return metadataService.save(metadataContainer);
    }

}
