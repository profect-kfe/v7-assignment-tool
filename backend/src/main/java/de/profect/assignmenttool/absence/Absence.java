package de.profect.assignmenttool.absence;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.profect.AbstractGUIState;
import de.profect.assignmenttool.employee.Employee;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "ABSENCES")
@JsonTypeName(value = "v7-assignment-absence")
@Data
@EqualsAndHashCode(callSuper = true)
public class Absence extends AbstractGUIState {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMPLOYEE_ID")
    @JsonProperty
    private Employee employee;

    @Column(name = "START_DATE")
    @JsonProperty
    private LocalDate startDate;

    @Column(name = "END_DATE")
    @JsonProperty
    private LocalDate endDate;

    @Column(name = "REASON")
    @JsonProperty
    private String reason;

    @Column(name = "GRAPH_ID")
    @JsonProperty
    private String graphId;

    @Override
    public String getControllerPath() {
        return "ABSENCE";
    }
}
