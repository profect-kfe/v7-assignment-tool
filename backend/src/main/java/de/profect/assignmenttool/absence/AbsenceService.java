package de.profect.assignmenttool.absence;

import de.profect.authentication.secured.SecuredService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kfe
 */
@Service
public class AbsenceService extends SecuredService<Absence> {

    @Override
    public String getPrivilegePrefix() {
        return "ABSENCE";
    }

    public void deleteByEmployeeId(int employeeId) {
        List<Absence> absences = repo.findAll();
        List<Absence> deleteAbsences = new ArrayList<>();
        for (Absence absence : absences) {
            int test = absence.getEmployee().getId();
            if (test == employeeId) {
                deleteAbsences.add(absence);
            }
        }
        repo.deleteAll(deleteAbsences);
    }
}
