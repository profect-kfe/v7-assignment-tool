package de.profect.assignmenttool.absence;

import de.profect.authentication.secured.SecuredController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kfe
 */
@RestController
@RequestMapping(value = "/absence")
public class AbsenceController extends SecuredController<Absence> {

}
