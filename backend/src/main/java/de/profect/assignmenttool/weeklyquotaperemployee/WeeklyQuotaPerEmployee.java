package de.profect.assignmenttool.weeklyquotaperemployee;


import com.fasterxml.jackson.annotation.JsonProperty;
import de.profect.assignmenttool.employee.Employee;
import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "WEEKLY_QUOTA_PER_EMPLOYEE")
@Data
public class WeeklyQuotaPerEmployee implements Serializable {

    private static final long serialVersionUID = 245599543205835817L;

    public WeeklyQuotaPerEmployee() {

    };

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @JsonProperty
    @NonNull
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMPLOYEE_ID")
    @JsonProperty
    private Employee employee;

    @Column(name = "WEEKLY_QUOTA")
    @JsonProperty
    private Double weeklyQuota;

    @Column(name = "WEEK_DATE")
    @JsonProperty
    private LocalDate weekDate;

    @Override
    public String toString() {
        return id + "," + employee + "," + weeklyQuota + "," + weekDate;
    }
}
