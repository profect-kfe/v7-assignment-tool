package de.profect.assignmenttool.weeklyquotaperemployee;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import de.profect.assignmenttool.employee.Employee;
import org.springframework.data.repository.CrudRepository;

public interface WeeklyQuotaPerEmployeeRepository extends CrudRepository<WeeklyQuotaPerEmployee, Integer>{

    @Override
    public void deleteById(Integer id);

    @Override
    public List<WeeklyQuotaPerEmployee> findAll();

    @Override
    public Optional<WeeklyQuotaPerEmployee> findById(Integer id);

    public List<WeeklyQuotaPerEmployee> findByEmployeeAndWeekDateAfter(Employee employee, LocalDate date);

    public void deleteByEmployeeAndWeekDateAfter(Employee employee, LocalDate date);

    @Override
    public <P extends WeeklyQuotaPerEmployee> P save(P persist);

    @Override
    public <P extends WeeklyQuotaPerEmployee> Iterable<P>  saveAll(Iterable<P> WeeklyHoursPerEmployeeList);

    public void deleteByEmployeeId(int employeeId);
}
