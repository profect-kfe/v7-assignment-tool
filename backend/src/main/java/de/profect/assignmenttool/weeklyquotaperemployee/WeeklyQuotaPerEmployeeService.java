package de.profect.assignmenttool.weeklyquotaperemployee;

import de.profect.assignmenttool.absence.Absence;
import de.profect.assignmenttool.absence.AbsenceRepository;
import de.profect.assignmenttool.employee.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class WeeklyQuotaPerEmployeeService {

    @Autowired
    @Lazy
    private WeeklyQuotaPerEmployeeRepository weeklyQuotaPerEmployeeRepository;

    @Autowired
    @Lazy
    private AbsenceRepository absenceRepository;

    public void updateWeeklyHoursForEmployee(Employee employee) {
        LocalDate now = LocalDate.now();
        weeklyQuotaPerEmployeeRepository.deleteByEmployeeAndWeekDateAfter(employee, now);

        List<Absence> absences = absenceRepository.findByEmployeeAndStartDateAfter(employee, now);
        Collections.sort(absences, Comparator.comparing(Absence::getStartDate));

        LocalDate monday = getNextMonday(LocalDate.now());
        List<WeeklyQuotaPerEmployee> weeklyQuotaPerEmployeeList = new ArrayList<>();

        while(monday.compareTo(employee.getRetirementDate()) < 0) {
            WeeklyQuotaPerEmployee weeklyQuotaPerEmployee = new WeeklyQuotaPerEmployee();
            weeklyQuotaPerEmployee.setEmployee(employee);

            weeklyQuotaPerEmployee.setWeeklyQuota(calculateWeeklyHours(employee, monday, absences));
            weeklyQuotaPerEmployee.setWeekDate(monday);
            weeklyQuotaPerEmployeeList.add(weeklyQuotaPerEmployee);

            monday = getNextMonday(monday);
        }

        weeklyQuotaPerEmployeeRepository.saveAll(weeklyQuotaPerEmployeeList);
    }


    private Double calculateWeeklyHours(Employee employee, LocalDate monday, List<Absence> absences) {
        LocalDate friday = getNextFriday(monday);
        Double weeklyHours = employee.getWeeklyHours();
        LocalDate currentDay = monday;

        for(Absence absence : absences) {
            if((absence.getStartDate().isBefore(friday) || absence.getStartDate().isEqual(friday))
                    && (absence.getEndDate().isAfter(currentDay) || absence.getEndDate().isEqual(currentDay))) {
                LocalDate absentDay = absence.getStartDate().isBefore(currentDay) ? currentDay : absence.getStartDate();
                while((absentDay.isBefore(friday) || absentDay.isEqual(friday))
                        && (absentDay.isBefore(absence.getEndDate()) || absentDay.isEqual(absence.getEndDate()))) {
                    weeklyHours -= employee.getHoursForWeekDay(currentDay.getDayOfWeek());
                    absentDay = absentDay.plusDays(1);
                    currentDay = currentDay.plusDays(1);
                }
            }
        }
        return weeklyHours;
    }

    private LocalDate getNextMonday(LocalDate date) {
        return date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
    }

    private LocalDate getNextFriday(LocalDate date) {
        return date.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
    }
}
