package com.contoso;

import graphtutorial.Authentication;
import com.microsoft.graph.models.extensions.User;
import graphtutorial.Graph;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import com.microsoft.graph.models.extensions.DateTimeTimeZone;
import com.microsoft.graph.models.extensions.Event;

import java.io.InputStream;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.io.IOException;
import java.util.Properties;

/**
 * Graph Tutorial
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Java Graph Tutorial");
        System.out.println();

        // Load OAuth settings
        final Properties oAuthProperties = new Properties();
        try {
            //oAuthProperties.load(App.class.getResourceAsStream("oAuth.properties"));
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream("oAuth.properties");
            oAuthProperties.load(inputStream);
        } catch (IOException e) {
            System.out.println("Unable to read OAuth configuration. Make sure you have a properly formatted oAuth.properties file. See README for details.");
            return;
        }

        final String appId = oAuthProperties.getProperty("app.id");
        final String[] appScopes = oAuthProperties.getProperty("app.scopes").split(",");
        final String clientSecret = oAuthProperties.getProperty("app.client_secret");

        // Get an access token
        Authentication.initialize(appId);
        //final String accessToken = Authentication.getUserAccessToken(appScopes);
        //final String accessToken = Authentication.getUserAccessTokenIWA(appScopes);
        final String accessToken = Authentication.getUserAccessWithClientSecret(clientSecret);
        //final String accessToken = Authentication.getUserAccessTokenDeviceCode(appScopes);

        // Greet the user
        //User user = Graph.getUser(accessToken);
        List<User> users = Graph.getUsers(accessToken);
        User testUser = getUser(users, "Nikolai Schuster");

        System.out.println("Welcome " + testUser.displayName);
        System.out.println();

        listCalendarEvents(accessToken, testUser);

/*        Scanner input = new Scanner(System.in);

        int choice = -1;

        while (choice != 0) {
            System.out.println("Please choose one of the following options:");
            System.out.println("0. Exit");
            System.out.println("1. Display access token");
            System.out.println("2. List calendar events");

            try {
                choice = input.nextInt();
            } catch (InputMismatchException ex) {
                // Skip over non-integer input
                input.nextLine();
            }

            // Process user choice
            switch(choice) {
                case 0:
                    // Exit the program
                    System.out.println("Goodbye...");
                    break;
                case 1:
                    // Display access token
                    System.out.println("Access token: " + accessToken);
                    break;
                case 2:
                    // List the calendar
                    listCalendarEvents(accessToken, testUser);
                    break;
                default:
                    System.out.println("Invalid choice");
            }
        }

        input.close();*/
    }

    private static String formatDateTimeTimeZone(DateTimeTimeZone date) {
        LocalDateTime dateTime = LocalDateTime.parse(date.dateTime);

        return dateTime.format(
                DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)) +
                " (" + date.timeZone + ")";
    }

    private static void listCalendarEvents(String accessToken, User user) {
        // Get the user's events
        //List<Event> events = Graph.getEvents(accessToken);
        List<Event> events = Graph.getUserEvents(accessToken, user);

        System.out.println("Events:");

        for (Event event : events) {
            System.out.println("Subject: " + event.subject);
            System.out.println("  Organizer: " + event.organizer.emailAddress.name);
            System.out.println("  Start: " + formatDateTimeTimeZone(event.start));
            System.out.println("  End: " + formatDateTimeTimeZone(event.end));
            System.out.println("  Categories: " + event.categories);
        }

        System.out.println();
    }

    /***
     * Searches users for a specific user with given firstAndLastName.
     * @param firstAndLastName "Firstname Lastname", e.g. "Nikolai Schuster"
     * @return The searched user object, if it exists, otherwise, if the list is not empty, the first user and else null.
     */
    private static User getUser(List<User> users, String firstAndLastName) {
        if (users == null || users.size() == 0) {
            return null;
        }

        return users.stream()
                .filter(user -> "Nikolai Schuster".equals(user.displayName))
                .findAny()
                .orElse(users.get(0));
    }
}