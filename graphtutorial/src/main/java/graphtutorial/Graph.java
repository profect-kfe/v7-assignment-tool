package graphtutorial;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.microsoft.graph.logger.DefaultLogger;
import com.microsoft.graph.logger.LoggerLevel;
import com.microsoft.graph.models.extensions.Event;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.models.extensions.User;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.options.QueryOption;
import com.microsoft.graph.requests.extensions.GraphServiceClient;
import com.microsoft.graph.requests.extensions.IEventCollectionPage;

/**
 * Graph
 */
public class Graph {

    private static IGraphServiceClient graphClient = null;
    private static SimpleAuthProvider authProvider = null;

    private static void ensureGraphClient(String accessToken) {
        if (graphClient == null) {
            // Create the auth provider
            authProvider = new SimpleAuthProvider(accessToken);

            // Create default logger to only log errors
            DefaultLogger logger = new DefaultLogger();
            logger.setLoggingLevel(LoggerLevel.ERROR);

            // Build a Graph client
            graphClient = GraphServiceClient.builder()
                    .authenticationProvider(authProvider)
                    .logger(logger)
                    .buildClient();
        }
    }

    public static User getUser(String accessToken) {
        ensureGraphClient(accessToken);

        // GET /me to get authenticated user
        User me = graphClient
                .me()
                .buildRequest()
                .get();


        return me;
    }

    public static List<User> getUsers(String accessToken) {
        ensureGraphClient(accessToken);

        // GET /me to get authenticated user
        List<User> users = graphClient
                .users()
                .buildRequest()
                .get()
                .getCurrentPage();

        return users;
    }

    public static List<Event> getEvents(String accessToken) {
        ensureGraphClient(accessToken);

        // Use QueryOption to specify the $orderby query parameter
        final List<Option> options = new LinkedList<Option>();

        //API: https://docs.microsoft.com/en-us/graph/query-parameters

        // Sort results by createdDateTime, get newest first
        //options.add(new QueryOption("orderby", "createdDateTime DESC"));

        // Filters events by subject starting with 'Fron'
        //options.add(new QueryOption("filter", "startswith(subject, 'Fron')"));

        // all options of same type, e.g. filter, have to be chained in a single queryoption
        options.add(new QueryOption("filter", "start/dateTime ge '2020-01-01' and start/dateTime lt '2020-12-31' and isCancelled eq false"));
        options.add(new QueryOption("orderby", "start/dateTime ASC"));

        // ge = greater equal, lt = less than, eq = equals
        // multiple conditions with and, or, ...

        // GET /me/events
        // API: https://docs.microsoft.com/en-us/graph/api/resources/event?view=graph-rest-1.0
        IEventCollectionPage eventPage = graphClient
                .me()
                .events()
                .buildRequest(options)
                .select("subject,organizer,start,end")
                .get();

        return eventPage.getCurrentPage();
    }

    public static List<Event> getUserEvents(String accessToken, User user) {
        ensureGraphClient(accessToken);

        // Use QueryOption to specify the $orderby query parameter
        final List<Option> options = new LinkedList<Option>();

        //API: https://docs.microsoft.com/en-us/graph/query-parameters

        // Sort results by createdDateTime, get newest first
        //options.add(new QueryOption("orderby", "createdDateTime DESC"));

        // Filters events by subject starting with 'Fron'
        //options.add(new QueryOption("filter", "startswith(subject, 'Fron')"));

        // all options of same type, e.g. filter, have to be chained in a single queryoption

        Option filterOption = new QueryOption("filter",
                "start/dateTime ge '2020-01-01' " +
                        "and start/dateTime lt '2020-12-31' " +
                        "and isCancelled eq false " +
                        "and categories/any(a: a eq 'Feiertag')"
        );

        Option orderByOption = new QueryOption("orderby", "start/dateTime ASC");

        Option countOption = new QueryOption("count", "true");

        options.add(filterOption);
        options.add(orderByOption);
        // Filter für Feiertag: and categories/any(a: a eq 'Feiertag')"

        // ge = greater equal, lt = less than, eq = equals
        // multiple conditions with and, or, ...


        // no idea how to read the count
/*        IEventCollectionPage countPage = graphClient
                .users(user.id)
                .events()
                .buildRequest(Arrays.asList(filterOption, countOption))
                .select("subject")
                .get();*/


        Option topOption = new QueryOption("top", 10);
        options.add(topOption);

        // GET /me/events
        // API: https://docs.microsoft.com/en-us/graph/api/resources/event?view=graph-rest-1.0
        IEventCollectionPage eventPage = graphClient
                .users(user.id)
                .events()
                .buildRequest(options)
                .select("subject,organizer,start,end,categories")
                .get();

        //return eventPage.getCurrentPage();
        return getAllEvents(eventPage);
    }

    private static List<Event> getAllEvents(IEventCollectionPage eventPage) {
        if (eventPage.getRawObject().get("@odata.nextLink") != null) {
            return Stream.concat(eventPage.getCurrentPage().stream(),
                    getAllEvents(eventPage.getNextPage().buildRequest().get()).stream())
                    .collect(Collectors.toList());
        } else {
            return eventPage.getCurrentPage();
        }
    }

}