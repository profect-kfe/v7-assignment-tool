package graphtutorial;

// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

import com.microsoft.aad.msal4j.ITokenCacheAccessAspect;
import com.microsoft.aad.msal4j.ITokenCacheAccessContext;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TokenCacheAspect implements ITokenCacheAccessAspect {

    private String data;
    private static String resource;

    public TokenCacheAspect(String fileName) {
        this.resource = fileName;
        this.data = readDataFromFile(fileName);
    }

    @Override
    public void beforeCacheAccess(ITokenCacheAccessContext iTokenCacheAccessContext) {
        iTokenCacheAccessContext.tokenCache().deserialize(data);
    }

    @Override
    public void afterCacheAccess(ITokenCacheAccessContext iTokenCacheAccessContext) {
        data = iTokenCacheAccessContext.tokenCache().serialize();
        // you could implement logic here to write changes to file
        persistData(data);
    }

    private static String readDataFromFile(String resource) {
        try {
            URL path = TokenCacheAspect.class.getClassLoader().getResource(resource);
            return new String(
                    Files.readAllBytes(
                            Paths.get(path.toURI())));
        } catch (Exception ex){
            System.out.println("Error reading data from file: " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    private static void persistData(String data) {
        // doesn't work with path.toString()
        URL path = TokenCacheAspect.class.getClassLoader().getResource(resource);
        //"C:/Users/nsc/Documents/outlookapi/graphtutorial/src/main/resources/sample_cache.json"
        try (FileWriter fw = new FileWriter("C:/Users/nsc/Documents/outlookapi/graphtutorial/src/main/resources/sample_cache.json")) {
            fw.write(data);
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            System.out.println("Error reading data from file: " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    }
}