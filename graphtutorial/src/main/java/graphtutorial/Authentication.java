package graphtutorial;

import java.net.MalformedURLException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.*;
import java.util.function.Consumer;

import com.microsoft.aad.msal4j.*;
import com.microsoft.aad.msal4j.IAccount;
import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.microsoft.aad.msal4j.IntegratedWindowsAuthenticationParameters;
import com.microsoft.aad.msal4j.MsalException;
import com.microsoft.aad.msal4j.PublicClientApplication;
import com.microsoft.aad.msal4j.SilentParameters;

/**
 * Authentication
 */
public class Authentication {

    // java code beispiele (außer confidential client natürlich...)
    // https://github.com/AzureAD/microsoft-authentication-library-for-java/tree/dev/src/samples/public-client

    private static String applicationId;
    // Set authority to allow only organizational accounts
    // Device code flow only supports organizational accounts
    private final static String authority = "https://login.microsoftonline.com/common/";
    private final static String AUTHORITY = "https://login.microsoftonline.com/organizations/";

    private final static String tenantId = "46390799-6bed-4d68-98bf-fbf4c15a073b";
    private final static String CLIENT_CREDENTIAL_AUTHORITY = "https://login.microsoftonline.com/46390799-6bed-4d68-98bf-fbf4c15a073b/";
    private final static Set<String> CLIENT_CREDENTIAL_SCOPE = Collections.singleton("https://graph.microsoft.com/.default");

    public static void initialize(String applicationId) {
        Authentication.applicationId = applicationId;
    }

    public static String getUserAccessToken(String[] scopes) {
        if (applicationId == null) {
            System.out.println("You must initialize Authentication before calling getUserAccessToken");
            return null;
        }

        Set<String> scopeSet = Set.of(scopes);

        ExecutorService pool = Executors.newFixedThreadPool(1);
        PublicClientApplication app;
        try {
            // Build the MSAL application object with
            // app ID and authority
            app = PublicClientApplication.builder(applicationId)
                    .authority(authority)
                    .executorService(pool)
                    .build();

        } catch (MalformedURLException e) {
            return null;
        }

        // Create consumer to receive the DeviceCode object
        // This method gets executed during the flow and provides
        // the URL the user logs into and the device code to enter
        Consumer<DeviceCode> deviceCodeConsumer = (DeviceCode deviceCode) -> {
            // Print the login information to the console
            System.out.println(deviceCode.message());
        };

        // Request a token, passing the requested permission scopes
        IAuthenticationResult result = app.acquireToken(
                DeviceCodeFlowParameters
                        .builder(scopeSet, deviceCodeConsumer)
                        .build()
        ).exceptionally(ex -> {
            System.out.println("Unable to authenticate - " + ex.getMessage());
            return null;
        }).join();

        pool.shutdown();

        if (result != null) {
            return result.accessToken();
        }

        return null;
    }

    public static String getUserAccessTokenIWA(String[] scopes) {
        if (applicationId == null) {
            System.out.println("You must initialize Authentication before calling getUserAccessToken");
            return null;
        }

        Set<String> scopeSet = Set.of(scopes);

        String blub;
        try {
            blub = acquireTokenIwa(scopeSet);
        } catch (Exception e) {
            return null;
        }

        return blub;
    }

    private static String acquireTokenIwa(Set<String> scopeSet) throws Exception {

        // Load token cache from file and initialize token cache aspect. The token cache will have
        // dummy data, so the acquireTokenSilently call will fail.
        //TokenCacheAspect tokenCacheAspect = new TokenCacheAspect("sample_cache.json");

/*
        PublicClientApplication pca = PublicClientApplication.builder(applicationId)
                .authority(authority)
                .setTokenCacheAccessAspect(tokenCacheAspect)
                .build();
*/

        ExecutorService pool = Executors.newFixedThreadPool(1);
        PublicClientApplication pca;
        try {
            // Build the MSAL application object with
            // app ID and authority
            pca = PublicClientApplication.builder(applicationId)
                    .authority(authority)
                    .executorService(pool)
                    .build();
        } catch (MalformedURLException e) {
            return null;
        }

        Set<IAccount> accountsInCache = pca.getAccounts().join();
        // Take first account in the cache. In a production application, you would filter
        // accountsInCache to get the right account for the user authenticating.
        IAccount account = accountsInCache.iterator().next();

        IAuthenticationResult result;
        try {
            SilentParameters silentParameters =
                    SilentParameters
                            .builder(scopeSet, account)
                            .build();

            // try to acquire token silently. This call will fail since the token cache
            // does not have any data for the user you are trying to acquire a token for
            result = pca.acquireTokenSilently(silentParameters).join();
        } catch (Exception ex) {
            if (ex.getCause() instanceof MsalException) {

                IntegratedWindowsAuthenticationParameters parameters =
                        IntegratedWindowsAuthenticationParameters
                                .builder(scopeSet, "nsc")
                                .build();

                // Try to acquire a IWA. You will need to generate a Kerberos ticket.
                // If successful, you should see the token and account information printed out to
                // console
                result = pca.acquireToken(parameters).join();
            } else {
                // Handle other exceptions accordingly
                throw ex;
            }
        }
        return result.accessToken();
    }


    public static String getUserAccessWithClientSecret(String clientSecret) {
        if (applicationId == null) {
            System.out.println("You must initialize Authentication before calling getUserAccessToken");
            return null;
        }

        String accessToken;
        try {
            accessToken = acquireTokenWithClientSecret(clientSecret);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return accessToken;
    }

    private static String acquireTokenWithClientSecret(String clientSecret) throws Exception {

        // i have no idea what the tokenCache is used (/useful) for when using client secret.
        // nothing is actually written into the .json file.
        // i just got it from the example code (can't find link currently :/ ).

        // Load token cache from file and initialize token cache aspect. The token cache will have
        // dummy data, so the acquireTokenSilently call will fail.
        TokenCacheAspect tokenCacheAspect = new TokenCacheAspect("app_token_cache.json");

        // This is the secret that is created in the Azure portal when registering the application
        IClientCredential credential = ClientCredentialFactory.createFromSecret(clientSecret);
        ConfidentialClientApplication cca =
                ConfidentialClientApplication
                        .builder(applicationId, credential)
                        .authority(CLIENT_CREDENTIAL_AUTHORITY)
                        .setTokenCacheAccessAspect(tokenCacheAspect)
                        .build();

        IAuthenticationResult result;
        try {
            SilentParameters silentParameters =
                    SilentParameters
                            .builder(CLIENT_CREDENTIAL_SCOPE)
                            .build();

            // try to acquire token silently. This call will fail since the token cache does not
            // have a token for the application you are requesting an access token for
            result = cca.acquireTokenSilently(silentParameters).join();
        } catch (Exception ex) {
            if (ex.getCause() instanceof MsalException) {

                ClientCredentialParameters parameters =
                        ClientCredentialParameters
                                .builder(CLIENT_CREDENTIAL_SCOPE)
                                .build();

                // Try to acquire a token. If successful, you should see
                // the token information printed out to console
                result = cca.acquireToken(parameters).join();
            } else {
                // Handle other exceptions accordingly
                throw ex;
            }
        }
        return result.accessToken();
    }

    public static String getUserAccessTokenDeviceCode(String[] scopes) {
        if (applicationId == null) {
            System.out.println("You must initialize Authentication before calling getUserAccessToken");
            return null;
        }

        Set<String> scopeSet = Set.of(scopes);

        String blub;
        try {
            blub = acquireTokenDeviceCode(scopeSet);
        } catch (Exception e) {
            return null;
        }

        return blub;
    }

    private static String acquireTokenDeviceCode(Set<String> scopes) throws Exception {

        // Load token cache from file and initialize token cache aspect. The token cache will have
        // dummy data, so the acquireTokenSilently call will fail.
        TokenCacheAspect tokenCacheAspect = new TokenCacheAspect("sample_cache.json");

        PublicClientApplication pca = PublicClientApplication.builder(applicationId)
                .authority(AUTHORITY)
                .setTokenCacheAccessAspect(tokenCacheAspect)
                .build();

        Set<IAccount> accountsInCache = pca.getAccounts().join();
        // Take first account in the cache. In a production application, you would filter
        // accountsInCache to get the right account for the user authenticating.
        IAccount account = accountsInCache.iterator().next();

        IAuthenticationResult result;
        try {
            SilentParameters silentParameters =
                    SilentParameters
                            .builder(scopes, account)
                            .build();

            // try to acquire token silently. This call will fail since the token cache
            // does not have any data for the user you are trying to acquire a token for
            result = pca.acquireTokenSilently(silentParameters).join();
        } catch (Exception ex) {
            if (ex.getCause() instanceof MsalException) {

                Consumer<DeviceCode> deviceCodeConsumer = (DeviceCode deviceCode) ->
                        System.out.println(deviceCode.message());

                DeviceCodeFlowParameters parameters =
                        DeviceCodeFlowParameters
                                .builder(scopes, deviceCodeConsumer)
                                .build();

                // Try to acquire a token via device code flow. If successful, you should see
                // the token and account information printed out to console, and the sample_cache.json
                // file should have been updated with the latest tokens.
                result = pca.acquireToken(parameters).join();
            } else {
                // Handle other exceptions accordingly
                throw ex;
            }
        }
        return result.accessToken();
    }
}