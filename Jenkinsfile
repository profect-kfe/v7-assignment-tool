pipeline {
	agent any
	environment {
		BUILD_VERSION = """${sh(
			returnStdout: true,
			script: 'echo 7.$(date +%y.%-m)-snapshot-$(date +%Y%m%d%H%M)'
		)}"""
		KUBECONFIG = '/etc/kube/infrastructure-user.kubeconfig'
        ENABLE_FRONTEND = 'true'
        ENABLE_FRONTEND_BUILD = 'true'
        ENABLE_FRONTEND_TEST = 'false'
        ENABLE_FRONTEND_DOCUMENT = 'false'
        ENABLE_FRONTEND_DEPLOY = 'false'
        ENABLE_FRONTEND_DOCKERIZE = 'true'

        ENABLE_BACKEND = 'true'
        ENABLE_BACKEND_BUILD = 'true'
        ENABLE_BACKEND_TEST = 'false'
        ENABLE_BACKEND_DOCUMENT = 'false'
        ENABLE_BACKEND_DEPLOY = 'false'
        ENABLE_BACKEND_DOCKERIZE = 'true'

	}
    stages {
        stage('build:frontend') {
		  when {
			expression { ENABLE_FRONTEND == 'true' && ENABLE_FRONTEND_BUILD == 'true' }
		  }
           steps {
               echo 'building frontend...'
               dir('./frontend') {
                   script {
					   echo 'deleting package-lock.json'
						  sh 'rm -r -f node_modules'
						  sh 'rm -r -f dist'
						  sh 'rm -f package-lock.json'
						echo 'executing npm install'
						sh 'npm install'

						// sh 'ng test --watch=false'

						echo 'building frontend'
						sh 'rm -f node_modules/@angular/compiler-cli/ngcc/__ngcc_lock_file__'
						sh 'node --max_old_space_size=8192 ./node_modules/@angular/cli/bin/ng build --prod --aot=false --build-optimizer=false --sourceMap=false --baseHref=/ --deployUrl=/'   
                   }
               }
           }
        }
		stage('dockerize:frontend') {
		      when {
        expression { ENABLE_FRONTEND == 'true' && ENABLE_FRONTEND_DOCKERIZE == 'true' }
      }
        	steps {
				echo 'dockerizing frontend...'
        		dir('./frontend') {
					echo 'building frontend docker image'
					sh 'docker build . --tag profectde/assignmenttool:frontend-${BUILD_VERSION}'

					echo 'deploying frontend docker image'
					sh 'docker login --username profectdeservice --password 249rZLVRZL54zE9uqTymJcSFnserTQk9'
					sh 'docker push profectde/assignmenttool:frontend-${BUILD_VERSION}'
        		}
        	}
        }
		stage('run:frontend') {
		  when {
			expression { ENABLE_FRONTEND == 'true' && ENABLE_FRONTEND_DOCKERIZE == 'true' }
		  }
		  steps {
			dir('./docker/k8s') {
			  echo 'starting v7-frontend service...'
				sh 'envsubst < v7-frontend-service.tmpl | kubectl apply -f -'
				echo 'started v7-frontend service...'
			}
		  }
        }
        stage('build:backend') {
		      when {
        expression { ENABLE_BACKEND == 'true' && ENABLE_BACKEND_BUILD == 'true' }
      }
			steps {
				echo 'building backend...'
				dir('./backend') {
					script {
						sh 'mvn clean package -Pk8s-profile -DskipTests'
					}
				}
			}
        }
		stage('dockerize:backend') {
		      when {
        expression { ENABLE_BACKEND == 'true' && ENABLE_BACKEND_DOCKERIZE == 'true' }
      }
        	steps {
				echo 'dockerizing backend...'
				dir('./backend') {
				
					echo 'building backend docker image'
					sh 'docker build . --tag profectde/assignmenttool:backend-${BUILD_VERSION}'
					
					echo 'deploying backend docker image'
					sh 'docker login --username profectdeservice --password 249rZLVRZL54zE9uqTymJcSFnserTQk9'
					sh 'docker push profectde/assignmenttool:backend-${BUILD_VERSION}'
				}
        	}
        }
		stage('run:backend') {
		  when {
			expression { ENABLE_BACKEND == 'true' && ENABLE_BACKEND_DOCKERIZE == 'true' }
		  }
		  steps {
			dir('./docker/k8s') {
			  echo 'starting v7-backend service...'
				sh 'envsubst < v7-backend-service.tmpl | kubectl apply -f -'
				echo 'started v7-backend service...'
			}
		  }
        }
    }
}
