import { AbstractDynamicFormControlComponent } from '@profect/v7-formbuilder-lib';
import { WorkflowSelectComponent, InitService, WorkflowGraphState } from '@profect/v7-core-lib';

export function mod() {

  //MOD 25.05 sme
  //Update 04.06: Fixed in Core.Can be removed with next core release (compare functions!)
  (<any>AbstractDynamicFormControlComponent.prototype).registerControl = function () {
    this.associatedCtrl = this.registrationSrv.registerControl(this, this.getValidators(), this.getAsyncValidators());
    //modification: added bind(this.associatedCtrl)
    //moved _reset(formState, options); out of if-clause
    const _reset = this.associatedCtrl.reset.bind(this.associatedCtrl);
    this.associatedCtrl.reset = (formState = null, options = {}) => {
      if (!formState) {
        formState = this.state.getInitValue();
      }
      _reset(formState, options);
    };
    this.applyStateSettings();
  }

  //MOD 09.06 sme
  //used two make two workflowselects possible
  //update 08.09. (kfe): adjusted to latest changes in core
  WorkflowSelectComponent.prototype.iteratingDataToProcess = async function (initService: InitService) {
    if (!initService ||
      !initService.workflowIDs ||
      initService.workflowIDs.length === 0) {
      return;
    }
    let workflowStates: WorkflowGraphState[] =
      await this.cs.getAllById(initService.workflowIDs) as WorkflowGraphState[];
    this.workflows = [];
    for (const wfStateId of initService.workflowIDs) {
      if (this.state.workflowIds.includes(wfStateId)) {
        this.workflows.push(workflowStates.find(state => state.id === wfStateId));
      }
    }
    this.sortToCategory();
  }
}
