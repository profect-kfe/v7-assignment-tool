import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterComponent, V7CoreLibModule, V7_CONFIG, DataviewConfig, InputConfig } from '@profect/v7-core-lib';
// import { V7CampaignManagementLibModule } from '@profect/v7-campaign-management-lib';
// import { V7DatasourceEntityLibModule } from '@profect/v7-datasource-entity-lib';
import { V7DatasourceSqlLibModule, PostgreSqlDBDataSource } from '@profect/v7-datasource-sql-lib';
// import { V7DataviewChartLibModule } from '@profect/v7-dataview-chart-lib';
import { V7DataviewCompassLibModule } from '@profect/v7-dataview-compass-lib';
// import { V7DataviewDashboardLibModule } from '@profect/v7-dataview-dashboard-lib';
// import { V7DataviewMapchartLibModule } from '@profect/v7-dataview-mapchart-lib';
// import { V7DataviewSelectionLibModule } from '@profect/v7-dataview-selection-lib';
// import { V7DmsLibModule } from '@profect/v7-dms-lib';
import { V7FormbuilderLibModule } from '@profect/v7-formbuilder-lib';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { Absence } from './assignment/absence/absence';
import { Customer } from './assignment/customer/customer';
import { Employee } from './assignment/employee/employee';
import { Project } from './assignment/project/project';
import { AbsenceService } from './assignment/absence/absence.service';
import { CustomerService } from './assignment/customer/customer.service';
import { EmployeeService } from './assignment/employee/employee.service';
import { ProjectService } from './assignment/project/project.service';
import { TableViewWithCustomColumnComponent } from './assignment/tableviewwithcustomcolumn/tableview-with-custom-column.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatListModule } from '@angular/material/list';
import { MatSortModule } from '@angular/material/sort';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { TableViewWithCustomColumnState } from './assignment/tableviewwithcustomcolumn/tableview-with-custom-column.state';
import { CdkTableModule } from '@angular/cdk/table';
import { CountUpModule } from 'countup.js-angular2';
import { ProjectTableViewFilterComponent } from './assignment/tableviewwithcustomcolumn/filter/proj-tableview-filter.component';
import { ProjectTableViewPaginationComponent } from './assignment/tableviewwithcustomcolumn/pagination/proj-tableview-pagination.component';
import { AssignmentService } from './assignment/assignment/assignment.service';
import { Assignment } from './assignment/assignment/assignment';
import { AssignmentValidator } from './assignment/assignment/validator/assignment-validator';
import { AssignmentValidatorState } from './assignment/assignment/validator/assignment-validator.state';
import { EmployeeValidator } from './assignment/employee/validator/employee-validator';
import { EmployeeValidatorState } from './assignment/employee/validator/employee-validator.state';
import { EmployeeHierarchyDefinition } from './assignment/employee/hierarchy/employee-hierarchy-definition.state';
import { EmployeeHierarchyDefinitionService } from './assignment/employee/hierarchy/employee-hierarchy-definition.service';
import { CustomerHierarchyDefinition } from './assignment/customer/hierarchy/customer-hierarchy-definition.state';
import { ProjectHierarchyDefinition } from './assignment/project/hierarchy/project-hierarchy-definition.state';
import { CustomerHierarchyDefinitionService } from './assignment/customer/hierarchy/customer-hierarchy-definition.service';
import { ProjectHierarchyDefinitionService } from './assignment/project/hierarchy/project-hierarchy-definition.service';
import { EmployeeTeamleaderConnectorState } from './assignment/employee/connector/employee-teamleader-connector.state';
import { EmployeeTeamleaderConnectorService } from './assignment/employee/connector/employee-teamleader-connector.service';
import { DialogConfirmConnectorService } from './assignment/tableviewwithcustomcolumn/dialogconfirmconnector/dialog-confirm-connector.service';
import { DynamicLabelMessageConnectorService } from './assignment/tableviewwithcustomcolumn/dynamiclabelmessage/dynamic-label-message-connector.service';
import { DialogConfirmConnectorState } from './assignment/tableviewwithcustomcolumn/dialogconfirmconnector/dialog-confirm-connector.state';
import { DynamicLabelMessageConnectorState } from './assignment/tableviewwithcustomcolumn/dynamiclabelmessage/dynamic-label-message-connector.state';
import { AlertDialog } from './services/alert/alert-dialog';
import { AlertService } from './services/alert/alert.service';
import { mod } from './core-modding'
import { EmployeeFormSaveConnectorState } from './assignment/employee/connector/employee-form-save-connector.state';
import { EmployeeFormSaveConnectorService } from './assignment/employee/connector/employee-form-save-connector.service';
import { GraphService } from './assignment/graph/graph.service';
import { GraphUpdateConnectorService } from './assignment/graph/graph-update-connector.service';
import { GraphUpdateConnectorState } from './assignment/graph/graph-update-connector.state';
import { ProjectFormSaveConnectorState } from './assignment/project/connector/project-form-save-connector.state';
import { ProjectFormSaveConnectorService } from './assignment/project/connector/project-form-save-connector.service';
import { ProjectValidator } from './assignment/project/validator/project-validator';
import { ProjectValidatorState } from './assignment/project/validator/project-validator.state';

registerLocaleData(localeDe, 'de');
mod();

@NgModule({
  declarations: [
    TableViewWithCustomColumnComponent,
    ProjectTableViewFilterComponent,
    ProjectTableViewPaginationComponent,
    AlertDialog
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    V7CoreLibModule,
    // V7CampaignManagementLibModule,
    // V7DatasourceEntityLibModule,
    V7DatasourceSqlLibModule,
    // V7DataviewChartLibModule,
    V7DataviewCompassLibModule,
    // V7DataviewMapchartLibModule,
    // V7DataviewSelectionLibModule,
    // V7DataviewDashboardLibModule,
    // V7DmsLibModule,
    V7FormbuilderLibModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatButtonToggleModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatTabsModule,
    MatDividerModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatSelectModule,
    MatListModule,
    MatMenuModule,

    MatSortModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    //TableViewPaginationComponent,
    //TableViewFilterComponent,
    //DataViewFooterComponent,
    CdkTableModule,
    CountUpModule,
    DataviewConfig,
    InputConfig,
  ],
  providers: [
    { provide: V7_CONFIG, useValue: environment },
    AbsenceService,
    AssignmentService,
    CustomerService,
    EmployeeService,
    ProjectService,
    CustomerHierarchyDefinitionService,
    EmployeeHierarchyDefinitionService,
    ProjectHierarchyDefinitionService,
    { provide: 'v7-assignment-absence', useClass: Absence },
    { provide: 'v7-assignment-assignment', useClass: Assignment },
    { provide: 'v7-assignment-customer', useClass: Customer },
    { provide: 'v7-assignment-employee', useClass: Employee },
    { provide: 'v7-assignment-project', useClass: Project },
    { provide: 'v7-tableview-cuco-component', useClass: TableViewWithCustomColumnState },
    { provide: 'v7-assignment-validator', useClass: AssignmentValidatorState },
    { provide: 'v7-employee-validator', useClass: EmployeeValidatorState },
    { provide: 'v7-project-validator', useClass: ProjectValidatorState },
    { provide: 'v7-customer-hierarchy-definition', useClass: CustomerHierarchyDefinition },
    { provide: 'v7-employee-hierarchy-definition', useClass: EmployeeHierarchyDefinition },
    { provide: 'v7-project-hierarchy-definition', useClass: ProjectHierarchyDefinition },
    { provide: 'v7-employee-teamleader-connector', useClass: EmployeeTeamleaderConnectorState },
    { provide: 'v7-postresql-data-source-object', useClass: PostgreSqlDBDataSource },
    EmployeeValidator,
    EmployeeTeamleaderConnectorService,
    AssignmentValidator,
    ProjectValidator,
    DialogConfirmConnectorService,
    { provide: 'v7-dialog-confirm-connector', useClass: DialogConfirmConnectorState },
    DynamicLabelMessageConnectorService,
    { provide: 'v7-dynamic-label-message-connector', useClass: DynamicLabelMessageConnectorState },
    AlertService,
    { provide: 'v7-employee-form-save-connector', useClass: EmployeeFormSaveConnectorState },
    EmployeeFormSaveConnectorService,
    { provide: 'v7-project-form-save-connector', useClass: ProjectFormSaveConnectorState },
    ProjectFormSaveConnectorService,
    GraphService,
    GraphUpdateConnectorService,
    { provide: 'v7-graph-update-connector', useClass: GraphUpdateConnectorState },
  ],
  bootstrap: [RouterComponent],
  entryComponents: [
    TableViewWithCustomColumnComponent,
    ProjectTableViewFilterComponent,
    ProjectTableViewPaginationComponent,
    AlertDialog
  ]
})
export class V7AppModule { }
