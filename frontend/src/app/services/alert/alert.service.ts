import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AlertDialog } from './alert-dialog';

@Injectable()
export class AlertService {

  alertDialogRef: MatDialogRef<AlertDialog>;

  constructor(public alertDialog: MatDialog, public waitDialog: MatDialog) {

  }

  alert(header: string, message: string, icon: string, size: string): void {
    this.alertDialogRef = this.alertDialog.open(AlertDialog, {
      data: {
        header: header,
        message: message,
        icon: icon
      },
      panelClass: "assignment-alertdialog-panel-wrapper-" + size
    });
  }

  asyncAlertAwait() {
    this.alertDialogRef = this.alertDialog.open(AlertDialog, {
      data: {
        wait: true,
        disableClose: true
      },
      panelClass: "assignment-alertdialog-panel-wrapper"
    });
  }

  asyncAlertShow(header: string, message: string, icon: string) {
    this.alertDialogRef.componentInstance.header = header;
    this.alertDialogRef.componentInstance.message = message;
    this.alertDialogRef.componentInstance.icon = icon;
    this.alertDialogRef.componentInstance.wait = false;
  }

  asyncAlertDismiss() {
    this.alertDialogRef.close();
  }

}
