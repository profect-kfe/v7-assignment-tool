import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'alert-dialog',
  template: `
    <div class="v7-panel panel-dialog assignment-alertdialog-panel">
      <div *ngIf="wait" class="assignment-alertdialog-wait">
        <mat-spinner [diameter]=50></mat-spinner>
      </div>
      <div *ngIf="!wait">
        <div *ngIf="icon" class="assignment-alertdialog-icon">
          <mat-icon class="entry-icon" aria-hidden="true" svgIcon="{{ icon }}"></mat-icon>
        </div>
        <h2 *ngIf="header" class="assignment-alertdialog-header">{{ header }}</h2>
        <div *ngIf="message" class="assignment-alertdialog-message" [innerHTML]="message"></div>
        <div class="assignment-alertdialog-buttonbar">
          <button class="btn btn-md btn-primary assignment-alertdialog-button" (click)="close()">OK</button>
        </div>
      </div>
    </div>
  `
})
export class AlertDialog {

  wait: boolean;
  header: string;
  message: string;
  icon: string;

  constructor(
    public dialogRef: MatDialogRef<AlertDialog>,
    @Inject(MAT_DIALOG_DATA) data) {
      this.wait = data.wait;
      this.header = data.header;
      this.message = data.message;
      this.icon = data.icon;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  close(): void {
    this.dialogRef.close();
  }

}
