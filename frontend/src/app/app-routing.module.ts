import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IconRegistrationService } from '@profect/v7-core-lib';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

  constructor(iconRegistrationService: IconRegistrationService) {
    iconRegistrationService.registerIcon('face', 'assets/icons/face-24px.svg');
    iconRegistrationService.registerIcon('watch_later', 'assets/icons/watch_later-24px.svg');
  }
  
}
