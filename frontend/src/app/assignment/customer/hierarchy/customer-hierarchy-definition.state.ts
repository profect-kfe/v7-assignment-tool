
import { Injectable } from "@angular/core";
import { V7ServiceLink, HierarchyDefinition, MetadataContainer } from '@profect/v7-core-lib';
import { CustomerHierarchyDefinitionService } from './customer-hierarchy-definition.service';

@Injectable()
@V7ServiceLink(CustomerHierarchyDefinitionService)
export class CustomerHierarchyDefinition extends HierarchyDefinition {

    public readonly __v7identifier: string = 'v7-customer-hierarchy-definition';

    public readonly CONTROLLERPATH: string = "customerhierarchydefinition";

    filter: MetadataContainer;
    withEmptyFolders: boolean;
    operationTypes: string[];
}
