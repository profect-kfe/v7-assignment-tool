import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Customer } from './customer';
import { AbstractGUIService, ApiService, ClassService, LocalDynamicObjectsService } from '@profect/v7-core-lib';

@Injectable()
export class CustomerService extends AbstractGUIService<Customer> {

  public readonly CONTROLLERPATH: string = "customer";

  constructor(protected injector: Injector,
    protected http: HttpClient,
    protected apiService: ApiService,
    protected classService: ClassService,
    protected localObjSrv: LocalDynamicObjectsService) {
    super(injector, http, apiService, classService, localObjSrv);
  }

  /**
   * Saves the given {@link Customer} and returns the saved state,
   * possibly containing a server generated id.
   *
   *  @param state The {@link Customer} that should be saved
   *  @returns A {@link Promise} returning the saved {@link Customer}
   */
  public async save(entity: Customer): Promise<Customer> {
    entity.caption = entity.division;
    return super.save(entity);
  }
}
