import { V7ServiceLink, AbstractGUIState } from '@profect/v7-core-lib';
import { CustomerService } from './customer.service';

@V7ServiceLink(CustomerService)
export class Customer extends AbstractGUIState {

    public readonly __v7identifier: string = 'v7-assignment-customer';

    customerName: string;
    division: string;
    
}
