import { V7ServiceLink, AbstractGUIState } from '@profect/v7-core-lib';
import { EmployeeService } from './employee.service';

@V7ServiceLink(EmployeeService)
export class Employee extends AbstractGUIState {

    public readonly __v7identifier: string = 'v7-assignment-employee';

    staffNumber: string;

    firstName: string;

    lastName: string;

    location: string;

    teamLeader: Employee;

    contractType: string;

    weeklyHours: number;

    mondayHours: number;

    tuesdayHours: number;

    wednesdayHours: number;

    thursdayHours: number;

    fridayHours: number;

    entryDate: Date;

    retirementDate: Date;

    comment: string;

    email: string;

    shorthand: string;

    isTeamleader: boolean;
}
