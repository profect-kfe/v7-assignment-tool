import {Injectable} from '@angular/core';
import {ConnectorManagerService, TargetConnectorService, TargetConnectorState} from '@profect/v7-core-lib';
import {DynamicFormControlCheckboxComponent, DynamicFormControlDropdownComponent, V7FormGroup} from '@profect/v7-formbuilder-lib';
import * as _ from 'lodash';
import { EmployeeService } from '../employee.service';
import { Employee} from '../employee';
import { AlertService } from '../../../services/alert/alert.service';
import * as uuid from 'uuid';

@Injectable()
export class EmployeeTeamleaderConnectorService extends TargetConnectorService {

    private lock: boolean = false;
    private connectorQueue = [];

    constructor(protected cms: ConnectorManagerService, protected employeeService: EmployeeService, protected alertService: AlertService) {
        super(cms);
    }

    public isUndoable(): boolean {
        return false;
    }

    public async doAction(connectorState: TargetConnectorState, doData: any) {

        console.debug("dodata", doData);

        if (this.lock) {
          this.connectorQueue.unshift([connectorState, doData]);
          return;
        }
        this.lock = true;

        let teamleaderCheckbox: DynamicFormControlCheckboxComponent = <DynamicFormControlCheckboxComponent> this.cms.getRegistered(connectorState.source);
        let teamleaderDropdown: DynamicFormControlDropdownComponent = <DynamicFormControlDropdownComponent> this.cms.getRegistered(connectorState.target);
        let formgroup: V7FormGroup = <V7FormGroup> teamleaderCheckbox.associatedCtrl.root;
        let employeeID = formgroup.controls.id.value;

        console.debug("formgroup", formgroup);
        console.debug("teamleaderCheckbox", teamleaderCheckbox);

        console.debug("executing employee teamleader connector for employee " + employeeID);
        console.debug("teamleader checkbox value: " + teamleaderCheckbox.associatedCtrl.value);

        if (teamleaderCheckbox.associatedCtrl.value) {
          teamleaderDropdown.associatedCtrl.setValue(null);
          teamleaderDropdown.associatedCtrl.disable();
        } else {
          teamleaderDropdown.associatedCtrl.enable();
        }

        console.debug("finished employee teamleader connector for employee " + employeeID);
        this.lock = false;
        if (this.connectorQueue.length > 0) {
          let doAction = this.connectorQueue.pop();
          this.connectorQueue = [];
          this.doAction(doAction[0], doAction[1]);
        }
    }

  }
