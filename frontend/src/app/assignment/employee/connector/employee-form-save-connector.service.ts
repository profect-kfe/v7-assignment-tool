import {Injectable, Injector} from '@angular/core';
import {ConnectorManagerService, TargetConnectorService, TargetConnectorState, NotificationService, ClassService} from '@profect/v7-core-lib';
import {FormSaveConnectorService, DynamicFormGroupComponent, V7FormGroup} from '@profect/v7-formbuilder-lib';
import * as _ from 'lodash';
import { EmployeeService } from '../employee.service';
import { Employee} from '../employee';
import { AlertService } from '../../../services/alert/alert.service';
import * as uuid from 'uuid';
import { EmployeeFormSaveConnectorState } from './employee-form-save-connector.state'

@Injectable()
export class EmployeeFormSaveConnectorService extends FormSaveConnectorService {

  constructor(
    cms: ConnectorManagerService,
    classService: ClassService,
    injector: Injector,
    notificationService: NotificationService,
    private employeeService: EmployeeService,
    private alertService: AlertService) {
      super(cms, classService, injector, notificationService);
  }

  public isUndoable(): boolean {
      return false;
  }

  public async doAction(connectorState: EmployeeFormSaveConnectorState, doData: any) {
    const targetComp: DynamicFormGroupComponent = this.cms.getRegistered(connectorState.target) as DynamicFormGroupComponent;
    let formgroup: V7FormGroup = <V7FormGroup> targetComp.associatedCtrl;
    let employeeID = formgroup.value.id;
    let isTeamleader = formgroup.value.isTeamLeader;
    console.debug("executing employee save form connector for employee: " + employeeID + ", isTeamleader: " + isTeamleader);
    console.debug("formgroup", formgroup);
    let employees = await this.employeeService.getAll();
    console.debug("fetched employees:", employees);
    let maySave = true;
    if (!isTeamleader) {
      let employeesWithTeamleaderReference = [];
      for (let employee of <Employee[]> employees) {
        if (employee.id !== employeeID && employee.teamLeader != null && employee.teamLeader.id === employeeID) {
          employeesWithTeamleaderReference.push(employee.shorthand);
          maySave = false;
        }
      }
      if (!maySave) {
        console.warn("can not uncheck teamleader - teamleader reference error. matched ID: " + employeeID)
        let employeesWithTeamleaderReference_stringified = "";
        if (employeesWithTeamleaderReference.length === 1) employeesWithTeamleaderReference_stringified = "another employee (" + employeesWithTeamleaderReference[0] + ")";
        else employeesWithTeamleaderReference_stringified += "other employees (" + employeesWithTeamleaderReference[0] + " [...])";
        this.alertService.alert(
          "Save Failed",
          "Can not deselect 'Is team leader' while this employee is still referenced as the team leader of " + employeesWithTeamleaderReference_stringified + ". You need to change the team leader of these employees first.",
          undefined,
          "small"
        );
      }
    }

    if (maySave) {
      return super.doAction(connectorState, doData);
    }

  }

}
