import {V7ServiceLink, TargetConnectorState} from '@profect/v7-core-lib';
import {FormSaveConnectorState} from '@profect/v7-formbuilder-lib';
import {EmployeeFormSaveConnectorService} from './employee-form-save-connector.service';

@V7ServiceLink(EmployeeFormSaveConnectorService)
export class EmployeeFormSaveConnectorState extends FormSaveConnectorState {
    public readonly __v7identifier: string = 'v7-employee-form-save-connector';
}
