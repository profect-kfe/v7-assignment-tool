import {V7ServiceLink, TargetConnectorState} from '@profect/v7-core-lib';
import {EmployeeTeamleaderConnectorService} from './employee-teamleader-connector.service';

@V7ServiceLink(EmployeeTeamleaderConnectorService)
export class EmployeeTeamleaderConnectorState extends TargetConnectorState {
    public readonly __v7identifier: string = 'v7-employee-teamleader-connector';
}
