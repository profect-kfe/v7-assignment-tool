import { Injectable } from '@angular/core';
import { V7ServiceLink } from '@profect/v7-core-lib';
import { AbstractAsyncValidatorState } from '@profect/v7-formbuilder-lib';
import { EmployeeValidator } from './employee-validator';

@Injectable()
@V7ServiceLink(EmployeeValidator)
export class EmployeeValidatorState extends AbstractAsyncValidatorState {

  public readonly __v7identifier: string = 'v7-employee-validator';

}
