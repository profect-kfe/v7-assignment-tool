import { Injectable } from '@angular/core';
import {  ValidationErrors } from '@angular/forms';
import * as _ from 'lodash';
import { EmployeeService } from '../employee.service';
import { AsyncValidator } from '../../validator/async-validator';
import { FormValidationErrors } from '../../validator/form-validation-errors';

@Injectable()
export class EmployeeValidator extends AsyncValidator {

  elementNames = [
    "staffNumber",
    "firstName",
    "lastName",
    "location",
    "employeeCtrl",
    "contractType",
    "weeklyHours",
    "retirementDate",
    "comment",
    "email",
    "shorthand",
    "isTeamleader"
  ];

  constructor(
    protected employeeService: EmployeeService
  ) {
    super();
  }

  async validate(): Promise<FormValidationErrors> {
    let errors = new FormValidationErrors();
    let staffNumbers = await this.employeeService.getStaffNumbers();
    if (staffNumbers.includes(this.controls["staffNumber"].value)) {
      let duplicateStaffNumberError = {duplicateStaffNumberError: "Cannot use duplicate staff number"};
      errors.addControlError("staffNumber", duplicateStaffNumberError);
      errors.addFormError(duplicateStaffNumberError);
    }
    return Promise.resolve(errors);
  }
}
