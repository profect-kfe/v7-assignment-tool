import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { HierarchyDefinitionService, ApiService, ClassService, LocalDynamicObjectsService } from '@profect/v7-core-lib';
import { EmployeeHierarchyDefinition } from './employee-hierarchy-definition.state';


@Injectable()
export class EmployeeHierarchyDefinitionService extends HierarchyDefinitionService<EmployeeHierarchyDefinition> {

    public readonly CONTROLLERPATH: string = "employeehierarchydefinition";

    constructor(protected injector: Injector,
        protected http: HttpClient,
        protected apiService: ApiService,
        protected classService: ClassService,
        protected localObjSrv: LocalDynamicObjectsService) {
        super(injector, http, apiService, classService, localObjSrv);
    }
}

