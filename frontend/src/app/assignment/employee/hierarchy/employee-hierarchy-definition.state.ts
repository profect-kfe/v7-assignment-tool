
import { Injectable } from "@angular/core";
import { V7ServiceLink, HierarchyDefinition, MetadataContainer } from '@profect/v7-core-lib';
import { EmployeeHierarchyDefinitionService } from './employee-hierarchy-definition.service';

@Injectable()
@V7ServiceLink(EmployeeHierarchyDefinitionService)
export class EmployeeHierarchyDefinition extends HierarchyDefinition {

    public readonly __v7identifier: string = 'v7-employee-hierarchy-definition';

    public readonly CONTROLLERPATH: string = "employeehierarchydefinition";

    filter: MetadataContainer;
    withEmptyFolders: boolean;
    operationTypes: string[];
}
