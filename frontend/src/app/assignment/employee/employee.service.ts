import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Employee } from './employee';
import { AbstractGUIService, ApiService, ClassService, LocalDynamicObjectsService, MetadataContainer } from '@profect/v7-core-lib';
import moment from 'moment-es6';

@Injectable()
export class EmployeeService extends AbstractGUIService<Employee> {

  public readonly CONTROLLERPATH: string = "employee";

  constructor(protected injector: Injector,
    protected http: HttpClient,
    protected apiService: ApiService,
    protected classService: ClassService,
    protected localObjSrv: LocalDynamicObjectsService) {
    super(injector, http, apiService, classService, localObjSrv);
  }

  /**
   * Saves the given {@link Employee} and returns the saved state,
   * possibly containing a server generated id.
   *
   *  @param state The {@link Employee} that should be saved
   *  @returns A {@link Promise} returning the saved {@link Employee}
   */
  public async save(entity: Employee): Promise<Employee> {
    entity.caption = entity.lastName + ", " + entity.firstName;
    entity.retirementDate = moment(entity.retirementDate).startOf('day').hours(12).toDate();
    return super.save(entity);
  }

  public async getStaffNumbers(): Promise<Array<string>> {
    let url: string = this.getControllerPath() + "/getstaffnumbers";
    let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    let response: any = await this.http.get(url, { headers: httpHeaders }).toPromise();
    return this.classService.deepCastToTypeScript(response);
  }
}
