import { Injectable } from '@angular/core';
import { V7ServiceLink } from '@profect/v7-core-lib';
import { AbstractAsyncValidatorState } from '@profect/v7-formbuilder-lib';
import { AssignmentValidator } from './assignment-validator';

@Injectable()
@V7ServiceLink(AssignmentValidator)
export class AssignmentValidatorState extends AbstractAsyncValidatorState {

  public readonly __v7identifier: string = 'v7-assignment-validator';

}
