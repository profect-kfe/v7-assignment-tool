import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ProjectService } from '../../project/project.service';
import { EmployeeService } from '../../employee/employee.service';
import { AsyncValidator } from '../../validator/async-validator';
import { FormValidationErrors } from '../../validator/form-validation-errors';

@Injectable()
export class AssignmentValidator extends AsyncValidator {
  elementNames = [
    "employee",
    "project",
    "startDate",
    "endDate",
    "weeklyHours",
    "mondayHours",
    "tuesdayHours",
    "wednesdayHours",
    "thursdayHours",
    "fridayHours"
  ]
  constructor(
    protected employeeService: EmployeeService,
    protected projectService: ProjectService
  ) {
    super();
  }

  async validate(): Promise<FormValidationErrors> {
    let errors = new FormValidationErrors();
    let _assignmentStartDate = this.controls["startDate"].value;
    let _assignmentEndDate = this.controls["endDate"].value;
    let assignmentStartDate = moment(_assignmentStartDate);
    let assignmentEndDate = moment(_assignmentEndDate);
    if (assignmentEndDate.diff(assignmentStartDate, "days") < 0) {
      let assignmentDateError = {assignmentDateError: "Start Date must precede End Date"};
      errors.addControlError("startDate", assignmentDateError);
      errors.addControlError("endDate", assignmentDateError);
      errors.addFormError(assignmentDateError);
    }

    let weeklyHoursErrors = {};
    if (this.controls["weeklyHours"].value !==
      this.controls["mondayHours"].value +
      this.controls["tuesdayHours"].value +
      this.controls["wednesdayHours"].value +
      this.controls["thursdayHours"].value +
      this.controls["fridayHours"].value) {
        weeklyHoursErrors["WeeklyHoursSumError"] = "Weekly hours must sum up correctly.";
    }

    let employee;
    if (this.controls["employee"].value != null) {
      await this.employeeService.get(<number><unknown> this.controls["employee"].value.id).then(_employee => {
        employee = _employee;
      });
    }

    let project;
    if (this.controls["project"].value != null) {
      await this.projectService.get(<number><unknown> this.controls["project"].value.id).then(_project => {
        project = _project;
      });
    }

    if (employee) {
      if (this.controls["weeklyHours"].value > this.controls["employee"]["weeklyHours"]) {
        weeklyHoursErrors["WeeklyHoursEmployeeHoursError"] = "Weekly hours must not exceed the employee's weekly hours.";
      }
    }

    if (Object.keys(weeklyHoursErrors).length > 0) {
      errors.addControlError("weeklyHours", weeklyHoursErrors);
      errors.addFormError(weeklyHoursErrors);
    }

    if (project) {
      let projectStartDate = moment(project.startDate);
      let projectEndDate = moment(project.endDate);
      let startOfOperation = moment(this.controls["startDate"].value);
      let endOfOperation = moment(this.controls["endDate"].value);
      if (startOfOperation.diff(projectStartDate, 'days') < 0) {
        let opStartError = {OpStartError: 'Start of operation must not precede project start date (' + moment(project.startDate).format("DD.MM.YYYY") + ').'};
        errors.addControlError("startDate", opStartError);
        errors.addFormError(opStartError);
      }
      if (projectEndDate.diff(endOfOperation, 'days') < 0) {
        let opEndError = {OpEndError: 'End of operation must not exceed project end date(' + moment(project.endDate).format("DD.MM.YYYY") + ').'};
        errors.addControlError("endDate", opEndError);
        errors.addFormError(opEndError);
      }
    }
    return Promise.resolve(errors);
  }

}
