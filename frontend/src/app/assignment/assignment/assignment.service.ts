import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Assignment } from './assignment';
import { AbstractGUIService, ApiService, ClassService, LocalDynamicObjectsService } from '@profect/v7-core-lib';
import { ProjectService } from '../project/project.service';
import { EmployeeService } from '../employee/employee.service';
import moment from 'moment-es6';

@Injectable()
export class AssignmentService extends AbstractGUIService<Assignment> {

  public readonly CONTROLLERPATH: string = "assignment";

  constructor(protected injector: Injector,
    protected http: HttpClient,
    protected apiService: ApiService,
    protected classService: ClassService,
    protected localObjSrv: LocalDynamicObjectsService,
    protected employeeService: EmployeeService,
    protected projectService: ProjectService) {
    super(injector, http, apiService, classService, localObjSrv);
  }

  /**
   * Saves the given {@link Assignment} and returns the saved state,
   * possibly containing a server generated id.
   *
   *  @param state The {@link Assignment} that should be saved
   *  @returns A {@link Promise} returning the saved {@link Assignment}
   */
  public async save(entity: Assignment): Promise<Assignment> {
    entity.caption = entity.project.projectKey;
    entity.startDate = moment(entity.startDate).startOf('day').hours(12).toDate();
    entity.endDate = moment(entity.endDate).startOf('day').hours(12).toDate();
    return super.save(entity);
  }
}
