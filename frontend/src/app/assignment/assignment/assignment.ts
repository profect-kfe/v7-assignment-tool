import { V7ServiceLink, AbstractGUIState } from '@profect/v7-core-lib';
import { AssignmentService } from './assignment.service';
import { Employee } from '../employee/employee';
import { Project } from '../project/project';

@V7ServiceLink(AssignmentService)
export class Assignment extends AbstractGUIState {

    public readonly __v7identifier: string = 'v7-assignment-assignment';

    employee: Employee;

    project: Project;

    startDate: Date;

    endDate: Date;

    dailyRate: number;

    weeklyHours: number;

    mondayHours: number;

    tuesdayHours: number;

    wednesdayHours: number;

    thursdayHours: number;

    fridayHours: number;
}