import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { ApiService, ClassService, LocalDynamicObjectsService, MetadataContainer } from '@profect/v7-core-lib';
import moment from 'moment-es6';

@Injectable()
export class GraphService {

  public readonly CONTROLLERPATH: string = "graph";

  constructor(protected injector: Injector,
    protected http: HttpClient,
    protected apiService: ApiService) {
  }

  public async updateAbsences(): Promise<Boolean> {
    let url: string = this.apiService.getServerUrl() + this.CONTROLLERPATH + "/updateabsences";
    let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    let response: any = await this.http.get(url, { headers: httpHeaders }).toPromise();
    return response;
  }
}
