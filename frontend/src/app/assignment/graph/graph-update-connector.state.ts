import { V7ServiceLink, TargetConnectorState } from '@profect/v7-core-lib';
import { GraphUpdateConnectorService } from './graph-update-connector.service';

@V7ServiceLink(GraphUpdateConnectorService)
export class GraphUpdateConnectorState extends TargetConnectorState {

    public readonly __v7identifier: string = 'v7-graph-update-connector';
}
