import { Injectable } from '@angular/core';
import { TargetConnectorService, IdentifierConnectorSource, IdentifierConnectorTarget, TableViewComponent, ConnectorManagerService, ButtonComponent, ButtonState } from '@profect/v7-core-lib';
import { GraphUpdateConnectorState } from './graph-update-connector.state';
import { GraphService } from './graph.service';

@Injectable()
export class GraphUpdateConnectorService extends TargetConnectorService {

  constructor(protected cms: ConnectorManagerService,
    protected graphService: GraphService) {
    super(cms);
  }

    public doAction(connectorState: GraphUpdateConnectorState, doData: string): boolean {
        if (super.doAction(connectorState, doData)) {
            let source: ButtonComponent<ButtonState> = <ButtonComponent<ButtonState>>this.cms.getRegistered(connectorState.source);
            source.setEnabled(false);
            this.graphService.updateAbsences().then(success => {
                if (success) {
                    let target: TableViewComponent = <TableViewComponent>this.cms.getRegistered(connectorState.target);
                    target.refresh();
                }
                let source: ButtonComponent<ButtonState> = <ButtonComponent<ButtonState>>this.cms.getRegistered(connectorState.source);
                source.setEnabled(true);
            });
            return true;
        }
        return false;
    }

}
