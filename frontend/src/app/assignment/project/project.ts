import { V7ServiceLink, AbstractGUIState } from '@profect/v7-core-lib';
import { ProjectService } from './project.service';
import { Customer } from '../customer/customer';
import { Employee } from '../employee/employee';

@V7ServiceLink(ProjectService)
export class Project extends AbstractGUIState {

    public readonly __v7identifier: string = 'v7-assignment-project';

    projectName: string;

    customerProjectName: string;

    invoiceRecipient: Customer;

    projectKey: string;

    benefitRecipient: Customer;

    projectType: string;

    startDate: Date;

    endDate: Date;

    projectLeader: Employee;

    contractType: string;

    sales: number;

    note: string;
}
