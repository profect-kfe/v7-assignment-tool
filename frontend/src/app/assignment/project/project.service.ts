import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Project } from './project';
import { AbstractGUIService, ApiService, ClassService, LocalDynamicObjectsService } from '@profect/v7-core-lib';
import { CustomerService } from '../customer/customer.service';
import { EmployeeService } from '../employee/employee.service';
import moment from 'moment-es6';

@Injectable()
export class ProjectService extends AbstractGUIService<Project> {

  public readonly CONTROLLERPATH: string = "project";

  constructor(protected injector: Injector,
    protected http: HttpClient,
    protected apiService: ApiService,
    protected classService: ClassService,
    protected localObjSrv: LocalDynamicObjectsService,
    protected employeeService: EmployeeService,
    protected customerService: CustomerService) {
    super(injector, http, apiService, classService, localObjSrv);
  }

  /**
   * Saves the given {@link Project} and returns the saved state,
   * possibly containing a server generated id.
   *
   *  @param state The {@link Project} that should be saved
   *  @returns A {@link Promise} returning the saved {@link Project}
   */
  public async save(entity: Project): Promise<Project> {
    entity.caption = entity.projectKey;
    entity.startDate = moment(entity.startDate).startOf('day').hours(12).toDate();
    entity.endDate = moment(entity.endDate).startOf('day').hours(12).toDate();
    return super.save(entity);
  }
}
