import { Injectable } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ProjectService } from '../../project/project.service';
import { EmployeeService } from '../../employee/employee.service';
import { AsyncValidator } from '../../validator/async-validator';
import { FormValidationErrors } from '../../validator/form-validation-errors';

@Injectable()
export class ProjectValidator extends AsyncValidator {

  elementNames = [
        "benefitRecipient",
        "projectName",
        "customerProjectName",
        "projectType",
        "startDate",
        "endDate",
        "projectLeader",
        "contractType",
        "sales",
        "notes"
    ];

  constructor(
    protected employeeService: EmployeeService,
    protected projectService: ProjectService
  ) {
    super();
  }

  async validate(): Promise<FormValidationErrors> {
    let errors = new FormValidationErrors();
    let _projectStartDate = this.controls["startDate"].value;
    let _projectEndDate = this.controls["endDate"].value;
    let projectStartDate = moment(_projectStartDate);
    let projectEndDate = moment(_projectEndDate);
    if (projectEndDate.diff(projectStartDate, "days") < 0) {
      let projectDateError = {projectDateError: "Start Date must precede End Date"};
      errors.addControlError("startDate", projectDateError);
      errors.addControlError("endDate", projectDateError);
      errors.addFormError(projectDateError);
    }
    return Promise.resolve(errors);
  }

}
