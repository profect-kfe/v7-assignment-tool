import { Injectable } from '@angular/core';
import { V7ServiceLink } from '@profect/v7-core-lib';
import { AbstractAsyncValidatorState } from '@profect/v7-formbuilder-lib';
import { ProjectValidator } from './project-validator';

@Injectable()
@V7ServiceLink(ProjectValidator)
export class ProjectValidatorState extends AbstractAsyncValidatorState {

  public readonly __v7identifier: string = 'v7-project-validator';

}
