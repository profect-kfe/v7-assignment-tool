import {Injectable, Injector} from '@angular/core';
import {ConnectorManagerService, TargetConnectorService, TargetConnectorState, NotificationService, ClassService} from '@profect/v7-core-lib';
import {FormSaveConnectorService, DynamicFormGroupComponent, V7FormGroup} from '@profect/v7-formbuilder-lib';
import * as _ from 'lodash';
import { ProjectService } from '../project.service';
import { Project} from '../project';
import { AlertService } from '../../../services/alert/alert.service';
import * as uuid from 'uuid';
import { ProjectFormSaveConnectorState } from './project-form-save-connector.state'
import { EmployeeService } from '../../employee/employee.service';
import { AssignmentService } from '../../assignment/assignment.service';
import { Assignment } from '../../assignment/assignment';
import moment from 'moment-es6';

@Injectable()
export class ProjectFormSaveConnectorService extends FormSaveConnectorService {

  constructor(
    cms: ConnectorManagerService,
    classService: ClassService,
    injector: Injector,
    notificationService: NotificationService,
    private employeeService: EmployeeService,
    private alertService: AlertService,
    private assignmentService: AssignmentService) {
      super(cms, classService, injector, notificationService);
  }

  public async doAction(connectorState: ProjectFormSaveConnectorState, doData: any) {
    console.debug("executing project save form connector");
    const targetComp: DynamicFormGroupComponent = this.cms.getRegistered(connectorState.target) as DynamicFormGroupComponent;
    let formgroup: V7FormGroup = <V7FormGroup> targetComp.associatedCtrl;
    let projectStartDate = formgroup.value.startDate;
    let projectEndDate = formgroup.value.endDate;
    let projectID = formgroup.getValue()["id"];
    let doActionResult = await super.doAction(connectorState, doData);
    let assignments = await this.assignmentService.getAll();
    let adjustedAssignments: Assignment[] = [];
    for (let assignment of assignments) {
      if (assignment.project.id === projectID) {
        let assignmentStartDate = assignment.startDate;
        let assignmentEndDate = assignment.endDate;
        let adjusted = false;
        if (projectStartDate) {
          if (!assignmentStartDate
            || +new Date(assignmentStartDate) < +new Date(projectStartDate)
            || +new Date(assignmentStartDate) > +new Date(projectEndDate)
          ) {
            assignment.startDate = projectStartDate;
            adjusted = true;
          }
        }
        if (projectEndDate) {
          if (!assignmentEndDate
            || +new Date(assignmentEndDate) > +new Date(projectEndDate)
            || +new Date(assignmentEndDate) < +new Date(projectStartDate)
          ) {
            assignment.endDate = projectEndDate;
            adjusted = true;
          }
        }
        if (adjusted) {
          adjustedAssignments.push(assignment);
        }
      }
    }
    if (adjustedAssignments.length > 0) {
      let alertText = "<div>The following assignment dates have been adjusted to fit into the new project interval:</div><br><ul>"
      for (let assignment of adjustedAssignments) {
        alertText += "<li>Projekt: " + assignment.project.projectName + ", Employee: " + assignment.employee.caption + ", New Assignment Interval: " + moment(assignment.startDate).format('YYYY-MM-DD') + " - " + moment(assignment.endDate).format('YYYY-MM-DD') + "</li>";
        console.debug("saving adjusted assignment: ", assignment);
        this.assignmentService.save(assignment);
      }
      alertText += "</ul>"
      this.alertService.alert("Assignments adjusted", alertText, undefined , "large");
    }
    console.debug("adjusted assignments:", adjustedAssignments);
    return doActionResult;
  }

}
