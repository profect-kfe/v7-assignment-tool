import {V7ServiceLink, TargetConnectorState} from '@profect/v7-core-lib';
import {FormSaveConnectorState} from '@profect/v7-formbuilder-lib';
import {ProjectFormSaveConnectorService} from './project-form-save-connector.service';

@V7ServiceLink(ProjectFormSaveConnectorService)
export class ProjectFormSaveConnectorState extends FormSaveConnectorState {
    public readonly __v7identifier: string = 'v7-project-form-save-connector';
}
