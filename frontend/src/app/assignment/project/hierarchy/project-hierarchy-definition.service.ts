import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { HierarchyDefinitionService, ApiService, ClassService, LocalDynamicObjectsService } from '@profect/v7-core-lib';
import { ProjectHierarchyDefinition } from './project-hierarchy-definition.state';


@Injectable()
export class ProjectHierarchyDefinitionService extends HierarchyDefinitionService<ProjectHierarchyDefinition> {

    public readonly CONTROLLERPATH: string = "projecthierarchydefinition";

    constructor(protected injector: Injector,
        protected http: HttpClient,
        protected apiService: ApiService,
        protected classService: ClassService,
        protected localObjSrv: LocalDynamicObjectsService) {
        super(injector, http, apiService, classService, localObjSrv);
    }
}

