
import { Injectable } from "@angular/core";
import { V7ServiceLink, HierarchyDefinition, MetadataContainer } from '@profect/v7-core-lib';
import { ProjectHierarchyDefinitionService } from './project-hierarchy-definition.service';

@Injectable()
@V7ServiceLink(ProjectHierarchyDefinitionService)
export class ProjectHierarchyDefinition extends HierarchyDefinition {

    public readonly __v7identifier: string = 'v7-project-hierarchy-definition';

    public readonly CONTROLLERPATH: string = "projecthierarchydefinition";

    filter: MetadataContainer;
    withEmptyFolders: boolean;
    operationTypes: string[];
}
