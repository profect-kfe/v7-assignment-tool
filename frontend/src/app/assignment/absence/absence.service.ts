import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Absence } from './absence';
import { AbstractGUIService, ApiService, ClassService, LocalDynamicObjectsService, MetadataContainer } from '@profect/v7-core-lib';
import { EmployeeService } from '../employee/employee.service';
import moment from 'moment-es6';

@Injectable()
export class AbsenceService extends AbstractGUIService<Absence> {

  public readonly CONTROLLERPATH: string = "absence";

  constructor(protected injector: Injector,
    protected http: HttpClient,
    protected apiService: ApiService,
    protected classService: ClassService,
    protected localObjSrv: LocalDynamicObjectsService,
    protected employeeService: EmployeeService) {
    super(injector, http, apiService, classService, localObjSrv);
  }

  /**
   * Saves the given {@link Absence} and returns the saved state,
   * possibly containing a server generated id.
   *
   *  @param state The {@link Absence} that should be saved
   *  @returns A {@link Promise} returning the saved {@link Absence}
   */
  public async save(entity: Absence): Promise<Absence> {
    entity.caption = entity.reason;
    entity.startDate = moment(entity.startDate).startOf('day').hours(12).toDate();
    entity.endDate = moment(entity.endDate).startOf('day').hours(12).toDate();
    return super.save(entity);
  }
}
