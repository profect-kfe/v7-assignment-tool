import { V7ServiceLink, AbstractGUIState } from '@profect/v7-core-lib';
import { AbsenceService } from './absence.service';
import { Employee } from '../employee/employee';

@V7ServiceLink(AbsenceService)
export class Absence extends AbstractGUIState {

    public readonly __v7identifier: string = 'v7-assignment-absence';

    employee: Employee;

    startDate: Date;

    endDate: Date;

    reason: string;
}