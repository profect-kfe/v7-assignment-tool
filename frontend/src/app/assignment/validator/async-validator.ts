import { AbstractAsyncValidator } from '@profect/v7-formbuilder-lib';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { AssignmentValidatorState } from '../assignment/validator/assignment-validator.state';
import { AsyncValidatorFn, AbstractControl, ValidationErrors, FormGroup } from '@angular/forms';
import { timer } from 'rxjs';
import { switchMap, first } from 'rxjs/operators';
import { FormValidationErrors } from './form-validation-errors';

@Injectable()
export class AsyncValidator implements AbstractAsyncValidator {

  elementNames: string[]; //override
  formGroup: FormGroup;
  controls: AbstractControl[];
  DEBOUNCE = 250 //set debounce time or null

  //override
  validate(): Promise<FormValidationErrors> {
    return Promise.resolve(new FormValidationErrors());
  }

  getAsyncValidator(state: AssignmentValidatorState): AsyncValidatorFn {
    console.debug(">> ASYNC VALIDATOR: validating form...");
    if (this.DEBOUNCE) {
      return (ctrl) => timer(this.DEBOUNCE).pipe(switchMap(() => validatorFn.bind(this)(ctrl)), first());
    } else {
      return validatorFn.bind(this);
    }
    async function validatorFn(formgroup: AbstractControl): Promise<ValidationErrors> {
      if (!formgroup) {
          throw ">> ASYNC VALIDATOR: no assignmentControl set";
      }
      this.formgroup = <FormGroup> formgroup;
      this.controls = (<FormGroup> formgroup).controls;
      //setTimeout(() => console.debug(this.formgroup), 10000);
      for (let elementName of this.elementNames) {
        if (this.controls[elementName] == null) {
          return Promise.resolve({});
        }
      }
      let errors: FormValidationErrors = await this.validate();
      console.debug(">> ASYNC VALIDATOR: validation result:", errors);
      //set control errors
      for (let elementName of this.elementNames) {
        if (errors.controlErrors.hasOwnProperty(elementName)) {
          this.setControlErrors(this.controls[elementName], errors.controlErrors[elementName]);
        } else {
          this.resetControlErrors(this.controls[elementName]);
        }
      }
      //return form errors
      return Promise.resolve(errors.formErrors);
    }
  }

  setControlErrors(ctrl: AbstractControl, _errors: ValidationErrors) {
    let errors = {};
    let required = ctrl.errors?.required;
    Object.assign(errors, _errors);
    if (required != null) {
      errors["required"] = required;
    }
    ctrl.setErrors(errors);
  }

  resetControlErrors(ctrl: AbstractControl) {
    let required = ctrl.errors?.required;
    if (required != null) {
      ctrl.setErrors({required: required});
    } else {
      ctrl.setErrors(null);
    }
  }

}
