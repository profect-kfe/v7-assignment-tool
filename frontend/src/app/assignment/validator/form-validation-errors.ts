import { ValidationErrors } from '@angular/forms';

export class FormValidationErrors {

  formErrors: ValidationErrors = {};
  controlErrors: { [elementName: string]: ValidationErrors; } = {}

  addFormError(error) {
    Object.assign(this.formErrors, error);
  }

  addControlError(elementName, error) {
    if (!this.controlErrors.hasOwnProperty(elementName)) {
      this.controlErrors[elementName] = {};
    }
    Object.assign(this.controlErrors[elementName], error);
  }
}
