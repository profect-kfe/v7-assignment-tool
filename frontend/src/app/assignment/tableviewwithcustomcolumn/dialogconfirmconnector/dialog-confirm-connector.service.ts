import { Injectable } from '@angular/core';
import { DialogConfirmConnectorState } from './dialog-confirm-connector.state';
import { TargetConnectorService, IdentifierConnectorSource, IdentifierConnectorTarget, TableViewComponent } from '@profect/v7-core-lib';
import { TableViewWithCustomColumnComponent } from '../tableview-with-custom-column.component';

@Injectable()
export class DialogConfirmConnectorService extends TargetConnectorService {

    public getDoActionData(connectorState: DialogConfirmConnectorState): string {
        if (this.cms.hasRegistered(connectorState.source)) {
            let source: IdentifierConnectorSource = <IdentifierConnectorSource>this.cms.getRegistered(connectorState.source);
            if (source.getIdentifier) {
                return source.getIdentifier();
            }
        }
        return null;
    }

    public getUndoActionData(connectorState: DialogConfirmConnectorState): string {
        if (this.cms.hasRegistered(connectorState.target)) {
            let target: IdentifierConnectorTarget = <IdentifierConnectorTarget>this.cms.getRegistered(connectorState.target);
            if (target.getIdentifier) {
                return target.getIdentifier();
            }
        }
        return null;
    }

    public doAction(connectorState: DialogConfirmConnectorState, doData: string): boolean {
        if (super.doAction(connectorState, doData)) {
            let target: TableViewComponent = <TableViewComponent>this.cms.getRegistered(connectorState.target);
            if (target instanceof TableViewWithCustomColumnComponent) {
                if (target.delete) {
                    target.delete();
                }
            }

            return true;
        }
        return false;
    }

}
