import { DialogConfirmConnectorService } from './dialog-confirm-connector.service';
import { V7ServiceLink, TargetConnectorState, Field } from '@profect/v7-core-lib';

@V7ServiceLink(DialogConfirmConnectorService)
export class DialogConfirmConnectorState extends TargetConnectorState {

    public readonly __v7identifier: string = 'v7-dialog-confirm-connector';
    identifierField: Field;
}
