import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'v7-table-view-pagination-component',
    templateUrl: './proj-tableview-pagination.html'
})
export class ProjectTableViewPaginationComponent {

    @Input()
    lineCount: number;

    @Input()
    limit: number;

    @Input()
    offset: number;

    @Output()
    pageChange: EventEmitter<number> = new EventEmitter();

    /**
     * Emits the firePaginationChange-Event
     */
    firePaginationChange(offset: number): void {
        this.pageChange.emit(offset);
    }

    /**
     * Returns the amount of required pages to display the given data, according
     * to the given limit.
     * return number
     */
    getMaxPages(): number {
        return Math.ceil(this.lineCount / this.limit);
    }

    /**
     * Calculates the correct offset to be fired.
     *
     */
    firePagination(page: number): void {
        this.offset = page * this.limit - this.limit;
        this.firePaginationChange(this.offset);
    }

    /**
     * Adding the movement to the current offset. If the resulting offset is
     * outside of the possible boundaries it is capped to the boundary.
     */
    fireNavPagination(movement: number): void {
        this.offset = this.offset + (this.limit * movement);
        if (this.offset < 0) {
            this.offset = 0;
            return;
        }
        if (this.offset > this.getMaxPages() * this.limit - this.limit) {
            this.offset = this.getMaxPages() * this.limit - this.limit;
            return;
        }
        this.firePaginationChange(this.offset);
    }

    /**
     * Create a number array filled with the number 1 to this.getMaxPages().
     * This is necessary to iterate through to create the pagination items
     *
     * return array<number>
     */
    getSmallPageArray(): Array<number> {
        return Array(this.getMaxPages()).fill(1).map((x, i) => i + 1); // +1 so the first entry is 1
    }

    /**
     * Creat a number array with the length of 5, filled with the pages to
     * display. If the given index is 3 or less it will be capped at 3. If the
     * given index is higher then this.getMaxPages()-3 it is capped to
     * this.getMaxPages()-3.
     *
     * return array<number>
     */
    getLargePageArray(index: number): Array<number> {
        if (index < 4) {
            index = 4;
        } else if (index > this.getMaxPages() - 3) {
            index = this.getMaxPages() - 3;
        }
        return Array(5).fill(1).map((x, i) => i + index - 2);
    }
}

