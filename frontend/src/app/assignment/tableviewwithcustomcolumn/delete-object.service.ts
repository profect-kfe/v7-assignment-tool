import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { TableViewWithCustomColumnState } from './tableview-with-custom-column.state';
import { DataAccessService, TableViewData, ApiService, ClassService, LocalDynamicObjectsService } from '@profect/v7-core-lib';

@Injectable({
  providedIn: 'root',
})
export class DeleteObjectService extends DataAccessService<TableViewWithCustomColumnState, TableViewData> {

  public readonly CONTROLLERPATH: string = "deleteservice";

  constructor(protected injector: Injector,
    protected http: HttpClient,
    protected apiService: ApiService,
    protected classService: ClassService,
    protected localObjSrv: LocalDynamicObjectsService) {
    super(injector, http, apiService, classService, localObjSrv);
  }


  public async deleteObject(identifier: string, objectType: string): Promise<string> {
    let url: string = this.getControllerPath() + "/deleteobject";
    //let params = new HttpParams().set("aMitarbeiter", aMitarbeiter).set("brid", brid);
    let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(url, { headers: httpHeaders, params: { "identifier": identifier, "objectType": objectType }, responseType: 'text' }).toPromise();
  }
}
