import { Inject, Component, HostBinding, Injector, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { TableViewComponent, ComponentInstance, ConnectorManagerService, ClassService, TableViewService, WebSocketService, TableViewHeaderColumnData, TableViewData, TableViewCellData, FunctionalUnitInjector, FunctionalUnit } from '@profect/v7-core-lib';
import { DeleteObjectService } from './delete-object.service';

@Component({
  selector: 'v7-tableview-cuco-component',
  templateUrl: './tableview-with-custom-column.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableViewWithCustomColumnComponent extends TableViewComponent {

  dialogMsg: string = '';

  @HostBinding('class') public get css() {
    return "";
  }

  constructor(@Inject(FunctionalUnitInjector) public funcUnit: FunctionalUnit,
    protected cms: ConnectorManagerService,
    protected injector: Injector,
    protected classService: ClassService,
    protected tableViewService: TableViewService,
    protected websocketService: WebSocketService,
    protected changeRef: ChangeDetectorRef,
    protected deleteService: DeleteObjectService,
  ) {
    super(funcUnit, cms, injector, classService, tableViewService, websocketService, changeRef);
    super.makeWebsocket();
  }

  public edit(rowNumber: number) {
    //TODO: select row when editing?
    if (this.data && rowNumber >= 0 && this.data.data.length > rowNumber && this.state.idField) {
      // const oneRow = new TableViewData();
      // oneRow.header = this.data.header;
      // oneRow.data = new Array<Array<TableViewCellData>>(this.data.data[rowNumber]);
      // this.selectedRow = oneRow;

      this.identifier = this.data.idRow[rowNumber].value;
      this.state.selectedRowIndex = rowNumber;
      this.cms.doAction(this.instance.id, "EDIT");
    } else {
      //this.selectedRow = null;
      this.identifier = null;
    }
  }

  public showDeleteDialog(rowNumber: number) {

    if (this.data && rowNumber >= 0 && this.data.data.length > rowNumber && this.state.idField) {

      console.log(this.data.header.findIndex(element => element.caption === 'caption'));
      let str: string = '';
      let str2: string = '';
      if (this.data.header.findIndex(element => element.caption === 'caption') > -1) {
        str = ' ' + this.data.data[rowNumber][this.data.header.findIndex(element => element.caption === 'caption')].value;
      } else if (this.data.header.findIndex(element => element.caption === 'First Name') > -1) {
        str = ' ' + this.data.data[rowNumber][this.data.header.findIndex(element => element.caption === 'First Name')].value +
          ' ' + this.data.data[rowNumber][this.data.header.findIndex(element => element.caption === 'Last Name')].value;
      } else if (this.data.header.findIndex(element => element.caption === 'Project Name') > -1) {
        str = ' ' + this.data.data[rowNumber][this.data.header.findIndex(element => element.caption === 'Project Name')].value;
      } else if (this.data.header.findIndex(element => element.caption === 'Customer Name') > -1) {
        str = ' ' + this.data.data[rowNumber][this.data.header.findIndex(element => element.caption === 'Customer Name')].value
          + ' - ' + this.data.data[rowNumber][this.data.header.findIndex(element => element.caption === 'Division')].value;
        str2 = ' This will also delete all projects related to this customer.';
      }

      this.dialogMsg = 'Permanently delete ' + str + ' ?' + str2;

      this.identifier = this.data.idRow[rowNumber].value;
      this.state.selectedRowIndex = rowNumber;
      this.cms.doAction(this.instance.id, 'SHOW_DELETE_REQUEST_DIALOG');
      this.cms.doAction(this.instance.id, 'PASS_DELETION_INFO_TO_DIALOG');
    } else {
      //this.selectedRow = null;
      this.identifier = null;
    }
  }

  public delete() {
    this.deleteService.deleteObject(this.identifier, this.state.caption).then(projects => {

      if (projects == null || projects === '') {
        // everything is fine and the employee was deleted
        this.refresh();
      } else {
        // employee could not be deleted because of existing project references
        this.dialogMsg = projects;
        this.cms.doAction(this.instance.id, 'SHOW_PROJECT_IDS_DIALOG');
        this.cms.doAction(this.instance.id, 'PASS_PROJECT_IDS_TO_DIALOG');
      }
      this.refresh();
    });
  }

  public getLabelMessage(): string {
    return this.dialogMsg;
  }

  public buildMatTableStringHeader(): string[] {
    if (this.data && this.data.header) {
      return super.buildMatTableStringHeader().concat(["edit", "delete"]);
    } else {
      return [];
    }
  }

  public buildEmptyDataSets(): TableViewCellData[][] {
    const emptyDataSets: TableViewCellData[][] = [];
    for (let i = 0; i < this.state.limit; i++) {
      const rowData: TableViewCellData[] = [];
      for (let j = 0; j < this.data.header.length; j++) {
        const cellData = new TableViewCellData();
        rowData.push(cellData);
      }
      emptyDataSets.push(rowData);
    }
    return emptyDataSets;
  }

}
