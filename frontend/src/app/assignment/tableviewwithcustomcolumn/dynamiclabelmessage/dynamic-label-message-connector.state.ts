import { V7ServiceLink, TargetConnectorState, Field } from '@profect/v7-core-lib';
import { DynamicLabelMessageConnectorService } from './dynamic-label-message-connector.service';

@V7ServiceLink(DynamicLabelMessageConnectorService)
export class DynamicLabelMessageConnectorState extends TargetConnectorState {

    public readonly __v7identifier: string = 'v7-dynamic-label-message-connector';
    identifierField: Field;
}
