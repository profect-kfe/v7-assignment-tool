import { Injectable } from '@angular/core';
import { TargetConnectorService, ConnectorManagerService, TableViewComponent, IdentifierConnectorTarget, LabelComponent } from '@profect/v7-core-lib';
import { DynamicLabelMessageConnectorState } from './dynamic-label-message-connector.state';
import { TableViewWithCustomColumnComponent } from '../tableview-with-custom-column.component';

@Injectable()
export class DynamicLabelMessageConnectorService extends TargetConnectorService {

    constructor(protected cms: ConnectorManagerService) {
            super(cms);
    }

    public getDoActionData(connectorState: DynamicLabelMessageConnectorState): string {
        if (this.cms.hasRegistered(connectorState.source)) {
            let source: TableViewComponent = <TableViewComponent>this.cms.getRegistered(connectorState.source);
            if (source instanceof TableViewWithCustomColumnComponent) {
                if (source.getLabelMessage) {
                    return source.getLabelMessage();
                }
            }
        }
        return null;
    }

    public getUndoActionData(connectorState: DynamicLabelMessageConnectorState): string {
        if (this.cms.hasRegistered(connectorState.target)) {
            let target: IdentifierConnectorTarget = <IdentifierConnectorTarget>this.cms.getRegistered(connectorState.target);
            if (target.getIdentifier) {
                return target.getIdentifier();
            }
        }
        return null;
    }

    public doAction(connectorState: DynamicLabelMessageConnectorState, doData: string): boolean {
        if (super.doAction(connectorState, doData)) {
            let label: LabelComponent = <LabelComponent>this.cms.getRegistered(connectorState.target);
            label.state.caption = doData;

            return true;
        }
        return false;
    }

}
