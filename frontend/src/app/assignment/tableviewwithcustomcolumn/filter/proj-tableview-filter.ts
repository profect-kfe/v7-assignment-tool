import { Field } from '@profect/v7-core-lib';


export class ProjectTableViewFilter {

    public readonly __v7identifier: string = 'v7-proj-tableview-filter';

    filterField: Field = null;
    filterValues: Array<string> = new Array<string>();

}
