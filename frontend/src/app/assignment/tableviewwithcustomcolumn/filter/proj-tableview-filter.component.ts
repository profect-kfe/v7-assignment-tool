import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { ProjectTableViewFilter } from './proj-tableview-filter';
import { Field, TableViewFilter, SearchFieldSelectionElementValues, SearchFieldSelectionElementService, SearchFieldSelectionElementState } from '@profect/v7-core-lib';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'v7-table-view-filter-component',
  templateUrl: './proj-tableview-filter.html'
})
export class ProjectTableViewFilterComponent implements OnInit {

  @HostBinding('class') public get css() {
    return 'tableview-filter-container';
  }

  @ViewChild('tableFilter') autocompleteInput: MatAutocomplete;

  @Input()
  tableViewFilters: ProjectTableViewFilter[] = [];

  @Input()
  selectedFields: Field[] = [];

  @Input()
  filterMinLength: number = 3;

  @Output()
  filterValueTrigger = new EventEmitter();

  _selectedTableViewFilter: TableViewFilter = null;

  searchFieldValue: string = '';

  searchValues: SearchFieldSelectionElementValues = null;

  selectedItems: any[] = [];

  @Input()
  set selectedTableViewFilter(fieldId: string) {
    if (fieldId) {
      this.searchFieldValue = '';
      this.searchValues = null;
      if (this._selectedTableViewFilter != null && this._selectedTableViewFilter.filterValues.length <= 0) {
        this.tableViewFilters.splice(this.tableViewFilters.indexOf(this._selectedTableViewFilter), 1);
      }
      if (this.isFieldFiltered(fieldId)) {
        this._selectedTableViewFilter = this.getTableViewFilter(fieldId);
        this.buildSelectedItems();
      } else {
        let tableViewFilter: TableViewFilter = new TableViewFilter();
        tableViewFilter.filterField = this.selectedFields.find(val => {
          if (val.getId().toString() == fieldId) {
            return val;
          }
        });

        this.tableViewFilters.push(tableViewFilter);
        this._selectedTableViewFilter = tableViewFilter;
      }
    } else {
      this._selectedTableViewFilter = null;
    }

  }

  constructor(protected service: SearchFieldSelectionElementService) {

  }

  ngOnInit() {
    this.buildSelectedItems();
  }

  async getSearchData() {
    let state: SearchFieldSelectionElementState = this.createSearchFieldState();

    await this.service.getDataForState(state).then(response => {
      this.searchValues = response;
    });
  }

  onInputChange(event) {
    this.searchFieldValue = event.target.value;
    if (this.filterMinLength >= 3 && this.searchFieldValue != '') {
      this.getSearchData();
    }
  }

  private createSearchFieldState(): SearchFieldSelectionElementState {
    let state: SearchFieldSelectionElementState = new SearchFieldSelectionElementState();
    state.searchLength = this.filterMinLength;
    state.inputValue = this.searchFieldValue;
    state.hierarchyDatabaseDefinition = null;
    state.field = this.getSelectedTableViewFilter().filterField;

    return state;
  }

  getSelectedTableViewFilter(): TableViewFilter {
    return this._selectedTableViewFilter;
  }

  buildSelectedItems() {
    if (this.getSelectedTableViewFilter() != null && this.getSelectedTableViewFilter().filterValues != null) {
      this.selectedItems = [];
      for (let i in this.getSelectedTableViewFilter().filterValues) {
        this.selectedItems.push({ id: i, caption: this.getSelectedTableViewFilter().filterValues[i] });
      }
    }
  }


  isFieldFiltered(fieldId: string): boolean {
    let isFieldFiltered: boolean = false;
    this.tableViewFilters.forEach(val => {
      if (val.filterField.getId().toString() == fieldId) {
        isFieldFiltered = true;
      }
    });
    return isFieldFiltered;
  }

  isSelectedTableViewFilter(fieldId: string) {
    return this._selectedTableViewFilter == this.getTableViewFilter(fieldId);
  }

  deselectTableViewFilter(): void {
    this.selectedTableViewFilter = null;
  }


  getTableViewFilter(fieldId: string): TableViewFilter {
    let filter: TableViewFilter = null;
    this.tableViewFilters.forEach(val => {
      if (val.filterField.id.toString() == fieldId) {
        filter = val;
      }
    })
    return filter;
  }

  addFilterValue(value: string) {
    this.searchFieldValue = value;
    if (value != '' && value != null && value != undefined && this.getSelectedTableViewFilter().filterValues.indexOf(value) == -1) {

      this.getSelectedTableViewFilter().filterValues.push(value);
      this.searchFieldValue = '';
      this.searchValues = null;
      this.buildSelectedItems();
      this.filterValueTrigger.emit();
    }
  }

  onEnterPressed(value: string, isMatOptionSelect: boolean) {
    if (this.autocompleteInput._keyManager.activeItem == null || isMatOptionSelect) {
      this.searchFieldValue = value;
      if (this.searchFieldValue && this.searchFieldValue.length >= 3 && this.searchFieldValue != '') {
        this.addFilterValue(value);
      }
    }

  }

  removeAllSelectedItems() {
    this.getSelectedTableViewFilter().filterValues = [];
    this.filterValueTrigger.emit();
  }

  removeSelectedItem(item: any) {
    this.getSelectedTableViewFilter().filterValues.splice(this.getSelectedTableViewFilter().filterValues.indexOf(item.caption), 1);
    this.filterValueTrigger.emit();
  }






}
