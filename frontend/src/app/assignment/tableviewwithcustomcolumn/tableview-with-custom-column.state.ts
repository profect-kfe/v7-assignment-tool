import { TableViewState } from '@profect/v7-core-lib';
import { Injectable } from '@angular/core';

@Injectable()
export class TableViewWithCustomColumnState extends TableViewState {
    public readonly __v7identifier: string = "v7-tableview-cuco-component";

    constructor() {
        super();
    }
}
