import { V7Configuration } from '@profect/v7-core-lib';

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment: V7Configuration = new V7Configuration();
environment.PRODUCTION = false;
environment.API_URL = 'https://localhost.profect.de:8443/v7_project/';
// environment.API_URL = 'https://localhost.profect.de:8443/v7/'; //To test it with local core server

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
