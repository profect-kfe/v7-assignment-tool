import { V7Configuration } from '@profect/v7-core-lib';



/**
 * In this file you can define the url of the server.
 *
 * This must not be mixed with the base-href that has to be configured
 * in the build-prod task in frontend/package.json
 * The base-href is actually the context path of the client.
 * Since the client can be hosted completely independent from
 * the server these two can be different.
 */
export const environment: V7Configuration = new V7Configuration();
environment.PRODUCTION = true;
environment.API_URL = 'https://assignment.profect.de/v7api/';
// environment.API_URL = 'https://localhost.profect.de:8443/v7_project/'; //To test it with local project server
// environment.API_URL = 'https://localhost.profect.de:8443/v7/'; //To test it with local core server
