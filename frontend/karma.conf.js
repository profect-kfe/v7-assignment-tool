// Karma configuration
// Generated on Wed Nov 08 2017 16:53:30 GMT+0100 (Mitteleuropäische Zeit)
module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine', 'karma-typescript'],

    // list of files / patterns to load in the browser
    files: [
        'src/**/*.spec.ts',
        'src/**/*.ts',
        'node_modules/zone.js/dist/zone.js',
        'node_modules/zone.js/dist/long-stack-trace-zone.js',
        'node_modules/zone.js/dist/async-test.js',
        'node_modules/zone.js/dist/fake-async-test.js',
        'node_modules/zone.js/dist/sync-test.js',
        'node_modules/zone.js/dist/proxy.js',
        'node_modules/zone.js/dist/jasmine-patch.js',
        'node_modules/reflect-metadata/Reflect.js'
//        // neccessary for the error when you test maybe
//         'node_modules/@profect/v7charts/require.min.2.1.1.js'
    ],

    // list of files to exclude
    exclude: [
        'src/main.ts'
    ],
    
    karmaTypescriptConfig: {
//            bundlerOptions: {
//                transforms: [
//                    require("karma-typescript-angular2-transform")
//                ],
//                entrypoints: /\.spec\.ts$/ 
//            },
            compilerOptions: {
                module: "commonjs"
            },
            tsconfig: "./tsconfig.json"
        },
        

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
       "src/**/*.spec.ts": "karma-typescript",
       "src/**/*.ts": "karma-typescript"
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'karma-typescript'],
//    reporters: ['progress'], //Disabled code coverage generation -ACW20180817


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],
    
//    browserNoActivityTimeout: 20000,


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  });
};
